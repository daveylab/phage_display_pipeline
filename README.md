# ProP-PD Data Analysis Pipeline
### Codebase for the analysis tools for the ProP-PD data

#### The major functionalities of the pipeline are:
- Read NGS peptide count files
- Read NGS selection information files
- Define screen data from library, bait, replicate, selection and condition information
- Create screen metadata
- Create crapome of promiscuous peptides
- Map peptides to human proteome using the PepTools server (see http://slim.icr.ac.uk/tools/peptools/)
- Annotate peptides using the PepTools server (mutations, PTMs, etc - see http://slim.icr.ac.uk/tools/peptools/help#instancesAnnotations)
- Annotate peptides with ELM consensuses, curated motifs, bait interaction data and bait shared significant ontology
- Set peptides quality checks
- Define the replicated, overlapping, high NGS count and significant specificity score peptides
- Define the peptide consensus confidence level
- Create outputs 

#### Running the ProP-PD Data Analysis Pipeline
Scripts and full instructions for running the pipeline are in the *./phage_display_tools* folder

#### Contributors:
Norman E. Davey <norman.davey@icr.ac.uk> Institute Of Cancer Research, Chelsea, SW3 6JB, London, United Kingdom.

#### Reference:
In Preparation