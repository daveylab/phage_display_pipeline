import os
import sys

import inspect

file_path = os.path.dirname(inspect.stack()[0][1])
sys.path.append(os.path.join(file_path,"../"))
import option_reader

sys.path.append(os.path.join(file_path,"../downloaders"))
import chemblDownloader
import pharosDownloader

from dataManager import dataManager

class druggabilityManager(dataManager):

	##------------------------------------------------------------------##
	## Inherits functions from dataManager
	##------------------------------------------------------------------##

	def setup_data(self):
		self.default_task_options = {
			"task":'help',
			"remake":False,
			"verbose":False,
			"debug":False,
			"logfile":False
		}

		self.allowed_options = [
			"task",
			"accession",
			"remake"
		]

		self.allowed_options_admin = [
		]


		self.task_options = [
			"parse_chembl_protein_data",
			"parse_chembl_protein_compounds",
			"parse_chembl_protein_targets",
			"parse_chembl_protein_therapeutic_compounds",
			"parse_chembl_protein_active_compounds",
			"parse_pharos_data",
			"parse_target_development_level",
			"parse_pharos_druggability_data",
			"parse_pharos_publication_data",
			"help"
		]

		self.task_skip_queue = [
			"help"
		]


		self.required = {
			'all':['accession']
		}

		self.test_options = {
			'accession':"P11802"
		}

		self.required_type = {
			'accession':"string"
		}

		# delimiter to split options
		self.list_options = {
			"accession":","
		}
		self.required_valid_options_list = {
			'remake':[True,False],
			'tasks':self.task_options
		}

		self.options = self.default_task_options

		for allowed_option in self.allowed_options:
			if allowed_option not in self.options:
				self.options[allowed_option] = None

		self.options.update(option_reader.load_commandline_options(self.options,self.options))

	##------------------------------------------------------------------##
	##------------------------------------------------------------------##

	def getData(self,status,task_options):

		###----######----######----###
		#   run_job run_job run_job  #
		###----######----######----###

		data = {}

		
		if task_options["task"] == "parse_chembl_protein_data":
			dataDownloaderObj = chemblDownloader.chemblDownloader()
			dataDownloaderObj.options.update(task_options)
			data = dataDownloaderObj.parse_chembl_protein_data()

		if task_options["task"] == "parse_chembl_protein_compounds":
			dataDownloaderObj = chemblDownloader.chemblDownloader()
			dataDownloaderObj.options.update(task_options)
			data = dataDownloaderObj.parse_chembl_protein_compounds()

		if task_options["task"] == "parse_chembl_protein_targets":
			dataDownloaderObj = chemblDownloader.chemblDownloader()
			dataDownloaderObj.options.update(task_options)
			data = dataDownloaderObj.parse_chembl_protein_targets()

		if task_options["task"] == "parse_chembl_protein_therapeutic_compounds":
			dataDownloaderObj = chemblDownloader.chemblDownloader()
			dataDownloaderObj.options.update(task_options)
			data = dataDownloaderObj.parse_chembl_protein_therapeutic_compounds()

		if task_options["task"] == "parse_chembl_protein_active_compounds":
			dataDownloaderObj = chemblDownloader.chemblDownloader()
			dataDownloaderObj.options.update(task_options)
			data = dataDownloaderObj.parse_chembl_protein_active_compounds()

		if task_options["task"] == "parse_pharos_data":
			dataDownloaderObj = pharosDownloader.pharosDownloader()
			dataDownloaderObj.options.update(task_options)
			data = dataDownloaderObj.parse_pharos_data()

		if task_options["task"] == "parse_pharos_druggability_data":
			dataDownloaderObj = pharosDownloader.pharosDownloader()
			dataDownloaderObj.options.update(task_options)
			data = dataDownloaderObj.parse_pharos_druggability_data()

		if task_options["task"] == "parse_pharos_publication_data":
			dataDownloaderObj = pharosDownloader.pharosDownloader()
			dataDownloaderObj.options.update(task_options)
			data = dataDownloaderObj.parse_pharos_publication_data()

		if task_options["task"] == "parse_target_development_level":
			dataDownloaderObj = pharosDownloader.pharosDownloader()
			dataDownloaderObj.options.update(task_options)
			data = dataDownloaderObj.parse_target_development_level()

		return data

##------------------------------------------------------------------##
##------------------------   END    END  ---------------------------##
##------------------------------------------------------------------##

if __name__ == "__main__":

	dataManagerObj = druggabilityManager()
	dataManagerObj.main()
