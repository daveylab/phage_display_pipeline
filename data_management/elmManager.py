import os
import sys

import inspect

file_path = os.path.dirname(inspect.stack()[0][1])
sys.path.append(os.path.join(file_path,"../"))
import option_reader

sys.path.append(os.path.join(file_path,"../downloaders"))
import elmDownloader

from dataManager import dataManager

class elmManager(dataManager):

	##------------------------------------------------------------------##
	## Inherits functions from dataManager
	##------------------------------------------------------------------##

	def setup_data(self):
		self.default_task_options = {
			'task':'help',
			'remake':False,
			'verbose':False,
			"debug":False,
			"logfile":False,
			'format': 'details',
			'max_gap_proportion': 0.0,
			'aligned_type': 'peptides_padded_pssm_aligned',
			'flanks': 5,
			'identifier_type': 'elm',
			'output_type':'details',
			'identifier': 'all'
		}

		self.allowed_options = [
			'task',
			'outfile',
			'accession',
			'pmid',
			'remake',
			'identifier_type',
			'identifier',
			'format',
			'flanks',
			'verbose'
		]

		self.allowed_options_admin = [
		]

		self.task_options = [
			"get_elm_classes",
			"get_elm_class_description",
			"get_elm_instances",
			"get_domain_dict",
			"get_domain_list",
			"get_interactions_list",
			"get_protein_dict",
			"get_protein_list",
			"get_aligned_classes",
			"get_pubmed_elm_class_mapping_dict",
			"get_pubmed_protein_mapping_dict",
			"help"
		]

		self.task_skip_queue = [
		]

		self.required = {
			'all':[],
			'get_pubmed_elm_class_mapping_dict':['pmid'],
			'get_pubmed_protein_mapping_dict':['pmid'],
			'get_aligned_classes':['identifier']
		}


		self.test_options = {
			'accession':"P20248",
			'identifier':"LIG_PTAP_UEV_1",
			'pmid':"16209941",
		}

		self.required_type = {
			'accession':"string",
			'remake':"bool"
		}

		self.required_format = {
			'accession':"\A([OPQ][0-9][A-Z0-9]{3}[0-9]|[A-NR-Z][0-9]([A-Z][A-Z0-9]{2}[0-9]){1,2})\-{0,1}[0-9]*\Z"
		}

		self.required_valid_options_list = {
			'remake':[True,False],
			'tasks':self.task_options
		}

		self.options.update(self.default_task_options)

		for allowed_option in self.allowed_options:
			if allowed_option not in self.options:
				self.options[allowed_option] = None

		self.options.update(option_reader.load_commandline_options(self.options,self.options))


	##------------------------------------------------------------------##

	def getData(self,status,task_options):

		###----######----######----###
		#   run_job run_job run_job  #
		###----######----######----###

		data = {}

		dataDownloaderObj = elmDownloader.elmDownloader()
		dataDownloaderObj.options.update(task_options)
		
		if task_options["task"] == "get_domain_dict":
			data = dataDownloaderObj.get_domain_dict()

		if task_options["task"] == "get_domain_list":
			data = dataDownloaderObj.get_domain_list()

		if task_options["task"] == "get_interactions_list":
			data = dataDownloaderObj.get_interactions_list()

		if task_options["task"] == "get_protein_dict":
			data = dataDownloaderObj.get_protein_dict()

		if task_options["task"] == "get_protein_list":
			data = dataDownloaderObj.get_protein_list()

		if task_options["task"] == "get_pubmed_elm_class_mapping_dict":
			data = dataDownloaderObj.get_pubmed_elm_class_mapping_dict()

		if task_options["task"] == "get_pubmed_protein_mapping_dict":
			data = dataDownloaderObj.get_pubmed_protein_mapping_dict()

		if task_options["task"] == "get_elm_instances":
			data = dataDownloaderObj.get_elm_instances()

		if task_options["task"] == "get_elm_classes":
			data = dataDownloaderObj.get_elm_classes()

		if task_options["task"] == "get_elm_class_description":
			data = dataDownloaderObj.get_elm_class_description()

		if task_options["task"] == "get_elm_class_peptides":
			data = dataDownloaderObj.get_elm_class_peptides()

		if task_options["task"] == "get_aligned_classes":
			data = dataDownloaderObj.getAlignedELMClasses()

		return data

	##------------------------------------------------------------------##
	##------------------------   END    END  ---------------------------##
	##------------------------------------------------------------------##

if __name__ == "__main__":

	dataManagerObj = elmManager()
	dataManagerObj.main()
