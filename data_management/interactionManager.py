import os
import sys
import inspect
import pprint

file_path = os.path.dirname(inspect.stack()[0][1])
sys.path.append(os.path.join(file_path,"../"))
import option_reader

sys.path.append(os.path.join(file_path,"../downloaders"))
import interactionDownloader

sys.path.append(os.path.join(file_path,"../interactions"))
import find_interaction

from dataManager import dataManager

class interactionManager(dataManager):

	##------------------------------------------------------------------##
	## Inherits functions from dataManager
	##------------------------------------------------------------------##

	def setup_data(self):

		self.default_task_options = {
			"task":'help',
			"remake":False,
			"verbose":False,
			"debug":False,
			"logfile":False,
			"use_hippie":True,
			"use_string":True,
			"use_huri":True,
			"use_bioplex":True,
			"use_viruses":True,
			"use_human_only":False,
			"use_viruses_only":False,
			"make_database":False,
			"require_interface_region":False,
			"add_paper_details":False,
			"collapse_interface_type":'bait',
			"require_interface_region":True,
			"cutoff":0.5,
			"identifier_type":"accession",
			"add_protein_details":False
		}

		self.allowed_options = [
			"task",
			'verbose',
			'remake',
			"outfile",
			"accession",
			"identifier",
			"identifier_type",
			"pmid",
			"pfam_id",
			"clan_id",
			"bait",
			"prey",
			"localisation",
			"number_of_interactors",
			"prey",
			"output_format",
			"use_hippie",
			"use_string",
			"use_huri",
			"use_bioplex",
			"use_viruses",
			"use_human_only",
			"use_viruses_only",
			"collapse_interface_type",
			"require_interface_region",
			"add_paper_details",
			"add_protein_details",
			"cutoff"
		]

		self.allowed_options_admin = [
		]

		self.task_options_admin = [
			"get_intact_interfaces"
			]

		self.task_options = [
			"collapse_by_complex",
			"get_complex_data",
			"get_domain_interactions",
			"get_extended_network",
			"get_interaction_existence",
			"get_interactions_by_pmid",
			"get_interactions_table",
			"get_interactions",
			"get_localisation_by_interactions",
			"get_localisation_interactions",
			"get_scaffolds",
			"get_shared_interactions",
			"get_intact_interfaces_by_protein",
			"get_intact_interfaces_by_domain",
			"get_intact_interfaces_by_domain_clan",
			"get_intact_interfaces_by_pmid",
			"load_cytoscape_example",
			"load_cytoscape_style",
			"make_cytoscape_json",
			"help"
		]

		self.required = {
			'all':[],
			"get_interactions":["accession"],
			"get_complex_data":["accession"],
			"collapse_by_complex":["accession"],
			"get_shared_interactions":["accession","number_of_interactors"],
			"get_extended_network":["accession"],
			"get_interactions_by_pmid":["pmid"],
			"get_domain_interactions":["accession","pfam_id"],
			"get_scaffolds":["bait","prey"],
			"get_localisation_by_interactions":["accession"],
			"get_localisation_interactions":["accession","localisation"],
			"get_interaction_existence":["bait","prey"],
			"get_intact_interfaces_by_protein":["accession"],
			"get_intact_interfaces_by_domain":["pfam_id"],
			"get_intact_interfaces_by_domain_clan":["clan_id"],
			"make_cytoscape_json":["accession"],
			"get_interactions_table":["identifier","identifier_type"],
			"get_subnetwork_plot":["accession"]
		}

		self.test_options = {
			'accession':"P20248",
			'pfam_id':"PF00400",
			'number_of_interactors':20,
			'localisation':'Endoplasmic reticulum,Nucleus',
			'bait':"P20248",
			'prey':"P06400",
			'pmid':"11742988",
			"clan_id":"CL0186"
		}

		self.required_type = {
			'accession':"string",
			'remake':"bool",
			"cutoff":"float",
			"bioplex":"bool",
			"number_of_interactors":"int",
			"localisation":"string"
		}

		self.required_format.update({
			'accession':"\A([OPQ][0-9][A-Z0-9]{3}[0-9]|[A-NR-Z][0-9]([A-Z][A-Z0-9]{2}[0-9]){1,2})\-{0,1}[0-9]*\Z",
			'pmid':'[0-9]+',
			'bait':"\A([OPQ][0-9][A-Z0-9]{3}[0-9]|[A-NR-Z][0-9]([A-Z][A-Z0-9]{2}[0-9]){1,2})\-{0,1}[0-9]*\Z",
			'prey':"\A([OPQ][0-9][A-Z0-9]{3}[0-9]|[A-NR-Z][0-9]([A-Z][A-Z0-9]{2}[0-9]){1,2})\-{0,1}[0-9]*\Z",
			'localisation':'[a-zA-Z\W\w]+'
		})

		# delimiter to split options
		self.list_options = {
			'accession':",",
			'identifier':",",
			'bait':",",
			'prey':",",
			'pmid':",",
			'localisation':","
		}

		self.required_valid_options_list = {
			'remake':[True,False],
			'require_interface_region':[True,False],
			'collapse_interface_type':['bait','prey','interface'],
			'tasks':self.task_options
		}

		self.options.update(self.default_task_options)

		for allowed_option in self.allowed_options:
			if allowed_option not in self.options:
				self.options[allowed_option] = None

		self.options.update(option_reader.load_commandline_options(self.options,self.options,purge_commandline=True))

		self.interactionDownloaderObj = None

	##------------------------------------------------------------------##

	def getData(self,status,task_options):

		###----######----######----###
		#   run_job run_job run_job  #
		###----######----######----###

		data = {}
	
		if self.interactionDownloaderObj == None:
			if task_options["use_viruses_only"]:
				self.interactionDownloaderObj = interactionDownloader.interactionDownloader(
					use_hippie=False,
					use_string=False,
					use_huri=False,
					use_bioplex=False,
					use_viruses=True
					)
					
			elif task_options["use_human_only"]:
				self.interactionDownloaderObj = interactionDownloader.interactionDownloader(
					use_hippie=True,
					use_string=True,
					use_huri=True,
					use_bioplex=True,
					use_viruses=False
					)
			else:
				self.interactionDownloaderObj = interactionDownloader.interactionDownloader(
					use_hippie=task_options['use_hippie'],
					use_string=task_options['use_string'],
					use_huri=task_options['use_huri'],
					use_bioplex=task_options['use_bioplex'],
					use_viruses=task_options['use_viruses']
					)
					
			self.interactionDownloaderObj.options['verbose'] = task_options['verbose']

		###----###
		###----###

		if task_options["task"] == "load_cytoscape_style":
			findInteractionsObj = find_interaction.findInteractions()
			data = findInteractionsObj.load_cytoscape_style()
			del findInteractionsObj

		if task_options["task"] == "load_cytoscape_example":
			findInteractionsObj = find_interaction.findInteractions()
			data = findInteractionsObj.load_cytoscape_example()
			del findInteractionsObj

		###----###
		###----###

		if task_options["task"] == "get_interactions":
			data = self.interactionDownloaderObj.getInteractions(task_options["accession"],"accession")

		if task_options["task"] == "get_complex_data":
			data = self.interactionDownloaderObj.getComplexData(task_options["accession"])

		###----###
		###----###

		if task_options["task"] == "collapse_by_complex":
			data = self.interactionDownloaderObj.collapseByComplex(task_options["accession"])

		if task_options["task"] == "get_shared_interactions":
			data = self.interactionDownloaderObj.getSharedInteractions(task_options["accession"],number_of_interactors = task_options["number_of_interactors"])

		if task_options["task"] == "get_extended_network":
			data = self.interactionDownloaderObj.getExtendedNetwork(task_options["accession"])

		if task_options["task"] == "get_interactions_by_pmid":
			data = self.interactionDownloaderObj.getInteractions(task_options["pmid"],"pmid")

		if task_options["task"] == "get_domain_interactions":
			data = self.interactionDownloaderObj.getDomainInteractions(task_options["accession"],task_options["pfam_id"])

		if task_options["task"] == "get_scaffolds":
			data = self.interactionDownloaderObj.getScaffolds(task_options["bait"],task_options["prey"])

		if task_options["task"] == "get_localisation_by_interactions":
			data = self.interactionDownloaderObj.getLocalisationByInteractions(task_options["accession"])

		if task_options["task"] == "get_localisation_interactions":
			data = self.interactionDownloaderObj.getLocalisationInteractions(task_options["accession"][0],task_options["localisation"])

		if task_options["task"] == "get_interaction_existence":
			self.interactionDownloaderObj.options['add_protein_details'] = task_options['add_protein_details']
			data = self.interactionDownloaderObj.existsInteractions(task_options["bait"],task_options["prey"])

		if task_options["task"] == "make_cytoscape_json":
			self.interactionDownloaderObj.options['cutoff'] = task_options['cutoff']
			data = self.interactionDownloaderObj.makeCytoscapeJson(task_options["accession"][0])

		if task_options["task"] == "get_interactions_table":
			self.interactionDownloaderObj.options['add_paper_details'] = task_options['add_paper_details']
			data = self.interactionDownloaderObj.getInteractionsTable(task_options["identifier"],task_options["identifier_type"])

		if task_options["task"] == "get_intact_interfaces":
			data = self.interactionDownloaderObj.getIntactInterfaces()

		if task_options["task"] == "get_intact_interfaces_by_domain":
			data = self.interactionDownloaderObj.getIntactInterfacesByDomain(task_options["pfam_id"][0])

		if task_options["task"] == "get_intact_interfaces_by_protein":
			self.interactionDownloaderObj.options['collapse_interface_type'] = task_options['collapse_interface_type']
			self.interactionDownloaderObj.options['require_interface_region'] = task_options['require_interface_region']
			data = self.interactionDownloaderObj.getIntactInterfacesByProtein(task_options["accession"][0])

		if task_options["task"] == "get_intact_interfaces_by_domain_clan":
			data = self.interactionDownloaderObj.getIntactInterfacesByDomainClan(task_options["clan_id"][0])

		if task_options["task"] == "get_intact_interfaces_by_pmid":
			data = self.interactionDownloaderObj.getIntactInterfacesByPMID(task_options["pmid"][0])

			

		### REQUIRES NETWORKX
		if task_options["task"] == "get_subnetwork_plot":
			data = self.interactionDownloaderObj.getSubNetworkPlot(task_options["accession"])

		return data

	##------------------------------------------------------------------##
	##------------------------   END    END  ---------------------------##
	##------------------------------------------------------------------##

if __name__ == "__main__":

	dataManagerObj = interactionManager()
	dataManagerObj.main()
