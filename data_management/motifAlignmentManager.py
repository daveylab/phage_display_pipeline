import os
import sys

import inspect

file_path = os.path.dirname(inspect.stack()[0][1])
sys.path.append(os.path.join(file_path,"../"))
import option_reader

sys.path.append(os.path.join(file_path,"../motif/"))
import alignPeptidesBySLiMFinder
import alignPeptidesByMotif
from dataManager import dataManager

### python motifAlignmentManager.py --task test --data_sources viruses --filter_by "motif taxonomy" --filter_term Viruses --filter_logic contains --verbose True --task get_statistics  --data_type "motif species"  --statistics_detailed True

class motifAlignmentManager(dataManager):

	##------------------------------------------------------------------##
	## Inherits functions from dataManager
	##------------------------------------------------------------------##

	def setup_data(self):
		self.default_rest_options = {
			"task":'help',
			"remake":False,
			"verbose":False,
			"debug":False,
			"logfile":False
		}

		self.allowed_options = [
			"task",
			"outfile",
			'variant',
			"use_peptide_aa_freqs",
			"topranks",
			"probcut",
			"variable_length_gaps",
			"termini",
			"peptides",
			"motif",
			"cluster_type",
			"aafreq_file",
			"gap_proportion",
			"slimfinder_background_frequencies"
		]

		self.allowed_options_admin = [
		]
	
		self.task_options = [
			"align_peptides_by_slimfinder",
			"find_motifs_by_slimfinder",
			"find_motif_clusters_by_slimfinder",
			"align_peptides_by_motif",
			"help"
		]

		self.task_skip_queue = [	
			"help"
		]

		self.required = {
			'all':['peptides']
		}

		self.test_options = {
			"peptides":"MAQKENSYPWPYG,TEGGEAQKENGIYSVTF,HVFEDGNKENYGLPQPK,SVHILEDKENVVAKQCT",
			"motif":"KEN"
		}

		self.required_type = {
			'align_peptides_by_motif':['peptides','motif']
		}

		self.convert_type = {
		}

		self.required_format = {
			'peptides':"^[ACDEFGHIKLMNPQRSTVWY\-]+$"
		}

		# delimiter to split options
		self.list_options = {
			"peptides":","
		}

		self.required_valid_options_list = {
			'remake':[True,False],
			'variable_length_gaps':[True,False],
			'termini':[True,False],
			'variant':['fast_heuristic','exhaustive_heuristic','exhaustive'],
			'cluster_type':["exact_residues","exact_residues_position","overlap"],
			'tasks':self.task_options
		}

		self.options = self.default_rest_options

		for allowed_option in self.allowed_options:
			if allowed_option not in self.options:
				self.options[allowed_option] = None

		self.options.update(option_reader.load_commandline_options(self.options,self.options,purge_commandline=True))
		
	##------------------------------------------------------------------##
	##------------------------------------------------------------------##

	def getData(self,status,task_options):

		###----######----######----###
		#   run_job run_job run_job  #
		###----######----######----###

		data = {}
		
		if task_options["task"] in ["align_peptides_by_slimfinder","find_motifs_by_slimfinder","find_motif_clusters_by_slimfinder"]:
			dataDownloaderObj = alignPeptidesBySLiMFinder.alignPeptidesBySLiMFinder()
			dataDownloaderObj.options.update(task_options)
			dataDownloaderObj.setup_paths()

			if task_options["task"] == "align_peptides_by_slimfinder":
				data = dataDownloaderObj.runAlignPeptides()

			if task_options["task"] == "find_motifs_by_slimfinder":
				data = dataDownloaderObj.runFindMotifs()

			if task_options["task"] == "find_motif_clusters_by_slimfinder":
				data = dataDownloaderObj.runFindClusters()
		
		if task_options["task"] in ["align_peptides_by_motif"]:
			if task_options["task"] == "align_peptides_by_motif":
				dataDownloaderObj = alignPeptidesByMotif.alignPeptidesByMotif()
				dataDownloaderObj.options.update(task_options)
				data = dataDownloaderObj.alignPeptidesByMotif()

		return data

##------------------------------------------------------------------##
##------------------------   END    END  ---------------------------##
##------------------------------------------------------------------##

if __name__ == "__main__":
	dataManagerObj = motifAlignmentManager()
	dataManagerObj.main()