import os
import sys

import inspect

file_path = os.path.dirname(inspect.stack()[0][1])
sys.path.append(os.path.join(file_path,"../"))
import option_reader

sys.path.append(os.path.join(file_path,"../motif_manager/"))
import motif_query

from dataManager import dataManager

### python motifManager.py --task test --data_sources viruses --filter_by "motif taxonomy" --filter_term Viruses --filter_logic contains --verbose True --task get_statistics  --data_type "motif species"  --statistics_detailed True

class motifManager(dataManager):

	##------------------------------------------------------------------##
	## Inherits functions from dataManager
	##------------------------------------------------------------------##

	def setup_data(self):
		self.default_rest_options = {
			"task":'help',
			"remake":False,
			"verbose":False,
			"debug":False,
			"logfile":False
		}

		self.allowed_options = [
			"task",
			"outfile",
			"data_sources",
			"database_name",
			"filter_by",
			"filter_term",
			"filter_terms",
			"filter_logic",
			"filter_logic_default",
			'elm_class',
			'gene_name',
			'protein_name',
			'accession',
			'pfam_id',
			'taxon_id',
			'taxonomy',
			'pmid',
			'username',
			'protein_family',
			'protein_type',
			"group_by",
			"merge_by",
			"data_type",
			"statistics_detailed",
			"case_sensitive",
			"use_columns",
			'remake',
			'update',
			'verbose',
			'make_tdt',
			'outfile',
			'collapse_lists',
			'collapse_key_data',
			'use_compact_output_tags_list'
		]

		self.allowed_options_admin = [
		]

		self.task_options_admin = [
			"load_intact_data",
			"load_elm_data",
			"load_pdb_data",
			"make_instance_table",
			"make_database",
			'load_raw_database'
			]

		self.task_options = [
			"get_instances",
			"get_instances_by_elm_class",
			"get_instances_by_gene_name",
			'get_instances_by_protein_name',
			'get_instances_by_accession',
			'get_instances_by_motif_accession',
			'get_instances_by_domain_accession',
			'get_instances_by_motif_protein_family',
			'get_instances_by_domain_protein_family',
			'get_instances_by_pfam_id',
			'get_instances_by_pmid',
			'get_instances_by_protein_family',
			'get_instances_by_taxon_id',
			'get_instances_by_taxonomy',
			'get_indexes_by_accession',
			'get_indexes_by_elm_class',
			'get_indexes_by_gene_name',
			'get_indexes_by_pfam_id',
			'get_indexes_by_pmid',
			'get_indexes_by_protein_family',
			'get_indexes_by_protein_name',
			'get_indexes_by_taxon_id',
			'get_indexes_by_taxonomy',
			'get_instances_by_username',
			"get_index",
			"get_motif_protein_index",
			"get_domain_protein_index",
			"get_family_index",
			"get_grouped_instances",
			"get_group_members",
			"get_statistics",
			"help"
		]

		self.task_skip_queue = [
			"help"
		]

		self.required = {
			'all':[],
			"get_instances":[],
			"get_grouped_instances":[],
			"get_index":['data_type'],
			"get_statistics":['data_type'],
			"make_database":["database_name"],
			'get_instances_by_elm_class':['elm_class'],
			'get_instances_by_gene_name':['gene_name'],
			'get_instances_by_protein_name':['protein_name'],
			'get_instances_by_accession':['accession'],
			'get_instances_by_pfam_id':['pfam_id'],
			'get_instances_by_pmid':['pmid'],
			'get_instances_by_protein_family':['protein_family'],
			'get_instances_by_taxon_id':['taxon_id'],
			'get_instances_by_taxonomy':['taxonomy'],
			'get_instances_by_username':['username']
		}

		self.test_options = {
			'accession':"P20248",
			'gene_name':"CCNA",
			'protein_name':"Cyclin-A2",
			'pfam_id':"PF00400",
			'data_type':'pmid',
			'elm_class':'LIG_APCC_ABBA_1',
			'pmid':"25669885",
			'protein_family':'cyclin family',
			'taxon_id':"9606",
			'taxonomy':"Metazoa",
			'username':"ndavey",
			"database_name":"cell_cycle",
			"group_by":"motif protein family"
		}

		self.required_type = {
			'data_sources':"string",
			'filter_by':"string",
			'group_by':"string",
			'data_type':"string"
		}

		self.convert_type = {
		}

		self.required_format = {
			'accession':"\A([OPQ][0-9][A-Z0-9]{3}[0-9]|[A-NR-Z][0-9]([A-Z][A-Z0-9]{2}[0-9]){1,2})\-{0,1}[0-9]*\Z"
		}

		# delimiter to split options
		self.list_options = {
			"data_sources":",",
			"filter_by":",",
			"filter_term":",",
			"filter_terms":",",
			"filter_logic":",",
			"data_type":","
		}

		self.required_valid_options_list = {
			'remake':[True,False],
			'merge_by':['overlap','domain','domain_region','elm_class','protein','any_binding','any'],
			'tasks':self.task_options
		}

		self.options = self.default_rest_options

		for allowed_option in self.allowed_options:
			if allowed_option not in self.options:
				self.options[allowed_option] = None

		self.options.update(option_reader.load_commandline_options(self.options,self.options,purge_commandline=True))

	##------------------------------------------------------------------##
	##------------------------------------------------------------------##

	def getData(self,status,task_options):

		###----######----######----###
		#   run_job run_job run_job  #
		###----######----######----###

		data = {}

		dataDownloaderObj = motif_query.motifQueryManager()
		dataDownloaderObj.options.update(task_options)
		dataDownloaderObj.motifLoaderObj.options.update(task_options)
		data = dataDownloaderObj.check_query()

		if data['status'] == "Success":
			dataDownloaderObj.load_data()

			if task_options["task"] == "get_instances":
				data = dataDownloaderObj.get_instances()
			elif task_options["task"] == "get_instances_by_elm_class":
				data = dataDownloaderObj.get_instances_by_elm_class()
			elif task_options["task"] == "get_instances_by_gene_name":
				data = dataDownloaderObj.get_instances_by_gene_name()
			elif task_options["task"] == "get_instances_by_protein_name":
				data = dataDownloaderObj.get_instances_by_protein_name()
			elif task_options["task"] == "get_instances_by_accession":
				data = dataDownloaderObj.get_instances_by_accession()
			elif task_options["task"] == "get_instances_by_motif_accession":
				data = dataDownloaderObj.get_instances_by_motif_accession()
			elif task_options["task"] == "get_instances_by_domain_accession":
				data = dataDownloaderObj.get_instances_by_domain_accession()
			elif task_options["task"] == 'get_instances_by_motif_protein_family':
				data = dataDownloaderObj.get_instances_by_motif_protein_family()
			elif task_options["task"] == 'get_instances_by_domain_protein_family':
				data = dataDownloaderObj.get_instances_by_domain_protein_family()
			elif task_options["task"] == "get_instances_by_pfam_id":
				data = dataDownloaderObj.get_instances_by_pfam_id()
			elif task_options["task"] == "get_instances_by_pmid":
				data = dataDownloaderObj.get_instances_by_pmid()
			elif task_options["task"] == "get_instances_by_protein_family":
				data = dataDownloaderObj.get_instances_by_protein_family()
			elif task_options["task"] == "get_instances_by_taxon_id":
				data = dataDownloaderObj.get_instances_by_taxon_id()
			elif task_options["task"] == "get_instances_by_taxonomy":
				data = dataDownloaderObj.get_instances_by_taxonomy()
			elif task_options["task"] == "get_instances_by_username":
				data = dataDownloaderObj.get_instances_by_username()


			elif task_options["task"] == "get_index":
				data = dataDownloaderObj.get_index()
			elif task_options["task"] == 'get_indexes_by_accession':
				data = dataDownloaderObj.get_indexes_by_accession()
			elif task_options["task"] == 'get_indexes_by_elm_class':
				data = dataDownloaderObj.get_indexes_by_elm_class()
			elif task_options["task"] == 'get_indexes_by_gene_name':
				data = dataDownloaderObj.get_indexes_by_gene_name()
			elif task_options["task"] == 'get_indexes_by_pfam_id':
				data = dataDownloaderObj.get_indexes_by_pfam_id()
			elif task_options["task"] == 'get_indexes_by_pmid':
				data = dataDownloaderObj.get_indexes_by_pmid()
			elif task_options["task"] == 'get_indexes_by_protein_family':
				data = dataDownloaderObj.get_indexes_by_protein_family()
			elif task_options["task"] == 'get_indexes_by_protein_name':
				data = dataDownloaderObj.get_indexes_by_protein_name()
			elif task_options["task"] == 'get_indexes_by_taxon_id':
				data = dataDownloaderObj.get_indexes_by_taxon_id()
			elif task_options["task"] == 'get_indexes_by_taxonomy':
				data = dataDownloaderObj.get_indexes_by_taxonomy()

			elif task_options["task"] == "get_motif_protein_index":
				data = dataDownloaderObj.get_motif_protein_index()
			elif task_options["task"] == "get_domain_protein_index":
				data = dataDownloaderObj.get_domain_protein_index()
			elif task_options["task"] == "get_family_index":
				data = dataDownloaderObj.get_family_index()

			elif task_options["task"] == "get_grouped_instances":
				data =  dataDownloaderObj.group_instances()
			elif task_options["task"] == "get_statistics":
				data = dataDownloaderObj.get_statistics()
			elif task_options["task"] == "make_instance_table":
				data = dataDownloaderObj.make_instance_table()
			elif task_options["task"] == "make_database":
				data = dataDownloaderObj.make_database()

			elif task_options["task"] == "load_intact_data":
				data = dataDownloaderObj.load_intact_data()
			elif task_options["task"] == "load_elm_data":
				data = dataDownloaderObj.load_elm_data()
			elif task_options["task"] == "load_pdb_data":
				data = dataDownloaderObj.load_pdb_data()

			elif task_options['task'] == 'load_raw_database':
				data = dataDownloaderObj.motifLoaderObj.load_raw_database()

			del dataDownloaderObj.motifLoaderObj

		return data

##------------------------------------------------------------------##
##------------------------   END    END  ---------------------------##
##------------------------------------------------------------------##

if __name__ == "__main__":
	dataManagerObj = motifManager()
	dataManagerObj.main()
