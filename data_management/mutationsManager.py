import os
import sys

import inspect

file_path = os.path.dirname(inspect.stack()[0][1])
sys.path.append(os.path.join(file_path,"../"))
import option_reader

sys.path.append(os.path.join(file_path,"../downloaders"))
import mutationsDownloader

from dataManager import dataManager

class mutationsManager(dataManager):

	##------------------------------------------------------------------##
	## Inherits functions from dataManager
	##------------------------------------------------------------------##

	def setup_data(self):
		self.default_rest_options = {
			"task":'help',
			"remake":False,
			"verbose":False,
			"debug":False,
			"logfile":False,
			"alternative_keys":False,
			"add_protein_data":False,
			"save_to_file":False
		}

		self.allowed_options = [
			"task",
			"outfile",
			'accession',
			'alternative_keys',
			'region_start',
			'region_end',
			'pmid',
			'pdb_id',
			"remake",
			"add_protein_data",
			"group_mutations_by",
			"mutation_output_compact",
			"mutation_output_type",
			"mutations_data_path",
			"region_end",
			"region_start",
			"remove_incorrectly_mapped_mutations",
			"require_clinical_significances",
			"skip_consequence_type",
			"group_mutation_by",
			"taxon",
			"use_consequence_type",
			"save_to_file"
		]

		self.allowed_options_admin = [
		]

		self.task_options = [
			"parse_gff",
			"parse_humsavar",
			'parse_mutations_by_pdb',
			"parse_mutations_by_pmid",  # Currently not working
			"parse_mutations",
			"parse_mutations_bulk",
			"help"
		]

		self.required = {
			'all':[],
			'parse_mutations_by_pmid':['pmid'],
			'parse_mutations_sequence':['accession'],
			'parse_mutations_bulk':['accession'],
			'parse_mutations_by_pdb':['pdb_id'],
			'parseMutations':['accession']
		}

		self.test_options = {
			'accession':"P06400",
			'pmid':"11742988",
			'pdb_id':"2AST"
		}

		self.required_type = {
			'accession':"string",
			'alternative_keys':"bool",
			'region_start':"int",
			'region_end':"int",
			'pmid':"string",
			'remake':"bool"
		}

		self.convert_type = {
			'pmid':"string",
		}

		self.required_format = {
			'accession':"\A([OPQ][0-9][A-Z0-9]{3}[0-9]|[A-NR-Z][0-9]([A-Z][A-Z0-9]{2}[0-9]){1,2})\-{0,1}[0-9]*\Z"
		}

		# delimiter to split options
		self.list_options = {
			'accession':","
		}

		self.required_valid_options_list = {
			'remake':[True,False],
			'tasks':self.task_options,
			'mutation_output_type':['json','tdt','webservice','position_collapsed']
			#--skip_consequence_type "frameshift","stop gained","inframe deletion","missense"
		}

		self.options = self.default_rest_options

		for allowed_option in self.allowed_options:
			if allowed_option not in self.options:
				self.options[allowed_option] = None

		self.options.update(option_reader.load_commandline_options(self.options,self.options))

	##------------------------------------------------------------------##
	##------------------------------------------------------------------##

	def getData(self,status,task_options):

		###----######----######----###
		#   run_job run_job run_job  #
		###----######----######----###

		data = {}

		dataDownloaderObj = mutationsDownloader.mutationsDownloader()
		dataDownloaderObj.options.update(task_options)

		if task_options["task"] == "parse_humsavar":
			data = dataDownloaderObj.parseHumsavar()
		if task_options["task"] == "parse_mutations":
			data = dataDownloaderObj.parseMutations()
		if task_options["task"] == "parse_mutations_bulk":
			data = dataDownloaderObj.parseMutationsBulk()
		if task_options["task"] == "parse_mutations_by_pdb":
			data = dataDownloaderObj.parseMutationsByPDB()
		if task_options["task"] == "parse_mutations_by_pmid":
			data = dataDownloaderObj.parseMutationsByPMID()
		if task_options["task"] == "parse_gff":
			data = dataDownloaderObj.parseGFF()

		return data

##------------------------------------------------------------------##
##------------------------   END    END  ---------------------------##
##------------------------------------------------------------------##

if __name__ == "__main__":
	dataManagerObj = mutationsManager()
	dataManagerObj.main()
