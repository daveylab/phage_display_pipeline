import os
import sys

import inspect

file_path = os.path.dirname(inspect.stack()[0][1])
sys.path.append(os.path.join(file_path,"../"))
import option_reader

sys.path.append(os.path.join(file_path,"../downloaders"))
import pdbDownloader
import uniprotDownloader

from dataManager import dataManager

class pdbManager(dataManager):

	##------------------------------------------------------------------##
	## Inherits functions from dataManager
	##------------------------------------------------------------------##

	def setup_data(self):
		self.default_task_options = {
			"task":'help',
			"remake":False,
			"verbose":False,
			"debug":False,
			"logfile":False
		}

		self.allowed_options = [
			"task",
			'outfile',
			"accession",
			"pdb_id",
			"pfam_id",
			"pmid",
			"remake"
		]

		self.allowed_options_admin = [
		]

		self.task_options = [	
			"get_structure_general_data",
			"get_structure_chain_data",
			"get_pdb_pfam",
			"get_pdb_uniprot",
			"get_pdb_pubmed",
			"get_pdb_details_uniprot",
			"help"
		]

		self.required = {
			'all':[],
			"get_structure_general_data":['pdb_id'],
			"get_structure_chain_data":['pdb_id'],
			"get_pdb_pfam":['pfam_id'],
			"get_pdb_uniprot":['accession'],
			"get_pdb_pubmed":['pmid']
		}

		self.test_options = {
			'accession':"P46527",
			'pfam_id':"PF00400",
			'pmid':"22153077",
			'pdb_id':"2AST",
		}

		self.required_type = {
			'accession':"string",
			'remake':"bool"
		}

		self.required_format = {
			'accession':"\A([OPQ][0-9][A-Z0-9]{3}[0-9]|[A-NR-Z][0-9]([A-Z][A-Z0-9]{2}[0-9]){1,2})\-{0,1}[0-9]*\Z"
		}

		# delimiter to split options
		self.list_options = {
			"accession":","
		}

		self.required_valid_options_list = {
			'remake':[True,False],
			'tasks':self.task_options
		}

		self.options = self.default_task_options

		for allowed_option in self.allowed_options:
			if allowed_option not in self.options:
				self.options[allowed_option] = None

		self.options.update(option_reader.load_commandline_options(self.options,self.options))

	##------------------------------------------------------------------##
	##------------------------------------------------------------------##

	def getData(self,status,task_options):

		###----######----######----###
		#   run_job run_job run_job  #
		###----######----######----###

		data = {}

		dataDownloaderObj = pdbDownloader.pdbDownloader()
		
		if task_options["task"] == "get_structure_general_data":
			dataDownloaderObj.options['pdbID'] = task_options['pdb_id']
			data = dataDownloaderObj.parseGeneralDataPDB()
			
		if task_options["task"] == "get_structure_chain_data":
			dataDownloaderObj.options['pdbID'] = task_options['pdb_id']
			data = dataDownloaderObj.parseMoleculePDB()
			
		if task_options["task"] == "get_pdb_pfam":
			dataDownloaderObj.options['pfam_id'] = task_options['pfam_id']
			data = dataDownloaderObj.parsePDBPfamxml()
			
		if task_options["task"] == "get_pdb_uniprot":
			dataDownloaderObj.options['accession'] = task_options['accession']
			data = dataDownloaderObj.parsePDBUniprotAccessions()
		
		if task_options["task"] == "get_pdb_details_uniprot":
			dataDownloaderObj.options['accession'] = task_options['accession']
			dataDownloaderObj.options['add_structure_details'] = True
			data = dataDownloaderObj.parsePDBUniprotAccessions()
				
		if task_options["task"] == "get_pdb_pubmed":
			dataDownloaderObj.options['pmid'] = task_options['pmid']
			data = dataDownloaderObj.parsePMIDPDBUniprot()
		
		return data

##------------------------------------------------------------------##
##------------------------   END    END  ---------------------------##
##------------------------------------------------------------------##

if __name__ == "__main__":

	dataManagerObj = pdbManager()
	dataManagerObj.main()
