import os
import sys

import inspect

file_path = os.path.dirname(inspect.stack()[0][1])
sys.path.append(os.path.join(file_path,"../"))
import option_reader

sys.path.append(os.path.join(file_path,"../downloaders"))
import pfamDownloader
import uniprotDownloader

from dataManager import dataManager

class pfamManager(dataManager):

	##------------------------------------------------------------------##
	## Inherits functions from dataManager
	##------------------------------------------------------------------##

	def setup_data(self):
		self.default_task_options = {
			"task":'help',
			"region_start":None,
			"region_end":None,
			"remake":False,
			"verbose":False,
			"debug":False,
			"logfile":False
		}

		self.allowed_options = [
			"task",
			"outfile",
			"accession",
			"pfam_id",
			"region_start",
			"region_end",
			"remake"
		]

		self.allowed_options_admin = [
		]

		self.task_options = [
			"parse_pfam",
			"parse_protein_pfam",
			"parse_protein_pfam_details",
			"parse_protein_pfam_instances",
			"help"
		]

		self.task_skip_queue = [
			"parse_pfam",
			"parse_protein_pfam",
			"parse_protein_pfam_details",
			"help"
		]

		self.required = {
			'all':[],
			"parse_pfam":['pfam_id'],
			"parse_protein_pfam":['accession'],
			"parse_protein_pfam_details":['accession'],
			"parse_protein_pfam_instances":['accession']
		}

		self.test_options = {
			'accession':"P20248",
			'pfam_id':"PF00400"
		}

		self.required_type = {
			'accession':"string",
			'remake':"bool"
		}

		# delimiter to split options
		self.list_options = {
		}

		self.required_valid_options_list = {
			'remake':[True,False],
			'tasks':self.task_options
		}

		self.options = self.default_task_options

		for allowed_option in self.allowed_options:
			if allowed_option not in self.options:
				self.options[allowed_option] = None

		self.options.update(option_reader.load_commandline_options(self.options,self.options))

	##------------------------------------------------------------------##
	##------------------------------------------------------------------##

	def getData(self,status,task_options):

		###----######----######----###
		#   run_job run_job run_job  #
		###----######----######----###

		data = {}

		if task_options["task"] == "parse_pfam":
			dataDownloaderObj = pfamDownloader.pfamDownloader()
			dataDownloaderObj.options.update(task_options)
			data = dataDownloaderObj.parsePfam()

		if task_options["task"] == "parse_protein_pfam":
			dataDownloaderObj = uniprotDownloader.uniprotDownloader()
			data = dataDownloaderObj.parseDomains(task_options['accession'])

		if task_options["task"] == "parse_protein_pfam_details":
			dataDownloaderObj = uniprotDownloader.uniprotDownloader()
			data = dataDownloaderObj.parseUniProtPfam(task_options['accession'])
		
		if task_options["task"] == "parse_protein_pfam_instances":
			dataDownloaderObj = uniprotDownloader.uniprotDownloader()
			data = dataDownloaderObj.parseUniProtPfamInstances(task_options['accession'])

		return data

##------------------------------------------------------------------##
##------------------------   END    END  ---------------------------##
##------------------------------------------------------------------##

if __name__ == "__main__":

	dataManagerObj = pfamManager()
	dataManagerObj.main()
