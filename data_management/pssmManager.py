import os
import sys

import inspect

file_path = os.path.dirname(inspect.stack()[0][1])
sys.path.append(os.path.join(file_path,"../"))
import option_reader

sys.path.append(os.path.join(file_path,"../downloaders"))
from pssmDownloader import pssmDownloader

from dataManager import dataManager

class pssmManager(dataManager):

	##------------------------------------------------------------------##
	## Inherits functions from dataManager
	##------------------------------------------------------------------##

	def setup_data(self):
		self.default_task_options = {
			"task":'help',
			"outfile":"",
			"remake":False,
			"verbose":False,
			"debug":False,
			"logfile":False
		}

		self.allowed_options = [
			"peptides"
		]

		self.allowed_options_admin = [
		]

		self.task_options = [
			"make_pssm",
			"make_pssm_motif",
			"make_pssm_aligned_peptides",
			"help"
		]

		self.required = {
			'all':[],
			'make_PSSM':['peptides'],
			'make_PSSM_motif':['peptides'],
			'make_PSSM_aligned_peptides':['peptides']
		}

		self.test_options = {
			"peptides":"MAQKENSYP,EAQKENGIY,DGNKENYGL,LEDKENVVA"
		}

		self.required_type = {
			'peptides':"string",
			'remake':"bool"
		}

		self.required_format = {
		}

		# delimiter to split options
		self.list_options = {
			'peptides':","
		}

		self.required_valid_options_list = {
			'remake':[True,False],
			'tasks':self.task_options
		}

		self.options = self.default_task_options

		for allowed_option in self.allowed_options:
			if allowed_option not in self.options:
				self.options[allowed_option] = None

		self.options.update(option_reader.load_commandline_options(self.options,self.options,purge_commandline=True))

	##------------------------------------------------------------------##
	##------------------------------------------------------------------##

	def getData(self,status,task_options):

		###----######----######----###
		#   run_job run_job run_job  #
		###----######----######----###

		data = {}

		dataDownloaderObj = pssmDownloader()
		dataDownloaderObj.options.update(task_options)

		if task_options["task"] == "make_pssm":
			data = dataDownloaderObj.make_PSSM()
		if task_options["task"] == "make_pssm_motif":
			data = dataDownloaderObj.make_PSSM_motif()
		if task_options["task"] == "make_pssm_aligned_peptides":
			data = dataDownloaderObj.make_PSSM_aligned_peptides()
	
		return data

##------------------------------------------------------------------##
##------------------------   END    END  ---------------------------##
##------------------------------------------------------------------##

if __name__ == "__main__":

	dataManagerObj = pssmManager()
	dataManagerObj.main()
