import sys

if sys.version_info[0] != 3 or sys.version_info[1] < 5:
    print("This script requires Python version 3.5 or greater")
    sys.exit(1)

import os
import inspect
file_path = os.path.dirname(inspect.stack()[0][1])
sys.path.append(os.path.join(file_path,"../"))
sys.path.append(os.path.join(file_path,"../downloaders/"))

from queueManager import queueManager

import uniprotDownloader 

# python queryManager.py --dataset_type accessibility --accession Q9BSI4,Q9NUX5,P54274,Q96AP0,Q15554,Q9NYB0 --task calculate_accessibility_consensus --verbose True
# python queryManager.py --dataset_type uniprot --accession Q9BSI4 --task parse_basic --verbose True
# python queryManager.py --dataset_type interface --protein_id Q9BSI4,Q9NUX5,P54274,Q96AP0,Q15554,Q9NYB0 --task find_interface_regions --verbose True 
# python queryManager.py --dataset_type motif --database_name complete --task get_instances_by_elm_class --elm_class LIG_PTAP --verbose True 
# python queryManager.py --dataset_type interaction --task get_interactions_table --accession P30304,P30305,P30307 --verbose True 
# python queryManager.py --bulk_taxon_id 9606 --reviewed True --verbose True --dataset_type interface --structures True --queue False --task find_interface_regions --chunks 10 --chunk_number 3

class queryManager:

	##------------------------------------------------------------------##

	def __init__(self):
		self.options = {
			"dataset_type":"",
			"test_all":False,
			"rest_api":"http://disorder.icr.ac.uk/webservices/rest/get/"
			}

		self.dataset_types = [
			"comparimotif",
			"ontology",
			"druggability",
			"uniprot", 
			"annotator",
			"pdb",
			"evolution",  
			"interaction",
			"motif_alignment",
			"mutations",
			"pfam", 
			"pubmed", 
			"accessibility",
			"architecture",
			"cell_cycle",
			"interface",
			"elm",
			"pssm", 
			"motif",
			"pocket"
		]

	##------------------------------------------------------------------##

	def queryFarmer(self,task="run_main"):
	
		if self.options["dataset_type"] == "annotator":
			import annotatorManager		
			dataManagerObj = annotatorManager.annotatorManager()

		elif self.options["dataset_type"] == "accessibility":
			import accessibilityManager
			dataManagerObj = accessibilityManager.accessibiltyManager()

		elif self.options["dataset_type"] == "cell_cycle":
			import cellcycleManager
			dataManagerObj = cellcycleManager.cellcycleManager()

		elif self.options["dataset_type"] == "druggability":
			import druggabilityManager
			dataManagerObj = druggabilityManager.druggabilityManager()

		elif self.options["dataset_type"] == "architecture":
			import architectureManager
			dataManagerObj = architectureManager.architectureManager()
			
		elif self.options["dataset_type"] == "evolution":
			import evolutionManager
			dataManagerObj = evolutionManager.evolutionManager()
		
		elif self.options["dataset_type"] == "interaction":
			import interactionManager
			dataManagerObj = interactionManager.interactionManager()
		
		elif self.options["dataset_type"] == "interface":
			import interfaceManager
			dataManagerObj = interfaceManager.interfaceManager()
		
		elif self.options["dataset_type"] == "elm":
			import elmManager
			dataManagerObj = elmManager.elmManager()

		elif self.options["dataset_type"] == "motif":
			import motifManager
			dataManagerObj = motifManager.motifManager()
		
		elif self.options["dataset_type"] == "mutations":
			import mutationsManager
			dataManagerObj = mutationsManager.mutationsManager()

		elif self.options["dataset_type"] == "ontology":
			import ontologyManager
			dataManagerObj = ontologyManager.ontologyManager()
		
		elif self.options["dataset_type"] == "pdb":
			import pdbManager
			dataManagerObj = pdbManager.pdbManager()
		
		elif self.options["dataset_type"] == "pfam":
			import pfamManager
			dataManagerObj = pfamManager.pfamManager()
		
		elif self.options["dataset_type"] == "pssm":
			import pssmManager
			dataManagerObj = pssmManager.pssmManager()
		
		elif self.options["dataset_type"] == "pubmed":
			import pubmedManager
			dataManagerObj = pubmedManager.pubmedManager()

		elif self.options["dataset_type"] == "motif_alignment":
			import motifAlignmentManager
			dataManagerObj = motifAlignmentManager.motifAlignmentManager()

		elif self.options["dataset_type"] == "uniprot":
			import uniprotManager
			dataManagerObj = uniprotManager.uniprotManager()

		elif self.options["dataset_type"] == "comparimotif":
			import consensusManager
			dataManagerObj = consensusManager.consensusManager()

		elif self.options["dataset_type"] == "pocket":
			import pocketManager
			dataManagerObj = pocketManager.pocketManager()
		else:
			return {"status":"Error","error_type":"Analysis type [" + self.options['dataset_type'] + "] does not exist."}
		
		## PURGE COMMANDLINE
		sys.argv = sys.argv[:1]

		dataManagerObj.options.update(self.options)

		if task == "check_options":
			return dataManagerObj.setupTask(self.options)
		elif task == "run_main":
			return dataManagerObj.main()
		elif task == "check_queue_requirement":
			return self.options["task"] in dataManagerObj.task_skip_queue 
		else:
			return {"status":"Error","error_type":"Task [" + task + "] does not exist."}
		
	##------------------------------------------------------------------##

	def processCommandline(self):

		#-------------#

		### IF CALLED FROM COMMANDLINE ###

		remove_sys_argv_values = []

		if "--rest_api" in sys.argv:
			#python3 queryManager.py  --verbose True --task test_rest_api --test_all True --rest_api http://disorder.icr.ac.uk:8080/rest/get/ 

			dataset_type_index = sys.argv.index("--rest_api")
			
			if dataset_type_index != -1:
				self.options['rest_api'] = sys.argv[dataset_type_index+1]
				remove_sys_argv_values += [dataset_type_index, dataset_type_index+1]
				
				remove_sys_argv_values.sort()
				sys_argv_updates = []
			

		if "--test_all" in sys.argv:
			#python3 queryManager.py  --verbose True --test_all True
			
			dataset_type_index = sys.argv.index("--test_all")
			
			if dataset_type_index != -1:
				self.options['test_all'] = sys.argv[dataset_type_index+1] == "True"
				remove_sys_argv_values += [dataset_type_index, dataset_type_index+1]
				
				remove_sys_argv_values.sort()
				sys_argv_updates = []

			for i in range(0,len(sys.argv)):
				if i not in remove_sys_argv_values:
					sys_argv_updates.append(sys.argv[i])

			sys.argv = sys_argv_updates
				
			if self.options['test_all']:
				if self.options['test_all']:
					original_sys_argv = sys.argv
					for dataset_type in self.dataset_types:
						print("#"*100)
						print("#","TESTING",dataset_type)
						print("#"+"-"*99)
						self.options["dataset_type"] = dataset_type
						self.options["verbose"] = False
						sys.argv = original_sys_argv
						self.queryFarmer()

				sys.exit()

		if "--dataset_type" in sys.argv:
			dataset_type_index = sys.argv.index("--dataset_type")

			if dataset_type_index != -1:
				self.options['dataset_type'] = sys.argv[dataset_type_index+1]
				remove_sys_argv_values += [dataset_type_index, dataset_type_index+1]
		
		if "--bulk_taxon_id" in sys.argv:
			grab_options = ['verbose','queue','bulk_taxon_id','reviewed','structures','task','chunks','chunk_number']
		
			for grab_option in grab_options:
				if "--" + grab_option in sys.argv:
					dataset_type_index = sys.argv.index("--" + grab_option)
					if dataset_type_index != -1:
						self.options[grab_option] = sys.argv[dataset_type_index+1]
						remove_sys_argv_values += [dataset_type_index, dataset_type_index+1]
						if self.options[grab_option] == "False": self.options[grab_option] = False
						if self.options[grab_option] == "True": self.options[grab_option] = True
		
		#-------------#

		remove_sys_argv_values.sort()
		sys_argv_updates = []

		for i in range(0,len(sys.argv)):
			if i not in remove_sys_argv_values:
				sys_argv_updates.append(sys.argv[i])

		sys.argv = sys_argv_updates
		return sys_argv_updates

	#########################################################
	#########################################################
	#########################################################
	#########################################################

	def main(self):
		### IF CALLED FROM COMMANDLINE ###
		sys_argv_updates = self.processCommandline()

		#-------------#

		if self.options['dataset_type'] not in self.dataset_types:
			status = {
				"status":"Error",
				"error_type":"Dataset type '" + self.options['dataset_type'] + "' is not a valid option. Please set --dataset_type from " + ",".join(self.dataset_types),
				"args":sys.argv,
				"options":self.options
				}

			if 'verbose' in self.options:
				if self.options['verbose'] == True:
					print(status)
			else:	
				print(status)

			return status

		#########################################################
		
		if 'bulk_taxon_id' in self.options:
			dataDownloaderObj = uniprotDownloader.uniprotDownloader()
			dataDownloaderObj.options.update(self.options)

			if "chunks" not in  self.options: self.options["chunks"] = 1
			if "chunk_number" not in  self.options: self.options["chunk_number"] = 1
			if "reviewed" not in  self.options: self.options["reviewed"] = True
			if "structures" not in  self.options: self.options["structures"] = False

			accessions = dataDownloaderObj.parse_uniprot_accession_taxa(self.options['bulk_taxon_id'] ,reviewed=self.options["reviewed"],structures=self.options["structures"],nofragment=False)

			
			chunks = int(self.options['chunks'])
			chunk_number = int(self.options['chunk_number']) - 1
			accessions_count = len(accessions)
			
			chunk_length = accessions_count/chunks
			chunk_start = int(chunk_length*chunk_number)
			chunk_end = int(chunk_length*(chunk_number+1))

			protein_counter = 0
			print("Proteins:",len(accessions),chunk_start,chunk_end)
			
			for accession in accessions[chunk_start:chunk_end]:
				protein_counter += 1
				print("#",protein_counter,"of",len(accessions[chunk_start:chunk_end]),"\t",accession,"\t",chunk_start,"\t",chunk_end)

				sys.argv = sys_argv_updates 
				if self.options["dataset_type"] == "interface":
					sys.argv.append("--protein_id")
				else:
					sys.argv.append("--accession")
					
				sys.argv.append(accession)
				self.queryFarmer()
		else:
			if "queue" in self.options:
				if self.options["server"]:
					queueManagerObj = queueManager()
					queueManagerObj.options.update(self.options)
					status = queueManagerObj.queue_director()
					return status
				else:
					return {
					"status":"Completed",
					"options":self.options,
					"data":self.queryFarmer(),
					#"check_options_response":check_options_response
					}
			else:
				return self.queryFarmer()

	#########################################################
	#########################################################
	#########################################################
	#########################################################
	
if __name__ == "__main__":
	queryManagerObj = queryManager()
	queryManagerObj.main()
