import sys

if sys.version_info[0] != 3 or sys.version_info[1] < 5:
    print("This script requires Python version 3.5 or greater")
    sys.exit(1)

import os
import inspect
file_path = os.path.dirname(inspect.stack()[0][1])

from queryManager import queryManager

class queryRunner:

	##------------------------------------------------------------------##

	def __init__(self,dataset_type, task, options={}, accession=None, pdb_id =None, pmid = None, pfam_id = None):
		self.queryManagerObj = queryManager()
		self.queryManagerObj.options['dataset_type'] = dataset_type
		self.queryManagerObj.options['task'] = task
		self.queryManagerObj.options.update(options)

		if accession != None:	
			self.queryManagerObj.options['accession'] = accession

		if pdb_id != None:
			self.queryManagerObj.options['pdb_id'] = pdb_id
			
		if pmid != None: 
			self.queryManagerObj.options['pmid'] = pmid
			
		if pfam_id != None:
			self.queryManagerObj.options['pfam_id'] = pfam_id
	
	##------------------------------------------------------------------##

	def run(self):
		self.data = self.queryManagerObj.main()
		return self.data
	
	##------------------------------------------------------------------##
	
if __name__ == "__main__":

	import pprint

	#########################################################

	print("#TESTING parse_uniprot")
	data = queryRunner("uniprot","parse_uniprot",accession="P06400").run()
	pprint.pprint(data)
	
	#########################################################

	print("#TESTING parse_pmid")
	data = queryRunner("pubmed","parse_pmid",pmid="11742988").run()
	pprint.pprint(data)

	#########################################################

	print("#TESTING get_structure_general_data")
	data = queryRunner("pdb","get_structure_general_data",pdb_id="2AST").run()
	pprint.pprint(data)

	#########################################################

	print("#TESTING parse_pfam")
	data = queryRunner("pfam","parse_pfam",pfam_id="PF00400").run()
	pprint.pprint(data)

	#########################################################

	print("#TESTING parse_uniprot")

	dataset_type = "pssm"
	task= "make_pssm"

	options = {}
	options['peptides'] ="AIPQIVIDA,PSPRIEITP,VTPIISIQE,RLPVIAVND,QVPNIYIQT,ESPRIEITS,GVPRITISD,KKPKIIITG,KKPKIIITG,ESPRIEITS"
	
	data = queryRunner(dataset_type,task,options).run()
	pprint.pprint(data)

	#########################################################

	print("#TESTING parse_uniprot")

	dataset_type = "motif_alignment"
	task= "align_peptides_by_slimfinder"

	options = {}
	options['topranks'] = 1
	options["variant"] = "fast_heuristic"
	options['use_peptide_aa_freqs'] = True
	options['peptides'] = "MAQKENSYPWPYG,TEGGEAQKENGIYSVTF,HVFEDGNKENYGLPQPK,SVHILEDKENVVAKQCT"
	
	data = queryRunner(dataset_type,task,options).run()
	pprint.pprint(data)





	


	
