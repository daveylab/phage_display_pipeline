import os
import sys

import inspect

file_path = os.path.dirname(inspect.stack()[0][1])
sys.path.append(os.path.join(file_path,"../"))
import option_reader

sys.path.append(os.path.join(file_path,"../downloaders"))
import uniprotDownloader

from dataManager import dataManager

class uniprotManager(dataManager):

	##------------------------------------------------------------------##
	## Inherits functions from dataManager
	##------------------------------------------------------------------##

	def setup_data(self):
		self.default_task_options = {
			"task":'help',
			"remake":False,
			"verbose":False,
			"reviewed":False,
			"mapping_from":'ACC+ID',
			"mapping_to":"ACC",
			"mapping_format":"tab",
			"flank_length":0,
			"uniref_identity":0.5,
			"debug":False,
			"logfile":False
		}

		self.allowed_options = [
			"task",
			'verbose',
			"outfile",
			"accession",
			"region_start",
			"region_end",
			"pdb_id",
			"taxon_id",
			"taxonomy",
			"identifier",
			"reviewed",
			"regions",
			"flank_length",
			"mapping_from",
			"mapping_to",
			"mapping_format",
			"pmid",
			'parse_features',
			'use_features',
			'parse_keywords',
			'parse_snps',
			'parse_attributes',
			'parse_disorder',
			'parse_isoforms',
			'parse_sequence',
			'parse_generic',
			"remake",
			'parseCommandline'
		]

		self.allowed_options_admin = [
		]

		self.task_options = [
			"parse_uniprot",
			"parse_uniprot_bulk",
			"parse_uniprot_by_pdb",
			"parse_mobidb",
			"parse_sequence",
			"parse_uniprot_pfam",
			"parse_attributes",
			"parse_features",
			"parse_basic",
			"parse_name",
			"parse_domains",
			"parse_go",
			"parse_isoforms",
			"parse_keywords",
			"parse_localisation",
			"parse_pdb",
			"parse_secondary_accessions",
			"parse_snps",
			"parse_region_sequence",
			"parse_region_features",
			"parse_secondary_structure",
			"parse_mutagenesis_site",
			"parse_diseases",
			"parse_ptms",
			"parse_regions_of_interest",
			"get_uniprot_mapping",
			"get_uniprot_mapping_details",
			"parse_taxon_identifier_information",
			"parse_uniprot_accession_taxa",
			"parse_taxonomy_information",
			"parse_uniprot_uniref_members",
			"parse_uniprot_uniref_clustername",
			"check_accession",
			"check_uniprot_entry",
			"help"
		]

		self.required = {
			'all':[],
			"parse_uniprot":["accession"],
			"parse_uniprot_bulk":["accession"],
			"parse_uniprot_by_pdb":["pdb_id"],
			"parse_mobidb":["accession"],
			"parse_sequence":["accession"],
			"parse_uniprot_pfam":["accession"],
			"parse_attributes":["accession"],
			"parse_features":["accession"],
			"parse_basic":["accession"],
			"parse_name":["accession"],
			"parse_domains":["accession"],
			"parse_go":["accession"],
			"parse_isoforms":["accession"],
			"parse_keywords":["accession"],
			"parse_localisation":["accession"],
			"parse_pdb":["accession"],
			"parse_secondary_accessions":["accession"],
			"parse_snps":["accession"],
			"parse_region_sequence":["regions"],
			"parse_region_features":["regions"],
			"get_uniprot_mapping":["identifier"],
			"get_uniprot_mapping_details":["identifier"],
			"parse_taxon_identifier_information":["taxon_id"],
			"parse_uniprot_accession_taxa":["taxon_id"],
			"parse_taxonomy_information":["taxonomy"],
			"parse_uniprot_uniref_members":["accession"],
			"parse_uniprot_uniref_clustername":["accession"],
			"check_accession":["accession"]
		}

		self.test_options = {
			'accession':"P06400",
			#'regions':"",#P04637;10;34,P04600;100;200,P20248;1;156",
			'pdb_id':"2AST",
			'taxon_id':"7227",
			'taxonomy':"Insecta",
			'identifier':"CCNA2_HUMAN"
		}

		self.required_type = {
			'accession':"string",
			'identifier':"string",
			'remake':"bool",
			'flank_length':"int"
		}

		self.required_format = {
			'accession':"\A([OPQ][0-9][A-Z0-9]{3}[0-9]|[A-NR-Z][0-9]([A-Z][A-Z0-9]{2}[0-9]){1,2})\-{0,1}[0-9]*\Z"
		}

		# delimiter to split options
		self.list_options = {
			"identifier":",",
			"accession":",",
			"regions":",",
			"pdb_id":",",
			"use_features":","
		}

		self.required_valid_options_list = {
			'remake':[True,False],
			'tasks':self.task_options
		}

		self.options.update(self.default_task_options)

		for allowed_option in self.allowed_options:
			if allowed_option not in self.options:
				self.options[allowed_option] = None

		if 'parseCommandline' not in self.options: self.options['parseCommandline'] = True
		if 'parseCommandline' in self.options: 
			if self.options['parseCommandline'] == None: 
				self.options['parseCommandline'] = True
				
		if self.options['parseCommandline']:
			self.options.update(option_reader.load_commandline_options(self.options,self.options))

	##------------------------------------------------------------------##

	def getData(self,status,task_options):

		###----######----######----###
		#   run_job run_job run_job  #
		###----######----######----###

		data = {}

		dataDownloaderObj = uniprotDownloader.uniprotDownloader()
		dataDownloaderObj.options.update(task_options)

		if task_options["task"] == "parse_uniprot":
			data = dataDownloaderObj.parseUniProt(task_options['accession'][0])
		if task_options["task"] == "parse_uniprot_bulk":
			data = dataDownloaderObj.parseUniProtBulk(task_options['accession'])
		if task_options["task"] == "parse_uniprot_by_pdb":
			data = dataDownloaderObj.parseUniProtByPDB(task_options['pdb_id'][0])
		if task_options["task"] == "parse_mobidb":
			data = dataDownloaderObj.parseMobiDB(task_options['accession'][0])
		if task_options["task"] == "parse_sequence":
			data = dataDownloaderObj.parseSequence(task_options['accession'][0])
		if task_options["task"] == "parse_uniprot_pfam":
			data = dataDownloaderObj.parseUniProtPfam(task_options['accession'][0])
		if task_options["task"] == "parse_attributes":
			data = dataDownloaderObj.parseAttributes(task_options['accession'][0])
		if task_options["task"] == "parse_features":
			data = dataDownloaderObj.parseFeatures(task_options['accession'][0])
		if task_options["task"] == "parse_basic":
			data = dataDownloaderObj.parseBasic(task_options['accession'][0])
		if task_options["task"] == "parse_name":
			data = dataDownloaderObj.parseName(task_options['accession'][0])
		if task_options["task"] == "parse_domains":
			data = dataDownloaderObj.parseDomains(task_options['accession'][0])
		if task_options["task"] == "parse_go":
			data = dataDownloaderObj.parseGO(task_options['accession'][0])
		if task_options["task"] == "parse_isoforms":
			data = dataDownloaderObj.parseIsoforms(task_options['accession'][0])
		if task_options["task"] == "parse_keywords":
			data = dataDownloaderObj.parseKeywords(task_options['accession'][0])
		if task_options["task"] == "parse_localisation":
			data = dataDownloaderObj.parseLocalisation(task_options['accession'][0])
		if task_options["task"] == "parse_pdb":
			data = dataDownloaderObj.parsePDBs(task_options['accession'][0])
		if task_options["task"] == "parse_secondary_accessions":
			data = dataDownloaderObj.parseSecondaryAccessions(task_options['accession'][0])
		if task_options["task"] == "parse_snps":
			data = dataDownloaderObj.parseSNPs(task_options['accession'][0])
		if task_options["task"] == "parse_region_sequence":
			data = dataDownloaderObj.parse_region_sequence(task_options['regions'],flank_length=task_options['flank_length'])
		if task_options["task"] == "parse_region_features":
			data = dataDownloaderObj.parse_region_features(task_options['regions'],flank_length=task_options['flank_length'])
		if task_options["task"] == "get_uniprot_mapping":
			data = dataDownloaderObj.get_uniprot_mapping(task_options['identifier'])
		if task_options["task"] == "get_uniprot_mapping_details":
			data = dataDownloaderObj.get_uniprot_mapping_details(task_options['identifier'],mapping_from=task_options["mapping_from"],mapping_to=task_options["mapping_to"],format=task_options["mapping_format"])
		if task_options["task"] == "parse_taxon_identifier_information":
			data = dataDownloaderObj.parse_taxon_identifier_information()
		if task_options["task"] == "parse_taxonomy_information":
			data = dataDownloaderObj.parse_taxonomy_information()
		if task_options["task"] == "parse_secondary_structure":
			data = dataDownloaderObj.parseSecondaryStructure(task_options['accession'][0])
		if task_options["task"] == "parse_mutagenesis_site":
			data = dataDownloaderObj.parseMutagenesis(task_options['accession'][0])
		if task_options["task"] == "parse_ptms":
			data = dataDownloaderObj.parsePTMs(task_options['accession'][0])
		if task_options["task"] == "parse_regions_of_interest":
			data = dataDownloaderObj.parseRegionsOfInterest(task_options['accession'][0])
		if task_options["task"] == "parse_diseases":
			data = dataDownloaderObj.parseDiseases(task_options['accession'][0])
		if task_options["task"] == "parse_uniprot_accession_taxa":
			data = dataDownloaderObj.parse_uniprot_accession_taxa(task_options['taxon_id'],reviewed=task_options['reviewed'])
		if task_options["task"] == "parse_uniprot_uniref_members":
			data = dataDownloaderObj.getUniProtUnirefMembers(task_options['accession'][0],task_options['uniref_identity'])
		if task_options["task"] == "parse_uniprot_uniref_clustername":
			data = dataDownloaderObj.getUniProtUnirefClusterName(task_options['accession'][0],task_options['uniref_identity'])
		if task_options["task"] == "check_accession":
			data = dataDownloaderObj.check_accession(task_options['accession'][0])
		if task_options["task"] == "check_uniprot_entry":	
			data = dataDownloaderObj.checkUniProtEntry(task_options['accession'][0])
			
		return data

	##------------------------------------------------------------------##
	##------------------------   END    END  ---------------------------##
	##------------------------------------------------------------------##

if __name__ == "__main__":

	dataManagerObj = uniprotManager()
	dataManagerObj.main()
