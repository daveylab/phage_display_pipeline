import os,json, pprint,inspect,sys

file_path = os.path.dirname(inspect.stack()[0][1])
sys.path.append(os.path.join(file_path,"../"))
sys.path.append(os.path.join(file_path,"../utilities/"))

import config_reader

from chembl_webresource_client.new_client import new_client
# https://chembl.gitbook.io/chembl-interface-documentation/web-services/chembl-data-web-services#obtain-the-pchembl-value-for-a-specific-compound-and-a-specific-target

class chemblDownloader():
	def __init__(self):
		self.options = {}

		self.options.update(config_reader.load_configeration_options(sections=["general","structure_reader"]))

		self.options['require_activity'] = True 
		self.options['load_drugs'] = True 
		self.options['download_chunk_size'] = 250
		self.options['chembl_data_path'] = os.path.join(self.options["data_path"], "chembl")
		
		self.target_data = {}

		self.check_directory()

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def check_directory(self):
		if not os.path.exists(os.path.join(self.options["chembl_data_path"])):
			os.mkdir(os.path.join(self.options["chembl_data_path"]))
		
		if not os.path.exists(os.path.join(self.options["chembl_data_path"],"targets_by_accession")):
			os.mkdir(os.path.join(self.options["chembl_data_path"],"targets_by_accession"))

		if not os.path.exists(os.path.join(self.options["chembl_data_path"],"compounds_by_target")):
			os.mkdir(os.path.join(self.options["chembl_data_path"],"compounds_by_target"))
		
		if not os.path.exists(os.path.join(self.options["chembl_data_path"],"drug")):
			os.mkdir(os.path.join(self.options["chembl_data_path"],"drug"))

		if not os.path.exists(os.path.join(self.options["chembl_data_path"],"drug_indication")):
			os.mkdir(os.path.join(self.options["chembl_data_path"],"drug_indication"))

		if not os.path.exists(os.path.join(self.options["chembl_data_path"],"proteins")):
			os.mkdir(os.path.join(self.options["chembl_data_path"],"proteins"))

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def grab_chembl_drug(self):
		download_molecule_chembl_id = []

		for molecule_chembl_id in self.options['molecule_chembl_id']:
			self.options['drug_json_path'] = os.path.join(self.options["chembl_data_path"],"drug",molecule_chembl_id + ".drug.chembl.json")
		
			if not os.path.exists(self.options['drug_json_path']):
				download_molecule_chembl_id.append(molecule_chembl_id)
	
		if len(download_molecule_chembl_id) > 0:
			chunks_count = int(len(download_molecule_chembl_id)/self.options['download_chunk_size']) + 1
			for chunk_number in range(0,chunks_count):
				chunk_start = chunk_number*self.options['download_chunk_size']
				chunk_end = min((chunk_number+1)*self.options['download_chunk_size'],len(download_molecule_chembl_id))
				print(chunk_number,chunk_start,chunk_end,len(download_molecule_chembl_id) )

				chunk_download_molecule_chembl_id = download_molecule_chembl_id[chunk_start:chunk_end]

				drug_client = new_client.drug
				drug_response = drug_client.filter(molecule_chembl_id__in = chunk_download_molecule_chembl_id)
				del drug_client

				drugs = {}

				for drug in drug_response:
					if drug['molecule_chembl_id'] not in drugs: drugs[drug['molecule_chembl_id']] = []
					drugs[drug['molecule_chembl_id']].append(drug)

				for molecule_chembl_id in chunk_download_molecule_chembl_id:
					self.options['drug_json_path'] = os.path.join(self.options["chembl_data_path"],"drug",molecule_chembl_id + ".drug.chembl.json")

					with open(self.options['drug_json_path'], 'w') as outfile:
						json_compounds = []

						if molecule_chembl_id in drugs:
							for compound in drugs[molecule_chembl_id]:
								json_compounds.append(dict(compound))

						json.dump(json_compounds, outfile)
		
		return_compounds = {}
		
		for molecule_chembl_id in self.options['molecule_chembl_id']:
			self.options['drug_json_path'] = os.path.join(self.options["chembl_data_path"],"drug",molecule_chembl_id + ".drug.chembl.json")
			drug_data = json.loads(open(self.options['drug_json_path'] , 'r').read())
			if len(drug_data) > 0:
				return_compounds[molecule_chembl_id] = drug_data[0]
			
		return return_compounds
	
	def grab_chembl_drug_indication(self):
		download_molecule_chembl_id = []

		for molecule_chembl_id in self.options['molecule_chembl_id']:
			self.options['drug_json_path'] = os.path.join(self.options["chembl_data_path"],"drug_indication",molecule_chembl_id + ".drug_indication.chembl.json")
		
			if not os.path.exists(self.options['drug_json_path']):
				download_molecule_chembl_id.append(molecule_chembl_id)
		
		if len(download_molecule_chembl_id) > 0:
			chunks_count = int(len(download_molecule_chembl_id)/self.options['download_chunk_size']) + 1
			for chunk_number in range(0,chunks_count):
				chunk_start = chunk_number*self.options['download_chunk_size']
				chunk_end = min((chunk_number+1)*self.options['download_chunk_size'],len(download_molecule_chembl_id))
				print(chunk_number,chunk_start,chunk_end,len(download_molecule_chembl_id))

				chunk_download_molecule_chembl_id = download_molecule_chembl_id[chunk_start:chunk_end]
				drug_indication_client = new_client.drug_indication
				drug_indication_response = drug_indication_client.filter(molecule_chembl_id=chunk_download_molecule_chembl_id)
				del drug_indication_client

				drug_indications = {}
				for drug_indication in drug_indication_response:
					if drug_indication['molecule_chembl_id'] not in drug_indications: drug_indications[drug_indication['molecule_chembl_id']] = []
					drug_indications[drug_indication['molecule_chembl_id']].append(drug_indication)

				for molecule_chembl_id in download_molecule_chembl_id:
					self.options['drug_json_path'] = os.path.join(self.options["chembl_data_path"],"drug_indication",molecule_chembl_id + ".drug_indication.chembl.json")
			
					with open(self.options['drug_json_path'], 'w') as outfile:
						json_compounds = []
						if molecule_chembl_id in drug_indications:
							for compound in drug_indications[molecule_chembl_id]:
								json_compounds.append(dict(compound))
					
						json.dump(json_compounds, outfile)
			
		return_compounds = {}
		
		for molecule_chembl_id in self.options['molecule_chembl_id']:
			self.options['drug_json_path'] = os.path.join(self.options["chembl_data_path"],"drug_indication",molecule_chembl_id + ".drug_indication.chembl.json")
			return_compounds[molecule_chembl_id] = json.loads(open(self.options['drug_json_path'] , 'r').read())

		return return_compounds

	def grab_chembl_protein_target(self):
		self.options['target_json_path'] = os.path.join(self.options["chembl_data_path"],"targets_by_accession", self.options['accession'][0] + ".targets.chembl.json")
		print(self.options['target_json_path'] )
		if os.path.exists(self.options['target_json_path']):
			print("#Reading",self.options['target_json_path'])
			targets_response= json.loads(open(self.options['target_json_path'] , 'r').read())
		else:
			print("#Writing",self.options['target_json_path'])
			target_client = new_client.target
			targets_response = target_client.filter(target_components__accession=self.options['accession'][0])
			del target_client

			pprint.pprint(targets_response)
			with open(self.options['target_json_path'], 'w') as outfile:
				targets = []
				for target in targets_response:
					targets.append(dict(target))

				json.dump(targets, outfile)

		return targets_response

	##------------------------------------------------------------------##

	def grab_chembl_compounds_targets(self):
		download_target_chembl_ids = []
		target_chembl_ids = list(self.target_data["targets"].keys())
		
		for target_chembl_id in target_chembl_ids:
			self.options['compound_json_path'] = os.path.join(self.options["chembl_data_path"],"compounds_by_target", target_chembl_id + ".compounds.chembl.json")

			if not os.path.exists(self.options['compound_json_path']):
				print("#Adding",target_chembl_id)
				download_target_chembl_ids.append(target_chembl_id)

		if len(download_target_chembl_ids) > 0:
			activity_client = new_client.activity
			compounds_response = new_client.activity.filter(target_chembl_id__in = download_target_chembl_ids)
			print("#Downloading",download_target_chembl_ids)
			compounds = {}
			for compound in compounds_response:
				if compound['target_chembl_id'] not in compounds: compounds[compound['target_chembl_id']] = []
				compounds[compound['target_chembl_id']].append(compound)

			del activity_client

			for chembl_target_id in download_target_chembl_ids:
				self.options['compound_json_path'] = os.path.join(self.options["chembl_data_path"],"compounds_by_target", chembl_target_id + ".compounds.chembl.json")

				with open(self.options['compound_json_path'], 'w') as outfile:
					#print("#Writing",chembl_target_id)
			
					json_compounds = []
					for compound in compounds[chembl_target_id]:
						json_compounds.append(dict(compound))

					json.dump(json_compounds, outfile)
		
		return_compounds = {}
		for target_chembl_id in target_chembl_ids:
			try:
				self.options['compound_json_path'] = os.path.join(self.options["chembl_data_path"],"compounds_by_target", target_chembl_id + ".compounds.chembl.json")
				return_compounds[target_chembl_id] = json.loads(open(self.options['compound_json_path'] , 'r').read())
			except:
				print("#Error opening",self.options['compound_json_path'] )

		return return_compounds

	##------------------------------------------------------------------##

	def parse_chembl_protein_data(self):

		self.options['protein_json_path'] = os.path.join(self.options["chembl_data_path"],"proteins", self.options['accession'][0] + ".protein.json")

		if not os.path.exists(self.options['protein_json_path'] ):
			print("#Cached",download_target_chembl_ids)
			
			self.load_chembl_target_data()
			self.load_chembl_compound_data()
			self.parse_chembl_active_compound_data()
			self.parse_chembl_drug_data()
			self.parse_chembl_drug_indication_data()

			with open(self.options['protein_json_path'], 'w') as outfile:
				json.dump(self.target_data, outfile)
		else:
			self.target_data = json.loads(open(self.options['protein_json_path'] , 'r').read())
		
	##------------------------------------------------------------------##

	def load_chembl_target_data(self):
		self.target_data = {
			"targets":{},
			"compounds":{},
			"active_compounds":{},
			"has_active_compound":False,
			}

		targets = self.grab_chembl_protein_target()
		
		for target in targets:
			for i in range(0,len(target['target_components'])):
				del target['target_components'][i]['target_component_synonyms']
				del target['target_components'][i]['target_component_xrefs']

			del target['cross_references']

			self.target_data["targets"][target['target_chembl_id']] = target
			self.target_data["targets"][target['target_chembl_id']]['compounds'] = {}

			del self.target_data["targets"][target['target_chembl_id']]['target_chembl_id']


	##------------------------------------------------------------------##

	def load_chembl_compound_data(self):

		target_compounds = self.grab_chembl_compounds_targets()
		
		for target in  target_compounds:
			compounds = target_compounds[target]
			for compound in compounds:
				target_chembl_id = compound['target_chembl_id']
				molecule_chembl_id = compound['molecule_chembl_id']
				activity_id = compound['activity_id']
				####
				
				if self.options['require_activity'] and compound["activity_comment"] in [None,'Inactive','Not Active','Not Determined']: continue
				
				if molecule_chembl_id not in self.target_data["compounds"]:
					self.target_data["compounds"][molecule_chembl_id] = {
						'experiments':{},
						'targets':[],
						'molecule_pref_name':compound['molecule_pref_name']
						}

				if target_chembl_id not in self.target_data["targets"]:
					self.target_data["targets"][target_chembl_id] = {
						'compounds':{}
						}

				####
				
				self.target_data["compounds"][molecule_chembl_id]['experiments'][activity_id] = compound

				if target_chembl_id not in self.target_data["compounds"][molecule_chembl_id]['targets']:
					self.target_data["compounds"][molecule_chembl_id]['targets'].append(target_chembl_id)
				
				if molecule_chembl_id not in self.target_data["targets"][target_chembl_id]['compounds']:
					self.target_data["targets"][target_chembl_id]['compounds'][molecule_chembl_id] = {
						'active':False,
						'experiments':{},
						'activity_comments':[],
						'molecule_pchembl_values':[],
						'molecule_pref_name':compound['molecule_pref_name'],
						'canonical_smiles':compound['canonical_smiles']
					}

				self.target_data["targets"][target_chembl_id]['compounds'][molecule_chembl_id]['experiments'][activity_id] = {
					"activity_comment":compound['activity_comment'],
					"assay_description":compound['assay_description'],
					"pchembl_value":compound['pchembl_value'],
					}
		
				if compound['pchembl_value'] not in self.target_data["targets"][target_chembl_id]['compounds'][molecule_chembl_id]["molecule_pchembl_values"] and compound['pchembl_value'] != None:
					self.target_data["targets"][target_chembl_id]['compounds'][molecule_chembl_id]["molecule_pchembl_values"].append(compound['pchembl_value'])

				if compound['activity_comment'].lower() not in self.target_data["targets"][target_chembl_id]['compounds'][molecule_chembl_id]["activity_comments"]:
					self.target_data["targets"][target_chembl_id]['compounds'][molecule_chembl_id]["activity_comments"].append(compound['activity_comment'].lower())

				if compound["activity_comment"].lower() in ["active","dose-dependent effect"]:
					self.target_data["targets"][target_chembl_id]['compounds'][molecule_chembl_id]['active'] = True
					self.target_data["has_active_compound"] = True
					self.target_data["active_compounds"][molecule_chembl_id] = {
						"molecule_pref_name":compound['molecule_pref_name']

					}
		
		return self.target_data

	##------------------------------------------------------------------##

	def parse_chembl_drug_data(self):
		if self.options['load_drugs']:
			self.options['molecule_chembl_id'] = self.target_data["compounds"].keys()
			self.target_data["drugs"] = {}
			self.target_data["drugs_details"] = {}
			self.target_data["has_drug"] = False

			drugs = self.grab_chembl_drug()

			for molecule_chembl_id in drugs:
				if len(drugs[molecule_chembl_id]) > 0:
					self.target_data["has_drug"] = True
					targets = {}
					for target_chembl_id in self.target_data["compounds"][molecule_chembl_id]['targets']:
						if not self.target_data["targets"][target_chembl_id]['compounds'][molecule_chembl_id]['active']: continue
				
						targets[target_chembl_id] = {
						'pref_name':self.target_data["targets"][target_chembl_id]['pref_name'],
						'target_type':self.target_data["targets"][target_chembl_id]['target_type'],
						'target_components':self.target_data["targets"][target_chembl_id]['target_components'],
						'active':self.target_data["targets"][target_chembl_id]['compounds'][molecule_chembl_id]['active'],
						'activity_comments':self.target_data["targets"][target_chembl_id]['compounds'][molecule_chembl_id]['activity_comments'],
						'experiments':self.target_data["targets"][target_chembl_id]['compounds'][molecule_chembl_id]['experiments']
						}
					
					if len(targets) > 0:
						self.target_data["drugs"][molecule_chembl_id] = {
							"molecule_pref_name":self.target_data["compounds"][molecule_chembl_id]['molecule_pref_name'],
							"first_approval":drugs[molecule_chembl_id]['first_approval'],
							"development_phase":drugs[molecule_chembl_id]['development_phase'],
							"synonyms":drugs[molecule_chembl_id]['synonyms'],
							"usan_stem_definition":drugs[molecule_chembl_id]['usan_stem_definition'],
							"drug_indications":[],
							"targets":targets,
							"is_drug":True,
							"passed_clinical_trial":drugs[molecule_chembl_id]['first_approval'] != None
						}

					self.target_data["drugs_details"][molecule_chembl_id] = drugs[molecule_chembl_id]
	
	##------------------------------------------------------------------##

	def parse_chembl_active_compound_data(self):
	
		self.options['molecule_chembl_id'] = self.target_data["compounds"].keys()
		
		for molecule_chembl_id in self.target_data["active_compounds"]:
			targets = {}
			for target_chembl_id in self.target_data["compounds"][molecule_chembl_id]['targets']:
		
				targets[target_chembl_id] = {
				'pref_name':self.target_data["targets"][target_chembl_id]['pref_name'],
				'target_type':self.target_data["targets"][target_chembl_id]['target_type'],
				'target_components':self.target_data["targets"][target_chembl_id]['target_components'],
				'active':self.target_data["targets"][target_chembl_id]['compounds'][molecule_chembl_id]['active'],
				'activity_comments':self.target_data["targets"][target_chembl_id]['compounds'][molecule_chembl_id]['activity_comments'],
				'experiments':self.target_data["targets"][target_chembl_id]['compounds'][molecule_chembl_id]['experiments']
				}
			
			if len(targets) > 0:
				self.target_data["active_compounds"][molecule_chembl_id]["targets"] = targets
				

			
	##------------------------------------------------------------------##

	def parse_chembl_drug_indication_data(self):
		if self.options['load_drugs']:
			self.options['molecule_chembl_id'] = self.target_data["compounds"].keys()
			self.target_data["drug_indications"] = {}
			self.target_data["drug_indications_details"] = {}
			self.target_data["has_drug_indication"] = False

			drug_indications = self.grab_chembl_drug_indication()

			for molecule_chembl_id in drug_indications:

				if len(drug_indications[molecule_chembl_id]) > 0:
					self.target_data["drug_indications_details"][molecule_chembl_id] = len(drug_indications[molecule_chembl_id])
					self.target_data["drug_indications_details"][molecule_chembl_id] = drug_indications[molecule_chembl_id]

					for drug_indication in drug_indications[molecule_chembl_id]:
						self.target_data["drugs"][molecule_chembl_id]["drug_indications"].append({
							"efo_term":drug_indication["efo_term"],
							"max_phase_for_ind":drug_indication["max_phase_for_ind"],
							"mesh_heading":drug_indication["mesh_heading"]
						})
					self.target_data["has_drug_indication"] = True
			
			return self.target_data

	##------------------------------------------------------------------##

	def parse_chembl_protein_targets(self):
		self.parse_chembl_protein_data()
		return {
			"targets":self.target_data["targets"],
			"targets_count":len(self.target_data["targets"])
			}

	##------------------------------------------------------------------##

	def parse_chembl_protein_compounds(self):
		self.parse_chembl_protein_data()
		return {
			"compounds":self.target_data["compounds"],
			"compounds_count":len(self.target_data["compounds"])
			}
	
	##------------------------------------------------------------------##

	def parse_chembl_protein_active_compounds(self):
		self.parse_chembl_protein_data()
		self.target_data["active_compounds"].update(self.target_data["drugs"])
		
		return  {
			"active_compounds":self.target_data["active_compounds"],
			"active_compounds_count":len(self.target_data["active_compounds"]),
			"drugs_count":len(self.target_data["drugs"])
			}

	##------------------------------------------------------------------##

	def parse_chembl_protein_therapeutic_compounds(self):
		self.parse_chembl_protein_data()
		return {
			"drugs":self.target_data["drugs"],
			"drugs_count":len(self.target_data["drugs"])
			}

if __name__ == "__main__":
	chemblDownloaderObj = chemblDownloader()
	chemblDownloaderObj.options['accession'] = [sys.argv[1]]
	chemblDownloaderObj.options['molecule_chembl_id'] = ['CHEMBL189963']

#	pprint.pprint(chemblDownloaderObj.parse_chembl_protein_therapeutic_compounds(),width=200)
#	chemblDownloaderObj.parse_chembl_protein_data()
#	pprint.pprint(chemblDownloaderObj.parse_chembl_protein_compounds(),width=200)

	pprint.pprint(chemblDownloaderObj.parse_chembl_protein_active_compounds(),width=200)
#	chemblDownloaderObj.parse_chembl_protein_targets()
#	pprint.pprint(chemblDownloaderObj.grab_chembl_drug())
#	chemblDownloaderObj.grab_chembl_drug_indication()

	