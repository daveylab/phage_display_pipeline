import sys
import os
import time
import traceback
import pprint
import re
import json
import hashlib
import copy
import inspect


file_path = os.path.dirname(inspect.stack()[0][1])
sys.path.append(os.path.join(file_path,"../"))

import config_reader

remapped_elm_offsets = {
"ELMI002473": {"Start":2987,"End":2991},
"ELMI002474": {"Start":3012,"End":3016},
'ELMI000104': {'Start': 1615, 'End': 1617},
'ELMI002814': {'Start': 327, 'End': 336},
'ELMI000932': {'Start': 105, 'End': 113},
'ELMI002487': {'Start': 448, 'End': 450},
'ELMI001470': {'Start': 126, 'End': 130},
'ELMI002735': {'Start': 108, 'End': 119},
'ELMI001702': {'Start': 833, 'End': 837},
'ELMI001701': {'Start': 1310, 'End': 1314},
'ELMI001291': {'Start': 1160, 'End': 1174},
'ELMI000082': {'Start': 772, 'End': 776},
'ELMI002209': {'Start': 1523, 'End': 1525},
'ELMI003084': {'Start': 4338, 'End': 4344},
'ELMI000182': {'Start': 712, 'End': 717},
'ELMI001484': {'Start': 1337, 'End': 1341},
'ELMI001485': {'Start': 1337, 'End': 1341},
'ELMI003696': {'Start': 580, 'End': 583},
'ELMI000815': {'Start': 542, 'End': 545},
'ELMI002859': {'Start': 91, 'End': 98},
'ELMI000749': {'Start': 2132, 'End': 2140},
'ELMI001181': {'Start': 439, 'End': 443},
'ELMI002478': {'Start': 1666, 'End': 1670},
}

id_mapping = {
"PF14604":"PF00018",
"PF07653":"PF00018",
"PF12763":"PF00036",
"SM00054":"PF00036",
"PF13499":"PF00036",
"SM00028":"PF00515",
"IPR017986":"PF00400",
"SM00370":"PF00560",
"SM00248":"PF00023",
"IPR006569":"PF04818",
"IPR016024":"PF00514"
}

others = """TRG_Cilium_RVxP_2	Unknown	Unknown	Unknown
LIG_UBA3_1	PF00899	ThiF	ThiF family/Repeat in ubiquitin-activating (UBA) protein
LIG_SUMO_SIM_par_1	PF11976	Rad60-SLD	Ubiquitin-2 like Rad60 SUMO-like/Ubiquitin family
LIG_SUMO_SIM_anti_2	PF11976	Rad60-SLD	Ubiquitin-2 like Rad60 SUMO-like/Ubiquitin family
LIG_OCRL_FandH_1	PF00620	RhoGAP	RhoGAP domain
LIG_GBD_WASP_1	PF00786	PDB	P21-Rho-binding domain
LIG_CAP-Gly_2	PF01302	CAP_GLY	CAP-Gly domain
LIG_APCC_Cbox_2	PF00515	TPR_1	Tetratricopeptide repeat
LIG_APCC_Cbox_1	PF00515	TPR_1	Tetratricopeptide repeat
CLV_PCSK_KEX2_1	PF00082	Peptidase_S8	Subtilase family
CLV_MEL_PAP_1	PF00089	Trypsin	Trypsin
LIG_UBA3_1	"PF00899"	"ThiF"	"ThiF family"
LIG_GBD_Chelix_1	"PF00786"	"PBD"	"P21-Rho-binding domain"
LIG_Vh1_VBS_1	"PF01044"	"Vinculin"	"Vinculin family"
MOD_Plk_4	"PF00069"	"Pkinase"	"Protein kinase domain"
MOD_Plk_1	"PF00069"	"Pkinase"	"Protein kinase domain"
DOC_CYCLIN_RxL_1	'PF00134"	"Cyclin_N"	Cyclin, N-terminal domain
MOD_Plk_2-3	"PF00069"       "Pkinase"       "Protein kinase domain"
LIG_PROFILIN_1	"PF00235"	"Profilin"	"Profilin"
DEG_Nend_Nbox_1	"PF02207"	"zf-UBR"	"Putative zinc finger in N-recognin (UBR box)"""

sys.path.append(os.path.join(file_path,"../utilities/"))
import utilities_downloader

from uniprotDownloader import uniprotDownloader

sys.path.append(os.path.join(file_path,"../motif/"))
import alignPeptidesByMSA
import alignPeptidesBySLiMFinder

class elmDownloader():

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def __init__(self):

		self.options = {
			"debug": False,
			'aligned_type':'peptides_padded_pssm_aligned',
			"verbose":False,
			'peptide_alignment_method':"slimfinder", # slimfinder | msa
			'output_type':"output_type"
			}

		self.options.update(config_reader.load_configeration_options(sections=["general"]))

		if 'aligned_type' not in self.options:
			self.options['aligned_type'] = "peptides_aligned"

		if 'max_gap_proportion' not in self.options:
			self.options['max_gap_proportion'] = 1
		else:
			self.options['max_gap_proportion'] = float(self.options['max_gap_proportion'])

		if 'flanks' in self.options:
			self.options['flanks'] = int(self.options['flanks'])
		else:
			self.options['flanks'] = 0

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def check_directory(self):
		if not os.path.exists(os.path.join(self.options["data_path"], "elm")):
			os.mkdir(os.path.join(self.options["data_path"], "elm"))

		if not os.path.exists(os.path.join(self.options["data_path"], "elm", "alignments")):
			os.mkdir(os.path.join(self.options["data_path"], "elm", "alignments"))

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def parseTDT(self,tsv_path,tdt_key):
		tdt_data = {}
		headers = []

		for line in open(tsv_path).read().strip().split("\n"):
			if len(line) > 0:

				lineBits = line.strip().replace('"','').split("\t")
				if line[0] == "#" or len(lineBits) <= 3:
					pass
				else:
					if headers == []:
						headers = lineBits
					else:
						try:
							if lineBits[headers.index(tdt_key)] not in tdt_data:
								tdt_data[lineBits[headers.index(tdt_key)]]= []

							tmp_dict = {}
							for i in range(0,len(headers)):
								if len(lineBits) > i:
									tmp_dict[headers[i]] = lineBits[i]
								else:
									tmp_dict[headers[i]] = "?"

							tdt_data[lineBits[headers.index(tdt_key)]].append(tmp_dict)

						except Exception as e:
							if self.options['verbose']:print(("skipping line",lineBits))
							print(e)

		return tdt_data

	#############################################################################
	#############################################################################
	#############################################################################
	#############################################################################
	#############################################################################

	def grabELMDomains(self):
		self.check_directory()

		out_path = os.path.join(self.options["data_path"],'elm','elm_domains.tsv')

		if os.path.exists(out_path):
			if (time.time() - os.path.getctime(out_path))/60/60/24 >  self.options["remake_age"]:
				os.remove(out_path)

		if not os.path.exists(out_path):
			url = 'http://elm.eu.org/infos/browse_elm_interactiondomains.tsv'

			sessionDownloaderObj = utilities_downloader.sessionDownloader()
			status = sessionDownloaderObj.download_file(url,out_path,method="GET",replace_empty=True)

			try:
				file_content = open(out_path).read().split("\n")

				version = file_content[0].split(':')[-1].strip()
				file_content = [x for x in file_content if not x.startswith('#')]

				file_content = '\n'.join(file_content)
				for mapping in id_mapping:
					file_content = file_content.replace(mapping,id_mapping[mapping])

				open(out_path,'w').write(file_content + "\n" + others )

			except Exception as e:
				print(e)

	#############################################################################
	#############################################################################
	#############################################################################
	#############################################################################
	#############################################################################

	def grabELMCandidates(self):
		self.check_directory()

		out_path = os.path.join(self.options["data_path"],'elm','elm_candidates.html')

		if os.path.exists(out_path):
			if (time.time() - os.path.getctime(out_path))/60/60/24 >  self.options["remake_age"]:
				os.remove(out_path)

		if not os.path.exists(out_path):
			url = 'http://elm.eu.org/elms/candidates'

			sessionDownloaderObj = utilities_downloader.sessionDownloader()
			status = sessionDownloaderObj.download_file(url,out_path,method="GET",replace_empty=True)

			try:
				file_content = open(out_path).read().split('\n')
				version = file_content[0].split(':')[-1].strip()
				file_content = [x for x in file_content if not x.startswith('#')]

				open(out_path,'w').write('\n'.join(file_content))

			except Exception as e:
				print(e)

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def parse_candidates(self):

		self.grabELMCandidates()

		from bs4 import BeautifulSoup

		html_path = os.path.join(self.options["data_path"],'elm','elm_candidates.html')
		out_json_path = os.path.join(self.options["data_path"],'elm','elm_candidates.json')
		out_tdt_path = os.path.join(self.options["data_path"],'elm','elm_candidates.tdt')
		out_pmid_tdt_path = os.path.join(self.options["data_path"],'elm','elm_candidates.pmid.tdt')

		if not os.path.exists(out_json_path):
			#########################################

			html = open(html_path).read()

			elm_pattern = '/elms/'
			pubmed_pattern = '"http://www.ncbi.nlm.nih.gov/pubmed/[0-9]+"'
			pdb_pattern = '"http://www.rcsb.org/pdb/cgi/explore.cgi\?pdbId\=[A-Z0-9]+"'

			soup = BeautifulSoup(html,features="lxml")

			candidates_table = soup.findAll("tbody")[0]

			rows = candidates_table.find_all('tr')
			data = []
			motif_classes = {}


			candidates_mapping_str = ""
			candidates_str = "\t".join(["motif_class","pmids","pdbs","description","notes"]) + "\n"

			#########################################

			for row in rows:
				cols = row.find_all('td')
				if len(cols) > 3:
					if cols[5].text.strip() in ["undergoing annotation","annotatable","first draft"]:
						motif_class = cols[0].text.upper()

						if motif_class in ["DOC_PP2B_PxIxI_2","LIG_SH2_SRC11","LIG_SH2_SRC1","LIG_SH2_SRC3","LIG_CDC20_ABBA_1"]: continue

						description = re.sub(' +', ' ',cols[3].text.replace("\n","").strip())
						notes = re.sub(' +', ' ',cols[4].text.replace("\n","").strip())


						motif_text_html = str(cols[2]) + str(cols[3])  + str(cols[4])
						pmids =[x.strip('"').split("/")[-1] for x in re.findall(pubmed_pattern, motif_text_html, flags=re.DOTALL)]
						pdbs = [x.strip('"').split("=")[-1] for x in re.findall(pdb_pattern, motif_text_html, flags=re.DOTALL)]

						for pmid in pmids:
							candidates_mapping_str += motif_class + "*\t" + pmid + "\n"
							candidates_mapping_str += "Novel Class\t" + pmid + "\n"

						motif_classes[motif_class] = {"re":cols[1].text,"pmids":pmids,"pdbs":pdbs,"description":description,"notes":notes}
						candidates_str += "\t".join([motif_class,",".join(pmids),",".join(pdbs),description,notes]) + "\n"

			#########################################

			open(out_pmid_tdt_path,"w").write(candidates_mapping_str.encode('utf-8').strip())
			open(out_tdt_path,"w").write(candidates_str.encode('utf-8').strip())

			with open(out_json_path, 'w') as outfile:
				json.dump(motif_classes, outfile)

			return motif_classes
		else:
			with open(out_json_path) as outfile:
				motif_classes = json.load(outfile)

			return motif_classes

	#############################################################################
	#############################################################################
	#############################################################################
	#############################################################################
	#############################################################################

	def grabELMInstances(self):
		self.check_directory()

		out_path = os.path.join(self.options["data_path"],'elm','elm_instances.tsv')

		if os.path.exists(out_path):
			if (time.time() - os.path.getctime(out_path))/60/60/24 >  self.options["remake_age"]:
				os.remove(out_path)


		if not os.path.exists(out_path):
			url = 'http://elm.eu.org/instances.tsv?q=*'

			dataDownloaderObj = uniprotDownloader()
			dataDownloaderObj.options["data_path"] = self.options["data_path"]

			sessionDownloaderObj = utilities_downloader.sessionDownloader()
			status = sessionDownloaderObj.download_file(url,out_path,method="GET",replace_empty=True)

			try:
				file_content = open(out_path).read().split('\n')
				version = file_content[0].split(':')[-1].strip()
				file_content = [x for x in file_content if not x.startswith('#')]

				file_content_str = file_content[0].strip() + '\tsequence\tprotein_name\tgene_name\tfamily\tspecies_common\ttaxonomy\ttaxon_id\n'

				for line in file_content[1:]:
					if len(line.split("\t")) > 1:
						accession = line.split("\t")[4].strip('"')
						start =  line.split("\t")[6].strip('"')
						end =  line.split("\t")[7].strip('"')
						print("instance", accession, start, end)

						protein_data = dataDownloaderObj.parseUniProt(accession)
						protein_sequence = dataDownloaderObj.parseUniProtFasta(accession)

						if "sequence" not in protein_sequence:
							pass
						else:

							file_content_str += line.strip()

							extras = []
							extras.append(protein_sequence["sequence"])

							if 'data' in protein_data:
								extras.append(protein_data['data']['protein_name'])
								extras.append(protein_data['data']['gene_name'])
								extras.append(protein_data['data']['family'])
								extras.append(protein_data['data']['species_common'])
								extras.append(protein_data['data']['taxonomy'])
								extras.append(protein_data['data']['taxon_id'])
							else:
								extras += ["","","","","",""]

							file_content_str += "\t" + "\t".join(extras)

							file_content_str += "\n"

				open(out_path,'w').write(file_content_str)

			except Exception as e:
				print("error @ grabELMInstances", e)
				raise

	#############################################################################
	#############################################################################
	#############################################################################
	#############################################################################
	#############################################################################

	def grabELMInteractions(self):
		self.check_directory()

		out_path = os.path.join(self.options["data_path"],'elm','elm_interactions_raw.tsv')

		if os.path.exists(out_path):
			if (time.time() - os.path.getctime(out_path))/60/60/24 >  self.options["remake_age"]:
				os.remove(out_path)

		if not os.path.exists(out_path):
			url = 'http://elm.eu.org/interactions/as_tsv'
			print(("Grabbing",url))

			sessionDownloaderObj = utilities_downloader.sessionDownloader()
			status = sessionDownloaderObj.download_file(url,out_path,method="GET",replace_empty=True)

			try:
				file_content = open(out_path).read().split('\n')
				version = file_content[0].split(':')[-1].strip()
				file_content = [x for x in file_content if not x.startswith('#')]

				open(out_path,'w').write('\n'.join(file_content))

			except Exception as e:
				print(e)

		self.extendELMInteractions()

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def extendELMInteractions(self):
		in_path = os.path.join(self.options["data_path"],'elm','elm_interactions_raw.tsv')
		out_path = os.path.join(self.options["data_path"],'elm','elm_interactions.tsv')

		if os.path.exists(out_path):
			if (time.time() - os.path.getctime(out_path))/60/60/24 >  self.options["remake_age"]:
				os.remove(out_path)

		extended_headers = ['proteinNameDomain',
		'proteinNameElm',
		'geneNameDomain',
		'geneNameElm',
		'speciesCommonDomain',
		'speciesCommonElm',
		'speciesDomain',
		'speciesElm',
		'taxonIdentifierDomain',
		'taxonIdentifierElm'
		]

		if os.path.exists(in_path) and not os.path.exists(out_path):
			dataDownloaderObj = uniprotDownloader()
			dataDownloaderObj.options["data_path"] = self.options["data_path"]

			lines = open(in_path).read().split("\n")
			headers = lines[0].strip().split("\t")
			rows = []

			for line in lines[1:]:
				line_bits = line.strip().split("\t")
				line_dict = {}

				try:
					if len(line_bits) == len(headers):
						for i in range(0,len(headers)):
							line_dict[headers[i]] = line_bits[i]

						domain_protein_data = dataDownloaderObj.parseUniProt(line_dict['interactorDomain'])
						elm_protein_data = dataDownloaderObj.parseUniProt(line_dict['interactorElm'])

						line_dict['proteinNameDomain'] = domain_protein_data['data']['protein_name']
						line_dict['proteinNameElm'] = elm_protein_data['data']['protein_name']
						line_dict['geneNameDomain'] = domain_protein_data['data']['gene_name']
						line_dict['geneNameElm'] = elm_protein_data['data']['gene_name']
						line_dict['speciesCommonDomain'] = domain_protein_data['data']['species_common']
						line_dict['speciesCommonElm'] = elm_protein_data['data']['species_common']
						line_dict['speciesDomain'] =  domain_protein_data['data']['species_scientific']
						line_dict['speciesElm'] =  elm_protein_data['data']['species_scientific']
						line_dict['taxonIdentifierDomain'] = domain_protein_data['data']['taxon_id']
						line_dict['taxonIdentifierElm'] = elm_protein_data['data']['taxon_id']

						row = []
						for header in headers + extended_headers:
							row.append(line_dict[header])

						rows.append("\t".join(row))
				except:
					print("Skipping interaction",line_dict['interactorDomain'],line_dict['interactorElm'],self.options["data_path"])

			out_str = "\t".join(headers + extended_headers) + "\n" + "\n".join(rows)

			open(out_path,"w").write(out_str)

	#############################################################################
	#############################################################################
	#############################################################################
	#############################################################################
	#############################################################################

	def grabELMClasses(self):
		self.check_directory()

		self.grabELMInstances()
		tsv_path = os.path.join(self.options["data_path"],"elm","elm_instances.tsv")
		elm_instances = self.parseTDT(tsv_path,"ELMIdentifier")

		out_path = os.path.join(self.options["data_path"],'elm','elm_classes.tsv')

		if os.path.exists(out_path):
			if (time.time() - os.path.getctime(out_path))/60/60/24 >  self.options["remake_age"]:
				os.remove(out_path)

		if not os.path.exists(out_path):
			url = 'http://elm.eu.org/elms/elms_index.tsv'
			sessionDownloaderObj = utilities_downloader.sessionDownloader()
			status = sessionDownloaderObj.download_file(url,out_path,method="GET",replace_empty=True)

			try:
				file_content = open(out_path).read().strip().split('\n')

				version = file_content[0].split(':')[-1].strip()
				file_content = [x.strip() for x in file_content if not x.startswith('#')]

				file_content_with_additions = [file_content[0] + '\t"Flexible_Length"']

				for line in file_content[1:]:
					line_bits = line.split("\t")
					ELMIdentifier = line_bits[1].strip('"')

					lengths = []

					if ELMIdentifier in elm_instances:
						for instance in elm_instances[ELMIdentifier]:
							lengths.append(int(instance['End']) - int(instance['Start']))

					line = "\t".join(line_bits) + "\t" + '"' + str(len(set(lengths)) > 1) + '"'
					file_content_with_additions.append(line)

				open(out_path,'w').write('\n'.join(file_content_with_additions))

			except Exception as e:
				print(e)
				raise


	#############################################################################
	#############################################################################
	#############################################################################
	#############################################################################
	#############################################################################

	def getAlignedELMClasses(self):

		self.check_directory()

		try:
			peptides = self.get_elm_class_peptides()
		except:
			print("Failed reading peptides")

		aligned_peptides = {}

		if self.options['identifier_type'] == "elm" and self.options['identifier'] == "all":
			elm_classes = list(peptides.keys())
		else:
			elm_classes = [self.options['identifier'] ]

		for elm_class in elm_classes:
			peptide_data = peptides[elm_class]
			response = self.getAlignedELMClass(elm_class,peptide_data)

			try:
				if self.options['format'] == "details":
					aligned_peptides[elm_class] = response
				else:
					aligned_peptides[elm_class] = {
						"aligned_peptides":response["aligned_peptides"],
						"aligned":response["aligned"],
						"aligned_by":response["aligned_by"]
					}
			except:
				pass

		return aligned_peptides

	def getAlignedELMClass(self,elm_class,peptide_data):
		try:
			forward_dict = {}
			reverse_dict = {}

			for accession in peptide_data:
				peptide = peptide_data[accession]

				forward_dict[accession] = peptide

				if peptide not in reverse_dict:
					reverse_dict[peptide] = []

				reverse_dict[peptide].append(accession)

			peptides = list(set(forward_dict.values()))
			peptides.sort()

			alignment_dict = {
				"peptides":peptides,
				"aligned_peptides":peptides,
				"aligned_peptides_by_motif":[],
				"aligned_peptides_by_motif_pssm":[],
				"aligned_peptides_by_msa":[],
				"motif_enrichment_tool":"",
				"msa_tool":"",
				"aligned":False,
				"aligned_by":"input",
				"aligned_by_tool":"",
				"motif":{},
				"motif_re":""
			}

			hash = str(hashlib.md5(",".join(peptides).encode()).hexdigest())
			elm_class_alignment_path = os.path.join(self.options["data_path"],"elm","alignments",elm_class +  "." + hash + ".alignment.json")

			if os.path.exists(elm_class_alignment_path) and not self.options['remake']:
				with open(elm_class_alignment_path) as outfile:
					alignment_dict = json.load(outfile)

					if self.options['verbose']:print(("reading:",elm_class_alignment_path))
			else:
				#####################
				####################
				#####################

				if self.options['verbose']:
					print(("#"*30))
					print(("#",elm_class))

				try:
					alignPeptideObj = alignPeptidesBySLiMFinder.alignPeptidesBySLiMFinder()

					alignPeptideObj.setup_paths()
					alignPeptideObj.options["peptides"] = ",".join(peptides)
					alignPeptideObj.options['blast_path'] = self.options["blastplus_path"]
					alignPeptideObj.options["remake"] = False
					alignPeptideObj.options["make_output"] = False
					result =  alignPeptideObj.runAlignPeptides()

					if 'motifs' in alignPeptideObj.data:
						if len(alignPeptideObj.data['motifs']) > 0:

							if float(alignPeptideObj.data['motifs'][1]["sig"]) < 0.0001 and elm_class not in ["DOC_CyclinA_RxL_1","LIG_SUMO_SIM_anti_2"]:

								aligned_by_motif = []
								aligned_by_pssm = []

								for peptide in alignPeptideObj.aligned_peptide_data['aligned_by_motif']:
									aligned_by_motif.append(alignPeptideObj.aligned_peptide_data['aligned_by_motif'][peptide]['aligned_peptide'])

								for peptide in alignPeptideObj.aligned_peptide_data['aligned_by_pssm']:
									aligned_by_pssm.append(alignPeptideObj.aligned_peptide_data['aligned_by_pssm'][peptide]['aligned_peptide'])

								alignment_dict["aligned_peptides"] = alignPeptideObj.aligned_peptide_data['peptides']
								alignment_dict["aligned_peptides_by_motif"] = aligned_by_motif
								alignment_dict["aligned_peptides_by_motif_pssm"] = aligned_by_pssm
								alignment_dict["aligned"] = True
								alignment_dict["motif_enrichment_tool"] = "SLiMFinder"
								alignment_dict["aligned_by"] = "SLiMFinder+pssm"
								alignment_dict["motif"] = alignPeptideObj.data['motifs'][1]
								alignment_dict["motif_re"] = alignment_dict["motif"]["motif"]

							else:
								print(("Skipping - Poor Motif Enrichment -",elm_class,alignPeptideObj.data['motifs'][1]["sig"],alignPeptideObj.data['motifs'][1]["motif"]))
				except:
					print(("SLiMFinder Failed",elm_class))

				#####################
				#####################
				#####################

				try:
					msa_options = {
					"tasks": "get_elm_instances",
					"format": "peptides",
					"aligned_type":"peptides",
					"identifier_type": "elm",
					"flanks": 0,
					"alignmentAlgorithm":"clustalw",
					"identifier": elm_class,
					"peptides":peptides
					}

					alignPeptideObj = alignPeptidesByMSA.alignPeptidesByMSA()
					alignPeptideObj.setup_paths()
					alignPeptideObj.options.update(msa_options)
					f = alignPeptideObj.runAlignPeptides()

					if not alignment_dict["aligned"]:
						alignment_dict["aligned_peptides"] = list(alignPeptideObj.data['aligned_peptides'].values())
						alignment_dict["aligned_by"] = "clustalw+pssm"
						alignment_dict["aligned"] = True

					alignment_dict["aligned_peptides_by_msa"] = list(alignPeptideObj.data['aligned_peptides'].values())
					alignment_dict["msa_tool"] = "clustalw"

				except:
					print(("MSA Failed",elm_class))

				if self.options['verbose']:
					print(("#",alignment_dict["aligned_by"]))
					print(("#",alignment_dict["motif_re"]))
					print(("\n".join(alignment_dict["aligned_peptides"])))
					print(("-"*30))

				with open(elm_class_alignment_path, 'w') as outfile:
					json.dump(alignment_dict, outfile)

					if self.options['verbose']: print(("writing:",elm_class_alignment_path))

			return alignment_dict
		except Exception as e:
			print(e)

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def parseELMClasses(self):
		self.grabELMClasses()

		tsv_path = os.path.join(self.options["data_path"],"elm","elm_classes.tsv")
		classes = self.parseTDT(tsv_path,"ELMIdentifier")

		return classes

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def parseELMDomains(self,key="Interaction Domain Id"):
		self.grabELMDomains()

		tsv_path = os.path.join(self.options["data_path"],"elm","elm_domains.tsv")
		domains = self.parseTDT(tsv_path,key)

		return domains

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def parseELM(self):

		self.grabELMInstances()
		self.grabELMClasses()
		self.grabELMDomains()
		self.grabELMInteractions()

		try:
			tsv_path = os.path.join(self.options["data_path"],"elm","elm_instances.tsv")

			instances = self.parseTDT(tsv_path,"Accession")
			tsv_path = os.path.join(self.options["data_path"],"elm","elm_classes.tsv")
			classes = self.parseTDT(tsv_path,"ELMIdentifier")

			tsv_path = os.path.join(self.options["data_path"],"elm","elm_domains.tsv")
			domains = self.parseTDT(tsv_path,"ELM identifier")

			interactions = self.getInteractions(instances)

			elm_data = {}
			for instance in instances:
				try:
					if instances[instance][0]["Primary_Acc"] not in elm_data:
						elm_data[instances[instance][0]["Primary_Acc"]] = {}

					if instance in remapped_elm_offsets:
						instances[instance][0]['Start'] = remapped_elm_offsets[instance]["Start"]
						instances[instance][0]['End'] = remapped_elm_offsets[instance]["End"]

					instances[instance][0]["binding_domains"] = {}
					instances[instance][0]["binding_domain_id"] = []
					instances[instance][0]["binding_domain_name"] = []
					instances[instance][0]["binding_domain_description"] = []
					
					if instances[instance][0]["ELMIdentifier"] not in ["LIG_KEPE_1","LIG_KEPE_2","LIG_KEPE_3"]:
						try:
							for domain in domains[instances[instance][0]["ELMIdentifier"]]:
								if domain['Interaction Domain Id'] in id_mapping:
									instances[instance][0]["binding_domain_id"].append(id_mapping[domain['Interaction Domain Id']])
									instances[instance][0]["binding_domain_name"].append(domain['Interaction Domain Name'])
									instances[instance][0]["binding_domain_description"].append(domain['Interaction Domain Description'])

									instances[instance][0]["binding_domains"][id_mapping[domain['Interaction Domain Id']]] = {
											"binding_domain_shortname":domain['Interaction Domain Description'],
											"binding_domain_longname":domain['Interaction Domain Name']
									}
								else:
									instances[instance][0]["binding_domain_id"].append(domain['Interaction Domain Id'])
									instances[instance][0]["binding_domain_name"].append(domain['Interaction Domain Name'])
									instances[instance][0]["binding_domain_description"].append(domain['Interaction Domain Description'])

									instances[instance][0]["binding_domains"][domain['Interaction Domain Id']] = {
											"binding_domain_shortname":domain['Interaction Domain Description'],
											"binding_domain_longname":domain['Interaction Domain Name']
									}
						except:
							instances[instance][0]["binding_domain_id"] =  "Unknown"
							instances[instance][0]["binding_domain_name"] =  "Unknown"
							instances[instance][0]["binding_domain_description"] = "Unknown"

						if 'sequence' not in instances[instance][0]:

							elm_data[instances[instance][0]["Primary_Acc"]][instance] = instances[instance][0]


							if instance in interactions:
								elm_data[instances[instance][0]["Primary_Acc"]][instance]["interactions"] = interactions[instance]
							else:
								elm_data[instances[instance][0]["Primary_Acc"]][instance]["interactions"] = []
							continue

						pattern = re.compile(classes[instances[instance][0]["ELMIdentifier"]][0]['Regex'])
						peptide = instances[instance][0]['sequence'][int(instances[instance][0]['Start'])-1:int(instances[instance][0]['End'])]

						if len(pattern.findall(peptide)) == 0:
							#if self.options["verbose"]:
							#print instances[instance][0]['sequence']

							closest_match = [None,None,None]
							pattern_iter = pattern.finditer(instances[instance][0]['sequence'])
							pattern_iter_counter = 0

							for m in pattern_iter:
								pattern_iter_counter += 1
								match_distance = abs(int(instances[instance][0]['Start']) - int(m.start()))

								if closest_match[1] == None:
									closest_match = [m.start(),match_distance,m.group()]
								else:
									if match_distance < closest_match[1]:
										closest_match = [m.start(),match_distance,m.group()]

							new_start = "-"
							new_end =  "-"
							match_distance = "-"
							match_peptide = "-"

							if closest_match[0] != None:
								new_start = closest_match[0] + 1
								new_end = closest_match[0] + len(closest_match[2])
								match_distance = closest_match[1]
								match_peptide = closest_match[2]

							"""
							print {instance:{"Start":new_start,"End":new_end}},

							print "Incorrect Mapping UniProt","\t",
							print instance,instances[instance][0]["Primary_Acc"],"\t",
							print instances[instance][0]['Start'],"\t",
							print instances[instance][0]['End'],"\t",

							print match_distance,"\t",
							print pattern_iter_counter,"\t",
							print peptide,"\t",
							print match_peptide,"\t",
							print instances[instance][0]['ELMIdentifier'],"\t",
							print classes[instances[instance][0]["ELMIdentifier"]][0]['Regex']
							"""

						else:
							elm_data[instances[instance][0]["Primary_Acc"]][instance] = instances[instance][0]


							if instance in interactions:
								elm_data[instances[instance][0]["Primary_Acc"]][instance]["interactions"] = interactions[instance]
							else:
								elm_data[instances[instance][0]["Primary_Acc"]][instance]["interactions"] = []



				except Exception as e:
					print(e)
					if self.options['verbose']:print(("Skipping instance",instance))
					#raise

			return {"status":"Found","data":elm_data}

		except Exception as e:
			if self.options["debug"]:
				raise

			return {"status":"Error","error_type":str(e)}

	####################################################################################################
	####################################################################################################
	####################################################################################################
	####################################################################################################
	####################################################################################################
	####################################################################################################

	def get_elm_classes(self):
		try:
			elm_classes = self.parseELMClasses()
			json_data = {}

			if self.options['identifier'] == "all":
				if self.options['output_type'] == "details":
					json_data = elm_classes
				if self.options['output_type'] == "identifiers":
					json_data = list(elm_classes.keys())
				if self.options['output_type'] == "names":
					json_data = {}
					for elm_class in elm_classes:
						json_data[elm_class] = elm_classes[elm_class][0]["FunctionalSiteName"]
			else:
				if self.options['identifier'] in elm_classes:
					json_data = elm_classes[self.options['identifier']]

			if len(json_data) > 0:
				return {"status": "Found", "data": json_data}
			else:
				return {"status": "Error", "error_type": "Entry not found - " + self.options['identifier'] + ":" + self.options['output_type']}

		except Exception as e:
			raise
			return {"status": "Error", "error_type": e}



	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def get_elm_class_description(self):

		description_json_path = os.path.join(self.options["data_path"],"elm","descriptions","description.json")

		with open(description_json_path) as outfile:
			description_json = json.load(outfile)

		return description_json

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def get_protein_list(self):

		elm_data = self.parseELM()
		elm_data = elm_data['data']

		protein_list = []
		for protein in elm_data:
			for instance in elm_data[protein]:
				protein_list.append(elm_data[protein][instance]["Primary_Acc"] )

		return {"status": "Found", "data": list(set(protein_list))}

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def get_protein_dict(self):

		elm_data = self.parseELM()
		elm_data = elm_data['data']

		return {"status": "Found", "data": elm_data}

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def get_domain_list(self):

		elm_data = self.parseELM()
		elm_data = elm_data['data']

		domain_list = []

		for protein in elm_data:
			for instance in elm_data[protein]:
				domain_list += elm_data[protein][instance]["binding_domain_id"] 

		return {"status": "Found", "data": list(set(domain_list))}

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def getInteractions(self,instances):
		tsv_path = os.path.join(self.options["data_path"],"elm","elm_interactions.tsv")

		interactions = self.parseTDT(tsv_path,"Elm")

		interaction_dict = {}
		interaction_dict_tmp = {}

		for elm_class in interactions:
			for interaction in interactions[elm_class]:
				interaction_list = [elm_class]
				for val in ['interactorElm','StartElm','StopElm']:
					interaction_list.append(interaction[val])

				if "_".join(interaction_list) not in interaction_dict_tmp:
					interaction_dict_tmp["_".join(interaction_list)] = []

				interaction_dict_tmp["_".join(interaction_list)].append(interaction)

		for instance in instances:
			try:
				tmp_id = "_".join([ instances[instance][0]['ELMIdentifier'], instances[instance][0]['Primary_Acc'], instances[instance][0]['Start'], instances[instance][0]['End']])
				if tmp_id in interaction_dict_tmp:
					if instance not in interaction_dict:
						interaction_dict[instance] = []

					interaction_dict[instance] += interaction_dict_tmp[tmp_id]
			except:
				print(("skipping instance",instance))

		return interaction_dict

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def get_interactions_list(self):

		elm_data = self.parseELM()
		elm_data = elm_data['data']

		interactions_list = {}

		for protein in elm_data:
			for instance in elm_data[protein]:
				if len(elm_data[protein][instance]['interactions']) > 0:
					domain = elm_data[protein][instance]['interactions'][0]['Domain']
					domain_protein = elm_data[protein][instance]['interactions'][0]['interactorDomain']
					elm_protein = elm_data[protein][instance]['interactions'][0]['interactorElm']

					if domain not in interactions_list:
						interactions_list[domain] = {}

					if domain_protein not in interactions_list[domain]:
						interactions_list[domain][domain_protein] = {}

					if elm_protein not in interactions_list[domain][domain_protein]:
						interactions_list[domain][domain_protein][elm_protein] = []

					elm_data[protein][instance]['interactions'][0]['elm_protein_name'] = elm_data[protein][instance]['protein_name']
					elm_data[protein][instance]['interactions'][0]['elm_instance'] = instance
					elm_data[protein][instance]['interactions'][0]['elm_gene_name'] = elm_data[protein][instance]['gene_name']

					interactions_list[domain][domain_protein][elm_protein].append(elm_data[protein][instance]['interactions'])

		return {"status": "Found", "data": interactions_list}

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def get_domain_dict(self):

		elm_data = self.parseELM()
		elm_data = elm_data['data']

		domain_dict = {}

		for protein in elm_data:
			for instance in elm_data[protein]:
				for binding_domain_description in elm_data[protein][instance]["binding_domain_description"]:
					if binding_domain_description not in domain_dict:
						domain_dict[binding_domain_description] = {}
						
					if elm_data[protein][instance]['ELMIdentifier'] not in domain_dict[binding_domain_description]:
					
						domain_dict[binding_domain_description][elm_data[protein][instance]['ELMIdentifier']] = []

					for interactor_accession in elm_data[protein][instance]["interactions"]:
						if interactor_accession['interactorDomain'] not in domain_dict[binding_domain_description][elm_data[protein][instance]['ELMIdentifier']]:
							domain_dict[binding_domain_description][elm_data[protein][instance]['ELMIdentifier']].append(interactor_accession['interactorDomain'])

		return {"status": "Found", "data": domain_dict}

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def get_pubmed_protein_mapping_dict(self,query_pmid=None):

		if query_pmid == None:
			query_pmid = self.options['pmid']

		elm_data = self.parseELM()
		elm_data = elm_data['data']

		pubmed_protein_mapping_dict = {}

		for protein in elm_data:
			for instance in elm_data[protein]:

				uniprot_accessions = [protein]
				if len(elm_data[protein][instance]['interactions']) > 0:
					domain_protein = elm_data[protein][instance]['interactions'][0]['interactorDomain']
					uniprot_accessions.append(domain_protein)

				pmids = elm_data[protein][instance]['References']

				for pmid in pmids.split():
					if pmid not in pubmed_protein_mapping_dict:
						pubmed_protein_mapping_dict[pmid] = {}

					for uniprot_accession in uniprot_accessions:
						if uniprot_accession not in pubmed_protein_mapping_dict[pmid]:
							pubmed_protein_mapping_dict[pmid][uniprot_accession] = {}

						pubmed_protein_mapping_dict[pmid][uniprot_accession][instance] = elm_data[protein][instance]['ELMIdentifier']

		if query_pmid != None:
			if query_pmid in pubmed_protein_mapping_dict:
				return pubmed_protein_mapping_dict[query_pmid]
			else:
				return {}
		else:
			return pubmed_protein_mapping_dict

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def get_pubmed_elm_class_mapping_dict(self,query_pmid=None):

		if query_pmid == None:
			query_pmid = self.options['pmid']

		elm_data = self.parseELM()
		elm_data = elm_data['data']

		pubmed_elmclass_mapping_dict = {}

		for protein in elm_data:
			for instance in elm_data[protein]:

				uniprot_accessions = [protein]
				if len(elm_data[protein][instance]['interactions']) > 0:
					domain_protein = elm_data[protein][instance]['interactions'][0]['interactorDomain']
					uniprot_accessions.append(domain_protein)

				pmids = elm_data[protein][instance]['References']

				for pmid in pmids.split():
					if pmid not in pubmed_elmclass_mapping_dict:
						pubmed_elmclass_mapping_dict[pmid] = []

					if elm_data[protein][instance]['ELMIdentifier'] not in pubmed_elmclass_mapping_dict[pmid]:
						pubmed_elmclass_mapping_dict[pmid].append(elm_data[protein][instance]['ELMIdentifier'])

		if query_pmid != None:
			if query_pmid in pubmed_elmclass_mapping_dict:
				return pubmed_elmclass_mapping_dict[query_pmid]
			else:
				return {}
		else:
			return pubmed_elmclass_mapping_dict

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def get_elm_class_peptides(self):

		elm_protein_instances = self.parseELM()
		elm_peptides = {}

		for protein in list(elm_protein_instances['data'].keys()):
			for elm_instance_id in elm_protein_instances['data'][protein]:

				elmIdentifier = elm_protein_instances['data'][protein][elm_instance_id]['ELMIdentifier']

				if elmIdentifier == self.options['identifier'] or self.options['identifier'] == "all":
					motif_start = max(0,int(elm_protein_instances['data'][protein][elm_instance_id]['Start']) - (self.options['flanks'] + 1))
					motif_end = int(elm_protein_instances['data'][protein][elm_instance_id]['End']) + (self.options['flanks'])

					peptide = elm_protein_instances['data'][protein][elm_instance_id]['sequence'][motif_start:motif_end]

					if elmIdentifier not in elm_peptides:
						elm_peptides[elmIdentifier] = {}

					if peptide != "":
						elm_peptides[elmIdentifier][elm_instance_id] = peptide
					else:
						print(("Peptide error",motif_start,motif_end,elmIdentifier,elm_instance_id))

		return elm_peptides

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def get_instances(self):
		try:
			elm_data = self.parseELM()

			elm_data = elm_data['data']

			#-------------------

			peptides = {}
			json_data = {}
			aligned_data = {}
			protein_list = []

			if self.options['identifier_type'] == "elm" and self.options['identifier'] != "all":
				aligned_data = self.getAlignedELMClasses()

			use_instances = []

			for protein in elm_data:
				for instance in elm_data[protein]:

					use = False
					if self.options['identifier_type'] == "uniprot":
						if elm_data[protein][instance]["Primary_Acc"] in self.options['identifier'].split(",") or self.options['identifier'] == "all":
							use_instances.append(instance)
							protein_list.append(elm_data[protein][instance]["Primary_Acc"] )

					if self.options['identifier_type'] == "elm":
						if elm_data[protein][instance]['ELMIdentifier'] == self.options['identifier'] or self.options['identifier'] == "all":
							use_instances.append(instance)

					if self.options['identifier_type'] == "organism":
						if elm_data[protein][instance]['Organism'] == self.options['identifier'] or self.options['identifier'] == "all":
							use_instances.append(instance)

					if self.options['identifier_type'] == "pfam":
						if elm_data[protein][instance]["binding_domain_id"] == self.options['identifier'] or self.options['identifier'] == "all":
							use_instances.append(instance)

			#-------------------
			#-------------------

			flanks_lengths = {}
			for aligned_type in ['peptides_aligned','peptides_padded_aligned','peptides_pssm_aligned','peptides_padded_pssm_aligned']:
				flanks_lengths[aligned_type] = {"right":{},"left":{}}

			#-------------------
			#-------------------

			for protein in elm_data:
				for instance in elm_data[protein]:
					if instance in use_instances:
						start =  int(elm_data[protein][instance]['Start'])
						end =  int(elm_data[protein][instance]['End'])

						l_flank = elm_data[protein][instance]["sequence"][max(0,start- self.options['flanks'] - 1):start-1].rjust(self.options['flanks'],"-")
						peptide = elm_data[protein][instance]["sequence"][start-1:end]
						r_flank = elm_data[protein][instance]["sequence"][end:min(end+self.options['flanks'],len(elm_data[protein][instance]["sequence"]))].ljust(self.options['flanks'],"-")


						elm_data[protein][instance]['l_flank'] = l_flank
						elm_data[protein][instance]['peptide'] = peptide
						elm_data[protein][instance]['extended_peptide'] = l_flank + peptide + r_flank
						elm_data[protein][instance]['r_flank'] = r_flank

						peptides[instance] = elm_data[protein][instance]['extended_peptide']


						if 'peptides_aligned' in aligned_data:
							if instance in aligned_data['peptides_aligned']:
								for aligned_type in ['peptides_aligned','peptides_pssm_aligned']:
									try:
										aligned_peptide = aligned_data[aligned_type][instance]

										left_pad = len(aligned_peptide) - len(aligned_peptide.lstrip("-"))
										right_pad = len(aligned_peptide) - len(aligned_peptide.rstrip("-"))
										left_flank = elm_data[protein][instance]["l_flank"]
										right_flank = elm_data[protein][instance]["r_flank"]

										aligned_padded_peptide = "-"*left_pad + left_flank + aligned_peptide.strip("-") + right_flank + "-"*right_pad
										aligned_padded_peptide = "-"*left_pad + left_flank + aligned_peptide.strip("-") + right_flank + "-"*right_pad

										#if aligned_type == 'peptides_aligned' or 'peptides_aligned' not in elm_data[protein][instance]:
										if aligned_type == 'peptides_aligned':
											elm_data[protein][instance]['peptides_aligned'] = aligned_peptide
											elm_data[protein][instance]['peptides_padded_aligned'] = aligned_padded_peptide

										if aligned_type == 'peptides_pssm_aligned':
											elm_data[protein][instance]['peptides_pssm_aligned'] = aligned_peptide
											elm_data[protein][instance]['peptides_padded_pssm_aligned'] = aligned_padded_peptide

									except:
										raise

						instance_data = copy.deepcopy(elm_data[protein][instance])

						#del instance_data["sequence"]
						json_data[instance] = instance_data
						del instance_data

				#-------------------
			#-------------------
			if 'format' in self.options:
				if self.options['format'] == "peptides" and self.options['aligned_type'] == "peptides":
					return peptides
			#-------------------
			#-------------------

			for instance in json_data:
				for aligned_type in ['peptides_aligned','peptides_padded_aligned','peptides_pssm_aligned','peptides_padded_pssm_aligned']:
					if aligned_type in json_data[instance]:
						aligned_peptide = json_data[instance][aligned_type]
						left_pad = len(aligned_peptide) - len(aligned_peptide.lstrip("-"))
						right_pad = len(aligned_peptide) - len(aligned_peptide.rstrip("-"))

						flanks_lengths[aligned_type]["left"][instance] = left_pad
						flanks_lengths[aligned_type]["right"][instance] = right_pad

			#-------------------
			#-------------------

			columns = {}

			if self.options['identifier_type'] == "elm" and self.options['identifier'] != "all":
				for aligned_type in ['peptides_aligned','peptides_padded_aligned','peptides_pssm_aligned','peptides_padded_pssm_aligned']:

					for instance in json_data:
						try:
							left_flank = min(flanks_lengths[aligned_type]['left'].values())
							right_flank = min(flanks_lengths[aligned_type]['right'].values())

							peptide = json_data[instance][aligned_type]

							json_data[instance][aligned_type] = json_data[instance][aligned_type][left_flank:len(peptide)-right_flank]

							if aligned_type == self.options['aligned_type']:
								for i in range(0,len(json_data[instance][aligned_type])):
									if i not in columns:
										columns[i] = []

									columns[i].append(json_data[instance][aligned_type][i])

								peptides[instance] = json_data[instance][aligned_type]
						except:
							pass#print(("ERROR",instance,"LINE 1140"))

				use_columns = []
				for i in range(0,len(columns)):
					if (1 - float(columns[i].count("-"))/len(columns[i])) > self.options['max_gap_proportion']:
						use_columns.append(i)

				if len(use_columns) == 0:
					use_columns = list(range(0,len(columns)))

				for instance in peptides:
					for i in range(0,len(peptides[instance])):
						try:
							peptides[instance] = json_data[instance][aligned_type][min(use_columns):max(use_columns)+1]
						except:
							pass#print(("MISSED",instance))

			for instance in peptides:
				rgaps = len(peptides[instance]) - len(peptides[instance].rstrip("-"))
				lgaps = len(peptides[instance]) - len(peptides[instance].lstrip("-"))
				peptide_length = len(peptides[instance].replace("-",""))
				offset = json_data[instance]['sequence'].find(peptides[instance].replace("-",""))

				fixed_peptide = json_data[instance]['sequence'][max(0,offset - lgaps):offset + peptide_length + rgaps]
				fixed_start = json_data[instance]['sequence'].find(fixed_peptide)  + 1
				fixed_end = fixed_start + len(fixed_peptide)  -1

				if fixed_start == 1:
					fixed_start = 1
					fixed_end = fixed_start + len(fixed_peptide) -1
					fixed_peptide = fixed_peptide.rjust(len(peptides[instance]),'-')

				if offset + peptide_length >= len(json_data[instance]['sequence']):
					fixed_end = fixed_start + len(fixed_peptide) - 1
					fixed_peptide = fixed_peptide.ljust(len(peptides[instance]),'-')

				if len(fixed_peptide) != len(peptides[instance]):
					fixed_peptide = fixed_peptide.ljust(len(peptides[instance]),'-')

				json_data[instance]['peptides_aligned_fixed'] = fixed_peptide
				json_data[instance]['peptides_aligned_fixed_start'] = fixed_start
				json_data[instance]['peptides_aligned_fixed_end'] = fixed_end

				peptides[instance] = fixed_peptide

			#-------------------
			#-------------------

			if 'format' in self.options:
				if self.options['format'] == "peptides":
					return {"status": "Found", "data": peptides}

				if self.options['format'] == "fasta":
					fasta = ""
					for elm_id in json_data:
						fasta += ">sp|" + json_data[elm_id]['Primary_Acc'] + "|" + json_data[elm_id]['ProteinName'] +  " " +  elm_id + " " + json_data[elm_id]['Start']  + "-" +  json_data[elm_id]['End']  + " OS=" +  json_data[elm_id]['Organism']  + "\n"
						fasta += peptides[elm_id] + "\n"

					return {"status": "Found", "data": fasta}


			#-------------------
			#-------------------

			if self.options['format'] == "curation" or self.options['format'] == "curation_tdt":
				instance_curations = []

				for elm_id in json_data:

					#if json_data[elm_id]['ELMIdentifier'] == "LIG_LYPXL_S_1":
					#	pprint.pprint(json_data[elm_id])


					pmids = json_data[elm_id]['References'].split()

					if len(pmids) == 0:
						pmids = ['']

					for pmid in pmids:
						motif_protein_uniprot_accession = json_data[elm_id]['Primary_Acc']
						motif_protein_name = json_data[elm_id]['protein_name']
						motif_gene_name = json_data[elm_id]['gene_name']
						motif_protein_family = json_data[elm_id]['family']
						motif_protein_species = json_data[elm_id]['Organism']
						motif_start_residue_number = json_data[elm_id]['Start']
						motif_end_residue_number = json_data[elm_id]['End']
						motif_sequence = json_data[elm_id]['extended_peptide']
						motif_taxonomy = json_data[elm_id]['taxonomy']
						motif_taxon_id = json_data[elm_id]['taxon_id']
						motif_protein_species_common = json_data[elm_id]['species_common']
						instance_assessment = json_data[elm_id]['InstanceLogic']
						experimental_method = json_data[elm_id]['Methods']

						domain_pfam_accession = json_data[elm_id]['binding_domain_id']
						domain_pfam_name = json_data[elm_id]['binding_domain_name']
						elm_class = json_data[elm_id]['ELMIdentifier']

						interaction_pdb = ",".join(json_data[elm_id]['PDB'].split())

						interactions = []

						if len(json_data[elm_id]['interactions']) == 0:
							interaction = {
							"domain protein uniprot accession": "",
							"domain protein name": "",
							"domain gene name": "",
							"domain protein family": "",
							"domain protein species": "",
							"domain start residue number":"",
							"domain end residue number":"",
							"domain end residue sequence": "",
							"domain taxon id": "",
							"domain species": "",
							"domain species common": "",
							"interaction kd":"",
							"interaction kd min":"",
							"interaction kd max": ""
							}

							interactions.append(interaction)
						else:
							for interaction_instance in json_data[elm_id]['interactions']:
								interaction = {
								"domain protein uniprot accession":interaction_instance['interactorDomain'],
								"domain protein name": interaction_instance['proteinNameDomain'],
								"domain gene name": interaction_instance['geneNameDomain'],
								"domain protein family": "",
								"domain protein species": interaction_instance['speciesDomain'],
								"domain start residue number":interaction_instance['StartDomain'],
								"domain end residue number":interaction_instance['StopDomain'],
								"domain taxon id": interaction_instance['taxonIdentifierDomain'],
								"domain species": interaction_instance['speciesDomain'],
								"domain species common": interaction_instance['speciesCommonDomain'],
								"interaction kd":interaction_instance['AffinityMin'],
								"interaction kd min":interaction_instance['AffinityMin'],
								"interaction kd max": interaction_instance['AffinityMax']
								}

								interactions.append(interaction)

						for interaction in interactions:

							instance_curation = {
							"source":"ELM",
							"source_id":elm_id,
							"instance id":"",
							"motif type":"",
							"task":"",
							"curator in progress":"",
							"curator names":"elm_consortium",
							"pmid":pmid,
							"publication title":"",
							"publication authors":"",
							"publication details":"",
							"motif protein uniprot accession":motif_protein_uniprot_accession,
							"motif gene name":motif_gene_name,
							"motif protein name":motif_protein_name,
							"motif protein family":motif_protein_family,
							"motif protein species":motif_protein_species,
							"motif start residue number":motif_start_residue_number,
							"motif end residue number":motif_end_residue_number,
							"motif sequence":motif_sequence,
							"motif taxonomy": motif_taxonomy,
							"motif taxon id": motif_taxon_id,
							"motif protein species common": motif_protein_species_common,
							"domain protein uniprot accession":interaction['domain protein uniprot accession'],
							"domain gene name":interaction['domain gene name'],
							"domain protein name":interaction['domain protein name'],
							"domain protein family":interaction['domain protein family'],
							"domain protein species":interaction['domain protein species'],
							"domain taxon id": interaction['domain taxon id'],
							"domain protein species common":interaction['domain species common'],
							"domain start residue number":interaction['domain start residue number'],
							"domain end residue number":interaction['domain end residue number'],
							"domain pfam name":domain_pfam_name,
							"domain pfam accession":domain_pfam_accession,
							"interaction pdb":interaction_pdb,
							"interaction kd":interaction['interaction kd'],
							"interaction kd min":interaction['interaction kd min'],
							"interaction kd max":interaction['interaction kd max'],
							"elm class":elm_class,
							"evidence classes":"3",
							"evidence methods":experimental_method,
							"evidence logic":"10",
							"reliabilities":"2",
							"curator's instance assessment":instance_assessment,
							"conditional motif":"",
							"conditional motif description":"",
							"cell state":"",
							"other notes":"",
							"interface type":""
							}

							instance_curations.append(instance_curation)

							if self.options['format'] == "curation_tdt":
								row = []
								for tag in ["source",
											"source_id",
											"instance id",
											"motif type",
											"task",
											"curator in progress",
											"curator names",
											"pmid",
											"publication title",
											"publication authors",
											"publication details",
											"motif protein uniprot accession",
											"motif gene name",
											"motif protein name",
											"motif protein family",
											"motif protein species",
											"motif start residue number",
											"motif end residue number",
											"motif sequence",
											"domain protein uniprot accession",
											"domain gene name",
											"domain protein name",
											"domain protein family",
											"domain protein species",
											"domain start residue number",
											"domain end residue number",
											"domain pfam name",
											"domain pfam accession",
											"interaction pdb",
											"interaction kd",
											"interaction kd min",
											"interaction kd max",
											"elm class",
											"evidence classes",
											"evidence methods",
											"evidence logic",
											"reliabilities",
											"curator's instance assessment",
											"conditional motif",
											"conditional motif description",
											"cell state",
											"other notes",
											"interface type"
											]:
									row.append(instance_curation[tag])

								print(("\t".join([str(x) for x in row])))

				return {"status": "Found", "data": instance_curations}

			#-------------------
			#-------------------

			if len(json_data) > 0:
				return {"status": "Found", "data": json_data}
			else:
				return {"status": "Error", "error_type": "Entry not found"}

		except Exception as e:
			raise
			return {"status": "Error", "error_type": str(e)}


	####################################################################################################
	####################################################################################################
	####################################################################################################

	def get_elm_instances(self,options = None):
		try:
			if options != None:
				self.options.update(options)

			#-------------------

			if 'get_protein_list' in self.options:
				if self.options["get_protein_list"]:
					return self.get_protein_list()

			#-------------------

			if 'get_protein_dict' in self.options:
				if self.options["get_protein_dict"]:
					return self.get_protein_dict()

			#-------------------

			if 'get_domain_list' in self.options:
				if self.options["get_domain_list"]:
					return self.get_domain_list()

			#-------------------

			if 'get_interactions_list' in self.options:
				if self.options["get_interactions_list"]:
					return self.get_interactions_list()

			#-------------------

			if 'get_domain_dict' in self.options:
				if self.options["get_domain_dict"]:
					return self.get_domain_dict()

			#-------------------

			if 'get_pubmed_protein_mapping_dict' in self.options:
				if self.options['get_pubmed_protein_mapping_dict']:
					pmid = None
					if "pmid" in self.options: pmid = self.options['pmid']
					return self.get_pubmed_protein_mapping_dict(pmid)

			#-------------------

			if 'get_pubmed_elm_class_mapping_dict' in self.options:
				if self.options['get_pubmed_elm_class_mapping_dict']:
					pmid = None
					if "pmid" in self.options: pmid = self.options['pmid']
					return self.get_pubmed_elm_class_mapping_dict(pmid)

			#-------------------

			return self.get_instances()

		except Exception as e:
			import traceback
			print(traceback.format_exc())
			return {"status": "Error", "error_type": str(e)}

if __name__ == "__main__":

	dataDownloaderObj = elmDownloader()
	dataDownloaderObj.options['data_path'] = '/Users/adminndavey/Documents/Work/data/'

	task = sys.argv[1]

	###----###
	###----###

	"""
	if task == "get_elm_instances":
		elm_class = sys.argv[1]

		options = {
		"tasks": "get_elm_instances",
		"format": "details",
		"max_gap_proportion": 0.0,
		"identifier_type": "elm",
		"aligned_type": "peptides_padded_pssm_aligned",
		"flanks": 5,
		"identifier": elm_class,
		"remake":False
		}

		dataDownloaderObj.options.update(options)

		print "#",elm_class
		response = dataDownloaderObj.get_elm_instances(options)

		elm_data = response["data"]
		headers = ['Primary_Acc','Start','End',"protein_name","gene_name","species",'extended_peptide','peptides_padded_pssm_aligned']
		print  "\t".join(headers)
		for instance in elm_data:
			elm_data[instance]['species'] = elm_data[instance]["Organism"]  + " (" + elm_data[instance]["species_common"] + ")"
			for header in headers:
				print elm_data[instance][header],'\t',


	if task == "parse_candidates":
		dataDownloaderObj.parse_candidates()


	if task == "pp1":
		for elm_class in ['DOC_PP1_SILK_1','DOC_PP1_MyPhoNE_1']:
			options = {
			"tasks": "get_elm_instances",
			"format": "details",
			"max_gap_proportion": 0.0,
			"identifier_type": "elm",
			"aligned_type": "peptides_padded_pssm_aligned",
			"flanks": sys.argv[1],
			"identifier": elm_class,
			"remake":False
			}

			dataDownloaderObj.options.update(options)

			print "#",elm_class
			response = dataDownloaderObj.get_elm_instances(options)

			elm_data = response["data"]
			headers = ['Primary_Acc','Start','End',"protein_name","gene_name","species",'extended_peptide','peptides_padded_pssm_aligned']
			print  "\t".join(headers)
			for instance in elm_data:
				elm_data[instance]['species'] = elm_data[instance]["Organism"]  + " (" + elm_data[instance]["species_common"] + ")"
				for header in headers:
					print elm_data[instance][header],'\t',

				print

	if task == "pfam":

		response = dataDownloaderObj.parseELM()

		elm_data = response["data"]

		pfam_acc = "PF00018"
		ELMIdentifier = "LIG_SH3_2"
		flanks = 5


		for protein in elm_data:
			for instance in elm_data[protein]:
				#if elm_data[protein][instance]['ELMIdentifier'] == ELMIdentifier:
				#if elm_data[protein][instance]["binding_domain_id"] == pfam_acc:
				#if elm_data[protein][instance]["Primary_Acc"] == "P06400":
				for header in ['Primary_Acc','ProteinName','Start','End','binding_domain_id','ELMIdentifier',"PDB","protein_name","species_common"]:
					print elm_data[protein][instance][header],'\t',

				start =  int(elm_data[protein][instance]['Start'])
				end =  int(elm_data[protein][instance]['End'])

				print elm_data[protein][instance]["sequence"][max(0,start- flanks - 1):start-1].rjust(5,"-"),
				print elm_data[protein][instance]["sequence"][start-1:end],
				print elm_data[protein][instance]["sequence"][end:min(end+flanks,len(elm_data[protein][instance]["sequence"]))].ljust(5,"-"),
				print

	"""
	task = "curation"

	if task == "curation":

		response = dataDownloaderObj.parseELM()

		elm_data = response["data"]

		flanks = 5

		for protein in elm_data:
			for instance in elm_data[protein]:
				#if elm_data[protein][instance]['ELMIdentifier'] not in ["CLV_Separin_Fungi","CLV_Separin_Metazoa","DEG_APCC_DBOX_1","DEG_APCC_KENBOX_2","DEG_APCC_TPR_1","DEG_CRL4_CDT2_1","DEG_CRL4_CDT2_2","DEG_SCF_FBW7_1","DEG_SCF_FBW7_2","DEG_SCF_SKP2-CKS1_1","DOC_CKS1_1","DOC_CyclinA_RxL_1","DOC_PP1_RVXF_1","DOC_PP1_SILK_1","DOC_PP2A_B56_1","LIG_APCC_ABBA_1","LIG_APCC_ABBAyCdc20_2","LIG_APCC_Cbox_1","LIG_APCC_Cbox_2","LIG_GLEBS_BUB3_1","LIG_MAD2","LIG_MLH1_MIPbox_1","LIG_PCNA_APIM_2","LIG_PCNA_PIPBox_1","LIG_PCNA_yPIPBox_3","LIG_Rb_LxCxE_1","LIG_Rb_pABgroove_1","LIG_REV1ctd_RIR_1","LIG_RPA_C_Fungi","LIG_RPA_C_Insects","LIG_RPA_C_Plants","LIG_RPA_C_Vert","MOD_CDK_SPK_2","MOD_CDK_SPxK_1","MOD_CDK_SPxxK_3","MOD_Plk_1","MOD_Plk_2-3","MOD_Plk_4"] and "P20248,P14635,P24385,P30281,P24864,P18887,P54274,Q15554,Q13257,Q15013,P27694,P12004,O60671,P06400,P28749,Q08999,P15927,Q00987,Q9BSI4,O60566,P33981,O43683,Q12888,Q92547,Q86YC2,O43684,Q12834,Q9UM11,P30291,Q96AP0,Q8WXE1,O14965,P30304,P38936,P46527,Q9NVH0,O60921,Q9Y6D9,P49959,O60934,Q99640,Q9NUX5,Q92878,Q99638,P35244,Q14683,Q9NZC9,P38398,P78396,O95067,Q8WWL7,O96020,O96017,Q14676,Q9UI95,P51587,P53350,Q9NYY3,Q9H4B4,O00444,O15151,Q13315,Q13535,Q96GD4,P54132,P30305,P30307,O14757,Q01094,Q14209,O00716,Q16254,Q15329,Q96AV8,A0AVK6,Q14674,Q9BXW9,O95997,Q5FBB7,Q562F6,P04637,Q9H3D4,O15350,P0C1S8,Q8WWL7,O94921,P35869,Q00534,Q7LBR1,O96017,Q9UPN4,P33552,Q12834,Q13042,P06493,Q8IZL9,P24941,Q00526,Q00535,P19784,Q9HD42,Q9Y3E7,O14757,Q9BPX3,Q53EZ4,Q13352,Q76B58,O60563,Q00610,Q8IYS8,Q9NQY0,Q9UJX3,Q99828,Q9BV73,Q9UJX6,Q13112,Q99741,P11802,Q14457,Q16667,P51587,Q13111,Q9UJX5,Q13315,Q5T280,Q9BS18,Q15021,Q9UQB9,Q9NR09,Q9Y6G9,Q9UM13,P60006,Q96DE5,Q7Z7A1,Q8N9N5,Q8N9V6,P38398,Q8IX12,O75909,Q8TDN4,Q9UJX2,Q86XI2,Q9NXR7,A8MT69,Q99459,Q09472,Q15007,P11171,Q9H8V3,Q8IUQ4,P57059,Q9Y5X1,Q7L8A9,Q7Z5K2,P55273,O15392,Q9UBU7,Q9Y421,Q96RF0,Q9H0E7,P83876,P49427,P63279,Q5FBB7,O43903,Q8TBC4,Q68DK2,O43670,Q6ZSB9,Q9H2G4,Q9NYG5,Q9UJX4,Q14008,P35638,P81274,P42772,Q5EE01,O43684,Q9P287,Q92771,Q9UJT9,Q8IYD1,P50613,Q02224,P78396,O60519,Q96K21,O75935,Q9H4H8,Q9NVJ2,Q9BW66,Q9NQW6,Q96IK1,Q99675,Q6IQ19,Q9H410,Q14204,Q6PJP8,Q96CT7,P51946,P42695,Q9UKL3,Q9BXW9,Q9NVI1,O60447,O75122,P28562,Q5VWQ8,P24385,P84090,P46527,P36404,Q8TDC3,Q15003,Q66LE6,Q86UP6,Q02241,Q53GT1,Q05655,Q9UPY8,O00139,Q9P2K6,Q6P1M3,Q16589,O43240,P09496,O43768,Q07666,P25205,Q9Y6D9,Q8N163,Q86XL3,Q8NFT6,P54826,O75449,Q8IZD2,P36405,Q9H211,Q96BM9,P62330,Q16254,O43491,O75496,Q9UJP4,P30281,Q8IXV7,Q13422,Q9C0B6,P49450,O43683,Q9Y5K6,Q66K89,Q5TKA1,D6RGH6,Q96GD4,Q69YQ0,Q15555,P46013,Q9BVA0,Q5XUX0,O00311,Q6P1J9,Q9UM11,P49917,Q5HY92,Q9BVC3,Q9UKB1,Q8NEU8,Q9UKG1,M0R2J8,P51808,Q96AV8,P24522,Q13620,O15182,O75419,Q9NWV8,O00716,O95990,Q53HL2,Q8NHZ8,Q15018,Q8WYP5,P04899,P49918,P41002,Q9BTV5,Q99618,Q8N137,Q9BXY8,Q9H1H9,Q86Y37,Q14511,Q7Z460,Q8IXT1,Q56NI9,Q9UKT4,O43781,O00409,P98177,O14578,P51812,Q15418,Q69YH5,Q9Y6B2,Q08050,Q5FWF5,P23443,P49916,P33993,P53778,Q02156,Q15691,Q6P0N0,Q9ULC4,Q13618,Q9P2G4,Q49MG5,Q96L34,P51948,Q8N4N8,P41279,Q13625,Q96FF9,A6NCL1,P46736,Q14203,P42773,P50995,P08754,P63172,Q8IYB7,Q14565,Q9NQC7,O95273,O14976,Q9NQR1,P20248,P14635,Q9ULG6,O75794,Q14566,Q96MT8,Q6PGQ7,Q8WV92,Q8IYU2,Q8TF76,Q9H6D7,Q92733,Q9ULG1,Q29RF7,P04637,P41208,Q9Y222,P18858,Q9C099,Q9Y266,Q6KC79,Q86YI8,Q86Y91,P52732,Q14680,P49321,Q9BYG5,O75400,O96013,Q9NQS7,O15350,Q8NFH3,Q8TEW0,Q99661,O94889,Q7Z4H7,P51955,Q7Z7K6,Q8ND76,A6NF83,O60583,Q9Y250,Q6MZP7,P49736,P50748,O60566,Q2NKX8,Q08379,Q7RTP6,Q9BRX2,Q8NI51,Q9P2N7,Q14676,Q9NRZ9,O95067,P61024,Q14980,Q01094,Q68CZ6,P51956,Q9UI95,Q9UK53,Q9UNL4,A0AVK6,Q96Q89,Q9NYL2,P04150,P0CG13,Q7L5Y9,P05783,Q96GX5,P28482,P58340,Q96C92,Q03188,Q99708,Q9NRM7,Q9NXR1,O75154,Q6NUQ1,Q9BVS4,Q8NA72,Q7Z3K3,Q16513,Q9H4B4,Q14207,P30307,Q9UPV0,Q96BT3,Q92974,Q14209,Q9P2K8,O00743,Q9NPB6,Q9BRP1,Q8TD19,Q8WVI7,Q96MF7,O75677,Q8WVB6,Q8NG31,P43034,Q5T686,P30305,Q9H0H5,Q9BRK4,B2CW77,Q96CS2,O94927,Q92831,Q14012,Q9H6H4,A8MVW5,Q9BZD4,O14777,Q6P1K2,Q99527,Q16659,P27361,Q96IY1,Q9NY59,P62140,P62136,Q6K0P9,Q9BTE3,P56211,Q99674,Q9H981,Q13526,P33991,O14965,Q9BTV7,P63096,Q9BW19,Q8NCD3,Q96KR4,Q8NHV4,O43663,P51610,Q01101,P51957,Q693B1,Q9BYG4,Q9P2J3,P52926,O15264,Q14CZ8,Q96PY6,Q8NFH4,Q9UJA3,P53990,Q96RW7,Q9NYP9,P19474,Q9BSK0,Q8NDF8,O60477,Q9Y5B0,Q13257,P51959,P30279,O75461,O60934,Q9NVM9,Q9UPP1,P11309,Q09028,Q6PGN9,P15173,P36873,Q9BSJ6,Q86V86,O14519,P42771,Q12798,Q8NG66,Q9NPI1,Q8N2Z9,Q8IWQ3,Q9UNH5,P16104,Q8WUM4,P48729,Q9BT25,Q8TEW8,P22674,Q8N726,P21127,Q15019,Q96T68,Q9UNS1,Q7Z2Z1,Q96EE3,Q9UH03,Q8IZU3,O60216,O75943,O94762,Q8IWB6,Q9BX26,O94804,O75410,Q7Z6K1,Q6ZMR5,Q8WVK7,Q8IX90,O95347,Q8WW12,Q99640,Q9P1W9,Q9BSD3,P63244,Q9HCM9,Q93096,Q9H5I1,Q8WVM7,Q99719,A1L190,Q9Y448,Q14683,Q9UJ98,Q6ZU15,Q9Y657,Q6IQ49,O15297,Q16181,Q6PIF2,Q8IZT6,O76064,Q9Y6A5,O96020,P31152,P11234,Q8TDY2,Q9H0Z9,Q9ULW0,Q14141,Q14188,Q5UIP0,O94811,P06400,Q9NS23,P12270,Q96DY7,O76095,Q13164,P78406,O95997,Q99871,Q9NVA2,Q8IYM1,O43236,Q8IXM3,Q8IZX4,Q86UD0,O95229,P23396,Q8WV41,P17020,Q6DKK2,Q53GS9,Q9NX01,Q9Y6I4,Q9UHD8,Q8N0S2,Q15431,Q5MJ68,Q9H4X1,Q9Y265,Q8NDN9,Q9Y3P9,O43463,Q14186,Q8NBT2,Q8NEB9,Q8N3U4,P49454,Q9H081,Q15831,Q9P258,O14894,Q8WYJ6,Q562F6,O43482,Q9NVX0,Q9HC98,Q86WB0,O95271,Q86UE8,Q9HBM1,Q9P0V9,Q92878,P54274,P50749,Q8NDV3,P40818,P33992,Q8N371,Q658P3,Q15554,Q96BD8,Q9UQE7,Q9UPT9,Q6NUK4,P69208,Q9H2L5,Q8N0Z3,P61289,Q8TAP9,P30304,O43566,H0YL14,Q96HU1,Q96EA4,P41220,Q08999,P21675,P40692,Q00839,Q16763,Q9H3M7,Q9BVW5,Q7RTN6,Q8IVT2,Q13564,P62491,P28749,O00762,Q9UN37,P62826,P18754,P12931,Q9H2D6,Q9UKI8,Q6ZVD7,Q99932,Q9NNW5,O60232,O75896,Q96T88,Q08J23,Q496M5,Q3YBR2,Q8TAE8,Q15398,Q9P0W2,Q96HI0,Q9C0K7,Q5MJ70,Q96PU4,Q99865,P30291,Q9NTI5,P85299,P04156,Q9BPZ2,P53350,P11233,Q12824,Q9NTJ3,Q8IY18,Q6NT89,Q9Y5T5,Q8TCY9,Q99816,O75604,P18583,Q86T82,Q96NB3,Q6R6M4,Q9H900,O43255,Q93008,Q9NUQ3,Q9BXS6,Q9NVV9,Q96R06,Q9UBP0,A6NKF1,Q9Y6X3,Q15326,Q96AY4,Q99986,Q8IXJ6,P61586,O95835,O00401,O43264,O75351,Q6N021,Q6UVJ0,Q96IF1,P38936,P24864,Q9UQ88,Q96EP1,P68400,Q5TZA2,Q9H1A4,Q9UBZ4,Q9HAW4,Q8WWK9,P62952".count(elm_data[protein][instance]['Primary_Acc']) > 0:
				if elm_data[protein][instance]['ELMIdentifier'] in ["DOC_CyclinA_RxL_1"]:
				#if elm_data[protein][instance]['ELMIdentifier'] in ["DOC_PP2A_B56_1"]:

				#if elm_data[protein][instance]['taxonomy'].count("Virus") > 0:
					start =  int(elm_data[protein][instance]['Start'])
					end =  int(elm_data[protein][instance]['End'])
					elm_data[protein][instance]['Peptide'] = elm_data[protein][instance]["sequence"][start-1:end]

					if len(elm_data[protein][instance]['interactions']) > 0:
						for interactor in  elm_data[protein][instance]['interactions']:


							affinityMin = ""
							if interactor["AffinityMin"] != None:
								affinityMin = interactor["AffinityMin"]

							for pmid in elm_data[protein][instance]['References'].split():
								row = [
								"",#Instance ID
								"Norman Davey",#name
								"ndavey",#username
								pmid,# PMID
								"",#Publication Title
								"",#Publication Authors
								"",#Publication Details
								"in_ELM",#Task
								elm_data[protein][instance]['Primary_Acc'],#Motif Protein Uniprot Accession
								elm_data[protein][instance]['gene_name'],#Motif Protein
								elm_data[protein][instance]['Organism'],#Virus
								elm_data[protein][instance]['Start'],#Motif Start Residue Number
								elm_data[protein][instance]['End'],#Motif End Residue Number
								elm_data[protein][instance]['Peptide'],#Motif Sequence
								interactor['interactorDomain'],#Domain Protein Uniprot Accession
								interactor["geneNameDomain"],#Domain Protein Gene
								interactor["speciesDomain"],#Domain Protein Host
								interactor['StartDomain'],#Domain Start Residue Number
								interactor['StopDomain'],#Domain End Residue Number
								elm_data[protein][instance]['binding_domain_description'],#Domain Pfam Name
								elm_data[protein][instance]['binding_domain_id'],#Domain Pfam Accession
								elm_data[protein][instance]['PDB'],#Interaction PDB
								affinityMin,#Interaction Kd
								elm_data[protein][instance]['ELMIdentifier'],#ELM Class
								"3",#evidence_classes (3 | experimental)
								elm_data[protein][instance]['Methods'],#evidence_methods (see evidence table)
								"10",#evidence_logic (Usually 10 | support)
								"2",#reliabilities (Usually 2 | certain)
								elm_data[protein][instance]['InstanceLogic'],#Curator's Instance Assessment (Usually TP)
								"",#Other Notes
								]
								print(("\t".join(row)))
					else:
						for pmid in elm_data[protein][instance]['References'].split():
							row = [
							"",#Instance ID
							"Norman Davey",#name
							"ndavey",#username
							pmid,# PMID
							"",#Publication Title
							"",#Publication Authors
							"",#Publication Details
							"in_ELM",#Task
							elm_data[protein][instance]['Primary_Acc'],#Motif Protein Uniprot Accession
							elm_data[protein][instance]['gene_name'],#Motif Protein
							elm_data[protein][instance]['Organism'],#Virus
							elm_data[protein][instance]['Start'],#Motif Start Residue Number
							elm_data[protein][instance]['End'],#Motif End Residue Number
							elm_data[protein][instance]['Peptide'],#Motif Sequence
							"",#Domain Protein Uniprot Accession
							"",#Domain Protein Gene
							"",#Domain Protein Host
							"",#Domain Start Residue Number
							"",#Domain End Residue Number
							elm_data[protein][instance]['binding_domain_description'],#Domain Pfam Name
							elm_data[protein][instance]['binding_domain_id'],#Domain Pfam Accession
							elm_data[protein][instance]['PDB'],#Interaction PDB
							"",#Interaction Kd
							elm_data[protein][instance]['ELMIdentifier'],#ELM Class
							"3",#evidence_classes (3 | experimental)
							elm_data[protein][instance]['Methods'],#evidence_methods (see evidence table)
							"10",#evidence_logic (Usually 10 | support)
							"2",#reliabilities (Usually 2 | certain)
							elm_data[protein][instance]['InstanceLogic'],#Curator's Instance Assessment (Usually TP)
							"",#Other Notes
							]
							print(("\t".join(row)))

					#print elm_data[protein][instance]["sequence"][max(0,start- flanks - 1):start-1].rjust(5,"-"),
					#print elm_data[protein][instance]["sequence"][start-1:end],
					#print elm_data[protein][instance]["sequence"][end:min(end+flanks,len(elm_data[protein][instance]["sequence"]))].ljust(5,"-"),
					#print
