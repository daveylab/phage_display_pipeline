import re
import sys
import json
import pprint
import os
import operator
import inspect
import time
import copy
import string

import dataset
import sqlalchemy

import uniprotDownloader
import pmidDownloader

file_path = os.path.dirname(inspect.stack()[0][1])
sys.path.append(os.path.join(file_path,"../"))
sys.path.append(os.path.join(file_path,"../utilities/"))

import utilities_downloader
import config_reader
import option_reader

sys.path.append(os.path.join(file_path,"../utilities/"))
import utilities_downloader
import utilities_error

sys.path.append(os.path.join(file_path,"../data_management/"))
import queryManager

printable = set(string.printable)

protein_tags = [
	'activation domain tag',
	'alkaline phosphatase tag',
	'au1 tag',
	'bifc tag',
	'biotin tag',
	'C-terminal firefly luciferase tag',
	'c-venus',
	'calmodulin binding peptide plus protein a tag',
	'calmodulin binding protein tag',
	'ceYFP',
	'cGFP',
	'cyan fluorescent protein tag',
	'cYFP',
	'dna binding domain tag',
	'eef tag',
	'enhanced green fluorescent protein tag',
	'enhanced yellow fluorescent protein tag',
	'enzyme tag',
	'fc-igg tag',
	'firefly luciferase protein tag',
	'firefly-c',
	'firefly-n',
	'flag tag',
	'fluorescent protein tag',
	'fluorescent protein tag',
	'fusion protein',
	'gal4 activation domain',
	'gal4 dna binding domain',
	'gaussia-c',
	'gaussia-n',
	'glu tag',
	'glutathione s tranferase tag',
	'green fluorescent protein tag',
	'ha tag',
	'his tag',
	'horseradish peroxidase tag',
	'kusabira-green protein tag',
	'lap tag',
	'luciferase tag',
	'luciferase-c',
	'luciferase-n',
	'maltose binding protein tag',
	'mcherry fluorescent protein tag',
	'mCitrine tag',
	'myc tag',
	'n-venus',
	'nanoluc-c',
	'nanoluc-n',
	'nGFP',
	'nub',
	'nubg',
	'nYFP',
	'neYFP',
	'one-strep-tag',
	'protein a tag',
	'pyo tag',
	'red fluorescent protein tag',
	'renilla luciferase protein tag',
	'renilla-c',
	'renilla-n',
	'rho tag',
	's tag',
	'strep ii tag',
	'strep tag',
	'sumo tag',
	'supercharged green fluorescent protein',
	't7 tag',
	'tag',
	'tandem tag',
	'transactivating tag',
	'ubiquitin reconstruction tag',
	'v5 tag',
	'venus c-terminal fragment',
	'venus fluorescent protein tag',
	'venus n-terminal fragment',
	'vp16 activation domain',
	'vsv tag',
	'xpress tag',
	'yellow fluorescent protein tag',
	'zz tag'
	]

class interactionDownloader():
	##------------------------------------------------------------------##

	def __init__(self,use_hippie=None,use_string=None,use_viruses=None,use_bioplex=None,use_huri=None,use_database=None,parse_commandline=True):

		self.options = {}
		self.options["task"] = None
		self.options["fetch_mapping"] = False
		self.options["remake_age"] = 90
		self.options["cutoff"] = 0
		self.options["add_paper_details"] = True
		self.options["add_protein_details"] = False

		self.options["postgres_username"] = "postgres"
		self.options["postgres_password"] = "Pass2020!"
		self.options["postgres_interaction_database_path"] = "localhost:5432/ppi"

		self.options.update(config_reader.load_configeration_options(sections=["general","interaction_downloader"]))

		if use_hippie != None or use_string !=None or use_viruses!= None or use_bioplex!= None or use_huri !=None:
			self.options["use_hippie"] = False
			self.options["use_string"] = False
			self.options["use_viruses"] = False
			self.options["use_bioplex"] = False
			self.options["use_huri"] = False

		if use_hippie != None:  self.options["use_hippie"] = use_hippie
		if use_string !=None: self.options["use_string"] = use_string
		if use_viruses!= None: self.options["use_viruses"] = use_viruses
		if use_bioplex!= None: self.options["use_bioplex"] = use_bioplex
		if use_huri  !=None: self.options["use_huri"] = use_huri
		if use_database != None: self.options['use_database'] = use_database

		try:
			if parse_commandline:
				self.options.update(option_reader.load_commandline_options(self.options,{}))
		except:
			pass

		self.setup_data_files()

	def setup_data_files(self):
		database_name_bits = []

		if self.options["use_hippie"]:
			database_name_bits.append('HIPPIE')

		if self.options["use_string"]:
			database_name_bits.append('STRING')

		if self.options["use_bioplex"]:
			database_name_bits.append('BIOPLEX')

		if self.options["use_huri"]:
			database_name_bits.append('HURI')

		if self.options["use_viruses"]:
			database_name_bits.append('Viruses')

		if len(database_name_bits) == 0:
			print("Not sure which data sources to use")
			sys.exit()

		self.options["database_name"] = "_".join(database_name_bits) + ".interactions"
		self.options["interactions_file"] = os.path.abspath(os.path.join(self.options["data_path"],"interactions",self.options["database_name"] + ".json"))

		if self.options["use_database"] and not self.options["make_database"]:
			self.open_database_connection()
		else:
			self.interactions = None

			if self.options['verbose']:print(("Loading ",self.options["database_name"]))
			if not os.path.exists(self.options["interactions_file"]):
				if self.options['verbose']:print(("Making",self.options["interactions_file"]))
				self.saveInteractions()

			if self.interactions == None:
				if self.options['verbose']:print(("Loading",self.options["interactions_file"]))
				with open(self.options["interactions_file"]) as data_file:
					self.interactions = json.load(data_file)
					if self.options['verbose']:print(("Loaded",self.options["interactions_file"]))

			self.complexes = {"complex_portal":{},"corum":{},"names":{}}
			self.subunits = {}

			self.downloadComplexPortal()
			self.downloadCorum()

			self.parseComplexPortal()
			self.parseCorum()

			if self.options["make_database"]:
				self.open_database_connection()
				self.make_database()
				sys.exit()

		#######

		self.options["cytoscape_path"] = os.path.abspath(os.path.join(self.options["data_path"],"interactions","cytoscape"))
		self.options["cytoscape_path_database"] = os.path.abspath(os.path.join(self.options["data_path"],"interactions","cytoscape",self.options["database_name"]))

		if not os.path.exists(self.options["cytoscape_path"]):
			os.mkdir(self.options["cytoscape_path"])

		if not os.path.exists(self.options["cytoscape_path_database"]):
			os.mkdir(self.options["cytoscape_path_database"])

	############################################################################################################################################

	def saveInteractions(self):
		interactions = {}

		if self.options["use_hippie"]:
			if len(interactions) == 0:counter = 0
			else: counter = max(interactions.keys())
			interactions = self.grabHIPPIE(interactions=interactions,counter=counter)

		if self.options["use_huri"]:
			if len(interactions) == 0:counter = 0
			else: counter = max(interactions.keys())
			interactions = self.grabHuRI(interactions=interactions,counter=counter)

		if self.options["use_bioplex"]:
			if len(interactions) == 0:counter = 0
			else: counter = max(interactions.keys())
			interactions = self.grabBioPlex(interactions=interactions,counter=counter)

		if self.options["use_string"]:
			if len(interactions) == 0:counter = 0
			else: counter = max(interactions.keys())
			interactions = self.grabSTRING(interactions=interactions,counter=counter)

		if self.options["use_viruses"]:
			if len(interactions) == 0:counter = 0
			else: counter = max(interactions.keys())
			interactions = self.grabViruses(interactions=interactions, counter=counter)

		with open(self.options["interactions_file"],"w") as outfile:
			json.dump(interactions, outfile)

		if self.options['verbose']:print(("Saved to:",self.options["interactions_file"]))

	############################################################################################################################################
	############################################################################################################################################
	############################################################################################################################################
	#######                                                                                                                              #######
	#######                                                DATABASE VERSION                                                              #######
	#######                                                                                                                              #######
	############################################################################################################################################
	############################################################################################################################################
	############################################################################################################################################

	def results_to_dictionary(self,results,key=None):

		if self.options["key_group"] != None:
			key = self.options["key_group"]

		if key == None:
			results_data = []

			try:
				for result in results:
					result_data = {}
					for use_header in results.keys:
						result_data[use_header] = result[use_header]

					results_data.append(result_data)
			except:
				results_data
		else:
			results_data = {}

			try:
				if key in results.keys:
					for result in results:

						if  result[key] not in results_data:
							results_data[result[key]] = []

						row = {}
						for use_header in results.keys:
							if use_header != key:
								row[use_header] = result[use_header]

						if len(row) == 1:
							if list(row.values())[0] not in results_data[result[key]] or list(row.values())[0] != "":
								results_data[result[key]].append(list(row.values())[0])
						else:
							if row not in results_data[result[key]]:
								results_data[result[key]].append(row)
			except:
				results_data

		return results_data

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def open_database_connection(self):
		self.open_sqlite_database_connection()

	def open_sqlite_database_connection(self):
		# connecting to a SQLite database
		# connecting to a SQLite database

		database_path = os.path.join(self.options["data_path"], "sqlite", self.options["database_name"] + '.db')
		if os.path.exists(database_path) or self.options["make_database"]:
			if self.options['verbose']: print(('Opening sqlite:///' + database_path))
			self.db = dataset.connect('sqlite:///' + database_path, engine_kwargs={'poolclass':sqlalchemy.pool.NullPool,'connect_args':{"check_same_thread": False}})
			if self.options['verbose']: print(('Opened sqlite:///' + self.options["database_name"] + '.db'))
		else:
			if self.options['verbose']: print(('Database does not exist - sqlite:///' + os.path.join(self.options["data_path"], "sqlite", self.options["database_name"] + '.db')))

			self.options["make_database"] = True
			self.setup_data_files()

	def open_postgres_database_connection(self):
		# connecting to a PostGres database
		# connecting to a PostGres database

		# https://towardsdatascience.com/local-development-set-up-of-postgresql-with-docker-c022632f13ea
		# docker pull postgres
		# docker images
		# docker run -d --name dev-postgres -e POSTGRES_PASSWORD=Pass2020! -v ${HOME}/postgres-data/:/var/lib/postgresql/data         -p 5432:5432
		# docker exec -it dev-postgres bash
		## psql -h localhost -U postgres
		### \l

		# docker run -d --name dev-postgres -e POSTGRES_PASSWORD=Pass2020! -v /Users/adminndavey/Documents/Work/data/postgres/:/var/lib/postgresql/data -p 5432:5432 postgres
		# docker pull dpage/pgadmin4
		# docker run -p 80:80 -e 'PGADMIN_DEFAULT_EMAIL=user@domain.local' -e 'PGADMIN_DEFAULT_PASSWORD=SuperSecret' --name dev-pgadmin -d dpage/pgadmin4
		# docker inspect dev-postgres -f "{{json .NetworkSettings.Networks }}"

		database_path = self.options["postgres_username"] + ":" + self.options["postgres_password"]  + "@" + self.options["postgres_interaction_database_path"]

		if not self.options["make_database"]:
			if self.options['verbose']: print(('Opening postgresql:///' + database_path))
			self.db = dataset.connect('postgresql://' + database_path)
			if self.options['verbose']: print(('Opened postgresql:///' + database_path))
		else:
			if self.options['verbose']: print(('Database does not exist - postgresql:///' + os.path.join(self.options["data_path"], "sqlite", self.options["database_name"] + '.db')))

			self.options["make_database"] = True
			self.setup_data_files()

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def convert_complex_portal_complex(self,complex_data):
		description = ""

		for text in [complex_data['Description'],complex_data['Comment']]:
			if text != None and text != "-":
				description += text + " "

		return {
		'ComplexName':complex_data['Recommended name'],
		'Subunits':complex_data['subunit_accessions'],
		'Evidence':complex_data['Experimental evidence'],
		'Description':"".join([x for x in description.strip() if x in printable]),
		'Cross references':complex_data['Cross references'],
		'Complex assembly':complex_data['Complex assembly'],
		'Stoichiometry':complex_data['Identifiers (and stoichiometry) of molecules in complex'],
		"Source":complex_data['source']
		}

	##------------------------------------------------------------------##
	##------------------------------------------------------------------##
	##------------------------------------------------------------------##

	def convert_corum_complex(self,complex_data):
		description = ""

		for text in [complex_data['Complex comment'],complex_data['Disease comment'],complex_data['Subunits comment']]:
			if text != None:
				description += text  + " "

		return {
		'ComplexName':complex_data['ComplexName'],
		'Subunits':complex_data['subunit_accessions'],
		'Evidence':complex_data['Protein complex purification method'],
		'Description':"".join([x for x in description.strip() if x in printable]),
		'Cross references':complex_data['PubMed ID'],
		'Complex assembly':"",
		'Stoichiometry':"",
		"Source":complex_data['source']
		}

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def make_database(self):
		self.make_database_subunits()
		self.make_database_complex()
		self.make_database_interactions()
		self.make_database_proteins()
		self.make_database_intact_interfaces()

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def make_database_proteins(self):
		if self.options['verbose']:print('Dropping proteins')
		self.db['proteins'].drop()

		accessions = self.getInteractionsAccessionsDatabase()

		if self.options['verbose']:print('Getting protein data')

		dataDownloaderObj = uniprotDownloader.uniprotDownloader()
		dataDownloaderObj.options["data_path"] = os.path.abspath(self.options["data_path"])

		insert_data = []

		for accession in accessions:
			try:
				protein_data = dataDownloaderObj.parseBasic(accession)

				if 'data' in protein_data:
					try:
						insert_protein_data = {
							'accession':protein_data['data']['accession'],
							'identifier':protein_data['data']['id'],
							'gene_name':protein_data['data']['gene_name'],
							'gene_names': "|".join(protein_data['data']['gene_names']),
							'protein_name':protein_data['data']['protein_name'],
							'protein_names':"|".join(protein_data['data']['protein_names']),
							'family':protein_data['data']['family'],
							'species_common': protein_data['data']['species_common'],
							'species_scientific': protein_data['data']['species_scientific'],
							'taxon_id':protein_data['data']['taxon_id'],
							'taxonomy':protein_data['data']['taxonomy'],
							'version':protein_data['data']['version']
						}

						insert_data.append(insert_protein_data)
					except:
						print("ERROR @ make_database_proteins- adding",protein_data)
				else:
					print("ERROR @ make_database_proteins- adding",protein_data)
			except:
				print("ERROR @ make_database_proteins- adding",accession)

		if self.options['verbose']:print("Inserting",len(insert_data),"proteins")
		status = self.db['proteins'].insert_many(insert_data)
		if self.options['verbose']:print(status)

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def make_database_subunits(self):

		if self.options['verbose']:print('Dropping subunits')
		self.db['subunits'].drop()
		if self.options['verbose']:print('Inserting subunits')
		insert_data = []

		for accession in self.subunits:
			for complexes_database in self.subunits[accession]:
				for complex_id in self.subunits[accession][complexes_database]:

					subunit = {
					"accession":accession,
					"source":complexes_database,
					"complex_id":complex_id
					}

					if subunit not in insert_data:
						insert_data.append(subunit)

		if self.options['verbose']:print(("Inserting",len(insert_data),"subunits"))
		status = self.db['subunits'].insert_many(insert_data)
		if self.options['verbose']:print(status)

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def make_database_complex(self):
		if self.options['verbose']:print('Dropping complexes')
		self.db['complexes'].drop()

		if self.options['verbose']:print('Inserting complexes')
		insert_data = []
		for complexes_database in ["complex_portal","corum"]:
			for complex_id in self.complexes[complexes_database]:

				if complexes_database == "complex_portal":
					complex_data = self.convert_complex_portal_complex(self.complexes[complexes_database][complex_id])

				if complexes_database == "corum":
					complex_data = self.convert_corum_complex(self.complexes[complexes_database][complex_id])

				complex_data["Subunits"] = "|".join(complex_data["Subunits"])
				complex_data['complex_id'] = str(complex_id)

				if complex_data not in insert_data:
					insert_data.append(complex_data)

		if self.options['verbose']:print(("Inserting",len(insert_data),"complexes"))
		status = self.db['complexes'].insert_many(insert_data)
		if self.options['verbose']:print(status)

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def make_database_interactions(self):
		if self.options['verbose']:print('Dropping interactions')
		self.db['interactions'].drop()

		if self.options['verbose']:print('Inserting interactions')
		insert_data = []
		for interaction in list(self.interactions.values()):
			interaction['method'] = "|".join(interaction['method'])
			interaction['source'] = "|".join(interaction['source'])
			insert_data.append(interaction)

		if self.options['verbose']:print(("Inserting",len(insert_data),"interactions"))
		status = self.db['interactions'].insert_many(insert_data)
		if self.options['verbose']:print(status)

	##------------------------------------------------------------------##

	def check_database_intact_interfaces(self):
		if "interfaces" not in self.db:
			self.make_database_intact_interfaces()

	##------------------------------------------------------------------##

	def make_database_intact_interfaces(self):
		if self.options['verbose']:print("loadIntactInterfaces")
		self.options["intact_features_file"] = os.path.join(self.options["data_path"],"interactions","./intact.binding_regions.json")
		if not os.path.exists(self.options["intact_features_file"]):
			self.loadIntactInterfaces()

		with open(self.options["intact_features_file"]) as data_file:
			binding_regions = json.load(data_file)

		interface_database = {}
		interface_domain_database = []

		for bait_accession in binding_regions:
			interface_instance = {}
			if not re.match("\A([OPQ][0-9][A-Z0-9]{3}[0-9]|[A-NR-Z][0-9]([A-Z][A-Z0-9]{2}[0-9]){1,2})\Z", bait_accession):
				if self.options['verbose']:print(('ERROR @ make_database_intact_interfaces - not a UniProt accession  ',bait_accession))
				continue

			interfaces = binding_regions[bait_accession]
			interface_proteins = list(interfaces.keys())

			protein_list = [bait_accession]

			for interface_protein in interface_proteins:
				if re.match("\A([OPQ][0-9][A-Z0-9]{3}[0-9]|[A-NR-Z][0-9]([A-Z][A-Z0-9]{2}[0-9]){1,2})\Z", interface_protein):
					protein_list.append(interface_protein)

			queryManagerObj = queryManager.queryManager()
			queryManagerObj.options['dataset_type'] = "uniprot"
			queryManagerObj.options['task'] = "parse_uniprot_bulk"
			queryManagerObj.options['accession'] = protein_list
			queryManagerObj.options['verbose'] = False

			proteins = queryManagerObj.main()

			for prey_accession in interfaces:
				for interface in interfaces[prey_accession]:
					try:
						interface_id =  "_".join([bait_accession,interface['bait_start'],interface['bait_end'],prey_accession,interface['prey_start'],interface['prey_end'],interface['pmid']])

						if interface_id in interface_database:
							interface_instance['method'].append(str(interface['method']))
						else:
							interface_instance = copy.deepcopy(interface)

							interface_instance['bait_accession'] = bait_accession
							interface_instance['prey_accession'] = prey_accession
							interface_instance['interface_id'] = interface_id
							interface_instance['pmid'] = interface['pmid']
							interface_instance['method'] = [str(interface['method'])]

							for tag in ['taxon_id','gene_name','protein_name']:
								if prey_accession in proteins['data']:
									if 'data' in proteins['data'][bait_accession]:
										if tag in proteins['data'][bait_accession]['data']:
											interface_instance["bait_" + tag] = proteins['data'][bait_accession]['data'][tag]
								else:
									interface_instance["bait_" + tag] = ""

							for tag in ['taxon_id','gene_name','protein_name']:
								if prey_accession in proteins['data']:
									if 'data' in proteins['data'][prey_accession]:
										if tag in proteins['data'][prey_accession]['data']:
											interface_instance["prey_" + tag] = proteins['data'][prey_accession]['data'][tag]
								else:
									interface_instance["prey_" + tag] = ""

							interface_database[interface_id] = interface_instance


							###############################
							### DOMAINS DOMAINS DOMAINS ###
							###############################

							if interface['bait_start'] != "" and interface['bait_end'] != "":
								queryManagerObj = queryManager.queryManager()
								queryManagerObj.options['dataset_type'] = "uniprot"
								queryManagerObj.options['task'] = "parse_uniprot_pfam"
								queryManagerObj.options['accession'] = bait_accession
								queryManagerObj.options['region_start'] = int(interface['bait_start'])
								queryManagerObj.options['region_end'] = int(interface['bait_end'])
								queryManagerObj.options['verbose'] = False

								bait_domain = queryManagerObj.main()

								if 'data' in bait_domain:
									for pfam_accession in bait_domain['data']:
										for match in bait_domain['data'][pfam_accession]['matches']:
											interface_domain_database.append({
												"protein_accession":bait_accession,
												"pfam_id":bait_domain['data'][pfam_accession]['pfam_id'],
												"clan_id":bait_domain['data'][pfam_accession]['clan_id'],
												"pfam_accession":pfam_accession,
												"start":match['start'],
												"end":match['end'],
												"region_ratio":match['region_ratio'],
												"interface_id":interface_id
												}
											)

							if interface['prey_start'] != "" and interface['prey_end'] != "":
								queryManagerObj = queryManager.queryManager()
								queryManagerObj.options['dataset_type'] = "uniprot"
								queryManagerObj.options['task'] = "parse_uniprot_pfam"
								queryManagerObj.options['accession'] = prey_accession
								queryManagerObj.options['region_start'] = int(interface['prey_start'])
								queryManagerObj.options['region_end'] = int(interface['prey_end'])
								queryManagerObj.options['verbose'] = False

								prey_domain = queryManagerObj.main()

								if 'data' in prey_domain:
									for pfam_accession in prey_domain['data']:
										for match in prey_domain['data'][pfam_accession]['matches']:
											interface_domain_database.append({
												"protein_accession":prey_accession,
												"pfam_id":prey_domain['data'][pfam_accession]['pfam_id'],
												"clan_id":prey_domain['data'][pfam_accession]['clan_id'],
												"pfam_accession":pfam_accession,
												"start":match['start'],
												"end":match['start'],
												"region_ratio":match['region_ratio'],
												"interface_id":interface_id
												}
											)
					except Exception:
						print("ERROR @ make_database_intact_interfaces - ",bait_accession, prey_accession, "error",utilities_error.getError())

			del queryManagerObj

		##############################

		interface_database_data = []
		for interface_id in interface_database:
			interface = interface_database[interface_id]
			interface['method'] = "|".join(interface['method'])
			interface_database_data.append(interface)

		if self.options['verbose']:print('Dropping interfaces')
		self.db['interfaces'].drop()
		if self.options['verbose']:print(("Inserting",len(interface_database),"interfaces"))
		status = self.db['interfaces'].insert_many(interface_database_data)
		if self.options['verbose']:print(status)


		if self.options['verbose']:print('Dropping interface domains')
		self.db['interface_domains'].drop()
		if self.options['verbose']:print(("Inserting",len(interface_domain_database)," interface domains"))
		status = self.db['interface_domains'].insert_many(interface_domain_database)
		if self.options['verbose']:print(status)

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def search_database(self,accession):
			statement = """select
				*
			from interactions
			where  a_accession = """ + '"' + accession + '" OR b_accession = ' +  '"' + accession + '"'
			if self.options['verbose']:print(statement)

			if self.options['verbose']:pprint.pprint(self.results_to_dictionary(self.db.query(statement)))

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def getProteinDatabase(self,accessions):
			accessions = [x for x in accessions if x not in [None,"Unknown",""]]
			search_accessions_formatted = ", ".join([str('"' + accession + '"') for accession in accessions])

			statement = """select *
			from proteins
			where accession in """ +  '(' + search_accessions_formatted + ')'

			if self.options['verbose']:print(statement)

			data = self.results_to_dictionary(self.db.query(statement),key='accession')

			protein_data = {}

			for protein in data:
				protein_data[protein] = data[protein][0]

			return protein_data

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def getInteractionsDatabase(self,identifiers,identifier_type):
		try:
			data = {}
			#########
			#########

			if isinstance(identifiers,(list)):
				if '' in identifiers:
					identifiers.remove('')
			else:
				identifiers = identifiers.split(",")

			#########
			#########

			chunk_size = 10000
			for chunks in range(0,len(identifiers),chunk_size):

				identifier = identifiers[chunks:min(len(identifiers),chunks+chunk_size)]

				if self.options['verbose']:print((chunks,min(len(identifiers),chunks+chunk_size),identifier))

				if identifier_type == "all":
					statement = """select *
					from interactions"""

					if self.options['verbose']:print(statement)

					data.update(self.results_to_dictionary(self.db.query(statement),key='pmid'))


				if identifier_type == "pmid":

					if "26186194" in identifier:
						identifier.remove("26186194")

					search_pmids_formatted = ", ".join([str(pmid) for pmid in identifier])

					statement = """select *
					from interactions
					where pmid in """ +  '(' + search_pmids_formatted + ')'

					if self.options["cutoff"] > 0:
						statement += ' AND confidence > ' + str(self.options["cutoff"])

					if self.options['verbose']:print(statement)

					data.update(self.results_to_dictionary(self.db.query(statement),key='pmid'))


				#########
				#########

				if identifier_type == "pairwise":
					search_accessions_formatted = ", ".join([str('"' + accession + '"') for accession in identifier])

					statement = """select *
					from interactions
					where (a_accession in """ +  '(' + search_accessions_formatted + ') AND b_accession in ' +  '(' + search_accessions_formatted + ') ) AND confidence > ' + str(self.options["cutoff"])
					if self.options['verbose']:print(statement)

					interactions = self.results_to_dictionary(self.db.query(statement))

					for interaction in interactions:
						a_accession = interaction['a_accession']
						b_accession = interaction['b_accession']

						if a_accession == None or b_accession == None: continue

						if a_accession in identifier and b_accession in identifier:
							if a_accession not in data:
								data[a_accession] = {}

							if b_accession not in data[a_accession]:
								data[a_accession][b_accession] = interaction


						if b_accession in identifier and a_accession in identifier:
							if b_accession not in data:
								data[b_accession] = {}

							if a_accession not in data[b_accession]:
								data[b_accession][a_accession] = interaction

				#########
				#########

				if identifier_type == "accession":
					search_accessions_formatted = ", ".join([str('"' + accession + '"') for accession in identifier])

					statement = """select *
					from interactions
					where (a_accession in """ +  '(' + search_accessions_formatted + ') OR b_accession in ' +  '(' + search_accessions_formatted + ')) AND confidence > ' + str(self.options["cutoff"])
					if self.options['verbose']:print(statement)

					interactions = self.results_to_dictionary(self.db.query(statement))

					for interaction in interactions:
						a_accession = interaction['a_accession']
						b_accession = interaction['b_accession']

						if a_accession == None or b_accession == None: continue

						if a_accession in identifier:
							pmid =  interaction["pmid"]
							if a_accession not in data:
								data[a_accession] = {}

							if b_accession not in data[a_accession]:
								data[a_accession][b_accession] = {"pmids":{},'confidence':interaction['confidence']}

							if interaction['confidence_source'] == 'hippie':
								data[a_accession][b_accession]['confidence'] = float(interaction['confidence'])
							data[a_accession][b_accession]["pmids"][pmid] = {
							'method':interaction['method'],
							'source':interaction['source']
							}

						if b_accession in identifier:
							pmid =  interaction["pmid"]
							if b_accession not in data:
								data[b_accession] = {}

							if a_accession not in data[b_accession]:
								data[b_accession][a_accession] = {"pmids":{},'confidence':interaction['confidence']}

							if interaction['confidence_source'] == 'hippie':
								data[b_accession][a_accession]['confidence'] =  float(interaction['confidence'])

							data[b_accession][a_accession]["pmids"][pmid] = {
							'method':interaction['method'],
							'source':interaction['source']
							}

			return {
			"status":"Success",
			"data":data
			}

			#########
			#########

		except Exception:
			print("ERROR @ getInteractionsDatabase - ",utilities_error.getError())
			raise
			#return {"status":"Error","error_type":utilities_error.getError()}

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def getInteractionsAccessionsDatabase(self):
		statement = "select DISTINCT a_accession from interactions"
		if self.options['verbose']:print(statement)
		accessions = list(self.results_to_dictionary(self.db.query(statement),key="a_accession").keys())

		statement = "select DISTINCT  b_accession from interactions"
		if self.options['verbose']:print(statement)
		accessions += list(self.results_to_dictionary(self.db.query(statement),key="b_accession").keys())

		return list(set(accessions))

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def getInteractionsPmidDatabase(self):
		statement = "select DISTINCT pmid, a_accession from interactions"
		if self.options['verbose']:print(statement)
		interactions = self.results_to_dictionary(self.db.query(statement),key="a_accession")

		statement = "select DISTINCT pmid, b_accession from interactions"
		if self.options['verbose']:print(statement)
		interactions.update(self.results_to_dictionary(self.db.query(statement),key="b_accession"))

		return interactions

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def getComplexDataDatabase(self,accession):

		complex_data_json = {}
		protein_data = {}

		statement = 'select DISTINCT accession, complex_id from subunits where accession = "' + accession + '"'
		if self.options['verbose']:print(statement)
		subunits = self.results_to_dictionary(self.db.query(statement),key="accession")

		dataDownloaderObj = uniprotDownloader.uniprotDownloader()
		dataDownloaderObj.options["data_path"] = os.path.abspath(self.options["data_path"])

		output_complex_data_json = {}
		for accession in subunits:
			complex_data_json[accession] = []

			search_complex_ids_formatted = ", ".join(['"' + str(complex_id) + '"' for complex_id in subunits[accession]])

			statement = 'select * from complexes where complex_id in (' + search_complex_ids_formatted + ')'
			if self.options['verbose']:print(statement)

			complex_data_json[accession] = self.results_to_dictionary(self.db.query(statement),key="complex_id")
			output_complex_data_json[accession] = []

			for complex in complex_data_json[accession]:
				complex_instance = complex_data_json[accession][complex][0]


				subunit_accessions = complex_instance['Subunits'].split("|")
				complex_instance['Subunits'] = {}
				complex_instance['complex_id'] = complex

				for subunit_accession in subunit_accessions:
					if subunit_accession not in protein_data:
						protein_data[subunit_accession] = dataDownloaderObj.parseBasic(subunit_accession)['data']


					gene_name = "-"
					protein_name = "-"

					if subunit_accession in protein_data:
						if 'gene_name' in protein_data[subunit_accession]:
							gene_name = protein_data[subunit_accession]['gene_name']

						if 'protein_name' in protein_data[subunit_accession]:
							protein_name = protein_data[subunit_accession]['protein_name']

					complex_instance['Subunits'][subunit_accession] = {
						"protein name":protein_name,
						"gene name":gene_name
					}

					stoichiometry = {}


				for protein in complex_instance['Stoichiometry'].split("|"):
					try:
						protein_accession = protein.split("(")[0]
						protein_stoichiometry= protein.split("(")[1].strip(")")

						if protein_stoichiometry == "0": continue

						if int(protein_stoichiometry) == 1:
							protein_stoichiometry = protein_stoichiometry + " copy"
						else:
							protein_stoichiometry = protein_stoichiometry + " copies"

						if protein_accession in complex_instance['Subunits']:
							complex_instance['Subunits'][protein_accession]['stoichiometry'] = protein_stoichiometry
					except:
						pass

				output_complex_data_json[accession].append(complex_instance)

		return output_complex_data_json

	############################################################################################################################################
	############################################################################################################################################
	############################################################################################################################################
	############################################################################################################################################
	############################################################################################################################################
	############################################################################################################################################
	############################################################################################################################################
	############################################################################################################################################
	############################################################################################################################################
	############################################################################################################################################
	############################################################################################################################################
	############################################################################################################################################

	def getInteractionsJSON(self,identifier,identifier_type):
		try:

			if isinstance(identifier,(list)):
				if '' in identifier:
					identifier.remove('')
			else:
				identifier = identifier.split(",")

			#####

			data = {}

			#####

			for interaction in list(self.interactions.keys()):
				if 'a_accession' not in self.interactions[interaction]: continue
				if 'b_accession' not in self.interactions[interaction]: continue
				a_accession = self.interactions[interaction]["a_accession"]
				b_accession = self.interactions[interaction]["b_accession"]

				if float(self.interactions[interaction]['confidence']) > self.options["cutoff"]:

					if identifier_type == "pmid":
						if self.interactions[interaction]["pmid"] in identifier:
							if self.interactions[interaction]['pmid'] not in data:
								data[self.interactions[interaction]['pmid']] = {}

							if a_accession not in data[self.interactions[interaction]['pmid']]:
								data[self.interactions[interaction]['pmid']][a_accession] = []

							if b_accession not in data[self.interactions[interaction]['pmid']]:
								data[self.interactions[interaction]['pmid']][b_accession] = []

							data[self.interactions[interaction]['pmid']][a_accession].append(b_accession)
							data[self.interactions[interaction]['pmid']][b_accession].append(a_accession)

					if identifier_type == "pairwise":

						if a_accession in identifier and b_accession in identifier:
							pmid = self.interactions[interaction]["pmid"]
							if a_accession not in data:
								data[a_accession] = {}

							if b_accession not in data[a_accession]:
								data[a_accession][b_accession] = {"pmids":{}}

							data[a_accession][b_accession]["pmids"][pmid] = {
							'confidence':self.interactions[interaction]['confidence'],
							'method':self.interactions[interaction]['method'],
							'source':self.interactions[interaction]['source']
							}

						if b_accession in identifier and a_accession in identifier:
							pmid =  self.interactions[interaction]["pmid"]
							if b_accession not in data:
								data[b_accession] = {}

							if a_accession not in data[b_accession]:
								data[b_accession][a_accession] = {"pmids":{}}

							data[b_accession][a_accession]["pmids"][pmid] = {
							'confidence':self.interactions[interaction]['confidence'],
							'method':self.interactions[interaction]['method'],
							'source':self.interactions[interaction]['source']
							}

					if identifier_type == "accession":

						if a_accession in identifier:
							pmid =  self.interactions[interaction]["pmid"]
							if a_accession not in data:
								data[a_accession] = {}

							if b_accession not in data[a_accession]:
								data[a_accession][b_accession] = {"pmids":{},'confidence':self.interactions[interaction]['confidence']}

							if self.interactions[interaction]['confidence_source'] == 'hippie':
								data[a_accession][b_accession]['confidence'] = float(self.interactions[interaction]['confidence'])

							data[a_accession][b_accession]["pmids"][pmid] = {

							'method':self.interactions[interaction]['method'],
							'source':self.interactions[interaction]['source']
							}

						if b_accession in identifier:
							pmid =  self.interactions[interaction]["pmid"]
							if b_accession not in data:
								data[b_accession] = {}

							if a_accession not in data[b_accession]:
								data[b_accession][a_accession] = {"pmids":{},'confidence':self.interactions[interaction]['confidence']}

							if self.interactions[interaction]['confidence_source'] == 'hippie':
								data[b_accession][a_accession]['confidence'] =  float(self.interactions[interaction]['confidence'])

							data[b_accession][a_accession]["pmids"][pmid] = {
							'method':self.interactions[interaction]['method'],
							'source':self.interactions[interaction]['source']
							}

			return {
			"status":"Success",
			"data":data
			}
		except Exception:
			raise

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def getInteractionsPMID(self):

		proteins = {}

		for line in open(os.path.join(self.options["data_path"],"interactions","./HIPPIE-current.mitab.txt")).read().strip().split("\n")[:]:
			line_bits = line.split("\t")

			try:
				if line_bits[2] not in proteins:
					proteins[line_bits[2]] = []


				for pmid in line_bits[8].split("|"):
					pmid = pmid.split(":")[1]
					proteins[line_bits[2]].append(pmid)

			except:
				pass

		pmids = []
		for protein in proteins:
			for pmid in proteins[protein]:
				if pmid not in pmids:
					pmids.append(pmid)
					break

		return list(set(pmids))

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def getComplexDataJSON(self,accession):

		complex_data_json = {}
		if accession in self.subunits:
			complex_data_json[accession] = {}
			for complexes_database in list(self.complexes.keys()):
				if complexes_database in self.subunits[accession]:
					for complex in self.subunits[accession][complexes_database]:
						if complexes_database == "complex_portal":
							complex_data_json[accession][complex] = self.convert_complex_portal_complex(self.complexes[complexes_database][complex])

						if complexes_database == "corum":
							complex_data_json[accession][complex] = self.convert_corum_complex(self.complexes[complexes_database][complex])

		return complex_data_json

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def getIntactTags(self):
		self.options["intact_features_file"] = os.path.join(self.options["data_path"],"interactions","./intact.tags.json")
		if not os.path.exists(self.options["intact_features_file"]):
			self.loadIntactInterfaces()

		with open(self.options["intact_features_file"]) as data_file:
			tags = json.load(data_file)

		return tags[self.options['accession']]

	##------------------------------------------------------------------##

	def getIntactMutations(self):
		self.options["intact_features_file"] = os.path.join(self.options["data_path"],"interactions","./intact.mutations.json")
		if not os.path.exists(self.options["intact_features_file"]):
			self.loadIntactInterfaces()

		with open(self.options["intact_features_file"]) as data_file:
			mutations = json.load(data_file)

		return mutations[self.options['accession']]

	##------------------------------------------------------------------##

	def getIntactInterfacesByDomainClan(self,clan_id = None):
		self.check_database_intact_interfaces()

		if clan_id != None:
			self.options['clan_id'] = clan_id

		if self.options['clan_id'] != None and self.options['clan_id'] != "":

			statement = 'select * from interface_domains where clan_id = "' + self.options['clan_id'] + '"'

			if self.options['verbose']:print(statement)
			interface_domains = self.results_to_dictionary(self.db.query(statement),key="interface_id")
			interface_id_formatted = '", "' .join(list(interface_domains.keys()))
			statement = 'select * from interfaces where interface_id in ("' + interface_id_formatted + '")'
			interfaces = self.results_to_dictionary(self.db.query(statement),key="interface_id")
		else:
			return {"status":"error",'error_type':"Set clan_id"}

		return self.processIntactInterfacesSQLResults(interfaces,interface_domains)


	def getIntactInterfacesByDomain(self,pfam_id = None):
		self.check_database_intact_interfaces()
		if pfam_id != None:
			self.options['pfam_id'] = pfam_id

		if self.options['pfam_id'] != None and self.options['pfam_id'] != "":

			statement = 'select * from interface_domains where pfam_accession = "' + self.options['pfam_id'] + '"'

			if self.options['verbose']:print(statement)
			interface_domains = self.results_to_dictionary(self.db.query(statement),key="interface_id")
			interface_id_formatted = '", "' .join(list(interface_domains.keys()))
			statement = 'select * from interfaces where interface_id in ("' + interface_id_formatted + '")'
			interfaces = self.results_to_dictionary(self.db.query(statement),key="interface_id")
		else:
			return {"status":"error",'error_type':"Set  pfam_id"}

		return self.processIntactInterfacesSQLResults(interfaces,interface_domains)

	def getIntactInterfaces(self):
		self.check_database_intact_interfaces()
		statement = 'select * from interfaces'
		if self.options['verbose']:print(statement)
		interfaces = self.results_to_dictionary(self.db.query(statement),key="interface_id")

		interface_id_formatted = '", "' .join(list(interfaces.keys()))
		statement = 'select * from interface_domains where interface_id in ("' + interface_id_formatted + '")'
		interface_domains = self.results_to_dictionary(self.db.query(statement),key="interface_id")

		return self.processIntactInterfacesSQLResults(interfaces,interface_domains)

	def getIntactInterfacesByProtein(self,accession = None):
		self.check_database_intact_interfaces()
		if accession != None:
			self.options['accession'] = accession

		if self.options['accession'] != None and self.options['accession'] != "":
			statement = 'select * from interfaces where prey_accession = "' + self.options['accession'] + '"'
			if self.options['verbose']:print(statement)
			interfaces = self.results_to_dictionary(self.db.query(statement),key="interface_id")

			interface_id_formatted = '", "' .join(list(interfaces.keys()))
			statement = 'select * from interface_domains where interface_id in ("' + interface_id_formatted + '")'
			interface_domains = self.results_to_dictionary(self.db.query(statement),key="interface_id")
		else:
			return {"status":"error",'error_type':"Set accession"}

		return self.processIntactInterfacesSQLResults(interfaces,interface_domains)

	def getIntactInterfacesByPMID(self,pmid = None):
		self.check_database_intact_interfaces()
		if pmid != None:
			self.options['pmid'] = pmid

		if self.options['pmid'] != None and self.options['pmid'] != "":
			statement = 'select * from interfaces where pmid = "' + self.options['pmid'] + '"'
			if self.options['verbose']:print(statement)
			interfaces = self.results_to_dictionary(self.db.query(statement),key="interface_id")

			interface_id_formatted = '", "' .join(list(interfaces.keys()))
			statement = 'select * from interface_domains where interface_id in ("' + interface_id_formatted + '")'
			interface_domains = self.results_to_dictionary(self.db.query(statement),key="interface_id")
		else:
			return {"status":"error",'error_type':"Set accession"}

		return self.processIntactInterfacesSQLResults(interfaces,interface_domains)


	def processIntactInterfacesSQLResults(self,interfaces,interface_domains):
		interfaces_json = {}

		for sql_interface_id in interfaces:
			interface = interfaces[sql_interface_id][0]
			if self.options['collapse_interface_type'] == "interface":
				interface_id = sql_interface_id

				if self.options["require_interface_region"]:
					if interface['bait_start'] == "" or interface['bait_end'] == "":
						continue

				if self.options["require_interface_region"]:
					if interface['prey_start'] == "" or interface['prey_end'] == "":
						continue

			if self.options['collapse_interface_type'] == "bait":
				interface_id = "_".join([interface['bait_accession'],interface['bait_start'],interface['bait_end']])

				if self.options["require_interface_region"]:
					if interface['bait_start'] == "" or interface['bait_end'] == "":
						continue

				binding_partner_accession = interface['prey_accession']

			if self.options['collapse_interface_type'] == "prey":
				if self.options["require_interface_region"]:
					if interface['prey_start'] == "" or interface['prey_end'] == "":
						continue

				interface_id = "_".join([interface['prey_accession'],interface['prey_start'],interface['prey_end']])
				binding_partner_accession = interface['bait_accession']

			interface['bait_region_domains_details'] = []
			interface['prey_region_domains_details'] = []
			interface['bait_region_domains_ids'] = []
			interface['prey_region_domains_ids'] = []
			interface['bait_region_domains_accessions'] = []
			interface['prey_region_domains_accessions'] = []
			interface['bait_region_domains_clan_ids'] = []
			interface['prey_region_domains_clan_ids'] = []

			if interface_id in interface_domains:
				for interface_domain in interface_domains[interface_id]:
					if interface_domain['protein_accession'] == interface['bait_accession']:

						interface['bait_region_domains_ids'].append(interface_domain["pfam_id"])
						interface['bait_region_domains_accessions'].append(interface_domain["pfam_accession"])
						interface['bait_region_domains_clan_ids'].append(interface_domain["clan_id"])
						interface['bait_region_domains_details'].append(interface_domain)

					if interface_domain['protein_accession'] == interface['prey_accession']:
						interface['prey_region_domains_ids'].append(interface_domain["pfam_id"])
						interface['prey_region_domains_details'].append(interface_domain)
						interface['prey_region_domains_accessions'].append(interface_domain["pfam_accession"])
						interface['prey_region_domains_clan_ids'].append(interface_domain["clan_id"])

			if self.options['collapse_interface_type'] == "interface":
				if interface_id not in interfaces_json:
					interfaces_json[interface_id] = []

				interfaces_json[interface_id].append(interface)
			else:
				if binding_partner_accession not in interfaces_json:
					interfaces_json[binding_partner_accession] = []

				interfaces_json[binding_partner_accession].append(interface)

		return interfaces_json

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	############################################################################################################################################
	############################################################################################################################################
	############################################################################################################################################
	#####
	#####				GENERICS - Not JSON or Database
	#####
	############################################################################################################################################
	############################################################################################################################################
	############################################################################################################################################

	def getInteractions(self,identifier,identifier_type):
		try:
			if self.options['use_database']:
				interactions = self.getInteractionsDatabase(identifier,identifier_type)
			else:
				interactions = self.getInteractionsJSON(identifier,identifier_type)

			if self.options["add_protein_details"]:
				interactions = self.addProteinNames(interactions)

			return interactions

		except Exception as e:
			print("ERROR @ getInteractions - ",e)
			return {"status":"Error","error_type":utilities_error.getError(),"data":{}}

	def addProteinNames(self,interactions):

		queryManagerObj = queryManager.queryManager()
		queryManagerObj.options['dataset_type'] = "uniprot"
		queryManagerObj.options['task'] = "parse_basic"

		if 'data' in interactions:
			for identifier in interactions['data']:
				for interaction_iter in range(0,len(interactions['data'][identifier])):
					try:
						queryManagerObj.options['accession'] = interactions['data'][identifier][interaction_iter]['a_accession']
						proteins = queryManagerObj.main()

						interactions['data'][identifier][interaction_iter]['a_gene_name'] = "?"
						interactions['data'][identifier][interaction_iter]['a_protein_name'] =  "?"

						interactions['data'][identifier][interaction_iter]['b_gene_name'] = "?"
						interactions['data'][identifier][interaction_iter]['b_protein_name'] =  "?"

						if 'data' in proteins:
							interactions['data'][identifier][interaction_iter]['a_gene_name'] = proteins['data']['gene_name']
							interactions['data'][identifier][interaction_iter]['a_protein_name'] = proteins['data']['protein_name']

						queryManagerObj.options['accession'] = interactions['data'][identifier][interaction_iter]['b_accession']
						proteins = queryManagerObj.main()
						if 'data' in proteins:
							interactions['data'][identifier][interaction_iter]['b_gene_name'] = proteins['data']['gene_name']
							interactions['data'][identifier][interaction_iter]['b_protein_name'] = proteins['data']['protein_name']
					except:
						pass

		del queryManagerObj

		return interactions

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def getComplexData(self,accession):

		if isinstance(accession,(list)):
			accession = accession[0]

		try:
			if self.options['use_database']:
				return self.getComplexDataDatabase(accession)
			else:
				return self.getComplexDataJSON(accession)

		except Exception as e:
			return {"status":"Error","error_type":utilities_error.getError()}

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def existsInteractions(self,baits,preys):
		try:
			interactions_existance = {}
			interactions_existance_tdt = ""

			#if isinstance(interactor_a,(list)):
			#	interactor_a = interactor_a[0]

			interactions = self.getInteractions(baits,"accession")['data']

			for bait in baits:
				interactions_existance[bait] = {}

				for prey in preys:
					interactions_existance[bait][prey] = {
								"pmid":{},
								"exists":False,
								"confidence":0,
								"interactors":0
							}

				if bait not in interactions: continue
				
				for prey in preys:
					if prey in interactions[bait]:
						interactions_existance[bait][prey] = {
							"pmid":interactions[bait][prey]['pmids'],
							"exists":True,
							"confidence": interactions[bait][prey]['confidence'],
							'interactors':len(list(interactions[bait].keys()))
						}
					else:
						interactions_existance[bait][prey]['interactors'] = len(list(interactions[bait].keys()))

					if self.options['add_protein_details']:
						queryManagerObj = queryManager.queryManager()
						queryManagerObj.options['dataset_type'] = "uniprot"
						queryManagerObj.options['task'] = "parse_basic"
						queryManagerObj.options['accession'] = prey
						proteins = queryManagerObj.main()

						interactions_existance[bait][prey]['id'] = proteins['data']['id']
						interactions_existance[bait][prey]['gene_name'] = proteins['data']['gene_name']
						interactions_existance[bait][prey]['protein_name'] = proteins['data']['protein_name']
						#interactions_existance[bait][prey]['family'] = proteins['data']['family']
						interactions_existance[bait][prey]['species_common'] = proteins['data']['species_common']
						interactions_existance[bait][prey]['species_scientific'] = proteins['data']['species_scientific']
						
					if self.options['output_format'] == "tdt":
						row = [prey,interactions_existance[bait][prey]["exists"],interactions_existance[bait][prey]["confidence"],",".join(list(interactions_existance[bait][prey]["pmid"].keys()))]
						interactions_existance_tdt += "\t".join([str(x) for x in row]) + "\n"

			if self.options['verbose']:
				print(interactions_existance_tdt)

			if self.options['output_format'] == "tdt":
				return interactions_existance_tdt
			else:
				return interactions_existance

		except Exception as e:
			return {"status":"Error","error_type":utilities_error.getError()}


	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def getLocalisationInteractions(self,interactor_a,localisations):
		try:
			data = {'localised_interactions':{},'interactions':{},"colocalised":{}}

			dataDownloaderObj = uniprotDownloader.uniprotDownloader()
			dataDownloaderObj.options["data_path"] = os.path.abspath(self.options["data_path"])

			query_localisation = dataDownloaderObj.parseLocalisation(interactor_a,generic=True)
			data['query_localisation'] = query_localisation['localisation']

			interactions = self.getInteractions([interactor_a],"accession")['data']

			for a_accession in interactions:
				for b_accession in interactions[a_accession]:


					bait_localisation = dataDownloaderObj.parseLocalisation(b_accession,generic=True)


					if 'localisation' in bait_localisation:
						data['interactions'][b_accession] = interactions[a_accession][b_accession]
						data['interactions'][b_accession]['localisations'] = bait_localisation['localisation']

						localisation_matches = {
						'basic_localisation':[],
						'localisation_go':[],
						'localisation_keyword':[],
						}

						colocalised = False

						bait_localisation = bait_localisation['localisation']

						try:
							localisation_matches['basic_localisation'] = list(set(bait_localisation['basic_localisation']).intersection(data['query_localisation'] ['basic_localisation']))
							if len(localisation_matches['basic_localisation']) > 0: colocalised = True
						except:
							pass

						try:
							localisation_matches['localisation_go'] = list(set(bait_localisation['localisation_go'].keys()).intersection(list(data['query_localisation']['localisation_go'].keys())))
							if len(localisation_matches['localisation_go']) > 0: colocalised = True
						except:
							pass

						try:
							localisation_matches['localisation_keyword'] = list(set(bait_localisation['localisation_keyword']).intersection(data['query_localisation']['localisation_keyword']))
							if len(localisation_matches['localisation_keyword']) > 0: colocalised = True
						except:
							pass

						if len(localisation_matches) > 0:
							if b_accession not in data['localised_interactions']:
								data['localised_interactions'][b_accession] = {}

							data['localised_interactions'][b_accession] = localisation_matches
							data['colocalised'][b_accession] = colocalised

						del bait_localisation

			return {
			"status":"Success",
			"data":data
			}
		except Exception:
			return {"status":"Error","error_type":utilities_error.getError()}

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def collapseByComplex(self,accessions):

		complexes_data_json = {}
		protein_complex_info = {}
		complex_protein_info = {}
		protein_data = {}
		complexes_names = {}
		complexes_sizes = {}
		complexes_sources = {}

		protein_complex_info_headers = [
			"accession",
			"gene_name",
			"protein_name",
			"largest complex",
			"complexes",
			"complex_names",
			"complex_proteins",
			"complex_sizes"
		]

		dataDownloaderObj = uniprotDownloader.uniprotDownloader()
		dataDownloaderObj.options["data_path"] = os.path.abspath(self.options["data_path"])

		for accession in accessions:
			complexes = self.getComplexData(accession)

			complex_protein_info[accession] = {}

			if accession in complexes:
				for complex_instance in complexes[accession]:
					complex_id  = complex_instance['complex_id']

					complexes_names[complex_id] = complex_instance['ComplexName']
					complexes_sizes[complex_id] = complex_instance['Subunits']
					complexes_sources[complex_id] = complex_instance['Source']

					if complex_id not in complexes_data_json:
						complexes_data_json[complex_id] = []

					complexes_data_json[complex_id].append(accession)

		for complex_id in list(complexes_data_json.keys()):
			complex_name = complexes_names[complex_id]

			if len(complexes_data_json[complex_id]) > 1:
				for accession in complexes_data_json[complex_id]:

					complex_name = complexes_names[complex_id]

					if accession in accessions:
						complex_protein_info[accession][str(complex_id)] = {"complex_name":complex_name, "subunits":complexes_data_json[complex_id]}

		for accession in accessions:
			if accession not in protein_data:
				protein_data[accession] = dataDownloaderObj.parseBasic(accession)['data']

			gene_name = "-"
			protein_name = "-"

			if 'gene_name' in protein_data[accession]:
				gene_name = protein_data[accession]['gene_name']

			if 'protein_name' in protein_data[accession]:
				protein_name = protein_data[accession]['protein_name']

			complexes = []
			complex_names = []
			complex_proteins = []
			complex_sizes = []

			largest = ["","","0","0"]
			for complex_id in complex_protein_info[accession]:
				complex_name = complex_protein_info[accession][complex_id]["complex_name"]
				subunits = ",".join(complex_protein_info[accession][complex_id]["subunits"])

				complex_size = len(subunits.split(","))
				complexes.append(complexes_sources[complex_id] + ":" + complex_id)
				complex_names.append(complex_name)
				complex_proteins.append(subunits)
				complex_sizes.append(str(complex_size))

				if complex_size > int(largest[2]):
					largest = [complexes_sources[complex_id] + ":" + complex_id, complex_name,str(complex_size),str(len(complexes_sizes[complex_id]))]

			largest_str = ""

			if largest[2] != "0":
				largest_str = ";".join(largest)

			protein_complex_info[accession] = {
				"accession":accession,
				"gene_name":gene_name,
				"protein_name":protein_name,
				"largest complex":largest_str,
				"complexes":"|".join(complexes),
				"complex_names":"|".join(complex_names),
				"complex_proteins":"|".join(complex_proteins),
				"complex_sizes":"|".join(complex_sizes)
			}

		if self.options["output_format"] == "tdt":

			protein_complex_info_tdt = "\t".join(protein_complex_info_headers)

			for accession in accessions:
				row = []
				for protein_complex_info_header in protein_complex_info_headers:
					row.append(protein_complex_info[accession][protein_complex_info_header])

				protein_complex_info_tdt += "\t".join(row) + "\n"

			print(protein_complex_info_tdt)
			return protein_complex_info_tdt

		if self.options["output_format"] == "JSON":
			return {
				"proteins":protein_complex_info,
				"complex":complex_protein_info
			}

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def getSharedInteractions(self,identifier,number_of_interactors=None,top_interactors=None):
		try:
			data = {
			"shared":{},
			"counter":{}
			}

			interactions = self.getInteractions(identifier,"accession")['data']

			for a_accession in interactions:
				if a_accession not in data["shared"]:
					data["shared"][a_accession] = {}

				for b_accession in interactions[a_accession]:

					if b_accession in identifier:
						data["shared"][a_accession][b_accession] = interactions[a_accession][b_accession]

					if a_accession in identifier:

						if b_accession not in data["shared"]:
							data["shared"][b_accession] = {}

						if a_accession not in data["shared"][b_accession]:
							data["shared"][b_accession][a_accession] = interactions[a_accession][b_accession]

			counter = {}
			for protein in data["shared"]:
				counter[protein] = len(data["shared"][protein])

			data["counter"] = counter

			##------------------------------------------------------------------##

			data["hub"] = {}

			if len(counter) > 0:
				if top_interactors == None and number_of_interactors == None:
					top_interactors = 1


				if number_of_interactors != None:
					for protein in counter:
						if counter[protein] > number_of_interactors:
							data["hub"][protein] = counter[protein]

				if top_interactors != None:
					sorted_counter = sorted(list(counter.items()), key=operator.itemgetter(1))

					for protein_count in sorted_counter[-top_interactors:]:
						data["hub"][protein_count[0]] = protein_count[1]

			del data["shared"]

			return {
			"status":"Success",
			"data":data
			}
		except Exception:
			return {"status":"Error","error_type":utilities_error.getError()}

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def getDomainInteractions(self,interactor_a,interactor_b_domain):
		try:
			data = {}
			dataDownloaderObj = uniprotDownloader.uniprotDownloader()
			dataDownloaderObj.options["data_path"] = os.path.abspath(self.options["data_path"])

			if not isinstance(interactor_a,(list)):
				interactor_a = [interactor_a]

			interactions = self.getInteractions(interactor_a,"accession")["data"]

			for a_accession in interactions:
				for b_accession in interactions[a_accession]:
					if a_accession != None and b_accession != None:
						response = dataDownloaderObj.parseDomains(b_accession)

						if interactor_b_domain in response['data']:
							if b_accession not in data:
								data[b_accession] = {}

							data[b_accession] = interactions[a_accession][b_accession]
							data[b_accession]['domains'] = response

			return {
			"status":"Success",
			"data":data
			}
		except Exception:
			return {"status":"Error","error_type":utilities_error.getError()}

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def getLocalisationByInteractions(self,proteins):
		data = {}

		dataDownloaderObj = uniprotDownloader.uniprotDownloader()
		dataDownloaderObj.options["data_path"] = os.path.abspath(self.options["data_path"])

		interactions = self.getInteractions(proteins,"accession")["data"]

		for a_accession in interactions:
			if a_accession == None: continue
			query_localisation = dataDownloaderObj.parseLocalisation(a_accession,generic=True)

			data[a_accession] = {
				'query_localisation':query_localisation,
				'interactions':{},
				'localised_interactions':{
					"go":{},
					"keyword":{}
					},
				'localised_interactions_details':{
					"go":[],
					"keyword":[]
					}
			}

			go_term_mapping = {}

			for b_accession in interactions[a_accession]:
				response = dataDownloaderObj.parseLocalisation(b_accession,generic=True)

				if b_accession not in data[a_accession]['interactions']:
					if 'localisation' in response:
						try:
							data[a_accession]['interactions'][b_accession] = response['localisation']

							for term in list(response['localisation']['localisation_go'].keys()):
								go_term_mapping[term] = response['localisation']['localisation_go'][term]
								if term not in data[a_accession]['localised_interactions']["go"]: data[a_accession]['localised_interactions']["go"][term] = 0
								data[a_accession]['localised_interactions']["go"][term] += 1

							for term in response['localisation']['localisation_keyword']:
								if term not in data[a_accession]['localised_interactions']["keyword"]: data[a_accession]['localised_interactions']["keyword"][term] = 0
								data[a_accession]['localised_interactions']["keyword"][term] += 1
						except:
							pass

			go = data[a_accession]['localised_interactions']["go"]
			keyword = data[a_accession]['localised_interactions']["keyword"]

			go_details_dict = {}
			for term in go:
				go_details_dict[term] = go_term_mapping[term]

			data[a_accession]['localised_interactions_details']["go"] = go_details_dict
			data[a_accession]['localised_interactions_details']["keyword"] =  list(keyword.keys())

		del go_term_mapping
		return data

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def getInteractionsTable(self,identifiers,identifier_type="accession",output_file=""):
		try:

			if identifiers[0] == "complete_database":
				interaction_data = self.getInteractions(identifiers,"all")['data']
			else:
				interaction_data = self.getInteractions(identifiers,identifier_type)['data']


			interaction_data_reformat = {}
			if identifier_type == "pmid":
				for pmid in interaction_data:
					for interaction in interaction_data[pmid]:
						accession_sorter = [interaction['a_accession'],interaction['b_accession']]
						accession_sorter.sort()
						[a_accession,b_accession] = accession_sorter

						if a_accession not in interaction_data_reformat:
							interaction_data_reformat[a_accession] = {}
						
						if b_accession not in interaction_data_reformat[a_accession]:
							interaction_data_reformat[a_accession][b_accession] = {"pmids":{},"confidence":0}

						interaction_data_reformat[a_accession][b_accession]['pmids'][pmid] = {
							"source":interaction['source'],
							"method":interaction['method']
						}

						if float(interaction['confidence']) > interaction_data_reformat[a_accession][b_accession]['confidence']:
							interaction_data_reformat[a_accession][b_accession]['confidence'] = float(interaction['confidence'])
				
				interaction_data = interaction_data_reformat
				
			pprint.pprint(interaction_data)

			dataDownloaderObj = uniprotDownloader.uniprotDownloader()
			dataDownloaderObj.options["data_path"] = os.path.abspath(self.options["data_path"])

			pmidDownloaderObj = pmidDownloader.pmidDownloader()
			pmidDownloaderObj.options["data_path"] = os.path.abspath(self.options["data_path"])

			interaction_data_header = [
				"hub accession",
				"hub id",
				"hub gene",
				"hub protein",
				"interactor accession",
				"interactor id",
				"interactor gene",
				"interactor protein",
				"method",
				"confidence",
				"source",
				"reference"
			]

			if self.options['add_paper_details']:
				interaction_data_header += [
					"authors",
					"title",
					"journal",
					"article_year",
					"article_details"
				]

			protein_data = {}
			article_data = {}
			interaction_data_rows = []

			for hub in interaction_data:
				response = dataDownloaderObj.parseBasic(hub)
				if response["status"] == "Success":
					protein_data[hub] = response['data']

				for interactor in interaction_data[hub]:
					try:
						if interactor not in protein_data:
							if interactor != None:
								response= dataDownloaderObj.parseBasic(interactor)
								if response["status"] == "Success":
									protein_data[interactor] = response['data']

									interaction_data_row = {}
									interaction_data_row["hub accession"] = hub
									interaction_data_row["hub id"] = protein_data[hub]['id']
									interaction_data_row["hub gene"] = protein_data[hub]['gene_name']
									interaction_data_row["hub protein"] = protein_data[hub]['protein_name']
									interaction_data_row["interactor accession"] = interactor
									interaction_data_row["interactor id"] = protein_data[interactor]['id']
									interaction_data_row["interactor gene"] = protein_data[interactor]['gene_name']
									interaction_data_row["interactor protein"] = protein_data[interactor]['protein_name']

									if 'confidence' in interaction_data[hub][interactor]:
										interaction_data_row["confidence"] = str(interaction_data[hub][interactor]['confidence'])
									else:
										interaction_data_row["confidence"] = []

									interaction_data_row["reference"] = []
									interaction_data_row["source"] = []
									interaction_data_row["method"] = []

									for paper in interaction_data[hub][interactor]['pmids']:

											if hub in protein_data and interactor in protein_data:

												if self.options['add_paper_details']:
													interaction_data_row["reference"] = paper
													interaction_data_row["method"] = interaction_data[hub][interactor]['pmids'][paper]['method'].split("|")
													interaction_data_row["source"] = interaction_data[hub][interactor]['pmids'][paper]['source']

													if paper not in article_data:
														article_data[paper] = pmidDownloaderObj.parsePMID(paper)

													if 'data' in article_data[paper]:
														interaction_data_row["authors"] = article_data[paper]['data']['long_author']
														interaction_data_row["title"] = article_data[paper]['data']['title']
														interaction_data_row["journal"] = article_data[paper]['data']['journal']
														interaction_data_row["article_year"] = article_data[paper]['data']['article_year']
														interaction_data_row["article_details"] = article_data[paper]['data']['article_details']

													interaction_data_rows.append(interaction_data_row)
												else:
													interaction_data_row["reference"].append(paper)
													interaction_data_row["source"] += interaction_data[hub][interactor]['pmids'][paper]['source'].split("|")

													if interaction_data[hub][interactor]['pmids'][paper]['source'] != 'STRING':
														interaction_data_row["method"] += interaction_data[hub][interactor]['pmids'][paper]['method'].split("|")

									if not self.options['add_paper_details']:
										interaction_data_row["reference"] = list(set(interaction_data_row["reference"]))
										interaction_data_row["method"] = list(set(interaction_data_row["method"]))
										interaction_data_row["source"] = list(set(interaction_data_row["source"]))
										interaction_data_rows.append(interaction_data_row)

					except Exception:
						print("ERROR @ getInteractionsTable - ",utilities_error.getError())
						raise

			#### NOT WORKING
			if self.options['output_format'] =='tdt':
				interaction_data_table_str = "\t".join(interaction_data_header) + "\n"

				for interaction_data_row in interaction_data_rows:
					row = []
					for header in interaction_data_header:
						if isinstance(interaction_data_row[header],(list)):
							row.append(",".join(interaction_data_row[header]))
						else:
							row.append(interaction_data_row[header])

					interaction_data_table_str += "\t".join(row) + "\n"

				return interaction_data_table_str

				"""
				if self.options['verbose']:
					print(str([x for x in interaction_data_table_str.strip() if x in printable] ))

				if output_file != "":
					if self.options['verbose']:print("Writing to:",output_file)
					open(output_file,"w").write(interaction_data_table_str.encode('utf-8'))
				"""
			else:
				return {
				"status":"Success",
				"header":interaction_data_header,
				"data":interaction_data_rows
			}
		except Exception:
			return {"status":"Error","error_type":utilities_error.getError()}
	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def getExtendedNetwork(self,identifiers):
		try:
			data = self.getInteractions(identifiers,"accession")

			extended_interactors = []
			if 'data' in data:
				interaction = data['data']

				for protein_a in interaction:
					extended_interactors += interaction[protein_a]

			return {
			"status":"Success",
			"data":extended_interactors
			}
		except Exception as e:
			return {"status":"Error","error_type":utilities_error.getError()}

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def getScaffolds(self,identifiers_a,identifiers_b,detail_level="detailed"):
		try:

			if not isinstance(identifiers_a,(list)):
				identifiers_a = [identifiers_a]

			if not isinstance(identifiers_b,(list)):
				identifiers_b = [identifiers_b]

			data = self.getInteractions(identifiers_a + identifiers_b,"accession")

			extended_interactors = {}

			if 'data' in data:
				interaction = data['data']

				for protein_b in identifiers_b:
					extended_interactors[protein_b] = {"all":{"direct":False,"indirect":[]}}

					if protein_b in interaction:
						for protein_a in identifiers_a:
							if protein_a in interaction:
								if protein_b in list(interaction[protein_a].keys()):
									extended_interactors[protein_b]["all"]["direct"] = True

								extended_interactors[protein_b]["all"]["indirect"] += list(set(interaction[protein_a].keys()).intersection(set(interaction[protein_b].keys())))

								if detail_level == "detailed":
									extended_interactors[protein_b][protein_a] = {
										"direct":protein_b in list(interaction[protein_a].keys()),
										"indirect":list(set(interaction[protein_a].keys()).intersection(set(interaction[protein_b].keys())))
										}

									extended_interactors[protein_b][protein_a]["shared_interactors"] = len(extended_interactors[protein_b][protein_a]["indirect"])
									extended_interactors[protein_b][protein_a]["bait_interactors"] = len(list(interaction[protein_a].keys()))
									extended_interactors[protein_b][protein_a]["prey_interactors"] = len(list(interaction[protein_b].keys()))

					#extended_interactors[protein_b]["all"]["indirect"]  = list(set(extended_interactors[protein_b]["all"]["indirect"]))

					protein_counter = {}

					for accession in extended_interactors[protein_b]["all"]["indirect"]:
						if accession not in protein_counter:
							protein_counter[accession] = 0

						protein_counter[accession] += 1

					extended_interactors[protein_b]["all"]["indirect"] = protein_counter

				counter = {}
				for protein_b in identifiers_b:
					for accession in extended_interactors[protein_b]["all"]["indirect"]:
						if accession not in counter:
							counter[accession] = 0

						counter[accession] += 1

			return {
			"status":"Success",
			"data":{"scaffolds":extended_interactors,"scaffold_counts":counter}
			}
		except Exception:
			return {"status":"Error","error_type":utilities_error.getError()}

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def getSubNetworks(self,identifiers):
		try:
			data = self.getInteractions(identifiers,"pairwise")

			interaction_matrix = {}
			if 'data' in data:
				interaction = data['data']

				for protein_a in identifiers:
					interaction_matrix[protein_a] = {}
					for protein_b in identifiers:
						interaction_matrix[protein_a][protein_b] = 0
						if protein_a in interaction:
							if protein_b in interaction[protein_a]:
								interaction_matrix[protein_a][protein_b] = len(interaction[protein_a][protein_b]['pmids'])
								interaction_matrix[protein_a][protein_b] = float(list(interaction[protein_a][protein_b]['pmids'].values())[0]['confidence'])
						else:
							interaction_matrix[protein_a][protein_b] = 0

			else:
				return {"status":"Error","error_type":"No interactions returned"}

			return {
			"status":"Success",
			"data":interaction_matrix
			}
		except Exception:
			return {"status":"Error","error_type":utilities_error.getError()}

	############################################################################################################################################
	############################################################################################################################################
	############################################################################################################################################
	######
	######   NetworkX  NetworkX  NetworkX NetworkX  NetworkX NetworkX   NetworkX   NetworkX NetworkX   NetworkX  NetworkX NetworkX
	######
	############################################################################################################################################
	############################################################################################################################################
	############################################################################################################################################

	def getSubNetworkPlot(self,identifiers):
		try:
			data = self.getInteractions(identifiers,"pairwise")

			if 'data' in data:
				interaction = data['data']

				import pip
				package = "networkx"

				try:
					__import__(package)
				except ImportError:
					pip.main(['install', package])

				import matplotlib.pyplot as plt
				import networkx as nx

				G = nx.Graph()

				for protein_a in identifiers:
					for protein_b in identifiers:
						if protein_a in interaction:
							if protein_b in interaction[protein_a]:
								#G.add_edge(protein_a, protein_b, weight= len(interaction[protein_a][protein_b]['pmids']))
								if protein_a != protein_b:
									G.add_edge(protein_a, protein_b, weight= float(list(interaction[protein_a][protein_b]['pmids'].values())[0]['confidence']))


				elarge = [(u, v) for (u, v, d) in G.edges(data=True) if d['weight'] > 0.85]
				esmall = [(u, v) for (u, v, d) in G.edges(data=True) if d['weight'] <= 0.85]

				pos = nx.spring_layout(G,scale=10,k=100)  # positions for all nodes

				# nodes
				nx.draw_networkx_nodes(G, pos, node_size=1500)

				# edges
				nx.draw_networkx_edges(G, pos, edgelist=elarge,
									width=6)
				nx.draw_networkx_edges(G, pos, edgelist=esmall,
									width=6, alpha=0.5, edge_color='b', style='dashed')

				# labels
				nx.draw_networkx_labels(G, pos, font_size=10, font_family='sans-serif')

				plt.axis('off')
				plt.show()
				fig.savefig(identifiers[0] + '.png')

				return {"status":"Success"}

		except Exception:
			return {"status":"Error","error_type":utilities_error.getError()}

	############################################################################################################################################
	############################################################################################################################################
	############################################################################################################################################
	######
	######   CYTOSCAPE  CYTOSCAPE  CYTOSCAPE CYTOSCAPE  CYTOSCAPE CYTOSCAPE   CYTOSCAPE   CYTOSCAPE CYTOSCAPE   CYTOSCAPE  CYTOSCAPE CYTOSCAPE
	######
	############################################################################################################################################
	############################################################################################################################################
	############################################################################################################################################

	def makeCytoscapeJson(self,accession,use_secondary_interactions = True,output_file=""):
		try:
			cytoscape_json = {}
			cytoscape_json_path = os.path.join(self.options["cytoscape_path_database"],accession + "." + str(self.options["cutoff"]) + ".json")

			if os.path.exists(cytoscape_json_path) and not self.options["remake"]:
				with open(cytoscape_json_path) as data_file:
					if self.options['verbose']: print(("Reading", cytoscape_json_path))
					cytoscape_json = json.load( data_file)

			else:
				protein_data = {}
				article_data = {}

				interaction_data = self.getInteractions([accession],"accession")['data']

				if use_secondary_interactions:
					interaction_secondary_data = self.getInteractions(list(interaction_data[accession].keys()),"pairwise")['data']
					interaction_data.update(interaction_secondary_data)


				complex_data = self.getComplexData(accession)

				dataDownloaderObj = uniprotDownloader.uniprotDownloader()
				dataDownloaderObj.options["data_path"] = os.path.abspath(self.options["data_path"])

				pmidDownloaderObj = pmidDownloader.pmidDownloader()
				pmidDownloaderObj.options["data_path"] = os.path.abspath(self.options["data_path"])

				added_nodes = []
				added_edges = []
				cytoscape_json = []
				cytoscape_json_nodes = []
				cytoscape_json_edges = []


				#####

				interactor_accessions = list(interaction_data.keys())

				for hub in interaction_data:
					if hub in complex_data:
						for complex in complex_data[hub]:
							for interactor in complex['Subunits']:
								interactor_accessions.append(interactor)

					interactor_accessions += list(interaction_data[hub].keys())

				protein_data = self.getProteinDatabase(list(set(interactor_accessions)))

				#####

				e_count = 0

				for hub in interaction_data:

					if hub in protein_data:
						try:
							if hub == accession:
								query = True
							else:
								query = False

							node = {
							"data": {
								"id": hub,
								"idInt":hub,
								"name":  protein_data[hub]['gene_name'],
								"score": 1,
								"query": query,
								"gene": True
							},
							"position": {
								"x": 1,
								"y": 1
							},
							"group": "nodes",
							"removed": False,
							"selected": False,
							"selectable": True,
							"locked": False,
							"grabbed": False,
							"grabbable": True
							}

							added_nodes.append(hub)
							if node not in added_nodes:
								cytoscape_json_nodes.append(node)
						except:
							print("ERROR @ makeCytoscapeJson -",protein_data)

					if hub in complex_data:
						for complex in complex_data[hub]:
							for interactor in complex['Subunits']:
								try:
									gene_name = interactor
									if interactor in protein_data:
										gene_name = protein_data[interactor]['gene_name']

									node = {
									"data": {
										"id":interactor,
										"idInt": interactor,
										"name": gene_name,
										"score": 2,
										"query": False,
										"gene": True
									},
									"position": {
										"x": 1,
										"y": 1
									},
									"group": "nodes",
									"removed": False,
									"selected": False,
									"selectable": True,
									"locked": False,
									"grabbed": False,
									"grabbable": True
									}

									added_nodes.append(interactor)
									if node not in added_nodes:
										cytoscape_json_nodes.append(node)

								except Exception:
									print("ERROR @ makeCytoscapeJson - complex 1",utilities_error.getError())
									raise

						for complex in complex_data[hub]:
							for interactor in complex['Subunits']:
								try:
									if interactor ==  "UNKNOWN": continue
									if interactor not in protein_data:
										if self.options['verbose']: print("Skipping",interactor)
										continue

									for interactor_2 in complex['Subunits']:

										e_count += 1

										if interactor in added_nodes and interactor_2 in added_nodes:

											if interactor == accession or interactor_2 == accession:
												group = "primary_complex"
											else:
												group = "secondary_complex"

											edge_labels = [interactor,interactor_2]
											edge_labels.sort()
											edge_check = "_".join(edge_labels)

											if edge_check not in added_edges:
												added_edges.append(edge_check)
												edge = {
												"data": {
													"source":interactor,
													"target": interactor_2,
													"weight": 50,
													"group": group,
													"networkId": 1,
													"networkGroupId": 1,
													"intn": True,
													"rIntnId": 1,
													"id": "e" + str(e_count)
												},
												"position": {},
												"group": "edges",
												"removed": False,
												"selected": False,
												"selectable": True,
												"locked": False,
												"grabbed": False,
												"grabbable": True
												}

												cytoscape_json_edges.append(edge)
										#else:
										#	print interactor,interactor_2

								except Exception:
									print("ERROR @ makeCytoscapeJson - complex 2",utilities_error.getError())
									raise


					if hub in interaction_data:
						for interactor in interaction_data[hub]:
							try:

								if interactor ==  "UNKNOWN": continue
								if interactor not in protein_data:
									if self.options['verbose']: print("Skipping",interactor)
									continue


								try:
									node = {
									"data": {
										"id": interactor,
										"idInt": interactor,
										"name":  protein_data[interactor]['gene_name'],
										"score": 1,
										"query": False,
										"gene": True
									},
									"position": {
										"x": 1,
										"y": 1
									},
									"group": "nodes",
									"removed": False,
									"selected": False,
									"selectable": True,
									"locked": False,
									"grabbed": False,
									"grabbable": True
									}

									added_nodes.append(interactor)
									if node not in added_nodes:
										cytoscape_json_nodes.append(node)
								except Exception:
									print(utilities_error.getError())
									raise


								if hub in added_nodes and interactor in added_nodes:
									e_count += 1

									if hub == accession or interactor == accession:
										edge_weight = float(interaction_data[hub][interactor]['confidence'])
										edge_weight = edge_weight*edge_weight*3
										group = "primary"
									else:
										edge_weight = float(interaction_data[hub][interactor]['confidence'])
										edge_weight = edge_weight*edge_weight
										edge_weight = edge_weight/2
										group = "secondary"

									edge_labels = [interactor,hub]
									edge_labels.sort()
									edge_check = "_".join(edge_labels)

									if edge_check not in added_edges:
										added_edges.append(edge_check)

										edge = {
										"data": {
											"source":hub,
											"target": interactor,
											"weight": edge_weight,
											"group": group,
											"networkId": 1,
											"networkGroupId": 1,
											"intn": True,
											"rIntnId": 1,
											"id": "e" + str(e_count)
										},
										"position": {},
										"group": "edges",
										"removed": False,
										"selected": False,
										"selectable": True,
										"locked": False,
										"grabbed": False,
										"grabbable": True
										}

										cytoscape_json_edges.append(edge)

								"""
								for paper in interaction_data[hub][interactor]['pmids']:
									try:
										if paper not in article_data:
											article_data[paper] = pmidDownloaderObj.parsePMID(paper)
									except Exception,e:
										print e
								"""

							except Exception:
								print("ERROR @ makeCytoscapeJson - interactor",utilities_error.getError())
								raise

				cytoscape_json = cytoscape_json_nodes + cytoscape_json_edges

				cytoscape_json_path = os.path.join(self.options["cytoscape_path_database"],accession + "." + str(self.options["cutoff"]) + ".json")

				with open(cytoscape_json_path,"w") as outfile:
					if self.options['verbose']: print(("Writing", cytoscape_json_path))
					json.dump(cytoscape_json, outfile)

			return cytoscape_json

		except Exception:
			return {"status":"Error","error_type":utilities_error.getError()}

	############################################################################################################################################
	############################################################################################################################################
	############################################################################################################################################
	######
	######   DOWNLOADERS  DOWNLOADERS  DOWNLOADERS DOWNLOADERS  DOWNLOADERS DOWNLOADERS   DOWNLOADERS   DOWNLOADERS DOWNLOADERS   DOWNLOADERS
	######
	############################################################################################################################################
	############################################################################################################################################
	############################################################################################################################################

	def check_directory(self):
		if not os.path.exists(os.path.join(self.options["data_path"], "interactions")):
			os.mkdir(os.path.join(self.options["data_path"], "interactions"))

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def downloadData(self,url,out_path,method='POST'):
		sessionDownloaderObj = utilities_downloader.sessionDownloader()
		status = sessionDownloaderObj.download_file(url,out_path,method=method,replace_empty=True)

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def downloadIntact(self):
		url = "ftp://ftp.ebi.ac.uk/pub/databases/intact/current/psimitab/intact.txt"
		out_path = os.path.join(self.options["data_path"],"interactions","intact.psitab.txt")

		sessionDownloaderObj = utilities_downloader.sessionDownloader()
		response = sessionDownloaderObj.download_file(url,out_path,method="FTP")


	def parseIntact(self,accession):
		if self.options['verbose']: print("Downloading Intact")
		if not os.path.exists(os.path.join(self.options["data_path"],"interactions","./intact.psitab.txt")):
			self.downloadIntact()
		if self.options['verbose']: print("Parsing Intact")


		if not isinstance(accession,list):
			accession = [accession]

		header = True

		data = {}

		with open(os.path.join(self.options["data_path"],"interactions","./intact.psitab.txt")) as f:
			for line in f:
				#print line
				if header:
					if self.options['verbose']: print("Parsing Intact")

					intact_header = line.split("\t")

					# print("header",intact_header)
					a_accession_index = intact_header.index("#ID(s) interactor A")
					b_accession_index = intact_header.index("ID(s) interactor B")
					method_index = intact_header.index("Interaction detection method(s)")
					pmid_index = intact_header.index("Publication Identifier(s)")
					confidence_index = intact_header.index("Confidence value(s)")
					id_index = intact_header.index("Interaction identifier(s)")
					source_index = intact_header.index("Source database(s)")

					header = False
				else:

					line_bits = line.split("\t")
					if len(line_bits) < 2: continue

					acc_A = line_bits[a_accession_index]
					acc_B = line_bits[b_accession_index]

					if acc_A.startswith("uniprotkb"):
						acc_A = acc_A.split(":")[1].strip().split("-")[0].strip()
					else:
						continue

					if acc_B.startswith("uniprotkb"):
						acc_B = acc_B.split(":")[1].strip().split("-")[0].strip()
					else:
						continue

					if acc_A not in accession and acc_B not in accession: continue


					confidence = line_bits[confidence_index].split(":")[-1]
					method = line_bits[method_index].replace("psi-mi:","")

					pmid_count = 0
					pmids = []
					references = line_bits[pmid_index]
					for reference in references.split("|"):
						if reference.split(":")[0] == "pubmed":
							pmid = reference.split(":")[1]
							pmid_count += 1

							if 'unassigned' in pmid.lower(): continue
							pmids.append(pmid)

					pmids = list(set(pmids))
					source = line_bits[source_index].replace("psi-mi:","")
					source = source.split("|")

					ids = line_bits[id_index].split("|")

					if acc_A in accession:
						if acc_A not in data:
							data[acc_A] = {}

						if acc_B not in data[acc_A]:
							data[acc_A][acc_B] = {"source": [], "references": {}, "confidence": confidence}

						data[acc_A][acc_B]['source'].extend(ids)
						data[acc_A][acc_B]['source'] = list(set(data[acc_A][acc_B]['source']))

						for pmid in pmids:
							if pmid in data[acc_A][acc_B]['references']:
								continue
							data[acc_A][acc_B]['references'][pmid] = {"method":[method], "source": source}

					else:
						if acc_B not in data:
							data[acc_B] = {}

						if acc_A not in data[acc_B]:
							data[acc_B][acc_A] = {"source": [], "references": {}, "confidence": confidence}

						data[acc_B][acc_A]['source'].extend(ids)
						data[acc_B][acc_A]['source'] = list(set(data[acc_B][acc_A]['source']))

						for pmid in pmids:
							if pmid in data[acc_B][acc_A]['references']:
								continue
							data[acc_B][acc_A]['references'][pmid] = {"method":[method], "source": source}

		return data



	def loadIntactInterfaces(self,interactions={},counter = 0):
		self.check_directory()
		proteins = []
		mappings = {}

		binding_regions_tags = ['sufficient binding region',
		#'binding-associated region',
		#'necessary binding region',
		#'direct binding region'
		]

		if self.options['verbose']: print("Downloading Intact")
		if not os.path.exists(os.path.join(self.options["data_path"],"interactions","./intact.psitab.txt")):
			self.downloadIntact()

		header = True

		tags = {}
		binding_regions = {}
		mutations = {}
		features_count = {}

		with open(os.path.join(self.options["data_path"],"interactions","./intact.psitab.txt")) as f:
			for line in f:
				#print line
				if header:
					if self.options['verbose']: print("Parsing Intact")

					intact_header = line.split("\t")

					a_accession_index = intact_header.index("#ID(s) interactor A")
					b_accession_index = intact_header.index("ID(s) interactor B")
					a_gene_index = intact_header.index("Alias(es) interactor A")
					b_gene_index = intact_header.index("Alias(es) interactor B")
					method_index = intact_header.index("Interaction detection method(s)")
					pmid_index = intact_header.index("Publication Identifier(s)")
					a_feature_index = intact_header.index("Feature(s) interactor A")
					b_feature_index = intact_header.index("Feature(s) interactor B")
					interaction_annotation_index = intact_header.index("Interaction annotation(s)")
					confidence_index = intact_header.index("Confidence value(s)")

					header = False
				else:

					line_bits = line.split("\t")

					try:
						lineBits = line.strip().split("\t")

						if lineBits[a_accession_index] == "-" or  lineBits[b_accession_index] == "-": continue

						a_accession = ""
						b_accession = ""

						for accession in lineBits[a_accession_index].split("|"):
							if accession.split(":")[0] == "uniprotkb":
								a_accession = accession.split(":")[1].split("-")[0]

						for accession in lineBits[b_accession_index].split("|"):
							if accession.split(":")[0] == "uniprotkb":
								b_accession = accession.split(":")[1].split("-")[0]

						if a_accession == "" or b_accession == "": continue

						a_gene = lineBits[a_gene_index].split("(gene name)")[0].split(":")[0]
						b_gene = lineBits[b_gene_index].split("(gene name)")[0].split(":")[0]
						confidence = lineBits[confidence_index].split(":")[-1]
						method = lineBits[method_index].replace("psi-mi:","")

						if method.count("cross-linking") > 0: continue

						references = lineBits[pmid_index]
						for reference in references.split("|"):
							if reference.split(":")[0] == "pubmed":
								pmid = reference.split(":")[1]

						##############################

						a_features = lineBits[a_feature_index].split("|")
						b_features = lineBits[b_feature_index].split("|")
						interaction_annotation = lineBits[interaction_annotation_index]

						binding_regions_tmp = {
							a_accession:[],
							b_accession:[]
						}

						##############################

						prey_start  = 0
						prey_end = 0

						for a_feature in a_features:
							if a_feature == "-": continue

							feature_type = a_feature.split(":")[0]
							feature_region = a_feature.split(":")[1]
							#print a_accession,"\t",b_accession,"\t",pmid,"\t",feature_type,"\t",feature_region

							if a_accession not in mutations:
								mutations[a_accession] = []

							if a_accession not in tags:
								tags[a_accession] = {}

							if a_accession not in binding_regions:
								binding_regions[a_accession] = {}

							#if feature_type not in features[a_accession]: features[a_accession][feature_type] = []

							start = feature_region.split("(")[0].split("-")[0].split("..")[0]
							end = feature_region.split("(")[0].split("-")[1].split("..")[-1]
							feature = {"pmid":pmid,"start":start,"end":end}

							if feature_type in protein_tags:
								if feature_type not in tags[a_accession]:
									tags[a_accession][feature_type] = []

								if start in ["c","n","?"]:
									tag = {"pmid":pmid,"terminus":start}
									if tag not in tags[a_accession][feature_type]:
										tags[a_accession][feature_type].append(tag)
								else:
									tag = {"pmid":pmid,"start":start,"end":end}
									if tag not in tags[a_accession][feature_type]:
										tags[a_accession][feature_type].append(tag)

							elif feature_type.count('mutation') > 0:
								mutations[a_accession].append({"pmid":pmid,"start":start,"end":end,"effect":feature_type,"prey":b_accession})
							elif feature_type in binding_regions_tags:
								#if b_accession not in binding_regions[a_accession]:
								#	binding_regions[a_accession][b_accession] = []

								if start.isdigit() and end.isdigit():
									interface = {"pmid":pmid,"start":start,"end":end,"type":feature_type}
									#binding_regions[a_accession][b_accession].append(interface)

									if interface not in binding_regions_tmp[a_accession]:
										binding_regions_tmp[a_accession].append(interface)
							else:
								if feature_type not in features_count: features_count[feature_type] = 0
								features_count[feature_type] += 1


						for b_feature in b_features:
							if b_feature == "-": continue

							feature_type = b_feature.split(":")[0]
							feature_region = b_feature.split(":")[1]

							if b_accession not in mutations:
								mutations[b_accession] = []
							if b_accession not in tags:
								tags[b_accession] = {}
							if b_accession not in binding_regions:
								binding_regions[b_accession] = {}

							start = feature_region.split("(")[0].split("-")[0]
							end = feature_region.split("(")[0].split("-")[1]
							feature = {"pmid":pmid,"start":start,"end":end}

							if feature_type in protein_tags:
								if feature_type not in tags[b_accession]:
									tags[b_accession][feature_type] = []

								if start in ["c","n","?"]:
									tag = {"pmid":pmid,"terminus":start}
									if tag not in tags[b_accession][feature_type]:
										tags[b_accession][feature_type].append(tag)
								else:
									tag = {"pmid":pmid,"start":start,"end":end}
									if tag not in tags[b_accession][feature_type]:
										tags[b_accession][feature_type].append(tag)

							elif feature_type.count('mutation') > 0:
								mutations[b_accession].append({"pmid":pmid,"start":start,"end":end,"effect":feature_type,"prey":a_accession})
							elif feature_type in binding_regions_tags:
								#if a_accession not in binding_regions[b_accession]:
								#	binding_regions[b_accession][a_accession] = []

								if start.isdigit() and end.isdigit():
									interface = {"pmid":pmid,"start":start,"end":end,"type":feature_type}
									#binding_regions[b_accession][a_accession].append(interface)

									if interface not in binding_regions_tmp[b_accession]:
										binding_regions_tmp[b_accession].append(interface)
							else:
								if feature_type not in features_count: features_count[feature_type] = 0
								features_count[feature_type] += 1

						a_binding_region = {
							"start":"",
							"end":"",
							"pmid":""
							}

						b_binding_region = {
							"start":"",
							"end":"",
							"pmid":""
						}

						if len(binding_regions_tmp[a_accession]) == 1:
							a_binding_region = {
								"start":binding_regions_tmp[a_accession][0]['start'],
								"end":binding_regions_tmp[a_accession][0]['end'],
								"pmid":binding_regions_tmp[a_accession][0]['pmid']
							}

						if len(binding_regions_tmp[b_accession]) == 1:
							b_binding_region = {
								"start":binding_regions_tmp[b_accession][0]['start'],
								"end":binding_regions_tmp[b_accession][0]['end'],
								"pmid":binding_regions_tmp[b_accession][0]['pmid']
							}
							counter += 1

						if len(binding_regions_tmp[a_accession]) == 1:
							interface = {
								"bait_start":a_binding_region['start'],
								"bait_end":a_binding_region['end'],
								"prey_start":b_binding_region['start'],
								"prey_end":b_binding_region['end'],
								"pmid":a_binding_region['pmid'],
								"method":method,
								"confidence":confidence
							}

							if b_accession not in binding_regions[a_accession]:
								binding_regions[a_accession][b_accession] = []

							if interface not in binding_regions[a_accession][b_accession]:
								binding_regions[a_accession][b_accession].append(interface)

						if len(binding_regions_tmp[b_accession]) == 1:
							interface = {
								"bait_start":b_binding_region['start'],
								"bait_end":b_binding_region['end'],
								"prey_start":a_binding_region['start'],
								"prey_end":a_binding_region['end'],
								"pmid":b_binding_region['pmid'],
								"method":method,
								"confidence":confidence
							}

							if a_accession not in binding_regions[b_accession]:
								binding_regions[b_accession][a_accession] = []

							if interface not in binding_regions[b_accession][a_accession]:
								binding_regions[b_accession][a_accession].append(interface)

					except:
						print(lineBits)
						raise

		self.options["intact_features_file"] = os.path.join(self.options["data_path"],"interactions","./intact.tags.json")
		with open(self.options["intact_features_file"],"w") as outfile:
			print(("Writing:" + self.options["intact_features_file"]))
			json.dump(tags, outfile)

		self.options["intact_features_file"] = os.path.join(self.options["data_path"],"interactions","./intact.binding_regions.json")
		with open(self.options["intact_features_file"],"w") as outfile:
			print(("Writing:" + self.options["intact_features_file"]))
			json.dump(binding_regions, outfile)

		self.options["intact_features_file"] = os.path.join(self.options["data_path"],"interactions","./intact.mutations.json")
		with open(self.options["intact_features_file"],"w") as outfile:
			print(("Writing:" + self.options["intact_features_file"]))
			json.dump(mutations, outfile)

		del tags
		del binding_regions
		del mutations

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def downloadSTRING(self):
		download_url = "https://stringdb-static.org/download/protein.links.full.v11.0/9606.protein.links.full.v11.0.txt.gz"
		out_path = os.path.join(self.options["data_path"],"interactions","./string.9606.tsv.gz")
		unzipped_out_path = os.path.join(self.options["data_path"],"interactions","./string.9606.tsv")
		self.downloadData(download_url,out_path)

		if os.path.exists(out_path) and not os.path.exists(unzipped_out_path):
			import gzip
			import shutil
			with gzip.open(out_path, 'rb') as f_in:
				with open(unzipped_out_path, 'wb') as f_out:
					shutil.copyfileobj(f_in, f_out)

		download_url = "https://string-db.org/mapping_files/uniprot/human.uniprot_2_string.2018.tsv.gz"
		out_path = os.path.join(self.options["data_path"],"interactions","./string.9606.mapping.tsv.gz")
		unzipped_out_path = os.path.join(self.options["data_path"],"interactions","./string.9606.mapping.tsv")
		self.downloadData(download_url,out_path)

		if os.path.exists(out_path) and not os.path.exists(unzipped_out_path):
			import gzip
			import shutil
			with gzip.open(out_path, 'rb') as f_in:
				with open(unzipped_out_path, 'wb') as f_out:
					shutil.copyfileobj(f_in, f_out)

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def downloadComplexPortal(self):
		download_url = "ftp://ftp.ebi.ac.uk/pub/databases/intact/complex/current/complextab/homo_sapiens.tsv"
		out_path = os.path.join(self.options["data_path"],"interactions","./complex_portal.complextab.9606.tsv")

		self.downloadData(download_url,out_path,method='FTP')

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def parseComplexPortal(self):
		complex_portal_path = os.path.join(self.options["data_path"],"interactions","./complex_portal.complextab.9606.tsv")
		complexes = open(complex_portal_path).read().strip().split("\n")
		complexes_header = complexes[0].strip("#").split("\t")
		for line in complexes[1:]:
			line_bits = line.split("\t")

			complex = {"source":"complex_portal"}

			for i in range(0,len(complexes_header)):
				complex[complexes_header[i]] = line_bits[i]

			subunits = complex["Identifiers (and stoichiometry) of molecules in complex"]
			subunit_accessions = []
			for subunit in subunits.split("|"):
				subunit_accession = subunit.split("(")[0]
				subunit_accessions.append(subunit_accession)
				if subunit_accession not in self.subunits:
					self.subunits[subunit_accession] = {}

				if "complex_portal" not in self.subunits[subunit_accession]:
					self.subunits[subunit_accession]["complex_portal"] = []


				self.subunits[subunit_accession]["complex_portal"].append(complex["Complex ac"])

			complex['subunit_accessions'] = subunit_accessions
			self.complexes["complex_portal"][complex["Complex ac"]] = complex
			self.complexes["names"][complex["Complex ac"]] = complex['Recommended name']

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def downloadCorum(self):

		download_url = "https://mips.helmholtz-muenchen.de/corum/download/coreComplexes.json.zip"
		out_dir = os.path.join(self.options["data_path"],"interactions")
		out_path = os.path.join(self.options["data_path"],"interactions","./coreComplexes.json.zip")
		unzipped_out_path = os.path.join(self.options["data_path"],"interactions","./coreComplexes.json")
		self.downloadData(download_url,out_path)

		if os.path.exists(out_path) and not os.path.exists(unzipped_out_path):
			import zipfile
			with zipfile.ZipFile(out_path, 'r') as zip_ref:
				zip_ref.extractall(out_dir)

		download_url = "https://mips.helmholtz-muenchen.de/corum/download/uniprot_corum_mapping.txt"
		out_path = os.path.join(self.options["data_path"],"interactions","./uniprot_corum_mapping.txt")
		self.downloadData(download_url,out_path)

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def parseCorum(self):
		corum_path = os.path.join(self.options["data_path"],"interactions","./coreComplexes.json")
		with open(corum_path) as data_file:
			corum_data = json.load(data_file)


		for complex in corum_data:
			complex["source"] = "corum"


			subunits = complex["subunits(UniProt IDs)"]

			for subunit_accession in subunits.split(";"):

				if subunit_accession not in self.subunits:
					self.subunits[subunit_accession] = {}

				if "corum" not in self.subunits[subunit_accession]:
					self.subunits[subunit_accession]["corum"] = []

				self.subunits[subunit_accession]["corum"].append(complex["ComplexID"])

			complex['subunit_accessions'] = subunits.split(";")
			self.complexes["corum"][complex["ComplexID"]] = complex
			self.complexes["names"][complex["ComplexID"]] = complex['ComplexName']

		#pprint.pprint(self.subunits)


	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def downloadHIPPIE(self):
		download_url = "http://cbdm-01.zdv.uni-mainz.de/~mschaefer/hippie/HIPPIE-current.mitab.txt"
		out_path = os.path.join(self.options["data_path"],"interactions","./HIPPIE-current.mitab.txt")
		self.downloadData(download_url,out_path)

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def grabHIPPIE(self,interactions={},counter = 0):
		self.check_directory()
		proteins = []
		mappings = {}

		print("Downloading HIPPIE")
		if not os.path.exists(os.path.join(self.options["data_path"],"interactions","./HIPPIE-current.mitab.txt")):
			self.downloadHIPPIE()

		print("Parsing HIPPIE")
		for line in open(os.path.join(self.options["data_path"],"interactions","./HIPPIE-current.mitab.txt")).read().strip().split("\n")[1:]:
			line_bits = line.split("\t")

			try:
				for pmid in line_bits[8].split("|"):
					pmid = pmid.split(":")[1]

					a_accession = line_bits[2].split(":")[1].strip('"').split("-")[0]
					b_accession = line_bits[3].split(":")[1].strip('"').split("-")[0]

					counter += 1
					interactions[counter] = {
						"a_accession_id":a_accession,
						"b_accession_id":b_accession,
						"a_accession_source":line_bits[2].split(":")[0],
						"b_accession_source":line_bits[3].split(":")[0],
						"method":line_bits[6].split("|"),
						"pmid":pmid,
						"confidence_source": 'hippie',
						"source":line_bits[12].split("|"),
						"confidence":line_bits[14]
					}

					if interactions[counter]["a_accession_source"] == "uniprotkb":
						proteins.append(interactions[counter]["a_accession_id"])

					if interactions[counter]["b_accession_source"] == "uniprotkb":
						proteins.append(interactions[counter]["b_accession_id"])

					counter += 1
			except:
				pass


		uniprotDownloaderObj = uniprotDownloader.uniprotDownloader()

		### Do it in chunks, otherwise bad request = 414 -> URL too long
		protein_list = list(set(proteins))
		print("proteins len",len(protein_list))
		per_request = 500
		for i in range(0,len(protein_list),per_request):
			mappings.update(uniprotDownloaderObj.get_uniprot_mapping(protein_list[i:i+per_request]))

		# print("mappings",mappings)
		# sys.exit()
		print("Processing HIPPIE")
		if os.path.exists(os.path.join(self.options["data_path"],"interactions","missing_mappings_in_hippie.tab")):
			missing_hippie_data_mappings = open(os.path.join(self.options["data_path"],"interactions","missing_mappings_in_hippie.tab") ).read().strip().split("\n")[1:]

			for mapping in missing_hippie_data_mappings:
				mapping_bits = mapping.split()
				for id in mapping_bits[0].split(","):
					mappings[id] = mapping_bits[1]


		print(("Remapping HIPPIE",len(interactions)))

		missing = []
		delete = []
		for interaction in interactions:
			##----##

			id = interactions[interaction]["a_accession_id"]

			if id in mappings:
				interactions[interaction]["a_accession"] = mappings[id]
			else:
				missing.append(id)

			##----##

			id = interactions[interaction]["b_accession_id"]

			if id in mappings:
				interactions[interaction]["b_accession"] = mappings[id]
			else:
				missing.append(id)

			##----##

		missing = list(set(missing))
		missing.sort()

		print(("#MISSING in HIPPIE",len(missing),len(delete)))
		print(("\n".join(missing)))
		print(("-"*10))

		print(("HiPPiE",counter,"interactions"))

		return interactions

	def grabHuRI(self,interactions={},counter = 0):

		self.check_directory()
		proteins = []

		huRI_path = os.path.join(self.options["data_path"],"interactions","HuRi.pmi25.txt")

		for line in open(huRI_path).read().strip().split("\n")[1:]:
			lineBits = line.strip().split("\t")

			a_accession = lineBits[0].split(":")[1].split("-")[0]
			b_accession = lineBits[1].split(":")[1].split("-")[0]
			a_gene = lineBits[5].split("(gene name)")[0].split(":")[1]
			b_gene = lineBits[6].split("(gene name)")[0].split(":")[1]
			confidence = lineBits[14].split(":")[-1]
			method = lineBits[6].replace("psi-mi:","")

			if len(a_accession.split("_")) > 1 or len(b_accession.split("_")) > 1: continue

			counter += 1

			interactions[counter]  = {
			'a_accession': a_accession,
			'a_accession_id': a_gene,
			'a_accession_source': 'uniprotkb',
			'b_accession': b_accession,
			'b_accession_id': b_gene,
			'b_accession_source': 'uniprotkb',
			'confidence': confidence,
			'confidence_source': 'IntAct',
			'method': [method],
			'pmid': '32296183',
			'source': ['HuRi']}


		print(("HuRi",counter,"interactions"))

		return interactions

	def grabBioPlex(self,interactions={},counter = 0):

		self.check_directory()
		proteins = []
		bioPlex_path = os.path.join(self.options["data_path"],"interactions","BioPlex_3.tsv")

		for line in open(bioPlex_path).read().strip().split("\r")[1:]:
			lineBits = line.strip().split("\t")

			a_accession = lineBits[2].strip('"').split("-")[0]
			b_accession = lineBits[3].strip('"').split("-")[0]
			a_gene = lineBits[2]
			b_gene = lineBits[3]
			confidence = lineBits[-1]
			method = 'MI_0943:(detection by mass spectrometry)'

			if len(a_accession.split("_")) > 1 or len(b_accession.split("_")) > 1: continue

			counter += 1

			interactions[counter]  = {
			'a_accession': a_accession,
			'a_accession_id': a_gene,
			'a_accession_source': 'uniprotkb',
			'b_accession': b_accession,
			'b_accession_id': b_gene,
			'b_accession_source': 'uniprotkb',
			'confidence': confidence,
			'confidence_source': 'bioplex',
			'method': [method],
			'pmid': '26186194',
			'source': ['bioplex_3']}

		print(("BioPlex",counter,"interactions"))

		return interactions

	def grabViruses(self,interactions={},counter = 0):

		self.check_directory()
		proteins = []
		mappings = {}

		huRI_path = os.path.join(self.options["data_path"],"interactions","vir-hu_ppis_multipledbs.mitab.tsv")
		lines = open(huRI_path).read().strip().split("\n")

		tag_count = 0
		for tag in lines[0].split("\t"):
			print((tag_count,"\t",tag))
			tag_count += 1

		for line in lines[1:]:
			try:
				lineBits = line.strip().split("\t")

				a_accession = ""
				b_accession = ""
				a_gene = ""
				b_gene = ""
				a_proprotein = ""
				b_proprotein = ""

				for accession in lineBits[0].split("|"):
					if accession.split(":")[0] == "uniprotkb":
						a_accession = accession.split(":")[1].split("-")[0]

						if len(accession.split(":")[1].split("-")) > 1:
							a_proprotein = accession.split(":")[1].split("-")[1]

				for accession in lineBits[1].split("|"):
					if accession.split(":")[0] == "uniprotkb":
						b_accession = accession.split(":")[1].split("-")[0]

					if len(accession.split(":")[1].split("-")) > 1:
						b_proprotein = accession.split(":")[1].split("-")[1]


				if len(lineBits[4].split(":")) > 1:
					a_gene = lineBits[4].split(":")[1]

				if len(lineBits[5].split(":")) > 1:
					b_gene = lineBits[5].split(":")[1]

				method = lineBits[6]

				pmid = lineBits[8].split(":")[1]
				for reference in lineBits[8].split("|"):
					if reference.split(":")[0] == "pubmed":
						pmid = reference.split(":")[1]

				try:
					source = lineBits[12].split("psi-mi:")[1]
				except:
					source = lineBits[12]

				confidence_source = lineBits[14].split(":")[0]
				confidence = lineBits[14].split(":")[-1]

				try:
					confidence = float(confidence)
				except:
					confidence = 0

				if a_accession == "" or b_accession == "":
					pprint.pprint( lineBits)
					continue

				if len(a_accession.split("_")) > 1 or len(b_accession.split("_")) > 1: continue

				counter += 1

				interactions[counter]  = {
				'a_accession': a_accession,
				'a_accession_id': a_gene,
				'a_accession_source': 'uniprotkb',
				'b_accession': b_accession,
				'b_accession_id': b_gene,
				'b_accession_source': 'uniprotkb',
				'confidence': confidence,
				'confidence_source': confidence_source,
				'method': [method],
				'pmid': pmid,
				'source': [source]}

				mappings[a_accession] = a_accession
				mappings[b_accession] = b_accession
			except Exception:
				print(("ERROR",utilities_error.getError()))
				raise

		print(("Viruses",counter,"interactions"))

		return interactions

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def saveHIPPIE(self):
		interactions = self.grabHIPPIE()

		with open(self.options["interactions_file"],"w") as outfile:
			json.dump(interactions, outfile)

		print(("Saved to:",self.options["interactions_file"]))

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def grabSTRING(self,interactions={},counter = 0):
		self.check_directory()

		self.downloadSTRING()

		proteins = []
		mappings = {}

		string_mapping_path = os.path.join(self.options["data_path"],"interactions","./string.9606.mapping.tsv")
		string_mapping_data = open(string_mapping_path).read().strip().split("\n")

		string_mapping = {}
		for line in string_mapping_data:
			line_bits = line.split("\t")
			string_mapping[line_bits[2]] ={"mapping":line_bits[1].split("|")[0],"score":line_bits[3]}


		string_path = os.path.join(self.options["data_path"],"interactions","./string.9606.tsv")
		string_interaction_data = open(string_path).read().strip().split("\n")

		header = string_interaction_data[0].split()

		combined_score_index = header.index('combined_score')

		for line in string_interaction_data[1:]:
			line_bits = line.split()

			if line_bits[0] in string_mapping and line_bits[1] in string_mapping:

				a_accession = string_mapping[line_bits[0]]["mapping"]
				b_accession = string_mapping[line_bits[1]]["mapping"]

				if len(a_accession.split("_")) > 1 or len(b_accession.split("_")) > 1: continue

				a_gene = ""
				b_gene = ""
				confidence = float(line_bits[combined_score_index])/1000

				if confidence > 0.7:
					evidence_types = []

					for i in range(2,len(line_bits)-1):
						if  int(line_bits[i]) > 0:
							evidence_types.append(header[i] + "(" + "%1.3f"%(float(line_bits[i])/1000) + ")")

					method = 'STRING algorithm (' + "|".join(evidence_types) + ')'

					counter += 1

					interactions[counter]  = {
					'a_accession': a_accession,
					'a_accession_id': a_gene,
					'a_accession_source': 'uniprotkb',
					'b_accession': b_accession,
					'b_accession_id': b_gene,
					'b_accession_source': 'uniprotkb',
					'confidence': confidence,
					'confidence_source': 'string',
					'method': [method],
					'pmid': '30476243',
					'source': ['STRING']}

			#else:
			#	print "Skip",line_bits[0],line_bits[0]

		return interactions

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def saveSTRING(self):
		interactions = self.grabSTRING()

		with open(self.options["interactions_file"],"w") as outfile:
			json.dump(interactions, outfile)

		print(("Saved to:",self.options["interactions_file"]))

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def saveHIPPIE_STRING(self):
		interactions = self.grabHIPPIE()

		interactions = self.grabSTRING(interactions=interactions, counter=max(interactions.keys()))

		with open(self.options["interactions_file"],"w") as outfile:
			json.dump(interactions, outfile)

		print(("Saved to:",self.options["interactions_file"]))


	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def saveHIPPIE_STRING_Viruses(self):

		interactions = self.grabViruses()
		interactions = self.grabHIPPIE(interactions=interactions, counter=max(interactions.keys()))
		interactions = self.grabSTRING(interactions=interactions, counter=max(interactions.keys()))

		with open(self.options["interactions_file"],"w") as outfile:
			json.dump(interactions, outfile)

		print(("Saved to:",self.options["interactions_file"]))


	############################################################################################################################################
	############################################################################################################################################
	############################################################################################################################################
	############################################################################################################################################
	############################################################################################################################################
	############################################################################################################################################


if __name__ == "__main__":
	import elmDownloader

	interactionDownloaderObj = interactionDownloader(use_hippie=True,use_string=False)

	#print "#getIntactTags"
	#interactionDownloaderObj.getIntactTags()
	#print "#getIntactMutations"
	#interactionDownloaderObj.getIntactMutations()
	print("#getIntactInterfaces")
	#interactionDownloaderObj.getIntactInterfacesByProtein('P06400')
	#interactionDownloaderObj.getIntactInterfacesByDomain('PF00400')

	interactionDownloaderObj.getIntactInterfaces()

	data = interactionDownloaderObj.getInteractionsTable(["complete_database"])

	"""
	#data = interactionDownloaderObj.getLocalisationByInteractionsDatabase(["P20248"])
	#pprint.pprint(data)

	#data = interactionDownloaderObj.getInteractionsPmidDatabase()
	#pprint.pprint(data)

	interactionDownloaderObj.grabHIPPIE()
	data  = interactionDownloaderObj.getInteractions(['10531037'],"pmid")
	pprint.pprint(data)


	interactionDownloaderObj.getInteractions([ '21325626', '12134156', '15542832', '11487705', '25728771', '17412691', '18372248', '18537269', '11970947', '16302269', '24648511', '21389117', '29535361', '9931424', '10744724', '22266653', '20513353', '12356736', '9121492', '1713687', '12535517', '26329581', '14505569', '24206843', '21949189', '10860980', '23851457', '25524645', '10973490', '11044456', '16318909', '11044453', '26787460', '9052859', '17008717', '18667490', '20160149', '13679512', '9353264', '11719221', '10722653', '19265707', '22179618', '23063329', '25447263', '10071213', '25220262', '1400319', '22864569', '22514274', '23789096', '23435424', '28617439', '18688269', '15861136', '17018293', '11959914', '25965408', '20962096', '14617065', '26812018', '10549285', '10549287', '19279214', '11940605', '30686098', '11114197', '14981079', '9635433', '11413191', '24483153', '17627416', '15282546', '10929717', '16300735', u'25287889', '16730985', '22645135', '24944246', '22645138', '3614197', '17959737', '11206074', '9301084', '12501189', '16987982', '16275651', '16987985', '14694083', '22280008', '23185543', '8585605', '22696216', '22879586', '16554297', '24347326', '18854309', '16739988', '12356908', '9199321', u'20170726', '23345399', '26289816', '6688526', '17911242', '25979333', '12545175', '12545174', '19116314', '1894649', '23277184', '18991627', '1321288', '15994232', '26621381', '20937900', '29769719', '25347203', '20937909', '15985462', '8387508', '14752279', '19220000', '18940602', '9882340', '11543664', '18940607', '17875928', '15475968', '18940609', '23343390', '22921828', '22921829', '16793551', '25364754', '7532003', '11087860', '10644451', u'19170759', '21381753', '15545272', '19759156', '22020938', '18675924', '12483220', '15653742', '15580265', '8223481', '20101225', '15385169', '27521277', '17470806', '11312275', '17595322', '25435426', '10559371', '12244092', '26607847', '18812515', '9111043', '10339564', '9228004', '9761669', '23519423', '11333902', '18307981', '23498919', '10642508', '21840989', '26655473', '19371038', '8757605', '22205987', '8336704', '25538301', '15053879', '24725409', '18996550', '25737554', '26974126', '27647889', '11830582', '10764582', '21596754', '10873803', '16338927', '16885030', '16968744', '22681887', '11304524', '19913032', '18695394', '17805301', '21572392', '7499194', '8137810', '18942858', '26866573', '1961250', '23260142', '10523294', '18462686', '12171920', '16120677', '15610023', '14563323', '23357850', '2332438', '19879249', '12597860', '16728978', '22781553', '16728977', '18235238', '20733055', '17467953', '21455173', '12379843', '15972895', '22357538', '26512081', '12821147', '8109975', '20870937', '18570922', '16516211', '23822808', '24190431', '9256276', '17030191', '15684378', '26778126', '15340059', '23891150', '29879296', '24013206', '8025104', '20025850', '23633599', '19217391', '18027972', '11459848', '11257508', '18089292', '24019758', '24019759', '20385547', '27226565', '19966293', '15100220', '22829593', '11287420', '24675874', '8811735', '19143627', '19143629', '10075680', '10811803', '17289678', '21282516', '16195415', '11022042', '8001118', '19573808', '23195958', '17426142', '22315981', '10604467', '11684012', '19699715', '17599046', '28343969', '22847007', '23824189', '10681503', '10980614', '20430886', '15084146', '9346908', '19472326', '17339105', '17363896', '18621669', '15782121', '20357771', '22373916', '24746913', '27522463', '25486442', '18829455', '26954546', '18829457', '18829459', '9892642', '10975256', '1379696', '11313386', '26098679', '10449404', '27120157', '12432082', '24025624', '11509611', u'23086940', '16615912', '16615911', '17046835', '16615918', '21292004', '12844440', '14580337', '28851290', '22742833', '23514889', '20800573', '9872948', '22474372', '18552128', '12553912', '21847096', '25649439', '18552123', '11029469', '17634291', '26321052', '20004165', '29097261', '19403681', '25773596', '1548757', '16007201', '16204249', '24318128', '12049740', '22961729', '17640984', '20534816', '21343341', '12842464', '12456792', '25417112', '21782458', u'17349956', '20599701', '17448445', '21806995', '11146653', '22801552', '10603327', '9582298', '10692452', '18988706', '18417588', '8631761', '22513087', '12853477', '19726685', '22030391', '23034805', '14705930', '15255944', '11922138', '9438863', '23370280', '9438860', '28490494', '16640460', '11546761', '12818177', '8846780', '10606744', '25260595', '12944491', '7651417', '12417729', '11206058', '11546812', '21826105', '18981220', '27210447', '11802161', '15677482', '15677483', '17591767', '12944271', '17496150', u'30692204', '24374310', '27692963', '17190600', '20541251', '10088725', '8349691', '17086192', '21858148', '12958591', '1518052', '12958592', '10567565', '21127055', '12750381', '9427284', '22038483', '10338211', u'14519764', '21163258', '23263282', '11292354', '11463824', '20066663', '17166829', '19560420', '30202032', '19131965', '1327748', '22960265', '17645729', '14962739', '15534001', '15189990', '18992740', '23831371', '14754904', '17074801', '23358114', '15678106', '19885390', '22700975', '20021564', u'16987104', '12700270', '11459182', '17667941', '10411507', '28524165', '15788653', '8077949', '17032734', '3477645', '22885064', '25284789', '18794347', '22527282', '12759351', '11395479', u'11466303', '9141492', '20595394', '9141499', u'22653441', '9772168', '11551507', '10359606', '15218065', '26455390', '8810294', '9742243', '24847070', '8259515', '9202153', '16552148', '10837147', '12296822', '15182197', '24379409', '24773565', '25525273', '28803053', '16332683', '12411514', '20129060', '8976553', '19061640', '9388480', '25637630', '10052460', '16045813', '16611631', '18464232', '11773444', '28758629', '8892760', '1656049', '22162637', '22877078', '12446723', '22001514', '9190289', '8408305', '12962633', '12364336', '19917253', '11694505', '26506893', '8783629', '18483302', '8551560', '10938122', '24163370', '25348421', '22328159', '19958775', '17684013', '30323330', '2332418', '19342883', '18512959', '22038251', '23869089', '21113170', '9054412', '28104399', '18321968', '18321969', '9922143', '15299527', '18321963', '19660095', '9731210', '17243659', '21073696', '17881440', '8670824', '17030608', '9660834', '12974625', '26232604', '17397263', '18319300', '21115828', '25500867', '17437719', '26365797', '12779325', '10775261', '23686610', '23767961', '21270126', '23316803', '18375760', '15169953', '26984404', '26148513', '16279839', '21418451', '2583108', '11445003', '19000816', '12805216', '12938160', '8618911', '2091026', '31012180', '17210633', '23972470', '22086178', '23437920', '23572190', '21440557', '28449373', '20406804', '14769041', '18974053', '16434399', '22430208', '7767791', '10871282', '18282135', '8764091', '16701206', '16982700', '11161218', '15548568', '17637677', '16331976', '16105874', '12426383', '22727448', '12834342', '24098557', '10393198', '11133752', '1703206', '26392564', '11486037', '21108043', '16497227', '26602186', '22009749', '10786835', '12946359', '11700563', '14722309', '20606006', '11283607', '21811582', '8599931', '22849721', '21629754', '11283605', '19497877', '27238018', '20660730', '9168117', '17046812', '9168119', '16234236', '16234232', '17687328', '12748172', '10356320', '11571651', '15093828', '9482110', '12686538', '19220811', '16024771', '10479680', '16516836', '10811804', '19250911', '19478065', '17005707', '17595523', '18691017', '19084525', '23623683', '9583676', '17495531', '16829512', '22308029', '24803334', '11359370', '23142980', '10413594', '18767165', '23746349', '21489993', '16319895', '10809226', '24078636', '22579248', '10228168', '23010168', '10811618', '26894538', '12138096', '22065579', '18356162', '19366705', '16377563', '9795107', '16728506', '25918396', '12032548', '15946939', '8572687', '25789078', '23511477', '17069616', '12351846', '25428348', '22932899', '10958686', '12072528', '15838047', '15064394', '10652141', '16287935', '2380988', '18511562', '24920672', '24920673', '16964287', '27223324', '16904109', '15108720', '8163528', '8163529', '8197205', '16326715', '21289110', '21289117', '9634700', '16737814', '17307877', '15485829', '20927106', '9535728', '20614028', '15297877', '21478859', '20151677', '25733683', '25162118', '10892742', '12408821', '10995736', '15581372', '11696592', '29084197', '20400536', '10995739', '11237865', '11343787', '23786411', '25833055', '11427703', '25497731', '18046415', '20521764', '14633984', '25620564', '22504882', '11509672', '10562550', '9736718', '10562558', '11060013', '27107013', '228015520', '21856747', '8648633', '22473211', '19863457', '22227520', '24218572', '17936704', '10615055', '24419384', '18237744', '20504928', '7526465', '12807910', '8416808', '15870263', '9718306', '22920569', '11352911', '17223572', '16439536', '19638580', '8985339', '18480409', '23372760', '10074433', '15048125', '23382378', '12040031', '14565955', '25413248', '11290324', '18658150', '21208191', '9384386', '9321393', '20481595', '19249802', '14551202', '21118994', '11086007', '18992810', '22484091', '11997522', '18317453', '11704659', '26489469', '26489467', '16388993', '19204326', '23396834', '17631896', '20731332', '9369453', '8346196', '10484781', '26259583', '30543055', '11406578', '24628338', '9560194', '9560191', '24662824', '23955592', '21893288', '12605673', '25436413', '7721860', '16978406', '9659918', '22106309', '9271500', '22952853', '23151250', '21256005', '12381297', '12574359', '10497201', '19906698', '16030255', '10050887', '22832024', '9632111', '15476828', '8649374', '8382303', '11559359', '11533658', '19955572', '16455800', '8670806', '16987806', '14694104', '23435234', '22437499', '12660173', '11577096', '26147758', '21979951', '15803393', '11376336', '21217699', '22151054', '20562877', '11110795', '17141806', '23514615', '17161424', '21394086', '21417405', '21417403', '1545814', '16600963', '22897611', '9774110', '3168999', '20961098', '19996458', '16045768', '15358138', '18638481', '12176995', '18638487', '21971545', '26188084', '9218800', '22778255', '23236378', '25789873', '8885240', '19473981', '8940068', '18677113', '17116869', '16338374', '14527949', '23622245', '23622247', '24009707', '18611385', '23999295', '9019158', '15536091', '25684205', '18757857', '11882663', '26156556', '27114510', '10990458', '9753456', '21233212', '26305355', '28918948', '10869435', '26191773', '25695766', '15929724', u'26100893', '12740362', '24656128', '25865894', '12695512', '22888118', '22304005', '30017387', '18328268', '9032315', '15930150', '26045430', '15451670', '21571226', '15952796', '15342622', '18359295', '23166294', '21437237', '12887903', '15077115', '25840370', '21791702', '14701740', '17637675', '22629406', '9795341', '14701748', '23706739', '12244309', '12244301', '19828451', '14993267', '7865888', '19170882', '21296877', '22000412', '7634336', '16472745', '17235285', '20086010', '19604478', '11311120', '16581765', '26108669', '17452787', '18078954', '10876243', '21549310', '24387107', '18097023', '9309223', '25049398', '18691969', '19801667', '19332537', '15664188', '25204619', '16128580', '11453642', '18552837', '23572542', '14532002', '14532005', '10196310', '10601285', '15140952', '11545585', '16734421', '16123127', '21164480', '17938171', '9642078', '11323714', '24818857', '15469925', '15988023', '25905427', '11752454', '22333914', '21220508', '28783324', '20410270', '17158913', '24186061', '24186063', '22350952', '16547135', '12005438', '21304492', '26212312', '27787230', '20018885', '22139841', '9628725', '25651062', '19679663', '23065011', '23146885', '26981620', '9000049', '14596919', '23222956', '11777906', '23723390', '14609321', '11910031', '24248336', '20018896', '24248335', '16420525', '16420526', '21098038', '19228696', '20098421', '11090629', '17274988', '14733955', '20376316', '16139797', '26388440', '11030334', '26158515', '16467373', '11340167', '15749016', '15643058', '11715018', '7961854', '26811464', '16253993', '3939318', '22665483', '19928859', '24802393', '24333682', '15496876', '18775730', '22180837', '19596783', '10488080', '19713766', '24389023', '10657980', '21720019', '12450395', '17498738', '8226743', '8425895', '19509060', '24217340', '17686488', '15897968', '15163743', '15857951', '24510189', '17554339', '11459829', '10488331', '26457367', '11773377', '21134747', '12673597', '12925736', '23948254', '27190515', '8617364', '18799424', '12446761', '12446767', '2661018', '15258145', '20581084', '22767592', '11739676', '10559878', '7721803', '22507528', '12679017', '7983060', '11112778', '11348595', '25605012', '22100450', '30770806', '15107838', '22642567', '19935683', '16860823', '21808300', '26779587', '15784259', '10692572', '10767429', '30068652', '9468142', '17550307', '20388706', '25825518', '15051490', '11062046', '23944708', '15199063', '8626715', '16749904', '16140763', '18598704', '9843497', '30887917', '20974890', '7835342', '12615919', '9261175', '15753281', '14593421', '18443147', '16476580', '15610735', '17580304', '10995387', '10995389', '29138501', '20679480', '20019667', '8994973', '15273250', '10718318', '22935697', '16777603', '16777606', '9774688', '24139988', '15465019', '11117531', '21597462', '15123241', '8384554', '14711814', '20444688', u'23213405', '25972089', '10357812', '12063245', '18591666', '11301005', '12386167', '8910476', '20463077', '15990112', '22427750', '11325528', u'24867636', '23239367', '15611062', '26126731', '8945519', '660947', '12093276', '19553342', '26120032', '10233168', '7768953', '24559989', '21889334', '11509732', '7520377', '23601647', '20716525', '25982527', '3049074', '20195521', '17523610', '22172677', '10404225', '10404224', '1370343', '16003391', '14769882', '20028483', '10069811', '26068101', '11080640', '20554778', '10982851', '14690593', '15047914', '27050421', '26919541', '10811646', '8662842', '17110954', '17110958', '19767730', '15558556', '9488039', '15342643', '23922814', '16919947', '19827079', '24962582', '15668399', '30142695', '15735345', '12717451', '26950368', '9778246', '11723239', '18001825', '20142502', '25092867', '12588984', '17488649', '25571794', '11595806', '16474402', '17229889', '21740913', '11122378', '18367543', '14757059', '14757058', '23650368', '8482351', '9130707', '16251358', '21639858', '17468107', '17507690', '11602581', '9130715', '21577200', '25305017', '25305014', '25703206', u'8549741', '17635580', '24472438', '26431026', '16376933', '8702720', '16782704', '27714789', '27272460', '11743730', '17660831', '10066782', '27539869', '12502716', '19481529', '24626927', '16123141', '11875078', '23989153', '26643845', '9647693', '17601348', '18589438', '19584057', '8798677', '21270402', '19283487', '20063036', '11790782', '25310241', '12702202', '22652772', '28165461', '19529962', '17158932', '19535340', '11546872', '29571744', '9302295', '20230749', '25046855', '25619998', '12054509', '16679322', '12655059', '23041411', '10477754', '22936661', '11827484', '25484072', '9010785', '11698662', '23345425', '22211259', u'28376302', '20084102', '22407016', '18089575', '23698002', '19297465', '27193083', '12598654', '14517333', '11463845', '10593983', '15843398', '20378546', '19903453', u'9407131', '21220922', '2191722', '17652093', '29547262', '24352630', '25704306', '9096331', '25691470', '11240641', '15141161', '22348310', '19923714', '8340410', '17177604', '8709278', '25330247', '22558212', '19223579', '28028053', '22184114', '24362311', '23613971', '25556945', '18818739', '23746845', '11148282', '30175106', '16481473', '15767683', '15184881', '17540008', '12218058', '24838245', '23408603', '27383630', '24038729', '23302862', '18828628', '19586769', '23112295', '12213210', '20676133', '27058167', '21886810', '12060657', '9952408', '10364454', '16230638', '20567258', '14604530', '10050770', '10087260', '17215520', '11389839', '24443581', '22589718', '21596426', '17073446', '17502104', '12692296', '16114898', '16889744', '14585836', '15134636', '8662627', '12475216', '27610751', '9864141', '30598546', '19590578', '17161399', '7862116', '8986721', u'28554535', '7637786', '18158288', '22086334', '22032967', '14617633', '15479160', '22578813', '22066785', '18441008', '12397362', '21642972', '21642970', '17706379', '22323608', '26556890', '12134053', '14581576', '9351806', '26901871', '18656483', '22117215', '8600458', '16455668', '19380881', '20493689', '15465032', '23178929', u'23719537', '15044687', '11884610', '22922483', '29911254', '11140636', '11140637', '8051149', '24503158', '1534854', '15611048', '21888622', '22074847', '14674748', '26886794', '22476084', '17045809', '14708007', '18701670', '22002947', '23873930', '10777694', '17410169', '15148369', '18378770', '15367628', '9155014', '20235147', '14752510', '20543827', '11895442', '11571228', '19412162', '14999017', '17997305', '9880530', '23574272', '12736686', '22550182', '22167198', '10733575', '25844500', '14718659', '18061918', '10037694', '18457419', '12837631', '1594241', '18498752', '22906961', '6509552', '17602779', '19179070', '10323858', '25331450', '23314252', '19610074', '10801818', '17116753', u'11152471', '22927406', '20804767', '11904443', '1898923', '11883939', '22345702', '11112693', '23480637', '11150305', u'21222654', '16314513', '18334649', '23422557', '24952945', '23886940', '12529330', '18951975', '16415352', '24498434', '11498591', '26888287', '24550280', '22483112', '20133841', '30745144', u'11739778', '25728130', '8493578', '11356847', '7644498', '30990354', '7644494', '11115515', '22859295', '7644493', '23886493', '22363506', '22762444', '16153703', '21388199', '28483528', '24583051', '22817901', '26541461', '16120600', '9891784', '15215314', '16267091', '10899983', '10804199', '19609947', '26267535', '22153507', '22153504', '12431059', '22998443', '20479275', '22512505', '21606197', u'18310074', '29120074', '22745433', '23019610', '26774286', '12808093', '18029081', '8244956', '14701726', '6088992', '23482567', '8294457', '15897460', '18321849', '9182573', '25762514', '16307917', '24056937', '26900867', '19535327', '19726719', '22770219', '22896795', '1655808', '10426949', '23169783', u'15741273', '20089832', '14661952', '16403626', '19996092', '21428964', '20603002', '19451643', '24235149', '10330143', '11136723', '9420328', '21550984', '11278283', '10492401', '1333132', '10497169', '26075520', '15933069', '21666679', '21666677', '14984200', '21666675', '12388558', '15111645', '1991323', '21640712', '18020708', '22493238', u'16979567', '17456742', '25966461', '11932255', '12465917', '16286470', '16286477', '24144444', '20067577', '15834155', '23942235', '12529437', '22670624', '19858290', '9837926', '17431426', '22267086', '19496083', '19321436', '23639431', '8145819', '16511824', '1959614', '10202017', '19362095', '20117114', '16141217', '10415358', '16618804', '18596123', '10880959', '20208155', '17884811', '15107605', '16179349', '15857993', '23946421', '12087097', '16432215', '21182795', '12087092', '10748198', '29728980', '17303464', '17438267', '10637287', '20922740', '21463458', '17010654', '18948112', '17005661', '20682773', '11158323', '19270695', '14563837', '7781597', '11714752', '8003955', '9770415', '24705462', '11098150', '16365047', '24443565', '15684325', '25599992', '11104681', '25371206', '20178991', '22193957', '26437244', '27670116', '30723127', u'12410313', '11991975', '9171063', '19349176', '8982089', '15733927', '20086174', '11062555', '17888002', u'9528787', '11960368', '20360068', '18301782', '14670955', '11266362', '17712528', '12470958', '19641106', '19318624', '19549826', '11713476', '17239901', '18381891', '17629515', '17612494', '27714489', '22178138', '16243840', '10848599', '28470624', '24830809', '10978185', '21070952', '24668264', u'14640981', '10884347', '12417334', '12920307', '27103431', '15561765', '11741838', '7502045', '11823424', '12818431', '12115603', u'10593973', '10574784', '15465051', '16987314', '10601308', '10558990', '15703190', '11309402', '17051221', '8148877', '20622153', '18995838', '18995837', '20670214', '25471691', '19296670', '11483531', '16148041', '21264300', '30232260', '17311912', '3023047', u'17521329', '20956805', '21642953', '10433269', '16294047', '16294045', '9045680', '26555021', '1448094', '21775434', '9155030', '12637568', '29982772', '16793770', '9360613', '28287329', '18384083', '17692377', '15269005', '18611856', '25959773', '21278251', '23289531', '15837811', '10594027', '17942599', '24120939', '1373379', '16087332', '16214902', '22552943', '10880350', '10619429', '21665971', '26046769', '18343812', '11443134', '21062891', '24931469', '18343819', '21628590', '11909521', '10331866', '16274484', '11960997', '14594809', '14594808', '12160749', '15137772', '20489202', '20953180', '15949434', '1361949', '21646404', '18550856', '10769203', '18640976', '12791132', '8757785', '10447205', '19966799', '16621890', '16763193', '10197533', '20403767', '17403670', '27568553', '27561354', '17115691', '20834162', '10608854', '19072710', '18160705', '19251642', '22974658', '9211913', '21709260', '23096565', u'22356909', '16002581', '7761456', '22825853', '20351172', '25314968', '18287102', '18033247', '2014240', '16888629', '25115701', '25115702', '29634995', '23560397', '26363069', '17308348', '22153076', '22153077', '11005803', '9312002', '12756325', '24166846', '30469473', '17583734', '9368007', '24782567', '18376416', '23535598', '14981085', '22070932', '22786881', '17451235', '25586196', '16936729', '11279116', '25160624', '15963504', '24401087', '23103942', '14754895', '3352735', '9724753', '23897474', '16096638', '9811710', '18036542', '1587268', '15081409', '31036839', '27435506', '11846567', '11080155', '18818650', '14760703', '11779231', '11247301', '15461588', '18451864', u'14698617', '11846568', '20801878', '20801879', '1850017', '22854063', '11278869', '16142913', '9636695', '22869380', '12297314', '17050540', '9531288', '22622329', '11320094', '10383444', '26344095', '19595719', '25122681', '16895911', '15611618', '19595712', '27109380', '7510218', '25921531', '9354468', '19635485', '12857913', u'29279382', '19343052', '10932246', '10932245', '19193720', '22292814', '15173090', '24037507', '16417524', '18270262', '23434407', '16861906', '14561767', '8910559', '18768933', '16630890', '21832154', '21832156', u'17356069', '2542570', '17387146', '21795339', '8637717', '21900604', '24225950', '23920481', '12791266', '11517213', '10766245', '15908698', '16452687', '15137387', '21080423', '20536183', '10655490', '11738041', '12736258', '21458667', '21458668', '15705574', '10485850', '7649995', '17286281', '24670654', '21621319', '10822175', '14676825', '24359314', '9514738', '29931371', '22971924', '8552082', '8590795', u'31282865', '8590799', '1585175', u'17411337', '22184408', '11116148', '19927120', '24297169', '25520155', '9586631', '14730349', '21617041', '23177648', '9136909', '21147767', '26277758', '20392845', '1400232', '18851831', '10744426', '16314496', '16314497', '26410074', '22569035', '15623515', '15246256', '24876499', '21464124', '1756714', '15747128', '21148483', '20889982', '20877636', '28241141', '15951818', '16407298', '2905485', '10206649', '21507970', '22331908', '10490654', '18644889', '16413780', '18948538', '10329713', '12610109', '23428413', '11024152', '16254000', '11024151', u'17088427', '16557530', '25771791', '20535204', '23435727', '12595704', '24190967', '16762837', '19388668', '19861722', '9405437', '10969074', '8951384', '14759370', '26278177', '25066235', '8034741', '16573649', '12095257', '11743162', '11167020', '16474394', '10655612', '24469451', '12200128', '26109052', u'16564677', '11483518', '10891264', '16094384', '9281590', '18198110', '11511363', '7482705', '24076405', '10949026', '30716436', '21060336', '10073691', '18477568', '24090409', '18682563', '9326598', '14567919', '10930472', '21880841', '11432818', '28165218', '11818066', '10075937', '10075936', '19996102', '20935510', '22196728', '26841867', '17998537', '21326229', '15039779', '23474121', '14718165', '16294029', '22426463', '16935857', '21586796', '11238913', '10432553', '20333243', '11804586', '17502622', '18039785', '14978303', '20139983', '20519395', u'26359549', '17013376', '9846878', '22940584', '22759634', '22940583', '10217147', '19592703', '12052894', '18562688', '11937504', '19966800', '19477150', '12438572', '12689827', '14970219', '24900545', '16740914', '21665952', '16024795', '21665954', '21253573', '15123707', '8652520', '17954914', '23709667', '2105883', '19762560', '9535908', '27660687', '9535906', '8900201', '18697946', '24570489', '10493881', '28412169', '10520998', '17277771', '21387510', '18456494', '15882055', '15811376', '12810624', '19846556', u'26368022', '21658602', u'12824157', '8413249', '8937981', '17125202', '22394562', '9736616', '11382783', '10766840', '22383884', '10982389', '21951660', '11746526', '15588076', '19805099', '18163231', '9889196', '17784785', '8910529', '7802660', '22158899', '22791833', '23897459', '17643418', '11782427', '19020622', '19020623', '11782423', '2023902', '21030588', '7753174', '11157753', '12601010', u'8253063', '22405274', '11123912', '27385341', '14597626', '1312117', '8313462', '21937719', u'9242697', '11163209', '19285944', '22944912', '19285942', '20890288', '19285948', '21820899', '7543098', '7592693', '28134250', '21167176', '18495155', '19221193', '17560331', '22385262', '15103331', '17698606', '18519589', '12970175', '9169476', '22609812', '21763699', '18462678', '12591950', '10581242', '17124499', '26836308', '7846044', '22124735', '12858163', '12858162', '21664945', '23181752', '12226657', '8350395', '22284675', '27700984', '22284677', '15707893', '22634725', '30429481', '14609943', '19617625', '15060171', '10022822', '19008859', '15851033', '16780873', '12511559', '19147004', '12909012', '16323247', '23924735', '19651603', '11005820', '19605354', '8307185', '11234020', '23202584', '11030623', '11462009', '23386746', '7527393', '9430642', '15020713', '9430646', '23204517', '1529676', '25203209', '11911888', '11911886', '30006004', '9385632', '10471276', '19759395', '19759397', '19759396', '10508785', '10508784', '19396163', '20613843', '28334776', '21576365', '16216492', '27105114', '11981034', '26387435', '17258463', '23020677', '12441389', '17287208', '22864287', '17956729', '17428861', '26308901', '21269824', '16870929', '9741842', '7890747', '25741013', '11336675', '11278436', '15650869', '18363964', '23686307', '15896295', '25281266', '18076570', '10386617', '15808506', '24073851', u'29539431', '21945277', '23045552', '24853335', '10866678', '24627488', '24627487', '16867982', '25533961', '24268138', '16931796', '26210801', '23275167', '12847106', '28388439', '27091468', '20236928', '17384198', '21912678', '22438540', '15659558', '22262834', '29321329', '20085797', '1505517', '22815741', '19887378', '20847188', '15122046', '23922389', '22226915', '22178396', '20835240', '20835242', '16725309', '15037754', '22820502', '18471979', '22707729', '23949117', '3680234', '17130831', '10984535', '10911998', '12730338', '11346650', '10607594', '8772383', '19139173', '17218263', '14572313', '27864780', '16873582', '9792678', '11432833', '8513497', '15065852', '16076959', '20732303', '12235145', u'19945436', '9174343', '11598133', '15780933', '12724311', '8638161', '17173287', '26149687', '16135522', '25003389', '28065508', '16061658', '27083547', '17506864', '15843525', '15880122', '11562348', '18808171', '8605870', '21472932', '23150880', '18025109', '9482731', '15217910', '20356955', '26812545', '17563362', '12021452', '18211824', '21295698', '24656772', '17167418', '20862670', '10756028', '12778114', '11000240', '21283809', '14523018', '22406115', '11748237', '20809635', '24732914', '14585966', '21757287', '29150959', '10739913', '9121467', '19661287', u'12134149', '12165860', '23667252', '17995939', '9446614', '9395490', '11498538', '10551784', '9381178', '16415881', '23452850', '10562497', '18172024', '15581622', '19103929', '24055376', '16413484', '12186925', '22355414', '16829959', '16905097', '20853131', '10102273', '17173039', '12899619', '12970458', '19900895', '18844669', '15707391', '22194335', '9886297', '12574161', '15299880', '16341776', '27393996', '24672054', '11551963', '21139087', '10734067', '10747019', '21591667', '21422294', '21422291', '11953314', '11278607', '12198152', '17962403', '17360776', '16046186', '11931631', '21878619', '22213702', '18285462', '12890487', '23022382', '11340086', '15457265', '7914876', '23213219', '17589525', '17589523', '18753140', '10756109', '25446517', '21643473', '23812376', '23416764', '22275375', '17096442', '26192331', '15701524', '1920406', '12006501', '31000703', '26011858', '20231292', '17525340', '11114888', '10675567', '9406551', '9148966', '12714599', '21813640', '12606564', '16055720', '14559997', '12191992', '2797153', '16775314', '25058147', '11585921', '16426231', '10910062', '23976922', '16223725', '18807043', '19796655', '18650806', '17518432', '18650809', '18519035', '18378670', '24211137', '20181952', '20181956', '11108725', '11416140', '11148209', '15385968', '19919182', '20015412', '10944104', '16740252', '11414283', '24777060', '22411985', '22869523', '20871934', '25258313', '21402913', '17082188', '25877959', '24947426', '11507039', '8703216', '19328688', '22334675', '9769216', '18305175', '16804161', '21254166', '16648845', '10194451', '29764992', '15456246', '16923391', '21183464', '17254542', '10347213', '7629113', '11087756', '16705178', '21852953', '18630911', '15252135', '1903399', '24408864', '18063693', '19956591', '10394359', '11178896', '26994663', '22467876', '21199872', '18264101', '22000519', '23308067', '11814051', '20971641', '20694007', '22034227', '11937718', '11390366', '24504455', '11747435', '23418362', '21071223', '11208154', '9765270', '11208151', '11313466', u'29255063', '22343721', '17650508', '22343722', '1868826', u'25605614', '9687515', '18387785', '15946941', '23279110', '16511165', '22965230', '27291650', '10748020', '10542266', '26631735', '19218245', '15316014', u'7498521', '16319068', '29802931', '20164337', '22827337', '21177863', '22475127', '28253953', '18718913', '8663600', '15021885', '19003214', '11729193', '17638903', '14527389', '22040025', '11342538', '17570393', '18623064', '11572799', '14962388', '25471186', u'30737283', '25753662', '15590687', '16455950', '16987956', '17517894', '12100158', '24656813', '19297620', '11595185', '15721263', '10373560', '10373567', '10816582', '11553328', '10891484', '25205354', '26455391', '23290554', '9495340', '24248472', '15817490', '17036045', '19490898', '17222824', u'18208387', '14734560', '10781592', '14668868', '11238952', '14500912', '10731636', '9356494', '2553699', '12011100', '10375508', '16113102', u'21841197', '11687614', '8790420', '25535763', '22065572', '18165683', '18165682', '23272056', '15123625', '16246733', '23519214', '12598123', '9891042', '21063388', '22804908', '17006545', '9257712', '9218480', '21665998', '23238369', '25819436', '20096703', '24311690', '16644733', '21673315', '11369882', '26101899', '26956473', '12956947', '21828056', '18533182', '19416967', '18713862', '23979597', '15811849', '22689061', '16371476', '16249185', '12732728', '2564811', '18642883', '16351739', '17620341', '12501191', '24513855', '15340083', '12915562', '25295397', '21542590', '25814387', '22669947', '22751671', u'9414128', '18764929', '10869359', '22516749', '24814345', '9560342', '17057344', '10947801', '10739244', '23328397', '23242554', '10747034', '18256197', '12581524', '25518860', '17897949', '22956906', '15457186', '25234465', '19841061', '11566880', '19910468', '29695421', '17684156', '25907764', '17913709', '18042454', '10978177', '8408068', '9427757', '10983986', '17589501', '30619736', '23838290', '20854373', '16631613', '21900231', '17938582', '22275354', '17295609', '17560372', '11390642', '11782172', '15351654', '28586350', '10402428', '25202994', '11859376', '11859375', '11278955', '15901726', '25882913', '20691192', '18345609', '10207003', '16689770', u'11877457', '25171412', '16060668', '16914312', '29317535', '26748096', '20502673', '10375506', '27219064', '18477395', '15917648', '11707412', '20471943', '17546040', '20471940', '17825319', '10794525', '26341557', '20713507', '11092919', '27113755', '19751214', '17466257', '28883156', '11689946', '1504238', '12538863', '28187833', '24685145', '29030390', '23331060', '23353824', '3545499', '14742432', '24361936', '16263722', '10194305', '9185541', '28127816', '15930632', '7588612', '19655253', '8070396', '20146435', '16894158', '3416830', '16605275', '9927649', '11154281', '11955436', '17660751', '15896703', '22678915', '23519660', '18644376', '23519663', '23519664', '8681387', '11244092', '21068219', '16186824', '16905171', '10880513', '16131756', '19542454', '10074141', '19542453', '19420140', '21726526', u'19366662', '12083782', '966094', '26249705', '10660573', '16810322', '7492542', '26160170', '17337444', '22981647', '10642537', '10642536', '2209556', '22713866', '11601975', '16436050', '15337744', '22580462', '22820093', '1560020', '12571275', '15161925', '12628254', '16475813', '26236013', '15276842', '15610006', '10207077', '9636171', '11389589', '22071690', '10329718', '21653319', '9151683', '11533444', '24095733', '19514019', '11810239', '21755366', '25864631', '7650689', '23752926', '29149593', '19072119', '29149597', '16765046', '18948118', '10995432', '15098669', '16870210', '20534535', '9171351', '14698205', '9065427', '21041410', '22512650', '22287628', u'29489893', '15994808', '12699619', '20817731', '15776429', '2275532', '12697749', '9885291', '12388757', '16434970', '8377189', '12970176', '12970172', u'27453045', '19955409', '21533551', '25821446', '27453048', '11904305', '12556518', '12559917', '16188887', '11314042', '15331747', '21059905', '25607641', '10353244', '15585864', '23071329', '17473018', '29376035', '18655064', '21192925', '26893308', '22366302', '20665622', '25540434', '18209102', '7749917', '26205105', '20625546', '21166898', '23619335', '12070168', '25546376', '16505387', '12070164', '17000760', '16627943', '20647774', '15931224', '30716585', '22277653', '19128015', '28768865', '19850956', '25963096', '16436281', '16682484', '16224021', '15342247', '20519406', '15342244', '11970895', '24359708', '7642699', '1654327', '21134643', '17661348', '11744745', '12732641', '25591003', u'12956961', '20868367', '14529280', '20858893', '15483625', '11641397', '23027949', '20449836', '12871950', '10358075', u'17374523', '21671662', '10878002', '22437941', '12391296', '20818436', '19329564', '18948948', '26280531', '23333244', '3943133', '16365032', '23334295', '12163175', '30544850', '9656992', '21762218', '18930707', '11487015', '20730100', '25502654', '21102411', '16282323', '26526852', '24921010', '17188490', '3367995', '14596591', u'29083893', '24520212', '12679038', '22751659', '25825779', '17591922', '15563469', '12138188', '27813245', '8819159', '3054508', '10788465', '12581645', '11533655', '11733528', '11420704', '8604142', '16188882', '16859681', '23850873', '19635407', '21871889', '21454704', '3287180', '20048151', '22178476', '22178479', '8702811', u'21896481', '17444623', '25774600', '15498494', '9269775', u'31247123', '19700648', '11884718', '16287863', '24379377', '27664121', '12684535', '21360154', '23925114', '22932904'],"pmid")

	sys.exit()

	test_set = ["Q13155","O95376","P49407","P29066","Q9UBL3","O95352","O15169","Q8N9N5","P10415","Q07817","P11274","O14503","P51587","Q9NPI1","Q9BX70","Q9Y297","Q9ESJ1","Q9BWC9","P38936","P17676","Q92793","P45481","P55060","P68400","Q14999","Q8IWT3","Q9P0U4","Q9UER7","Q92841","P17844","Q9NRR4","Q9BV47","O14641","P03126","P06463","Q09472","P15036","Q86XK2","O43524","P49841","P32780","Q13547","Q86Z02","P09429","P61978","P61978","P02829","P34931","P38646","P04792","P42858","Q7Z6Z7","Q16666","Q08619","O14920","Q9UHH9","Q6NYC1","Q92993","Q9H7Z6","O60341","Q8IZD2","Q16363","P43356","Q9UBF1","P46821","Q15759","Q8IW41","Q00987","O15151","Q9UHC7","O75970","P04731","P19338","Q9Y618","P23511","P25208","Q9Y3T9","O60936","P06748","P06748","Q15466","P22736","O43847","P89055","O60285","P49757","Q96FW1","Q8TEW0","P09874","Q96KB5","P35232","O75925","O75928","Q8N2W9","Q13526","P53350","P29590","P30405","P36873","Q8WUF5","P30153","Q13362","Q05655","P61289","Q05397","Q06609","Q06330","Q96PM5","Q6PCD5","P23396","Q8N488","P23297","P29034","P26447","P04271","Q15424","Q8WTS6","P31947","Q96ST3","Q96EB6","Q923E4","Q15796","Q9NRG4","Q8R5A0","O95863","Q06945","P08047","Q12772","Q96SB4","P63165","Q86TM6","P20226","P15884","Q96GM8","Q12888","Q12888","Q13625","Q9H3D4","O88898","P13693","O15164","Q15672","P26687","P0CG48","Q05086","Q96PU4","Q9H9J4","Q9H9J4","Q93009","P11473","Q99986","Q14191","O14980","P12956","P61981","P63104"]
	data = interactionDownloaderObj.getSubNetworks(test_set)
	pprint.pprint(data)
	sys.exit()


	data = interactionDownloaderObj.existsInteractions("Q8N3Y1","Q9H0W5")
	pprint.pprint(data)
	sys.exit()

	data = interactionDownloaderObj.getLocalisationInteractions("Q96G97",['Endoplasmic reticulum','Nucleus'])
	pprint.pprint(data)

	sys.exit()

	print("HERE")
	data = interactionDownloaderObj.getDomainInteractions("P20248","PF00400")
	pprint.pprint(data)


	test_set = ["Q13155","O95376","P49407","P29066","Q9UBL3","O95352","O15169","Q8N9N5","P10415","Q07817","P11274","O14503","P51587","Q9NPI1","Q9BX70","Q9Y297","Q9ESJ1","Q9BWC9","P38936","P17676","Q92793","P45481","P55060","P68400","Q14999","Q8IWT3","Q9P0U4","Q9UER7","Q92841","P17844","Q9NRR4","Q9BV47","O14641","P03126","P06463","Q09472","P15036","Q86XK2","O43524","P49841","P32780","Q13547","Q86Z02","P09429","P61978","P61978","P02829","P34931","P38646","P04792","P42858","Q7Z6Z7","Q16666","Q08619","O14920","Q9UHH9","Q6NYC1","Q92993","Q9H7Z6","O60341","Q8IZD2","Q16363","P43356","Q9UBF1","P46821","Q15759","Q8IW41","Q00987","O15151","Q9UHC7","O75970","P04731","P19338","Q9Y618","P23511","P25208","Q9Y3T9","O60936","P06748","P06748","Q15466","P22736","O43847","P89055","O60285","P49757","Q96FW1","Q8TEW0","P09874","Q96KB5","P35232","O75925","O75928","Q8N2W9","Q13526","P53350","P29590","P30405","P36873","Q8WUF5","P30153","Q13362","Q05655","P61289","Q05397","Q06609","Q06330","Q96PM5","Q6PCD5","P23396","Q8N488","P23297","P29034","P26447","P04271","Q15424","Q8WTS6","P31947","Q96ST3","Q96EB6","Q923E4","Q15796","Q9NRG4","Q8R5A0","O95863","Q06945","P08047","Q12772","Q96SB4","P63165","Q86TM6","P20226","P15884","Q96GM8","Q12888","Q12888","Q13625","Q9H3D4","O88898","P13693","O15164","Q15672","P26687","P0CG48","Q05086","Q96PU4","Q9H9J4","Q9H9J4","Q93009","P11473","Q99986","Q14191","O14980","P12956","P61981","P63104"]
	data = interactionDownloaderObj.getSharedInteractionsDatabase(test_set,number_of_interactors = 30)
	pprint.pprint(data)
	sys.exit()

	data = interactionDownloaderObj.getComplexDataDatabase("P20248")
	pprint.pprint(data)


	cyclin_a2_interactors =  ['O00716','O15350','O43303','O43663','O60343','O60566','O75340','O75496','O75665','O95072','P04264','P04637','P05455','P06400','P06401','P06454','P06493','P08047','P09681','P09884','P10244','P11802','P12004','P12931','P14635','P16401','P17480','P19022','P19525','P20248','P21675','P22301','P23511','P23769''P24941','P25208','P27694','P28749','P29374','P30260','P30304','P30307','P31751','P33552','P35222','P38398','P38936','P39748','P43364','P46527','P47712','P49848','P49918','P50553','P50613','P51114','P51587','P54198','P54725','P55036','P60900','P61024','P62324','P62807','P62877','P63208','P78396','P99999','Q00526','Q00534','Q00535','Q00597','Q01094','Q02539','Q04759','Q08999','Q09472','Q0VAA5','Q12834','Q13111','Q13257','Q13309','Q13352','Q13415','Q13416','Q13616','Q13627','Q14493','Q14676','Q15544','Q15545','Q16254','Q2TAL5','Q53RE8','Q6PJG2','Q6PKC3','Q7L5N1','Q7Z2Z1','Q86T82','Q86VM9','Q8IVL5','Q8IWL3','Q8NG08','Q8TDR0','Q92738','Q92769','Q92793','Q92831','Q92905','Q96AB6','Q96DY7','Q96EV8','Q96P4','Q96RL1','Q99640','Q99741','Q9BXP5','Q9BY12','Q9H147','Q9H211','Q9H7P9','Q9NR09','Q9NYG5','Q9NZJ0','Q9P021','Q9II4','Q9JX5','Q9LD6','Q9M11','Q9PN9','Q9PP1','Q9Y243','Q9Y3Z3','Q9Y463','P31749']

	data = interactionDownloaderObj.getInteractionsDatabase(cyclin_a2_interactors,"pairwise")
	pprint.pprint(data)


	data = interactionDownloaderObj.getInteractionsDatabase(["10531037"],"pmid")
	pprint.pprint(data)

	data = interactionDownloaderObj.getInteractionsDatabase(["P20248"],"accession")
	pprint.pprint(data)


	sys.exit()

	data = interactionDownloaderObj.makeCytoscapeJson("P20248")

	sys.exit()

	data = interactionDownloaderObj.getInteractions(["P20248"],"accession")
	pprint.pprint(data['data'])



	interactionDownloaderObj.getComplexData("P20248")

	cyclin_a2_interactors = ['O00716','O15350','O43303','O43663','O60343','O60566','O75340','O75496','O75665','O95072','P04264','P04637','P05455','P06400','P06401','P06454','P06493','P08047','P09681','P09884','P10244','P11802','P12004','P12931','P14635','P16401','P17480','P19022','P19525','P20248','P21675','P22301','P23511','P23769''P24941','P25208','P27694','P28749','P29374','P30260','P30304','P30307','P31751','P33552','P35222','P38398','P38936','P39748','P43364','P46527','P47712','P49848','P49918','P50553','P50613','P51114','P51587','P54198','P54725','P55036','P60900','P61024','P62324','P62807','P62877','P63208','P78396','P99999','Q00526','Q00534','Q00535','Q00597','Q01094','Q02539','Q04759','Q08999','Q09472','Q0VAA5','Q12834','Q13111','Q13257','Q13309','Q13352','Q13415','Q13416','Q13616','Q13627','Q14493','Q14676','Q15544','Q15545','Q16254','Q2TAL5','Q53RE8','Q6PJG2','Q6PKC3','Q7L5N1','Q7Z2Z1','Q86T82','Q86VM9','Q8IVL5','Q8IWL3','Q8NG08','Q8TDR0','Q92738','Q92769','Q92793','Q92831','Q92905','Q96AB6','Q96DY7','Q96EV8','Q96P4','Q96RL1','Q99640','Q99741','Q9BXP5','Q9BY12','Q9H147','Q9H211','Q9H7P9','Q9NR09','Q9NYG5','Q9NZJ0','Q9P021','Q9II4','Q9JX5','Q9LD6','Q9M11','Q9PN9','Q9PP1','Q9Y243','Q9Y3Z3','Q9Y463','P31749']
	interactionDownloaderObj.collapseByComplex(cyclin_a2_interactors)

	#data = interactionDownloaderObj.getInteractionsTable(["P20248"],"accession",sys.argv[1])

	#data = interactionDownloaderObj.getInteractions(["P20248"],"accession")
	#pprint.pprint(data['data']['P20248'].keys())
	sys.exit()

	test_set = ["Q13155","O95376","P49407","P29066","Q9UBL3","O95352","O15169","Q8N9N5","P10415","Q07817","P11274","O14503","P51587","Q9NPI1","Q9BX70","Q9Y297","Q9ESJ1","Q9BWC9","P38936","P17676","Q92793","P45481","P55060","P68400","Q14999","Q8IWT3","Q9P0U4","Q9UER7","Q92841","P17844","Q9NRR4","Q9BV47","O14641","P03126","P06463","Q09472","P15036","Q86XK2","O43524","P49841","P32780","Q13547","Q86Z02","P09429","P61978","P61978","P02829","P34931","P38646","P04792","P42858","Q7Z6Z7","Q16666","Q08619","O14920","Q9UHH9","Q6NYC1","Q92993","Q9H7Z6","O60341","Q8IZD2","Q16363","P43356","Q9UBF1","P46821","Q15759","Q8IW41","Q00987","O15151","Q9UHC7","O75970","P04731","P19338","Q9Y618","P23511","P25208","Q9Y3T9","O60936","P06748","P06748","Q15466","P22736","O43847","P89055","O60285","P49757","Q96FW1","Q8TEW0","P09874","Q96KB5","P35232","O75925","O75928","Q8N2W9","Q13526","P53350","P29590","P30405","P36873","Q8WUF5","P30153","Q13362","Q05655","P61289","Q05397","Q06609","Q06330","Q96PM5","Q6PCD5","P23396","Q8N488","P23297","P29034","P26447","P04271","Q15424","Q8WTS6","P31947","Q96ST3","Q96EB6","Q923E4","Q15796","Q9NRG4","Q8R5A0","O95863","Q06945","P08047","Q12772","Q96SB4","P63165","Q86TM6","P20226","P15884","Q96GM8","Q12888","Q12888","Q13625","Q9H3D4","O88898","P13693","O15164","Q15672","P26687","P0CG48","Q05086","Q96PU4","Q9H9J4","Q9H9J4","Q93009","P11473","Q99986","Q14191","O14980","P12956","P61981","P63104"]
	data = interactionDownloaderObj.getSharedInteractions(test_set,number_of_interactors = 30)
	pprint.pprint(data)

	data = interactionDownloaderObj.getSubNetworkPlot(["Q9NYY8","Q58FF8","Q8N183","P57740","Q9Y3P9","P07942","P10155","P52209","Q99729","Q13356","Q86VR2","P23258","Q9NRH3","Q9UNH5","Q4ADV7","Q99622","Q5HYK7","Q9H4B6","Q9BTE1","Q8N350","Q8NFG4","Q53SF7","Q08AD1","A7KAX9","Q13496","P56945","Q9HCG8","Q8WUM0","Q9Y613","Q7LBC6","Q15811","O15160","Q6P3S1","Q9Y3D0","Q8WUM4","Q9H6E4","P62736","P63267","P68032","P68133","Q13480","L0R8F8","Q9NQG6","P30291","Q8TAF3","P50749","Q9NQT8","Q9Y2S7","Q96NU1","Q14677","Q9BZE9","O75648","Q9NXH9","Q8IWQ3","P04350","Q9UKX7","Q15154","Q9UIS9","Q13523","Q8N3A8","Q14103","Q86WR7","O15511","Q9Y4C2","Q14CB8","Q9BYG5","Q96AY4","Q5EBL8","Q9NRR6","P49674","Q13946","P19532","Q8IV42","Q9C0D5","Q13310","Q2NL82","P98177","Q9UEY8","Q9UBI1","Q9BPU6","P62068","O75317","Q92783","O75116","O14936","P07498","Q6NUP7","Q92572","Q9UPY8","Q15555","Q8IZP0","Q9UPT9","Q9NU19","Q13049","O95208","P05166","Q9UIK4","P12111","P08134","P61586","P22681","Q8NCA5","Q8WYA6","Q96BK5","Q70EL1","P51648","Q8WXX5","Q9P260","Q12906","O60716","Q06210","Q8WWL2","Q99426","Q9H5Z1","Q9NQC7","Q9H694","Q8N6H7","Q9UBC2","Q8N7R7","Q13724","Q8WWQ0","Q9BZH6","Q12959","Q9BRK0","Q9UQ84","O43819","Q6P161","P11279","Q9NQP4","Q13616","Q6ZV73","Q70E73","Q8NEY1","P51665","P62306","Q9BPZ7","Q6V1X1","Q68EM7","P0DPB5","P0DPB6","Q9H7U1","Q86U06","Q9UHY1","Q96RT1","P42356","Q96IF1","O14981","Q9P270","Q9Y3M8","Q8NFU7","P30304","Q9BXB5","Q9BXB4","P51571","Q96D71","Q04206","Q6P1N0","Q8TE49","Q9Y617","Q9UJU6","P49840","Q9NP81","Q9C0I1","P41223","P08708","Q92777","O94973","Q96EY1","O95470","Q13285","Q15637","A6NIH7","O75909","P37198","Q9NXR1","Q08211","P11171","O95685","O43426","Q9NTZ6","O00401","Q15020","Q9UHV9","O60684","O15228","P21291","Q7Z3T8","Q9Y305","Q9NXD2","C9JLW8","Q07955","Q9Y6W5","Q96P47","O75717","Q9UBS4","Q7Z6E9","P48426","O00429","O94885","Q8TDC3","O75822","Q9UGL1","Q14966","Q9UL46","Q6UUV7","P41743","Q9UKF7","Q9NYJ8","Q14839","P49642","O43524","P30405","Q9Y3F4","P78406","Q8IWC1","Q52LJ0","P0C0L4","P36873","P19784","P61088","Q96FZ2","Q7Z6B7","Q8WUY9","P60510","Q14166","Q9NQX3","Q5JXC2","P47974","Q9H2C0","Q9BT23","Q9NPI6","Q08495","Q8IX18","Q9H3G5","Q6WKZ4","Q9HCD6","Q96FJ2","Q9HAP2","O95721","Q5JU85","O94804","Q9Y3R5","Q9ULL1","P80192","P52907","O94988","Q9Y6B6","Q9NR31","O60502","Q96NW4","Q7L8J4","Q9UPQ9","Q9H999","Q9Y4G8","P78346","Q96CV9","Q9H0H5","Q9UJM3","P21580","O14802","O95248","Q01850","P27987","Q9BVL2","P06454","O14757","Q92597","P61158","P32322","Q6PI48","P49755","Q8WWM7","Q5U5Q3","Q6ZN04","Q8N1F7","P55010","P60953","P40937","Q9Y2U9","Q15417","Q9UHB6","Q6NYC1","P20700","Q92539","Q09019","Q14257","Q99417","Q9H3N1","Q15181","Q13257","P38919","Q9H4A3","Q6NUK4","Q14008","Q7L576","P67870","O43399","P62854","Q14457","Q09161","Q01433","P85037","Q9H2M9","Q9Y6D9","O43432","Q86VQ1","P29966","Q96J01","Q8TCY9","Q9NZQ3","Q15437","Q8N5S9","O00750","Q8TEJ3","P67809","P41091","Q2VIR3","Q15404","Q9NYF3","Q969J3","Q6F5E8","Q9NYI0","Q15436","O60573","Q9Y4L1","Q10713","P62834","P61224","P05387","Q9NYB9","Q9GZV5","Q86W92","P61160","O94776","Q07666","Q02833","Q92541","Q92621","P62753","Q96CX2","O15355","P42677","Q96FS4","Q9ULC4","Q6P9H4","Q96QZ7","Q05682","Q96B26","Q06124","P00338","Q8NHG8","P36507","Q99627","P19484","Q9UPU9","Q08AE8","Q9H3P2","O60333","Q13136","Q9P0V3","P40939","P31751","Q8IXH7","Q9NYL2","Q5TAQ9","Q9H857","Q8N8E3","Q9UQL6","Q15369","Q9UPU5","P27635","O14964","Q6UUV9","Q8TC76","O94921","P48556","Q13322","Q9HAV4","Q96T51","P42566","Q969Z0","Q8NHQ8","P59998","Q86Z20","O15063","P84090","Q9BXW9","Q5VTR2","P49458","Q9P2F8","P46060","Q9H6Z4","O15294","O14974","Q08J23","O15211","Q9UQC2","P50750","E9PAV3","Q13765","Q07352","Q5VU43","Q5T8P6","Q14558","Q7L2J0","Q00537","O75400","Q9Y2Z4","Q9HB20","O60282","Q9Y2T2","Q9BVG4","Q15293","Q9BQK8","Q96QF0","Q96S53","P62136","Q96NE9","A6NKD9","Q7L7X3","Q86TI0","P37268","Q13045","P68400","P09884","Q07157","Q9Y312","Q06323","Q00341","Q8N9M5","P04062","Q9H6H4","O95544","Q9UNX3","P61254","Q86UW7","Q8IZD4","Q07021","P52597","Q13188","Q9Y6E0","Q9Y520","P06493","Q14244","O14776","Q02750","P30084","Q9UJF2","Q96GV9","P23528","Q8WXX0","O15144","Q96PK6","P60842","P21127","Q14195","Q8N3F8","P46937","P21359","Q9BST9","P35568","Q9H1K1","P36543","P60709","P63261","Q15185","P49591","O60292","Q9H2G2","Q9BZ23","Q8N8S7","P15056","O43719","O43684","Q7L9L4","Q9H8S9","Q9Y2J2","Q9Y285","P61221","Q00587","P18615","O00264","Q07002","O43314","Q9HB19","Q9H2V7","Q9Y657","P98175","Q16555","O00443","Q6DN90","Q2TAY7","Q16576","P27448","Q92538","P36776","Q8N122","O75688","Q6IA86","Q86X55","O75167","O60343","P41227","Q9C073","Q10567","Q9P2Y5","Q7Z3B4","P05783","P24752","P35610","Q6AWC2","Q8NFW8","O15056","O43318","Q7Z6J0","Q92841","O94875","Q6UB35","Q4G0J3","Q96AV8","P48729","P30307","Q9Y4E8","Q9BXP5","Q16630","P09429","Q9ULJ3","Q6ZW31","Q96B36","O43747","P43487","P52272","Q9BZF3","Q13617","P61962","P17858","Q92934","Q9NUQ3","P16949","Q14153","Q86SQ0","Q7L804","O14980","P30305","Q86X02","Q14739","Q9P1Y5","Q5VZ89","Q86X27","O94979","P26045","P31946","Q9Y2K2","Q15370","P40926","Q9ULR3","Q12778","P09493","O75821","P29144","Q9P0L2","Q99471","Q13098","Q8IYB3","P23381","Q8TEQ6","Q8NEB9","Q13627","Q16851","Q15678","Q9BT78","P08237","Q5SQI0","Q13625","P08727","P31153","Q5PRF9","P62277","P56524","Q9H7J1","P11940","O75420","P35544","P62861","P49815","P15170","Q5VV41","P10398","P08670","P49588","Q8TEH3","Q5SW79","Q9H4L5","Q6P1L5","Q53ET0","P31943","Q7Z4V5","P22626","O43852","Q14680","P63162","P14678","P25685","P04049","P17844","P07814","Q9Y2U5","P52292","Q8TEU7","Q15057","P63241","Q6Y7W6","Q15750","Q15003","Q6ICG6","P47756","Q8WUI4","Q9UKV0","Q8TC71","O43295","O75044","P11274","Q13009","Q9UBF8","Q12931","Q58FF3","Q8IVT5","Q4KMP7","Q6IQ23","Q13576","P00367","P17812","O43491","Q02241","P62913","P30101","Q9Y224","P15924","P61201","P54577","Q9NRA8","Q9Y2A7","Q14671","Q96F86","Q96DT0","Q9Y3R0","Q9UQB8","P27348","Q86X29","P41250","P40222","P26196","Q03393","P54105","O43175","O43896","O95835","P35813","Q9Y2H1","Q99759","Q16543","P55081","Q7KZI7","Q00536","P51659","Q5T5U3","Q9Y3I0","Q9NSK0","Q15459","O60884","O15075","P55196","P26358","O00571","Q9Y4H2","Q16204","Q9UDY2","Q05519","Q8WX92","Q14157","Q8WWI1","Q92598","P62081","P26639","O14639","O60825","Q15785","Q92499","Q13451","O75390","Q7Z401","P04264","Q9UIA9","Q8WWY3","Q01105","P31689","Q01968","Q9Y2H2","Q5VYK3","Q92625","O75122","Q8TEW0","O60763","P61981","P31947","Q9Y3B8","Q6PKG0","Q14C86","Q9ULT8","Q96PU5","P05787","P30153","Q13347","Q04637","P23396","Q06830","Q5JSL3","Q9P2R3","Q6P597","Q9H0B6","O96013","Q7KZF4","Q7Z460","Q9Y6Y0","Q07866","A0MZ66","Q15056","P46940","Q99959","O14654","Q14978","P40818","Q04917","Q02790","Q93008","P63104","Q15208","P33176","P62258","P49321","P23588"])
	pprint.pprint(data)

	data = interactionDownloaderObj.getExtendedNetwork(["Q9NYY8","Q58FF8","Q8N183","P57740","Q9Y3P9"])
	pprint.pprint(data)

	data = interactionDownloaderObj.getInteractions(["10531037"],"pmid")
	pprint.pprint(data)


	data = interactionDownloaderObj.getDomainInteractions("Q99814","PF01847")
	pprint.pprint(data)

	b56_accessions = ["Q15172","Q15173","Q14738","Q13362","Q16537"]
	b56_substrates = ['Q6T4R5','Q00537','Q14814','Q69YN4','Q86VQ1']
	data = interactionDownloaderObj.getScaffolds(b56_accessions,b56_substrates)
	pprint.pprint(data)

	sys.exit()


	cn = ["Q08209","P16298","P48454","P63098","Q96LZ3"]
	bioid = ["P23677","P24588","P13861","Q2M2I8","Q9Y4W6","P53805","Q92543","P42356","Q8IWC1","P46020","Q16799","P98174","Q5TH69","O75643","P51617","Q86VP6","Q5VYK3","Q9BVV6","P49023","P00338","Q5JTW2","Q9Y6M7","Q9BYI3","Q8NA72","O14802","P07195","O43303","Q8N0Z3","Q2TAA2","Q9NR09","Q9UKA4","Q9UPY3","Q14126","Q8N3C0","Q99733","P28290","Q15942","Q03001","Q8TDM6","P29317","Q8IWZ3","Q9Y6D6","Q12959","Q70E73","P28340","Q01484","Q15208","Q96AB3","Q9Y6W5","Q9NQT8","P22314","Q9Y6Y8","Q8TCU4","Q14204","Q9UPN3","Q8TEU7","P42224","Q96CT7","Q8IVF2","Q8NE71","Q9Y3S1","Q12968","Q5T5U3","O43795","Q9UHB6","Q99567","O60566","O14617","Q14160","P46939","Q9H4A3","Q13501","O75665","P78527","P55196","O75694","Q15154","Q05209","Q9H2G2","Q68DQ2","Q96JQ2","Q14152","P35611","Q9UEY8","Q9ULE6","P11171","O60271","Q15758","Q96A65","Q96R06","P13796","Q9P2D0","Q9BSJ8","P49915","Q6P2Q9","Q9NSD9","Q14651","Q8NBF2","Q86TP1","Q96RG2","Q96BY7","Q9Y450","Q7Z478","Q9BRR8","P13797","P61758","O43379","Q96A49","Q9BY89","P54136","Q5JSH3","Q15813","Q9H3E2","P06702","P13010","Q96LB3","Q8IW35","O95817","P35998","O15498","P31323","P35580","Q2NKX8","Q5H9R7","P61011","P08195","Q53EZ4","O15084","Q92598","Q7Z3T8","P06576","P11940","Q66PJ3","Q9NQC3","Q14533","Q7Z2K8","P05387","Q07866","O60502","Q96C19","P53618","Q8IX90","Q00610","P05388","O43491","O00161","Q9C0C9","P61956","O95684","Q9UK76","P40763","Q9UNZ2","Q02413","P35241","Q6I9Y2","O14974","Q14974","Q9Y2J2","Q16576","Q9HCC0","Q9UKK9","P24534","P63098","Q9C0B1","Q9Y4R8","Q9H3K6","O60841","Q14258","P10809","P40227","Q96CN7","Q9Y2X3","P54578","Q06124","Q9BV38","Q06830","P60709","P04406","P50914","P19367","P31930","P0DMV8","P0DMV9","Q9BT25","P62857","Q99426","P31948","Q8TCG1","Q16543","P43487","Q96C92","Q9HB71","P50402","Q71U36","Q9H3U1","Q02790"]
	data = interactionDownloaderObj.getScaffolds(cn,bioid)

	interactors = interactionDownloaderObj.getInteractions(list(data['data']['scaffold_counts'].keys()),"accession")

	dataDownloaderObj = uniprotDownloader.uniprotDownloader()
	dataDownloaderObj.options["data_path"] = os.path.abspath(interactionDownloaderObj.options["data_path"])

	for protein in data['data']['scaffold_counts']:
		scaffold_count = data['data']['scaffold_counts'][protein]
		interactor_count = len(list(interactors['data'][protein].keys()))

		print(scaffold_count,"\t", end=' ')
		print(interactor_count,"\t", end=' ')
		print("%1.3f"%(float(scaffold_count)/interactor_count),"\t", end=' ')
		print(protein,"\t", end=' ')
		print()

	for protein in data['data']['scaffolds']:
		try:
			for subunit in cn:
				print(protein,"\t", end=' ')

				protein_data = dataDownloaderObj.parseBasic(protein)['data']
				for tag in ['id','gene_name']:
					print(protein_data[tag],"\t", end=' ')

				print(subunit,"\t", end=' ')
				protein_data = dataDownloaderObj.parseBasic(subunit)['data']
				for tag in ['id','gene_name']:
					print(protein_data[tag],"\t", end=' ')

				for tag in ['shared_interactors','prey_interactors', 'bait_interactors', 'direct']:
					print(data['data']['scaffolds'][protein][subunit][tag],"\t", end=' ')

				indirect_proteins = []
				for indirect_protein in data['data']['scaffolds'][protein][subunit]['indirect']:
					protein_data = dataDownloaderObj.parseBasic(indirect_protein)['data']
					indirect_proteins.append(protein_data['id'] + "|" + protein_data['gene_name'])

				print("; ".join(indirect_proteins))
		except:
			print()

		#print data['data']['scaffolds'][protein]['direct']


	data = interactionDownloaderObj.getLocalisationByInteractions(['Q3LFD5'])
	pprint.pprint(data)

	data = interactionDownloaderObj.getInteractionsDatabase(["10531037"],"pmid")
	pprint.pprint(data)
	#data = interactionDownloaderObj.getInteractions(["P04629"],"accession")
	#pprint.pprint(data)

	#data = interactionDownloaderObj.getLocalisationInteractions("Q96G97",['Endoplasmic reticulum','Nucleus'])
	#pprint.pprint(data)

	#data = interactionDownloaderObj.getDomainInteractions("Q99814","PF01847")
	#pprint.pprint(data)

	#data = interactionDownloaderObj.getInteractions(["10531037"],"pmid")
	#pprint.pprint(data)

	#data = interactionDownloaderObj.existsInteractions("Q8N3Y1","Q9H0W5")
	#pprint.pprint(data)


	elmDownloaderObj = elmDownloader.elmDownloader()
	elmDownloaderObj.options["data_path"] = "/Users/normandavey/Documents/Work/Websites/slimdb/data"

	dataDownloaderObj = uniprotDownloader.uniprotDownloader()
	dataDownloaderObj.options["data_path"] =  "/Users/normandavey/Documents/Work/Websites/slimdb/data"


	response = elmDownloaderObj.parseELM()

	elm_data = response["data"]

	elm_class_proteins = {}
	for protein in elm_data:
		for instance in elm_data[protein]:
				#data = interactionDownloaderObj.getDomainInteractions(elm_data[protein][instance]["Primary_Acc"].split("-")[0],elm_data[protein][instance]["binding_domain_id"] )



				if elm_data[protein][instance]['ELMIdentifier'] not in elm_class_proteins:
					elm_class_proteins[elm_data[protein][instance]['ELMIdentifier']] = []

				elm_class_proteins[elm_data[protein][instance]['ELMIdentifier']].append(elm_data[protein][instance]["Primary_Acc"])

	print elm_class_proteins

	for elm_class in elm_class_proteins:
		test_set = elm_class_proteins[elm_class]
		print elm_class,test_set

		data = interactionDownloaderObj.getSharedInteractions(test_set)
		print data["data"]["hub"]
		for accession in data["data"]["hub"]:
			protein_data = dataDownloaderObj.parseBasic(accession)
			pprint.pprint(protein_data)


	sys.exit()



	data = interactionDownloaderObj.getDomainInteractions("Q16665","PF01847")
	pprint.pprint(data)

	data = interactionDownloaderObj.getDomainInteractions("Q24167","PF01847")
	pprint.pprint(data)

	data = interactionDownloaderObj.getDomainInteractions("Q9Y2N7","PF01847")
	pprint.pprint(data)

	data = interactionDownloaderObj.getDomainInteractions("G5EGD2","PF01847")
	pprint.pprint(data)

	data = interactionDownloaderObj.getDomainInteractions("Q99814","PF01847")
	pprint.pprint(data)

	data = interactionDownloaderObj.getInteractions(["10531037"],"pmid")
	pprint.pprint(data)

	#data = interactionDownloaderObj.existsInteractions("Q8N3Y1","Q9H0W5")
	#pprint.pprint(data)

"""
