import os,re,time,string,json,pprint,requests,inspect, sys

import pmidDownloader
import uniprotDownloader

file_path = os.path.dirname(inspect.stack()[0][1])
sys.path.append(os.path.join(file_path,"../"))
sys.path.append(os.path.abspath(os.path.join(file_path,"../utilities")))
sys.path.append(os.path.abspath(os.path.join(file_path,"../downloaders")))

import uniprotDownloader

import utilities_downloader
import utilities_error

import config_reader
import option_reader

# python mutationsDownloader.py --accession Q96RK0 --mutation_output_type tdt --region_start 660 --region_end 660 --use_consequence_type "missense"
# python mutationsDownloader.py --accession Q96RK0 --mutation_output_type tdt --region_start 660 --region_end 660 --skip_consequence_type "frameshift,stop gained,inframe deletion"

aa_code_mappings = {
"Ala":"A",
"Arg":"R",
"Asn":"N",
"Asp":"D",
"Cys":"C",
"Gln":"Q",
"Glu":"E",
"Gly":"G",
"His":"H",
"Ile":"I",
"Leu":"L",
"Lys":"K",
"Met":"M",
"Phe":"F",
"Pro":"P",
"Ser":"S",
"Thr":"T",
"Tyr":"Y",
"Val":"V",
"Trp":"W"
}

skip_disease_tags = ["","Benign","benign","Uncertain significance","Uncertain significance","Unclassified","Not provided","Likely benign","likely benign",'Polymorphism','Benign/likely benign']

class mutationsDownloader():
	##------------------------------------------------------------------##

	def __init__(self,parseCommandline=False):
		self.options = {}

		self.options["mutation_output_compact"] = False
		self.options["mutation_output_type"] = "json"
		self.options["region_start"] = None
		self.options["region_end"] = None
		self.options["accession"] = None
		self.options["taxon"] = None
		self.options["use_consequence_type"] = []
		self.options["skip_consequence_type"] = []
		self.options["group_mutations_by"] = None
		self.options["remove_incorrectly_mapped_mutations"] = False
		self.options["require_clinical_significances"] = False

		config_options = config_reader.load_configeration_options(sections=["general","structure_reader"])

		for config_option in config_options:
			self.options[config_option] = config_options[config_option]

		self.options["mutations_data_path"] = os.path.join(self.options["data_path"],'mutations')

		if parseCommandline:
			try:
				self.options.update(option_reader.load_commandline_options(self.options,self.options))
			except:
				print("Options parsing error in mutationDownloader")
				pass


		if not isinstance(self.options["use_consequence_type"], list):
			self.options["use_consequence_type"] = self.options["use_consequence_type"].split(',')
		if not isinstance(self.options["skip_consequence_type"], list):
			self.options["skip_consequence_type"] = self.options["skip_consequence_type"].split(',')

		if not os.path.exists(self.options["mutations_data_path"]):
			os.mkdir(self.options["mutations_data_path"])

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def parseMutationsByPMID(self,pmid=None):

		if pmid != None:
			if "pmid" in self.options:
				self.options["pmid"] = pmid
			else:
				return {"status":"Error","error_type":"pmid not set"}

		pmidDownloaderObj = pmidDownloader.pmidDownloader()
		pmidDownloaderObj.options["data_path"] = self.options["data_path"]
		return pmidDownloaderObj.parseFeaturesByPMID(self.options["pmid"] ,['mutagenesis site'])

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def grabHumsavar(self):
		url = 'https://www.uniprot.org/docs/humsavar.txt'
		# url = "ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/variants/humsavar.txt"
		out_path = os.path.join(self.options["data_path"], "uniprot","humsavar.txt")
		sessionDownloaderObj = utilities_downloader.sessionDownloader()
		status = sessionDownloaderObj.download_file(url,out_path,method="GET",replace_empty=True, remake_age=60)

	def parseHumsavar(self,accession = None,alternative_keys=False):

		if accession != None: self.options["accession"] = accession

		self.grabHumsavar()
		out_path = os.path.join(self.options["data_path"], "uniprot","humsavar.txt")

		mutations = {"mutations":{},"positions":{},"phenotype":{},"protein":{}}
		table = False

		with open(out_path) as fileobject:
			for line in fileobject:
				line_bits = line.split()
				if table and len(line_bits) > 5:
					accession = line_bits[1]

					try:
						wildtype = aa_code_mappings[line_bits[3][2:5]]
					except:
						wildtype = "X"

					try:
						mutant = aa_code_mappings[line_bits[3][-3:]]
					except:
						mutant = "X"

					position = line_bits[3][5:-3]

					if self.options["accession"] != None:
						if self.options["accession"] != accession:
							continue

					if alternative_keys:
						if line_bits[2] in mutations["mutations"]:
							if line_bits[4] != mutations["mutations"][line_bits[2]]['clinicalSignificances']:
								if line_bits[4] == "Disease" or mutations["mutations"][line_bits[2]]['clinicalSignificances'] == "Disease":
									mutations["mutations"][line_bits[2]]['clinicalSignificances'] = "Disease"
								else:
									mutations["mutations"][line_bits[2]]['clinicalSignificances'] += "," + line_bits[4]

							if " ".join(line_bits[6:]).strip() != "":
								mutations["mutations"][line_bits[2]]['description'] += ","  + " ".join(line_bits[6:])


						else:
							mutations["mutations"][line_bits[2]] = {
							"accession":accession,
							"begin":position,
							"end":position,
							"wildType":wildtype,
							"alternativeSequence":mutant,
							"FTId": line_bits[2],
							"dbSNP": line_bits[5],
							"variant_type": line_bits[4],
							"variant": line_bits[3],
							"description":" ".join(line_bits[6:]),
							'consequenceType':'missense',
							'xrefs':[{'name': 'UniProt', 'id': line_bits[2]}],
							'sourceType': 'uniprot_curation',
							'clinicalSignificances':line_bits[4],
							"description":" ".join(line_bits[6:])
							}

					else:
						mutations["mutations"][line_bits[2]] = {
						"accession":accession,
						"position":position,
						"wildtype":wildtype,
						"mutant":position,
						"FTId": line_bits[2],
						"dbSNP": line_bits[5],
						"variant_type": line_bits[4],
						"variant": line_bits[3],
						"description":" ".join(line_bits[6:])
						}

					if line_bits[4] not in mutations["phenotype"]:
						mutations["phenotype"][line_bits[4]] = {}

					if accession not in mutations["protein"]:
						mutations["protein"][accession] = {}

					if position not in mutations["phenotype"][line_bits[4]]:
						mutations["phenotype"][line_bits[4]][position] = []

					if position not in mutations["protein"][accession]:
						mutations["protein"][accession][position] = []

					mutations["phenotype"][line_bits[4]][position].append(line_bits[2])
					mutations["protein"][accession][position].append(line_bits[2])

					if position not in mutations["positions"]:
						mutations["positions"][position] = []

					mutations["positions"][position].append(line_bits[2])


				if len(line_bits) > 0:
					if line_bits[0] == "_________":
						table = True

		return mutations

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def grabGFF(self):
		url = "http://www.uniprot.org/uniprot/?sort=&desc=&compress=no&query=reviewed:yes%20taxonomy:9606&fil=&force=no&preview=true&format=gff"
		out_path = os.path.join(self.options["mutations_data_path"], "human.gff")

		sessionDownloaderObj = utilities_downloader.sessionDownloader()
		status = sessionDownloaderObj.download_file(url,out_path,method="GET",replace_empty=True)

	def parseGFF(self):
		self.grabGFF()
		out_path = os.path.join(self.options["mutations_data_path"], "human.gff")
		gff_data = {}
		with open(out_path) as fileobject:
			for line in fileobject:
				line_bits = line.split("\t")
				if len(line_bits) > 7:
					try:
						if line_bits[2] in ["Natural variant","Mutagenesis"]:
							accession = line_bits[0]
							position = line_bits[3]
							details = line_bits[8].split("Note=")[1].split(";")[0]
							type = line_bits[2]

							if accession not in gff_data:
								gff_data[accession] = {}

							if position not in gff_data[accession]:
								gff_data[accession][position] = []

							gff_data[accession][position].append(type + "|" + details)

					except Exception as e:
						print("E",line_bits,e)

		return gff_data

	##------------------------------------------------------------------##
	## https://www.ebi.ac.uk/proteins/api/doc/#!/variation/search
	##------------------------------------------------------------------##

	def grabMutations(self,accession):

		if isinstance(accession,(list)):
			accession = accession[0]

		out_path = os.path.join(self.options["mutations_data_path"],accession +'.json')

		if os.path.exists(out_path):
			if (time.time() - os.path.getctime(out_path))/60/60/24 >  self.options["remake_age"]:
				os.remove(out_path)

		if not os.path.exists(out_path) or self.options["remake"]:
			url = "https://www.ebi.ac.uk/proteins/api/variation?offset=0&size=-1&accession=" + accession

		#	print("Grabbing",url)

			try:
				r = requests.get(url, headers={ "Accept" : "application/json"})

				if not r.ok:
				  r.raise_for_status()
				  sys.exit()

				responseBody = r.text
				with open(out_path, 'w') as outfile:
					json.dump(responseBody, outfile)

				#print(responseBody.encode('utf8'))
				#open(out_path,"w").write(responseBody.encode('utf8'))
				time.sleep(1)
			except Exception as e:
				print(e)
				raise

			#print("Grabbed",out_path)

	##------------------------------------------------------------------##

	def parseMutationsSequence(self,accession=None):
		out_path = os.path.join(self.options["data_path"],'mutations', self.options["accession"]  +'.json')

		sequence = ""

		with open(out_path) as data_file:
			try:
				mutations = json.load(data_file)

				sequence = mutations[0]['sequence']

			except:
				sequence = ""

		return sequence

	##------------------------------------------------------------------##

	def parseMutationsByPDB(self,accession=None, start=None,end=None):

		mutationData = {}

		import pdbDownloader
		dataDownloaderObj = pdbDownloader.pdbDownloader()
		dataDownloaderObj.options['pdbID'] = self.options['pdb_id']

		accessions = dataDownloaderObj.getPDBProteins()

		for accession in accessions:
			mutationData[accession] = self.parseMutations(accession)

		if self.options['add_protein_data']:

			proteins = {}
			accessions = mutationData.keys()
			proteins = self.addProteinDetails(accessions)

			return {
				"proteins":proteins,
				"mutations":mutationData
			}
		else:
			return mutationData

	##------------------------------------------------------------------##

	def parseMutationsBulk(self,accession=None, start=None,end=None):

		mutationData = {}

		for accession in self.options["accession"]:
			mutationData[accession] = self.parseMutations(accession)

		if self.options['add_protein_data']:

			proteins = {}
			accessions = mutationData.keys()
			proteins = self.addProteinDetails(accessions)

			return {
				"proteins":proteins,
				"mutations":mutationData
			}
		else:
			return mutationData
	##------------------------------------------------------------------##

	def addProteinDetails(self,accessions):
		proteins = {}
		uniprotDownloaderObj = uniprotDownloader.uniprotDownloader()
		for protein_accession in accessions:
			if protein_accession not in proteins:
				protein_data = uniprotDownloaderObj.parseBasic(protein_accession)
				gene_name = ""
				protein_name = ""

				try:
					gene_name = protein_data['data']['gene_name']
				except:
					pass

				try:
					protein_name = protein_data['data']['protein_name']
				except:
					pass

				proteins[protein_accession] = {
					'gene_name':gene_name,
					'protein_name':protein_name
				}

		return proteins

	##------------------------------------------------------------------##

	def parseMutations(self,accession=None, start=None,end=None):
		try:

			if accession != None: self.options["accession"] = accession
			if start != None: self.options["region_start"] = start
			if end != None: self.options["region_end"] = end

			if isinstance(self.options["accession"],(list)):
				self.options["accession"] = self.options["accession"][0]

			self.grabMutations(self.options["accession"])

			out_path = os.path.join(self.options["data_path"],'mutations', self.options["accession"]  +'.json')

			with open(out_path,'r') as data_file:
				try:
					mutations = json.load(data_file)

					if isinstance(mutations,(list)):
						pass
					else:
						try:
							mutations = json.loads(mutations)
						except ValueError as e:
							pass
				except:
					raise
					return {}

			#mutations[0]['features'] = []
			humsaVarData = self.parseHumsavar(self.options["accession"],alternative_keys=True)

			for humsaVar in humsaVarData['mutations']:
				try:
					if not mutations:
						# grab missing sequence, if no mutations were found in EBI
						uniprotDownloaderObj = uniprotDownloader.uniprotDownloader()
						protein_data = uniprotDownloaderObj.parseBasic(self.options['accession'])
						mutations = [{"features": [], "sequence": protein_data['data']['sequence']}]
					mutations[0]['features'] += [humsaVarData['mutations'][humsaVar]]
				except Exception as e:
					print("error", e)
					print(out_path)
					# pprint.pprint(humsaVarData['mutations'][humsaVar])
					# pprint.pprint(mutations)
					# sys.exit()

			if len(mutations) == 0:
				return {}

			self.stats = {}
			self.stats['protein_sequence'] = mutations[0]['sequence']
			self.stats['protein_length'] = len(mutations[0]['sequence'])

			#-------------------------------------------------#
			# Convert frameshift end to cover rest of protein #
			#-------------------------------------------------#

			features = []
			for variant in mutations[0]['features']:
				if variant['consequenceType'] in ["frameshift","stop gained"]:
					variant['end'] = str(self.stats['protein_length'])
					variant['coverage'] = "%1.2f"%(float(int(variant['end']) - int(variant['begin']))/self.stats['protein_length'])
				else:
					variant['coverage'] = 0

				features.append(variant)

			mutations[0]['features'] = features

			#------------------

			features = []
			for variant in mutations[0]['features']:
				if len(self.options["use_consequence_type"]) > 0:
					if variant['consequenceType'] in self.options["use_consequence_type"] :
						features.append(variant)
				elif len(self.options["skip_consequence_type"]) > 0:
					if variant['consequenceType'] not in self.options["skip_consequence_type"] :
						features.append(variant)
				else:
					features.append(variant)

			mutations[0]['features'] = features

			#------------------
			#-------------------

			if self.options["region_start"] == None and self.options["region_end"] != None: self.options["region_start"] = 0
			if self.options["region_start"] != None and self.options["region_end"] == None: self.options["region_end"] = self.stats['protein_length']  + 1

			if self.options["region_start"] ==None and self.options["region_end"]==None:
				pass
			else:
				selected_range = list(range(int(self.options["region_start"] ),int(self.options["region_end"])+1))

				features = []

				if len(mutations) == 0: return {}

				for variant in mutations[0]['features']:
					feature_range = list(range(int(variant['begin']),int(variant['end'])+1))

					if len(set(feature_range).intersection(selected_range)) > 0:
						features.append(variant)

				mutations[0]['features'] = features

			formatted_data = self.formatMutations(mutations)

			if self.options["mutation_output_type"] in ["webservice"] and self.options["save_to_file"]:
				out_path = os.path.join(self.options["data_path"],'mutations', self.options["accession"] +  '.webservice.json')

				if not os.path.exists(out_path):
					with open(out_path, 'w') as outfile:
						if isinstance(formatted_data,(list)):
							formatted_data = formatted_data[0]
						json.dump(formatted_data, outfile)

			return formatted_data

		except Exception as e:
			error = utilities_error.getError()
			return {
			 	"status":"Error",
				"errortype":  error
				}

	##------------------------------------------------------------------##

	def cleanClinicalSignificances(self,clinicalSignificances):

		clinical_significance_names = ["Disease","Risk factor","Affects","Association","Benign","Likely benign","Conflicting interpretations of pathogenicity","Drug response","Likely pathogenic","Pathogenic","Not provided","Other","Protective","Uncertain significance","Confers sensitivity","Unclassified","Polymorphism"]

		cleaned = []
		clinical_significance = re.split(";|,|\|",clinicalSignificances)

		for tmp in clinical_significance:
			tmp = tmp.strip()
			if "_" in tmp:
				tmp = " ".join(tmp.split("_"))
			if tmp.lower().startswith("variant of"):
				tmp = tmp[11:]
			tmp = tmp[:1].upper() + tmp[1:].lower()

			if tmp in clinical_significance_names:
				cleaned.append(tmp)
			else:
				print("sth wrong",tmp)

		cleaned = list(set(cleaned))
		cleaned.sort()

		return ",".join(cleaned)

	##------------------------------------------------------------------##

	def translateClinicalSignificances(self,clinicalSignificances,associations):
		# TODO
		# ADD - Association, Drug response, Affects, risk factor, confers sensitivity
		#

		snp_type = "snp"
		hover_str = ""

		if len(associations) > 0:
			if (clinicalSignificances.lower().count('pathogen') or clinicalSignificances.lower().count('disease')):
				snp_type = "snp_disease_pathogenic"
				hover_str += "<br><b>Associations: </b>" +  ", ".join(associations)
			else:
				snp_type = "snp_disease"
				hover_str += "<br><b>Associations: </b>" +  ", ".join(associations)
		else:
			for clinicalSignificanceTag in ['pathogen','disease','association', 'drug response', 'affects', 'risk factor', 'confers sensitivity']:
				if clinicalSignificances.lower().count(clinicalSignificanceTag) > 0:
					snp_type = "snp_pathogenic"

		return snp_type

	##------------------------------------------------------------------##

	def formatMutations(self,mutations):
		if self.options["mutation_output_type"] == "position_collapsed":
			mutations_output_type = {}
		elif self.options["group_mutations_by"] == None:
			mutations_output_type = []
		else:
			mutations_output_type = {}

		if self.options["group_mutations_by"] == None:
			return mutations

		if self.options["mutation_output_compact"] or self.options["mutation_output_type"] in ["list","tdt","webservice","position_collapsed"] or self.options["group_mutations_by"] != None:

			#--------------------------------#

			for variant in mutations[0]['features']:

				row = []

				if 'geneName' in mutations[0]:
					row.append(mutations[0]['geneName'])
				else:
					row.append("")

				if 'accession' in mutations[0]:
					row.append(mutations[0]['accession'])
				else:
					row.append("")

				if variant['consequenceType'] == "-":
					variant['consequenceType'] = "Other"

				if 'clinicalSignificances' not in variant:
					variant['clinicalSignificances'] = "Unclassified"

				if self.options["require_clinical_significances"]:
					if 'clinicalSignificances' not in variant: continue

					if len(list(set(variant['clinicalSignificances'].split(",")).difference(skip_disease_tags))) == 0: continue


				for tag in ['begin','end','wildType','alternativeSequence','consequenceType','clinicalSignificances']:
					if tag in variant:
						row.append(variant[tag])
					else:
						row.append("")
						variant[tag] = ""

				if int(variant['begin']) > self.stats['protein_length']:
					variant['residue_mapped_correctly'] = False

				else:
					if variant['consequenceType'] in ["frameshift","stop gained","inframe deletion"]:
						variant['residue_mapped_correctly'] = variant['wildType'] == self.stats['protein_sequence'][int(variant['begin'])-1]
						#print(variant['wildType'],self.stats['protein_sequence'][int(variant['begin'])-1])

						if not variant['residue_mapped_correctly']:
							if self.options["remove_incorrectly_mapped_mutations"]: continue
					else:
						variant['residue_mapped_correctly'] = variant['wildType'] == self.stats['protein_sequence'][int(variant['begin'])-1:int(variant['end'])]

						if not variant['residue_mapped_correctly']:
							if self.options["remove_incorrectly_mapped_mutations"]: continue

				#--------------------------------#

				xrefs = []
				pmids = []
				associations = []
				descriptions = []

				if 'description' in variant:
					descriptions.append(variant['description'])

				for xref in variant["xrefs"]:
					xrefs.append(xref['name'] + ":" + xref['id'])

				if 'association' in variant:
					for association in variant['association']:
						for tag in ['name']:#,'description','disease','xrefs','evidences']:
							if tag in association:
								associations.append(association[tag])

						if "evidences" in association:
							for evidence in association["evidences"]:
								if evidence["source"]['name'] == "pubmed":
									pmids.append(evidence["source"]['id'])


				variant["xrefs"] = xrefs
				variant["pmids"] = pmids
				variant["associations"] = associations
				variant["descriptions"] = descriptions

				row.append(";".join(xrefs))
				row.append(";".join(associations))
				row.append(";".join(pmids))
				row.append(";".join(descriptions))

				#--------------------------------#
				#--------------------------------#

				if isinstance(variant['clinicalSignificances'],list):
					variant['clinicalSignificances']=";".join([x['type'] for x in variant['clinicalSignificances']])

				variant['clinicalSignificance'] = self.translateClinicalSignificances(variant['clinicalSignificances'],variant["associations"])

				#--------------------------------#
				#--------------------------------#

				if self.options["group_mutations_by"] == None:
					if self.options["mutation_output_type"] == "tdt":
						mutations_output_type.append('\t'.join(row))

					if self.options["mutation_output_type"] == "list":
						mutations_output_type.append(row)
				else:


					if self.options["mutation_output_type"] == "position_collapsed":
						pass
					else:
						if variant[self.options["group_mutations_by"]] not in  mutations_output_type:
							if self.options["mutation_output_type"] == "webservice":
								mutations_output_type[variant[self.options["group_mutations_by"]]] = {}
							else:
								mutations_output_type[variant[self.options["group_mutations_by"]]] = []

						if self.options["mutation_output_type"] == "tdt":
							mutations_output_type[variant[self.options["group_mutations_by"]]].append('\t'.join(row))

						if self.options["mutation_output_type"] == "list":
							mutations_output_type[variant[self.options["group_mutations_by"]]].append(row)

					if self.options["mutation_output_type"] == "webservice":
						compact_variant = {}

						for tag in ['begin','end','wildType','alternativeSequence','consequenceType','clinicalSignificances','clinicalSignificance',"xrefs","pmids","associations","residue_mapped_correctly","descriptions",'xrefs']:
							compact_variant[tag] = variant[tag]

						if variant['begin'] not in mutations_output_type[variant[self.options["group_mutations_by"]]]:
							mutations_output_type[variant[self.options["group_mutations_by"]]][variant['begin']] = {
								"wildtype":variant['wildType'],
								"snps":[],
								"associations":[],
								"pmids":[],
								"mutated_to":[],
								"counter":0}

						for association in compact_variant['associations']:
							if association not in mutations_output_type[variant[self.options["group_mutations_by"]]][variant['begin']]["associations"]:
								mutations_output_type[variant[self.options["group_mutations_by"]]][variant['begin']]["associations"].append(association)

						for alternativeSequence in compact_variant['alternativeSequence']:
							if alternativeSequence not in mutations_output_type[variant[self.options["group_mutations_by"]]][variant['begin']]["mutated_to"]:
								mutations_output_type[variant[self.options["group_mutations_by"]]][variant['begin']]["mutated_to"].append(alternativeSequence)

						for pmid in compact_variant['pmids']:
							if pmid not in mutations_output_type[variant[self.options["group_mutations_by"]]][variant['begin']]["pmids"]:
								mutations_output_type[variant[self.options["group_mutations_by"]]][variant['begin']]["pmids"].append(pmid)

						mutations_output_type[variant[self.options["group_mutations_by"]]][variant['begin']]["counter"] += 1

					if self.options["mutation_output_type"] == "position_collapsed":
						compact_variant = {}
						for tag in ['begin','end','wildType','alternativeSequence','consequenceType','clinicalSignificances','clinicalSignificance',"xrefs","pmids","associations","residue_mapped_correctly","descriptions"]:
							if tag == 'clinicalSignificances':
								compact_variant[tag] = self.cleanClinicalSignificances(variant[tag])
							else:
								compact_variant[tag] = variant[tag]

						if variant['begin'] not in mutations_output_type:
							mutations_output_type[variant['begin']] = {}

						if variant[self.options["group_mutations_by"]] not in mutations_output_type[variant['begin']]:
							mutations_output_type[variant['begin']][variant[self.options["group_mutations_by"]]] = {
									"wildtype":variant['wildType'],
									"associations":[],
									"pmids":[],
									"xrefs":[],
									"consequence_type":[],
									"clinical_significance":[],
									"counter":0}


						if compact_variant['clinicalSignificances']:
							for clinical_significance in compact_variant['clinicalSignificances'].split(","):
								if clinical_significance not in mutations_output_type[variant['begin']][variant[self.options["group_mutations_by"]]]['clinical_significance']:
									mutations_output_type[variant['begin']][variant[self.options["group_mutations_by"]]]['clinical_significance'].append(clinical_significance)

						for association in compact_variant['associations']:
							if association not in mutations_output_type[variant['begin']][variant[self.options["group_mutations_by"]]]["associations"]:
								association = association.strip()
								mutations_output_type[variant['begin']][variant[self.options["group_mutations_by"]]]["associations"].append(association)

						for pmid in compact_variant['pmids']:
							if pmid not in mutations_output_type[variant['begin']][variant[self.options["group_mutations_by"]]]["pmids"]:
								mutations_output_type[variant['begin']][variant[self.options["group_mutations_by"]]]["pmids"].append(pmid)

						mutations_output_type[variant['begin']][variant[self.options["group_mutations_by"]]]["xrefs"] += compact_variant['xrefs']
						mutations_output_type[variant['begin']][variant[self.options["group_mutations_by"]]]["xrefs"] = list(set(mutations_output_type[variant['begin']][variant[self.options["group_mutations_by"]]]["xrefs"]))
						mutations_output_type[variant['begin']][variant[self.options["group_mutations_by"]]]["consequence_type"] = compact_variant['consequenceType']


						mutations_output_type[variant['begin']][variant[self.options["group_mutations_by"]]]["counter"] += 1


					if self.options["mutation_output_type"] == "json":
						if self.options["mutation_output_compact"]:
							compact_variant = {}
							for tag in ['begin','end','wildType','alternativeSequence','consequenceType','clinicalSignificances','clinicalSignificance',"xrefs","pmids","associations","residue_mapped_correctly","descriptions"]:
								compact_variant[tag] = variant[tag]

							mutations_output_type[variant[self.options["group_mutations_by"]]].append(compact_variant)

						else:
							mutations_output_type[variant[self.options["group_mutations_by"]]].append(variant)

			#--------------------------------#

		if self.options["group_mutations_by"] != None:
			return mutations_output_type
		elif self.options["mutation_output_type"]=="list":
			return mutations_output_type
		elif self.options["mutation_output_type"] == "tdt":
			return "\n".join(mutations_output_type)
		elif self.options["mutation_output_type"] == "webservice":
			return mutations_output_type
		else:
			return mutations

	##------------------------------------------------------------------##

	def makeMutationsOutput(self):

		mutations = self.parseMutations()
		tags = []

		if self.options["region_start"] != None:tags.append(str(self.options["region_start"]) + "-" + str(self.options["region_end"]))
		if self.options["group_mutations_by"] != None: tags.append(self.options["group_mutations_by"])
		if self.options["mutation_output_compact"]:tags.append('compact')

		tags.append("processed")
		tags.append(self.options["mutation_output_type"])
		tag_str = ".".join(tags)

		out_path = os.path.join(self.options["data_path"],'mutations', self.options["accession"] + "." + tag_str)
		out_path = "/Users/normandavey/Documents/Work/Websites/davey_lab/data/test_data/mutations/" + self.options["accession"] + "." + tag_str
		print(("Written to:" + out_path))

		if self.options["mutation_output_type"] in ["list","json"]:
			with open(out_path, 'w') as outfile:
				json.dump(mutations, outfile)

		else:
			open(out_path,"w").write(mutations)

	##------------------------------------------------------------------##

	def get_mutations(self,rest_options=None):
		if rest_options != None: self.options = rest_options

		try:
			data = self.parseMutations()

			return {
			 	"status":"Success",
				"data":data
				}

		except Exception as e:
			return {
			 	"status":"Error",
				"errortype": "SLiM Tools" + str(e)
				}

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##


if __name__ == "__main__":
	import sys
	dataDownloaderObj = mutationsDownloader(parseCommandline=True)

	protein_test = True
	taxon_test = False
	elm_test = False
	compact_json_test= False

	if dataDownloaderObj.options["accession"] == None and dataDownloaderObj.options["taxon"] == None:
		print("Set Uniprot Accession --accession")
		sys.exit()

	##------------------------------------------------------------------##

	if compact_json_test:
		dataDownloaderObj.makeMutationsOutput()

	##------------------------------------------------------------------##

	if protein_test:
		print(dataDownloaderObj.parseMutations())

	##------------------------------------------------------------------##

	if taxon_test:
		uniprotDownloaderObj = uniprotDownloader.uniprotDownloader()
		uniprotDownloaderObj.options = dataDownloaderObj.options

		accessions = uniprotDownloaderObj.parse_uniprot_accession_taxa(dataDownloaderObj.options["taxon"],reviewed=True)

		counter = 0
		for accession in accessions:
			try:
				print(counter,"\t",accession)
				dataDownloaderObj.options["accession"] = accession
				dataDownloaderObj.makeMutationsOutput()
				counter += 1
			except:
				pass

	##------------------------------------------------------------------##

	if elm_test:
		import elmDownloader
		elmDownloaderObj = elmDownloader.elmDownloader()
		elmDownloaderObj.options['data_path'] = '/Users/normandavey/Documents/Work/Websites/slimdb/data/'

		elm_data = elmDownloaderObj.parseELM()["data"]
		elm_data_sorted = list(elm_data.keys())
		elm_data_sorted.reverse()

		for protein in elm_data_sorted:
			for instance in elm_data[protein]:

				if elm_data[protein][instance]['taxon_id'] == "9606":
					for header in ['Primary_Acc','ProteinName','Start','End','binding_domain_id','ELMIdentifier',"PDB","protein_name","species_common","descriptions"]:
						print(elm_data[protein][instance][header],'\t')
					print()

					accession = elm_data[protein][instance]['Primary_Acc']
					start = int(elm_data[protein][instance]['Start'])
					end = int(elm_data[protein][instance]['End'])

					try:
						dataDownloaderObj.options["accession"] = accession
						dataDownloaderObj.options["region_start"] = start
						dataDownloaderObj.options["region_end"] = end
						dataDownloaderObj.parseMutations()
					except:
						pass

	##------------------------------------------------------------------##
