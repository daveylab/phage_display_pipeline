import sys
import os
import re
import time
import math
import copy
import warnings
import traceback
import pickle
import pprint
import Bio.PDB
import inspect
import json

#from xml.dom import minidom
from xml.etree import cElementTree as elementtree

warnings.filterwarnings("ignore")
import uniprotDownloader
import pmidDownloader

file_path = os.path.dirname(inspect.stack()[0][1])
sys.path.append(os.path.join(file_path,"../"))
import config_reader

sys.path.append(os.path.join(file_path,"../utilities/"))
import utilities_downloader

#-----
import logging
logger = logging.getLogger(__name__)
#-----

def del_bio_entity(entity):
	"""
	Delete a bio pdb entity.

	:exception TypeError: An atom is not an Entity and has no __getitem__()
	"""
	try:
		for child in entity:
			del_bio_entity(child)
	except TypeError:
		# entity is an atom
		pass

	entity.detach_parent()

class pdbDownloader():
	def __init__(self):
		self.options = {"add_structure_details":False}

		config_options = config_reader.load_configeration_options(sections=["general","structure_reader"])

		for config_option in config_options:
			self.options[config_option] = config_options[config_option]

		self.options["aa_codes"] = {"SEP": "s", "TPO": "t", "PTR": "y", "ASQ": "d", "NLE": "l", "AIB": "?", "MSE": "m",
		                            "HYP": "p", "MMO": "r", "DAL": "a", "DVA": "v", "DLE": "l", "DLY": "k", "DTY": "y",
		                            "DTR": "w", "GLX": "Z", "ASX": "B", "ALA": "A", "LEU": "L", "ARG": "R", "LYS": "K",
		                            "ASN": "N", "MET": "M", "ASP": "D", "PHE": "F", "CYS": "C", "PRO": "P", "GLN": "Q",
		                            "SER": "S", "GLU": "E", "THR": "T", "GLY": "G", "TRP": "W", "HIS": "H", "TYR": "Y",
		                            "ILE": "I", "VAL": "V", "DG":  "g", "DT":  "t", "DC":  "c", "DA":  "a", "G":   "g",
		                            "U":   "u", "C":   "c", "A":   "a" }

		self.options["dssp_maximum_accessibility"] = {"A": 124, "B": 157.5, "C": 94, "D": 154, "E": 187, "F": 221,
		                                              "G": 89, "H": 201, "I": 193, "K": 214, "L": 199, "M": 216,
		                                              "N": 161, "P": 130, "Q": 192, "R": 244, "S": 113, "T": 151,
		                                              "V": 169, "W": 264, "Y": 237, "X": 179, "Z": 189.5}
		self.options["dssp_fixedwidths"] = {"count": [0, 4],
		                                    "offset": [5, 9],
		                                    "chain": [10, 11],
		                                    "aa": [12, 14],
		                                    "SecondaryStructure": [16],
		                                    "3-turns/helix": [18],
		                                    "4-turns/helix": [19],
		                                    "5-turns/helix": [20],
		                                    "geometricalBend": [21],
		                                    "chirality": [22],
		                                    "betaBridgeLabel1": [23],
		                                    "betaBridgeLabel2": [24],
		                                    "betaBridgePartnerResnum": [25, 28],
		                                    "betaSheetLabel": [29, 32],
		                                    "solventAccessibility": [34, 37],
		                                    "N-H-->O": [42, 49],
		                                    "O-->H-N": [52, 60],
		                                    "N-H-->O": [63, 71],
		                                    "O-->H-N": [74, 82],
		                                    "TCO": [84, 90],
		                                    "KAPPA": [92, 96],
		                                    "ALPHA": [98, 102],
		                                    "PHI": [103, 107],
		                                    "PSI": [110, 114],
		                                    "X-CA": [117, 121],
		                                    "Y-CA": [124, 128],
		                                    "Z-CA": [130, 135]}

		self.options["pfam_pdb_mappings"] = os.path.join(self.options["data_path"], "pdb", "all.pfam.xml")

		self.uniprotDownloaderObj = uniprotDownloader.uniprotDownloader()

		self.data = {}
		self.data["intramolecular_interactions"] = {}

		self.sessionDownloaderObj = utilities_downloader.sessionDownloader()
		


	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def check_directory(self):
		if not os.path.exists(os.path.join(self.options["data_path"], "pdb")):
			os.mkdir(os.path.join(self.options["data_path"], "pdb"))

		for folder in ['description','pfam','molecule','pfam_mapping']:
			if not os.path.exists(os.path.join(self.options["data_path"], "pdb",folder)):
				os.mkdir(os.path.join(self.options["data_path"], "pdb",folder))
			
	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##


	def alignStrings(self, str1, str2, returnScore=False):
		
		strLength = len(str2)
		offset = -len(str2)

		maxOffset = [0, 0]

		str1 = "-" * len(str2) + str1 + "-" * len(str2)

		while len(str2) <= len(str1):
			matches = [str1[i] == str2[i] for i in range(0, len(str2))].count(True)

			if matches > maxOffset[1]:
				maxOffset = [offset, matches]

			offset += 1
			str2 = "*" + str2

		if strLength > 0:
			if float(maxOffset[1]) / strLength < 0.5:
				if returnScore:
					return ["False", float(maxOffset[1]) / strLength]
				else:
					return "False"
			else:
				if returnScore:
					return [maxOffset[0], float(maxOffset[1]) / strLength]
				else:
					return maxOffset[0]

	##------------------------------------------------------------------##

	#def grabPDBPfamxml(self):
	#
	#	self.check_directory()
	#	# Possibly update to : "http://www.rcsb.org/pdb/rest/hmmer?file=hmmer_pdb_all.txt"
	#	# q=pfam_accession:PF00400&wt=json&fl=pdb_id,pfam_name,pfam_clan_name,pfam_description
	#	# https://www.ebi.ac.uk/pdbe/search/pdb/select?q=pfam_accession:PF00400&wt=json&fl=pdb_id,pfam_name,pfam_clan_name,pfam_description,number_of_polymer_entities
	#	url = "http://www.rcsb.org/pdb/rest/hmmer"
	#	out_path = os.path.abspath(self.options["pfam_pdb_mappings"])
	#	
	#	self.sessionDownloaderObj.download_file(url,out_path,method="GET",replace_empty=True)

	def grabPDBPfamxml(self):
		self.check_directory()
		url =  'https://www.ebi.ac.uk/pdbe/search/pdb/select?q=pfam_accession:' + self.options['pfam_id']  + '&wt=json&fl=pdb_id,pfam_name,pfam_clan_name,pfam_description&rows=5000'
		
		out_path = os.path.join(self.options["data_path"] + "/pdb/pfam_mapping/", self.options['pfam_id'] + ".pdb.json")
		logger.debug("Grabbing Pfam mapping data for structure:" + out_path)

		response = self.sessionDownloaderObj.download_file(url,out_path,method="GET",JSON=True,replace_empty=True)
		logger.debug(response)
		return out_path

	##------------------------------------------------------------------##

	def parsePDBPfamxml(self, pfam=None, root=None):
		
		if pfam != None:
			self.options["pfam_id"] = pfam

		# https://www.ebi.ac.uk/pdbe/search/pdb/select?q=pfam_accession:PF00400&wt=json&fl=pdb_id,pfam_name,pfam_clan_name,pfam_description
		
		
		json_path = self.grabPDBPfamxml()

		with open(json_path) as outfile:
			json_content = json.load(outfile)

		pfam_hit_data = {}
		if 'response' in json_content:
			if 'docs' in json_content['response']:
				for pfam_hit in  json_content['response']['docs']:
					pdb_id = pfam_hit["pdb_id"].upper()
					pfam_hit_data[pdb_id] = {}
					#pfamHit_data[pdb_id]["chainId"] = pfamHit.attrib["pfamDesc"] + "." +  pfamHit.attrib["chainId"]
					pfam_hit_data[pdb_id]["pfamAcc"] = self.options["pfam_id"]
					pfam_hit_data[pdb_id]["pfamName"] = pfam_hit["pfam_name"]
					pfam_hit_data[pdb_id]["structureId"] = pfam_hit["pfam_description"]

		return pfam_hit_data

	##------------------------------------------------------------------##

	def parsePDBUniprotAccessions(self,uniprot_accessions = None):
		
		if uniprot_accessions != None:
			self.options["accession"] = uniprot_accessions
			
		pdbHit_data = {}
		for uniprot_accession in self.options["accession"] :
			pdbHit_data[uniprot_accession] = self.parsePDBUniprot(uniprot_accession)
		

		if self.options['add_structure_details']:
			pdbHit_details_data = {}

			for pdb_id in pdbHit_data[uniprot_accession]:
				self.options['pdbID'] = pdb_id
				structure_details = self.parseGeneralDataPDB(addReferenceDetails=False)

				if 'data' in structure_details:
					pdbHit_details_data[pdb_id] = structure_details['data']
			
			return pdbHit_details_data

		return pdbHit_data

	def parsePMIDPDBUniprot(self,pmid = None):
		
		if pmid != None:
			self.options['pmid'] = pmid
			
		pmidDownloaderObj = pmidDownloader.pmidDownloader()
		pmidDownloaderObj.options["data_path"] = self.options["data_path"]

		pdbs = pmidDownloaderObj.parsePDBByPMID(self.options['pmid'])

		uniprot_accessions = {}

		for pdb in pdbs['data']['pdbs']:
			self.options['pdbID'] = pdb
			chains = self.parseMoleculePDB()

			if chains == None: continue
			for chain in chains:
				uniprot_accession = chains[chain]['accession']
				if uniprot_accession == "": continue

				if uniprot_accession not in uniprot_accessions:
					uniprot_accessions[uniprot_accession] = []

				if pdb not in uniprot_accessions[uniprot_accession]:
					uniprot_accessions[uniprot_accession].append(pdb)

		return uniprot_accessions

	def parsePDBUniprot(self,uniprot_accession):
		pfamHit_data = {}
		protein_data = self.uniprotDownloaderObj.parseUniProt(uniprot_accession)
		pdb_ids = []

		if protein_data['status'] == 'Error':
			print(uniprot_accession,protein_data)
		else:
			for pdb_id in protein_data['data']["PDB"]:
				pdb_ids.append(pdb_id)

		return pdb_ids

	##------------------------------------------------------------------##
	
	def getPDBProteins(self):

		accessions = []
		
		chain_details = self.parseMoleculePDB()

		try:
			for chain in chain_details:
				accession = chain_details[chain]['accession']
				if accession != "":
					accessions.append(accession)
		except:
			pass
				
		return accessions

	##------------------------------------------------------------------##
	
	def getPDBsProteins(self):
		self.self.options['pdbID']
		accessions = []
		
		chain_details = self.parseMoleculePDB()

		try:
			for chain in chain_details:
				accession = chain_details[chain]['accession']
				if accession != "":
					accessions.append(accession)
		except:
			pass
				
		return accessions
	##------------------------------------------------------------------##

	def grabPDBStructure(self):
		
		if len(self.options['pdbID']) == 4:

			self.check_directory()

			url = "http://files.rcsb.org/download/" + self.options['pdbID'] + ".pdb"
			
			out_path = os.path.abspath(os.path.join(self.options["data_path"], "pdb", self.options['pdbID'] + ".pdb"))
			self.sessionDownloaderObj.download_file(url,out_path,method="GET",replace_empty=True)

	##------------------------------------------------------------------##

	def findIntraMolecularInteractions(self,protein_chain):
		interactor_residues ={}

		self.check_directory()

		intramolecular_file = os.path.join(self.options["data_path"] , "pdb","intramolecular/" , self.options["pdbID"] + "." + protein_chain.id + '.txt')

		if not os.path.exists(intramolecular_file):
			for query_res in protein_chain.child_list:
				contact = False
				for contact_res in protein_chain.child_list:
					if "N" in contact_res and "N" in query_res:
						diff_vector  = contact_res["N"].coord - query_res["N"].coord
						distance = math.sqrt(sum(diff_vector*diff_vector))

						if distance < 10:
							interactor = protein_chain.id
							contact = True

							if query_res not in interactor_residues:
								interactor_residues[query_res] = []

							if contact_res not in interactor_residues:
								interactor_residues[contact_res] = []

							interactor_residues[query_res].append(contact_res)
							interactor_residues[contact_res].append(query_res)
					else:
						pass#print contact_res,"Nope"

			interactor_residues_summer = []

			for residue in interactor_residues:
				interactor_residues_summer.append(len(set(interactor_residues[residue])) - 3)
				#print residue,set(interactor_residues[residue])

			if len(interactor_residues_summer) != 0:
				output = open(intramolecular_file, 'w')
				output.write("%1.5f"%(float(sum(interactor_residues_summer))/len(interactor_residues_summer)))
				output.close()

				return float(sum(interactor_residues_summer))/len(interactor_residues_summer)
			else:
				return 100.0
		else:
			try:
				return float(open(intramolecular_file, 'r').read())
			except:
				os.remove(intramolecular_file)
				return 100

	def findInteractingChains(self):

		self.check_directory()
		self.grabPDB()


		pdb_filename = os.path.join(self.options["data_path"] , "pdb" , self.options["pdbID"] + ".pdb")

		sequences = {}
		peptide_chains = []

		structure = Bio.PDB.PDBParser().get_structure(self.options["pdbID"], pdb_filename)

		for chain_a in structure[0].child_list:
			sequences[self.options["pdbID"] + "." + chain_a.id] = ""

			for res in chain_a.child_list:
				if res.resname.strip() in self.options["aa_codes"]:
					self.options["aa_codes"][res.resname.strip()]
					sequences[self.options["pdbID"] + "." + chain_a.id] += self.options["aa_codes"][res.resname.strip()]

			if "sequence" not in self.data["chain_details"][self.options["pdbID"] + "." + chain_a.id]:
				 self.data["chain_details"][self.options["pdbID"] + "." + chain_a.id]["sequence"] = sequences[self.options["pdbID"] + "." + chain_a.id]

			self.data["intramolecular_interactions"][self.options["pdbID"] + "." + chain_a.id] = self.findIntraMolecularInteractions(chain_a)

			if self.data["intramolecular_interactions"][self.options["pdbID"] + "." + chain_a.id] < self.options["max_intramolecular_interactions"]:
				peptide_chains.append(self.options["pdbID"] + "." + chain_a.id)

		interactors = {}
		for chain_a in structure[0].child_list:
			for chain_b in structure[0].child_list:

				if self.options["pdbID"] + "." + chain_b.id in peptide_chains or self.options["pdbID"] + "." + chain_a.id in peptide_chains:
					if chain_a != chain_b:

						[interactor, interactor_residues, pocket_sequence] = self.findInteractingResidues(chain_b, chain_a)

						if interactor != False:
							chain_b_sequence = ""
							chain_a_sequence = ""

							for chain_b_res in chain_b.child_list:
								if chain_b_res.resname in self.options["aa_codes"]:
									chain_b_sequence += self.options["aa_codes"][chain_b_res.resname]

							for chain_a_res in chain_a.child_list:
								if chain_a_res.resname in self.options["aa_codes"]:
									chain_a_sequence += self.options["aa_codes"][chain_a_res.resname]

							if (self.options["pdbID"] + "." + chain_a.id) not in interactors:
								interactors[self.options["pdbID"] + "." + chain_a.id] = {}

							tmp_domains = {}

							for domain in self.data["chain_details"][self.options["pdbID"] + "." + chain_a.id]['domains']:
								for res in list(set(interactor_residues)):
									try:
										if int(res) >= int(domain["pdbResNumStart"]) and int(res) < int(
												domain["pdbResNumEnd"]):
											if len(tmp_domains) == 0:
												tmp_domains = {
													'start': [],
													'end': [],
													'pfamAcc': [],
													'pfamName': [],
													'pfamDesc': [],
													'pdbResNumStart': [],
													'pdbResNumEnd': []
												}

											tmp_domains['pfamAcc'].append(domain['pfamAcc'].split(".")[0])
											tmp_domains['pfamName'].append(domain['pfamName'])
											tmp_domains['pfamDesc'].append(domain['pfamDesc'])
											tmp_domains['pdbResNumStart'].append(domain['pdbResNumStart'])
											tmp_domains['pdbResNumEnd'].append(domain['pdbResNumEnd'])

											break
									except:
										print("Error mapping chain domains")

							interactors[self.options["pdbID"] + "." + chain_a.id][self.options["pdbID"] + "." + chain_b.id] = {
								"interacting_residues": list(set(interactor_residues)),
								"binding_partner_sequence": chain_b_sequence,
								"binding_surface_sequence": chain_a_sequence,
								"binding_surface_interface": pocket_sequence,
								"binding_surface_interface_details": tmp_domains
							}

		del_bio_entity(structure)

		return interactors

	##------------------------------------------------------------------##

	def findInteractingChain(self, chain_motif_id):

		self.check_directory()

		self.grabPDB(self.options["pdbID"])

		pdb_filename = os.path.join(self.options["data_path"],"pdb" , self.options["pdbID"] + ".pdb")
		structure = Bio.PDB.PDBParser().get_structure(self.options["pdbID"], pdb_filename)
		model = structure[0]

		# print structure[0].child_list
		chain_a = model[chain_motif_id]

		interactors = {}

		for chain_b in structure[0].child_list:
			if chain_a != chain_b:

				[interactor, interactor_residues, pocket_sequence] = self.findInteractingResidues(chain_b, chain_a)

				if interactor != False:
					chain_b_sequence = ""
					chain_a_sequence = ""

					for chain_b_res in chain_b.child_list:
						if chain_b_res.resname in self.options["aa_codes"]:
							chain_b_sequence += self.options["aa_codes"][chain_b_res.resname]

					for chain_a_res in chain_a.child_list:
						if chain_a_res.resname in self.options["aa_codes"]:
							chain_a_sequence += self.options["aa_codes"][chain_a_res.resname]

					interactors[self.options["pdbID"] + "." + interactor] = {
						"interacting_residues": list(set(interactor_residues)),
						"binding_partner_sequence": chain_b_sequence,
						"binding_surface_sequence": chain_a_sequence,
						"binding_surface_interface": pocket_sequence
					}

		del_bio_entity(structure)

		return interactors

	##------------------------------------------------------------------##

	def findInteractingResidues(self, chain_motif, chain_protein):
		interactor = False
		interactor_residues = []
		pocket_sequence = ""

		for protein_res in chain_protein.child_list:
			contact = False
			for motif_res in chain_motif.child_list:

				if self.data['chain_details'][self.options["pdbID"] + "." + chain_motif.id]['polymer_type'] == "protein" and  self.data['chain_details'][self.options["pdbID"] + "." + chain_protein.id]['polymer_type'] == "protein":
					if "CA" in motif_res and "CA" in protein_res:
						diff_vector = motif_res["CA"].coord - protein_res["CA"].coord
						distance = math.sqrt(sum(diff_vector * diff_vector))

						if distance < self.options["inter_chain_distance"]:
							interactor = chain_motif.id
							interactor_residues.append(protein_res.get_id()[1])
							contact = True

				elif self.data['chain_details'][self.options["pdbID"] + "." + chain_motif.id]['polymer_type'] in ["rna","dna"] and  self.data['chain_details'][self.options["pdbID"] + "." + chain_protein.id]['polymer_type'] == "protein":
					protein_res_id = None
					for atom in motif_res:
						if "CA" in protein_res:
							diff_vector = atom.coord - protein_res["CA"].coord
							distance = math.sqrt(sum(diff_vector * diff_vector))
							if distance < self.options["inter_chain_distance"]:
								interactor = chain_motif.id
								contact = True
								protein_res_id = protein_res.get_id()[1]

					if protein_res_id:
						interactor_residues.append(protein_res_id)

				elif self.data['chain_details'][self.options["pdbID"] + "." + chain_motif.id]['polymer_type'] == "protein" and  self.data['chain_details'][self.options["pdbID"] + "." + chain_protein.id]['polymer_type'] in ["rna","dna"]:
					protein_res_id = None

					for atom in protein_res:
						if "CA" in motif_res:
							diff_vector = atom.coord - motif_res["CA"].coord
							distance = math.sqrt(sum(diff_vector * diff_vector))

							if distance < self.options["inter_chain_distance"]:
								interactor = chain_motif.id
								contact = True
								protein_res_id = protein_res.get_id()[1]

					if protein_res_id:
						interactor_residues.append(protein_res_id)

			if contact:
				if protein_res.resname.strip() in self.options["aa_codes"]:
					pocket_sequence += self.options["aa_codes"][protein_res.resname.strip()]
			else:
				if protein_res.resname.strip() in self.options["aa_codes"]:
					pocket_sequence += "."

		interactor_residues = list(set(interactor_residues))
		interactor_residues.sort()

		if len(interactor_residues) < 3:
			interactor = False
			interactor_residues = []
			pocket_sequence = ""

		return [interactor, interactor_residues, pocket_sequence]

	##------------------------------------------------------------------##
	## Obselete 
	## def grabPDB(self):
	## 	if len(self.options['pdbID']) == 4:
	## 		url = "http://www.rcsb.org/pdb/rest/describePDB?structureId=" + self.options['pdbID']
	## 	
	## 		out_path = os.path.join(self.options["data_path"] , "pdb", self.options['pdbID'] + ".xml")
	## 		self.sessionDownloaderObj.download_file(url,out_path,method="GET",replace_empty=True)
	## 		return out_path
	## 	else:
	## 		return ""
	##------------------------------------------------------------------##

	def grabPDB(self):
		if len(self.options['pdbID']) == 4:

			self.check_directory()
			url = 'https://data.rcsb.org/graphql?query={%0A%20 entry(entry_id:"' + self.options['pdbID'] + '") {%0A%20%20%20 entry {%0A%20%20%20%20%20 id%0A%20%20%20 }%0A%20%20%20 struct {%0A%20%20%20%20%20 title%0A%20%20%20 }%0A%20%20%20 rcsb_primary_citation {%0A%20%20%20%20%20 pdbx_database_id_PubMed%0A%20%20%20%20%20 rcsb_authors%0A%20%20%20 }%0A%20%20%20 pubmed {%0A%20%20%20%20%20 rcsb_pubmed_central_id%0A%20%20%20 }%0A%20%20%20 rcsb_entry_info {%0A%20%20%20%20%20 experimental_method%0A%20%20%20%20%20 polymer_entity_count%0A%20%20%20 }%0A%20%20%20 refine {%0A%20%20%20%20%20 ls_d_res_high%0A%20%20%20 }%0A%20%20%20 pdbx_database_PDB_obs_spr {%0A%20%20%20%20%20 replace_pdb_id%0A%20%20%20 }%0A%20%20%20 struct_keywords {%0A%20%20%20%20%20 pdbx_keywords%0A%20%20%20 }%0A%20%20%20 rcsb_entry_info {%0A%20%20%20%20%20 entity_count%0A%20%20%20%20%20 polymer_entity_count%0A%20%20%20%20%20 deposited_polymer_monomer_count%0A%20%20%20%20%20 deposited_atom_count%0A%20%20%20 }%0A%20%20%20 rcsb_accession_info {%0A%20%20%20%20%20 deposit_date%0A%20%20%20%20%20 initial_release_date%0A%20%20%20%20%20 revision_date%0A%20%20%20 }%0A%20%20%20 audit_author {%0A%20%20%20%20%20 name%0A%20%20%20 }%0A%20%20%20 pdbx_database_related {%0A%20%20%20%20%20 db_id%0A%20%20%20%20%20 content_type%0A%20%20%20%20%20 details%0A%20%20%20 }%0A%20%20%20 pdbx_database_status {%0A%20%20%20%20%20 pdb_format_compatible%0A%20%20%20 }%0A%20 }%0A}'
			out_path = os.path.join(self.options["data_path"] , "pdb", "description",self.options['pdbID'] + ".description.json")
			self.sessionDownloaderObj.download_file(url,out_path,method="GET",JSON=True,replace_empty=True)
			
			return out_path
		else:
			return ""

	##------------------------------------------------------------------##

	def parseMoleculePDB(self):

		json_path = self.grabMoleculePDB()

		#
		# PDB molecule data
		#

		if os.path.exists(json_path):
			try:
				return self.parsePolymerData({}, json_path)
			except Exception as e:
				print(e)
		else:
			print(json_path,"does not exist")

	##------------------------------------------------------------------##
	##
	## def grabMoleculePDB(self):
	##	#
	##	# PDB molecule data
	##	#
	##	url = "http://www.rcsb.org/pdb/rest/describeMol?structureId=" + self.options['pdbID']
	##
	##	out_path = os.path.join(self.options["data_path"] + "/pdb/", self.options['pdbID'] + ".molecule.xml")
	##	self.sessionDownloaderObj.download_file(url,out_path,method="GET",replace_empty=True)
	##
	##------------------------------------------------------------------##

	def grabMoleculePDB(self):
		#
		# PDB molecule data
		#

		logger.debug("Grabbing molecule data for structure")

		url = 'https://data.rcsb.org/graphql?query=%7B%0A%20%20entry(entry_id:%22' + self.options['pdbID'] + '%22)%20%7B%0A%20%20%20%20entry%20%7B%0A%20%20%20%20%20%20id%0A%20%20%20%20%7D%0A%20%20%20%20polymer_entities%20%7B%0A%20%20%20%20%20%20rcsb_polymer_entity_container_identifiers%20%7B%0A%20%20%20%20%20%20%20%20entry_id%0A%20%20%20%20%20%20%20%20auth_asym_ids%0A%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20entity_poly%20%7B%0A%20%20%20%20%20%20%20%20rcsb_entity_polymer_type%0A%20%20%20%20%20%20%20%20rcsb_sample_sequence_length%0A%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20rcsb_polymer_entity%20%7B%0A%20%20%20%20%20%20%20%20pdbx_description%0A%20%20%20%20%20%20%20%20pdbx_fragment%0A%20%20%20%20%20%20%20%20pdbx_ec%0A%20%20%20%20%20%20%09pdbx_mutation%0A%20%20%20%20%20%20%20%20details%0A%20%20%20%20%20%20%20%20formula_weight%0A%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20rcsb_entity_source_organism%20%7B%0A%20%20%20%20%20%20%20%20ncbi_scientific_name%0A%20%20%20%20%20%20%20%20ncbi_taxonomy_id%0A%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20rcsb_polymer_entity_name_com%20%7B%0A%20%20%20%20%20%20%20%20name%0A%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20uniprots%20%7B%0A%20%20%20%20%20%20%20%20rcsb_uniprot_container_identifiers%20%7B%0A%20%20%20%20%20%20%20%20%20%20uniprot_id%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20rcsb_uniprot_protein%20%7B%0A%20%20%20%20%20%20%20%20%20%20name%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20value%0A%20%20%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%7D%0A%20%20%20%20%7D%0A%20%20%7D%0A%7D'
		
		out_path = os.path.join(self.options["data_path"] + "/pdb/molecule/", self.options['pdbID'] + ".molecule.json")
		self.sessionDownloaderObj.download_file(url,out_path,method="GET",replace_empty=True)
	
		return out_path
	
	##------------------------------------------------------------------##
	
	def grabPfamPDB(self):
		#
		# PDB domain data
		#

		self.check_directory()
		
		url =  'https://www.ebi.ac.uk/pdbe/api/mappings/pfam/' + self.options['pdbID'] 
		out_path = os.path.join(self.options["data_path"] + "/pdb/pfam/", self.options['pdbID'] + ".pfam.json")
		logger.debug("Grabbing Pfam data for structure:" + out_path)

		response = self.sessionDownloaderObj.download_file(url,out_path,method="GET",JSON=True,replace_empty=True)
		logger.debug(response)
		return out_path
	##------------------------------------------------------------------##

	def grabDSSP(self):

		logger.debug("Grabbing DSSP data for structure")

		out_path = os.path.join(self.options["data_path"] ,"pdb", self.options['pdbID'] + ".dssp")
		pdb_path = os.path.join(self.options["data_path"] , "pdb", self.options['pdbID'] + ".pdb")

		if not os.path.exists(out_path) and os.path.exists(pdb_path):
			print(out_path)
			try:
				cmd = self.options['dssp_path']  + " -i " + pdb_path + " -o " + out_path
				print(cmd)
				os.system(cmd)
			except Exception as e:
				print("6",e)

		if os.path.exists(out_path):
			if os.stat(out_path).st_size == 0:
				os.remove(out_path)


	##------------------------------------------------------------------##

	def parseDSSP(self):
		dssp_data = {}
		dssp_data["chains"] = {}
		dssp_data["statistics"] = {}

		try:
			atData = False

			dssp_path = os.path.join(self.options["data_path"] , "pdb", self.options['pdbID'] + ".dssp")
			for line in open(dssp_path).read().strip().split("\n"):

				if atData == True:
					aaTmpDict = {}
					for header in self.options["dssp_fixedwidths"]:
						start = self.options["dssp_fixedwidths"][header][0]

						if len(self.options["dssp_fixedwidths"][header]) == 1:
							stop = start + 1
						else:
							stop = self.options["dssp_fixedwidths"][header][1] + 1

						aaTmpDict[header] = line[start:stop].strip()

					if len(aaTmpDict["chain"]) > 0:

						if aaTmpDict["chain"] not in dssp_data["chains"]:
							dssp_data["chains"][aaTmpDict["chain"]] = {}
							dssp_data["statistics"][aaTmpDict["chain"]] = {}

						dssp_data["chains"][aaTmpDict["chain"]][int(aaTmpDict["count"])] = copy.deepcopy(aaTmpDict)

				if line.strip()[0] == "#":
					atData = True

			for chain in list(dssp_data["chains"].keys()):
				for header in self.options["dssp_fixedwidths"]:

					dataDict = {}
					for offset in dssp_data["chains"][chain]:
						dataDict[offset] = dssp_data["chains"][chain][offset][header]

					dssp_data["statistics"][chain][header] = dataDict

		except Exception as e:
			print(e)
			raise

		return dssp_data

	##------------------------------------------------------------------##

	def stripsequenceDSSP(self, dssp_data, chain):
		sequence = ""
		buried_residues = ""
		surface_residues = ""
		ss = ""
		sa = {}
		# pprint.pprint(dssp_data)

		try:
			if chain in dssp_data["statistics"]:
				print(list(dssp_data["statistics"][chain]["aa"].keys()))
				for offset in range(min(dssp_data["statistics"][chain]["aa"].keys()),
				                    max(dssp_data["statistics"][chain]["aa"].keys()) + 1):

					if offset in dssp_data["statistics"][chain]["aa"]:
						sequence += dssp_data["statistics"][chain]["aa"][offset]
					else:
						sequence += "-"

					if "SecondaryStructure" in dssp_data["statistics"][chain]:
						if offset in dssp_data["statistics"][chain]["SecondaryStructure"]:
							if dssp_data["statistics"][chain]["SecondaryStructure"][offset] != "":
								ss += dssp_data["statistics"][chain]["SecondaryStructure"][offset]
							else:
								ss += "-"
						else:
							ss += "-"

					if "solventAccessibility" in dssp_data["statistics"][chain]:
						if offset in dssp_data["statistics"][chain]["solventAccessibility"]:
							sa[offset] = float(dssp_data["statistics"][chain]["solventAccessibility"][offset])
							if min(1, float(dssp_data["statistics"][chain]["solventAccessibility"][offset]) / self.options["dssp_maximum_accessibility"][dssp_data["statistics"][chain]["aa"][offset].upper()]) < 0.5:
								buried_residues += dssp_data["statistics"][chain]["aa"][offset]
								surface_residues += "x"
							else:
								buried_residues += "x"
								surface_residues += dssp_data["statistics"][chain]["aa"][offset]
						else:
							buried_residues += "-"
							surface_residues += "-"
		except:
			raise

		chain_data = {"buried_residues": buried_residues,"surface_residues": surface_residues,  "sequence": sequence, "ss": ss, "sa":sa}
		return chain_data

	##------------------------------------------------------------------##

	#
	#
	#
	# I've split this function out into  new better class
	#
	#

	#Use calculateAccessibility

	def mapDSSP(self, dssp_data, chain, acc, start, stop, sequence):

		chain_data = {
			"surface_accessibility_normalised": [],
			"ss": [],
			"motif_region": [],
			"chain": [],
			"buried_residues": [],
			"offset_score": [],
			"matchMotif": [],
			"ssClass": []
		}

		try:
			if chain in dssp_data["statistics"]:

				dssp_chain_data = {}
				dssp_chain_data["sequence_check"] = {}
				dssp_chain_data["secondary_structure_code"] = {}
				dssp_chain_data["solvent_accessibility"] = {}
				dssp_chain_data["surface_accessibility_normalised"] = {}

				structureAvailable = True
				motif_region = ""
				ss = ""
				acc_sequence = ""

				gappedPDBseq = ""
				for offset in range(min(dssp_data["statistics"][chain]["aa"]),
				                    max(dssp_data["statistics"][chain]["aa"]) + 1):
					if offset in dssp_data["statistics"][chain]["aa"]:
						gappedPDBseq += dssp_data["statistics"][chain]["aa"][offset]
					else:
						gappedPDBseq += "-"

				[offset, offset_score] = self.alignStrings(sequence, gappedPDBseq, True)

				print(" ".join(offset, offset_score))
				print(sequence)
				print(gappedPDBseq)

				if offset != "False":
					adj = offset - min(dssp_data["statistics"][chain]['solventAccessibility'])

					for offset in dssp_data["statistics"][chain]['solventAccessibility']:
						dssp_chain_data["sequence_check"][(offset + adj)] = dssp_data["statistics"][chain]["aa"][offset]
						dssp_chain_data["solvent_accessibility"][(offset + adj)] = dssp_data["statistics"][chain]["solventAccessibility"][offset]

						if len(dssp_data["statistics"][chain]["SecondaryStructure"][offset]) > 0:
							dssp_chain_data["secondary_structure_code"][(offset + adj)] = \
								dssp_data["statistics"][chain]["SecondaryStructure"][offset]
						else:
							dssp_chain_data["secondary_structure_code"][(offset + adj)] = "-"

					errorPDB = "True"

					try:
						for i in range(start - 1, stop + 1):
							if i in dssp_chain_data["sequence_check"]:
								dssp_chain_data["surface_accessibility_normalised"][i] = []
								motif_region += dssp_chain_data["sequence_check"][i]
								ss += dssp_chain_data["secondary_structure_code"][i]

								errorPDB = False
							else:
								motif_region += "x"
								ss += "x"

						for i in range(start - 1, stop + 1):
							if i in dssp_chain_data["sequence_check"]:
								accessibilityScore = dssp_chain_data["solvent_accessibility"][i]
								dssp_chain_data["surface_accessibility_normalised"][i] = min(1,float(accessibilityScore) / self.options["dssp_maximum_accessibility"][ dssp_chain_data[ "sequence_check"][i].upper()])

						for i in range(start - 1, stop + 1):
							if i in dssp_chain_data["sequence_check"]:
								if dssp_chain_data["surface_accessibility_normalised"][i] < 0.5:
									acc_sequence += dssp_chain_data["sequence_check"][i]
								else:
									acc_sequence += "x"

						if not errorPDB:
							chain_data["ss"].append(ss.replace("G", "H").replace("I", "H").replace("B", "E"))
							chain_data["motif_region"].append(motif_region)
							chain_data["offset_score"].append(offset_score)
							chain_data["buried_residues"] = acc_sequence
							chain_data["resolved_start"] = min(dssp_chain_data["sequence_check"].keys()) + 1
							chain_data["resolved_end"] = max(dssp_chain_data["sequence_check"].keys()) + 1
							chain_data["surface_accessibility_normalised"] = dssp_chain_data[
								"surface_accessibility_normalised"]

					except Exception as e:
						print(self.options['pdbID'], e, dssp_chain_data["sequence_check"])
			else:
				print("No statistics for",chain)
		except Exception as e:
			print("ERROR",e)
			raise

		return chain_data

	##------------------------------------------------------------------##

	def parseGeneralDataPDB(self, json_path=None, addReferenceDetails=True):
		
		if json_path == None:
			json_path = self.grabPDB()
		
		entry = {}
		with open(json_path) as outfile:
			json_content = json.load(outfile)
			if 'entry' in json_content:
				entry = json_content['entry']

		structure_data = {
			"pdb_id":self.options["pdbID"],
			"status":"ACTIVE"
		}

		try:
			structure_data["title"] = entry['struct']['title']
		except:
			structure_data["title"] = ""

		try:
			structure_data["number_entities"] = int(entry['rcsb_entry_info']['polymer_entity_count'])
		
		except:
			structure_data["number_entities"] = 0

		try:
			structure_data["pmid"] = entry['rcsb_primary_citation']['pdbx_database_id_PubMed']

			if addReferenceDetails:
				pmidDownloaderObj = pmidDownloader.pmidDownloader()
				pmidDownloaderObj.options["data_path"] = self.options["data_path"]
				
				reference_data = pmidDownloaderObj.parsePMID(structure_data["pmid"],get_proteins=False)

				if 'data' in reference_data:	
					structure_data["reference"] = reference_data['data']
		except:
			structure_data["pmid"] = None

		try:
			structure_data["experimental_method"] = entry['rcsb_entry_info']['experimental_method']
		except:
			structure_data["experimental_method"] = ""

		try:
			structure_data["resolution"] = entry['refine'][0]['ls_d_res_high']
		except:
			structure_data["resolution"] = ""

		try:
			structure_data["structure_authors"] = []
			
			for author in entry['audit_author']:
				structure_data["structure_authors"].append(author['name'])
		except:
			structure_data["structure_authors"] = ""

		try:
			structure_data["citation_authors"] = entry['rcsb_primary_citation']['rcsb_authors']
		except:
			structure_data["citation_authors"] = ""

		"""
		if structure_data["number_entities"] <= 1:
			return {"status": "Error", "error_type": "Only one entity in structure. Not a complex."}
		
		if ["status"] == "OBSOLETE":
			return {"status": "Error", "error_type": "Entry is marked as obsolete."}
		"""
		if "structure" in self.data:
			self.data["structure"] = structure_data
		else:
			return {"status": "Error",'data':structure_data}

	##------------------------------------------------------------------##
	def parsePfamDataPDB(self):
		pfamHit_data = {"ids": []}	

		if self.data["structure"]["number_entities"] > 1 and self.data["structure"]["status"] != "OBSOLETE":
			
			json_path = self.grabPfamPDB()
			print(json_path)

			pfam_tree = elementtree.parse(json_path)
			pfam_root = pfam_tree.getroot()

			for pfamHit in pfam_root.iter('pfamHit'):
				if self.options["pdbID"] + "." + pfamHit.attrib["chainId"] not in pfamHit_data:
					pfamHit_data[self.options["pdbID"] + "." + pfamHit.attrib["chainId"]] = {
						"domains": [],
					}

				pfamHit_data_temp = {}

				if pfamHit.attrib["pfamAcc"] not in pfamHit_data["ids"]:
					pfamHit_data["ids"].append(pfamHit.attrib["pfamAcc"])

				pfamHit_data_temp["pfamAcc"] = pfamHit.attrib["pfamAcc"]
				pfamHit_data_temp["pfamName"] = pfamHit.attrib["pfamName"]
				pfamHit_data_temp["pfamDesc"] = pfamHit.attrib["pfamDesc"]
				pfamHit_data_temp['pdbResNumStart'] = pfamHit.attrib["pdbResNumStart"]
				pfamHit_data_temp['pdbResNumEnd'] = pfamHit.attrib["pdbResNumEnd"]

				pfamHit_data[self.options["pdbID"] + "." + pfamHit.attrib["chainId"]]["domains"].append(pfamHit_data_temp)

		return pfamHit_data

	def parsePolymerData(self, pfamHit_data, json_path):
		with open(json_path) as outfile:
			entry = json.load(outfile)

		self.data["chain_details"] = {}
		structureProteinData = {}

		for polymer in entry.polymer_entities:
			
			polymer_type = entry.polymer_entities.entity_poly.rcsb_entity_polymer_type
			polymer_length = entry.polymer_entities.entity_poly.rcsb_sample_sequence_length

			try:
				polymer_description = entry.polymer_entities.rcsb_polymer_entity.pdbx_description
			except Exception as e:
				polymer_description = "Unknown"

			try:
				accession = entry.macroMolecule.accession.id

				try:
					structureProteinData[accession] = self.uniprotDownloaderObj.parseUniProt(accession.strip())
				except Exception as e:
					raise

				if len(structureProteinData[accession]['data']['PDB']) == 0:
					structureProteinData[accession]['data']['PDB'] = {self.options["pdbID"]: []}

			except Exception as e:
				accession = ""
				structureProteinData[accession] = {'status': 'not found',
				                                   'data': {'protein_name': "", 'sequence': "", 'species_common': "",
				                                            'taxonomy': "", 'accession': "", 'PDB': {self.options["pdbID"]: []},
				                                            'version': "", 'taxon_id': "", 'species_scientific': "",
				                                            'id': "", 'gene_name': ""}}

			try:
				fragment_description = entry.polymer_entities.rcsb_polymer_entity.pdbx_fragment
			except Exception as e:
				fragment_description = "Unknown"

			if fragment_description == None:
				fragment_description =  "Unknown"

			polymer_chains = []
			pfam_data = []

			# entry.polymer_entities.rcsb_polymer_entity_container_identifiers.auth_asym_ids
			for chain in polymer.iter('chain'):
				polymer_chains.append(chain.attrib['id'])
				if self.options["pdbID"] + "." + chain.attrib['id'] in pfamHit_data:
					pfam_data = pfamHit_data[self.options["pdbID"] + "." + chain.attrib['id']]["domains"]

			for chain in polymer.iter('chain'):
				try:
					self.data["chain_details"][self.options["pdbID"] + "." + chain.attrib['id']] = {
						"pdb_start": -1,
						"pdb_end": -1,
						"start": -1,
						"end": -1,
						"mean_intramolecular_interactions": -1,
						"mean_surface_accessibility": -1
					}

					self.data["chain_details"][self.options["pdbID"] + "." + chain.attrib['id']]["polymer_description"] = polymer_description
					self.data["chain_details"][self.options["pdbID"] + "." + chain.attrib['id']]["fragment_description"] = fragment_description
					self.data["chain_details"][self.options["pdbID"] + "." + chain.attrib['id']]["polymer_length"] = polymer_length
					self.data["chain_details"][self.options["pdbID"] + "." + chain.attrib['id']]["polymer_type"] = polymer_type
					self.data["chain_details"][self.options["pdbID"] + "." + chain.attrib['id']]["polymer_chains"] = polymer_chains
					self.data["chain_details"][self.options["pdbID"] + "." + chain.attrib['id']]["accession"] = accession
					self.data["chain_details"][self.options["pdbID"] + "." + chain.attrib['id']]['id'] = structureProteinData[accession]['data']["id"]
					self.data["chain_details"][self.options["pdbID"] + "." + chain.attrib['id']]['species_scientific'] = structureProteinData[accession]['data']["species_scientific"]
					self.data["chain_details"][self.options["pdbID"] + "." + chain.attrib['id']]['species_common'] = structureProteinData[accession]['data']["species_common"]
					self.data["chain_details"][self.options["pdbID"] + "." + chain.attrib['id']]["protein_name"] = structureProteinData[accession]['data']["protein_name"]
					self.data["chain_details"][self.options["pdbID"] + "." + chain.attrib['id']]["gene_name"] = structureProteinData[accession]['data']["gene_name"]


					if self.options["pdbID"] in structureProteinData[accession]['data']["PDB"]:
						if chain.attrib['id'] in structureProteinData[accession]['data']["PDB"][self.options["pdbID"]]:
							self.data["chain_details"][self.options["pdbID"] + "." + chain.attrib['id']]["pdb_start"] = structureProteinData[accession]['data']["PDB"][self.options["pdbID"]][chain.attrib['id']]['start']
							self.data["chain_details"][self.options["pdbID"] + "." + chain.attrib['id']]["pdb_end"] = structureProteinData[accession]['data']["PDB"][self.options["pdbID"]][chain.attrib['id']]['end']

					self.data["chain_details"][self.options["pdbID"] + "." + chain.attrib['id']]["sequence"] = structureProteinData[accession]['data']["sequence"]
					self.data["chain_details"][self.options["pdbID"] + "." + chain.attrib['id']]["domains"] = pfam_data

				except:
					raise

		return self.data["chain_details"]

	def parsePDB(self, pdb):
		self.options["pdbID"] = pdb

		if not os.path.exists(self.options["data_path"] + "/pdb/intramolecular/"):
			os.mkdir(self.options["data_path"] + "/pdb/intramolecular/")

		proteins_data = {"pdbid": str(self.options["pdbID"])}
		self.data["structure"] = {"pdb_id": pdb, "proteins": [], "domains": [], "motifs": [], "pocket_instances": {},
		                  "motif_present": False}
		self.data["chain_details"] = {}

		self.grabPDB()
		xml_path = os.path.join(self.options["data_path"] + "/pdb/", self.options["pdbID"] + ".xml")

		pdb_interfaces_filename = os.path.join(self.options["data_path"] + "/interfaces/", self.options["pdbID"] + ".json")

		error_pattern = re.compile("<error>.+</error>")

		try:
			if not os.path.exists(pdb_interfaces_filename) or self.options["remake"] == True:
				if os.path.exists(xml_path):
					if len(open(xml_path).read()) == 0:
						return {"status": "Error", "error_type": "no data returned"}

					#########################################
					###  GENERAL DATA
					#########################################

					self.parseGeneralDataPDB(xml_path)

					if self.data["structure"]["status"] != "Error" and self.data["structure"]["status"] != "OBSOLETE":

						#########################################
						###  DOMAINS
						#########################################

						pfamHit_data = self.parsePfamDataPDB()

						#########################################
						###  INTERACTORS
						#########################################

						if self.data["structure"]["number_entities"] > 1 and self.data["structure"]["number_entities"] < 8 and self.data["structure"]["status"] != "OBSOLETE":
							#####
							#####
							#####

							json_path = self.grabMoleculePDB()

							xml_path = os.path.join(self.options["data_path"] + "/pdb/molecule/", self.options["pdbID"] + ".molecule.xml")

							if os.path.exists(xml_path):
								self.data["chain_details"] = self.parsePolymerData(pfamHit_data, json_path)

							#####
							#####  INTERFACES
							#####

							self.grabPDBStructure()

							try:
								interfaces = self.findInteractingChains()
							except:
								print("Error making interfaces")
								interfaces = {}
								raise

							#####
							#####  MERGE
							#####

							if len(interfaces) > 0:
								self.grabDSSP()
								dssp_data = self.parseDSSP()

								for chain in self.data["chain_details"]:
									if chain in interfaces:
										self.data["chain_details"][chain]["interfaces"] = interfaces[chain]

										for interface in interfaces[chain]:
											if interface in interfaces:
												if chain in interfaces[interface]:
													self.data["chain_details"][chain]["interfaces"][interface]['binding_partner_surface_interface_details'] = interfaces[interface][chain]['binding_surface_interface_details']

								for chain in self.data["chain_details"]:
									if len(self.data["chain_details"][chain]['accession']) != 0:
										self.data["structure"]["proteins"].append(self.data["chain_details"][chain]['accession'])

									if chain in interfaces:
										try:
											dssp = self.stripsequenceDSSP(dssp_data, chain.split(".")[-1])

											self.data["chain_details"][chain]["ss"] = dssp["ss"]
											self.data["chain_details"][chain]["buried_residues"] = dssp["buried_residues"]
											self.data["chain_details"][chain]["chain_construct"] = dssp['sequence']
											self.data["chain_details"][chain]["mean_surface_accessibility"] = float(sum(dssp['sa'].values()))/len(list(dssp['sa'].values()))

										except Exception as e:
											self.data["chain_details"][chain]["ss"] = ""
											self.data["chain_details"][chain]["buried_residues"] = ""
											self.data["chain_details"][chain]["chain_construct"] = ""
											self.data["chain_details"][chain]["mean_surface_accessibility"] = -1
										try:
											[offset, score] = self.alignStrings(self.data["chain_details"][chain]["sequence"],self.data["chain_details"][chain]["chain_construct"],True)
										except:
											print("No alignment mapping found")
											offset = 'False'

										if offset != 'False':
											self.data["chain_details"][chain]["start"] = offset + 1
											self.data["chain_details"][chain]["end"] = offset + len(self.data["chain_details"][chain]["chain_construct"])
											self.data["chain_details"][chain]["mapped_sequence"] = self.data["chain_details"][chain]["sequence"][offset:offset + len( self.data["chain_details"][chain]["chain_construct"])]
											self.data["chain_details"][chain]["mapped_sequence_extended"] = self.data["chain_details"][chain]["sequence"][offset:offset + len(self.data["chain_details"][chain]["chain_construct"])]
										else:
											self.data["chain_details"][chain]["start"] = -1
											self.data["chain_details"][chain]["end"] = -1
											self.data["chain_details"][chain]["mapped_sequence"] = ""

										try:
											self.data["chain_details"][chain]["mean_intramolecular_interactions"] = self.data["intramolecular_interactions"][chain]
										except:
											self.data["chain_details"][chain]["mean_intramolecular_interactions"] = -1

										if "chain_construct" in self.data["chain_details"][chain]:
											if self.data["intramolecular_interactions"][chain] < self.options["max_intramolecular_interactions"]:
											#if len(self.data["chain_details"][chain]["chain_construct"]) < self.options["max_peptide_length"]:
												self.data["chain_details"][chain]["motif_present"] = True

												#
												# CRUDE CHECK IF BOTH INSTANCES ARE MOTIF LIKE e.g WW DOMAIN
												#
												for interface_tmp in interfaces:
													if chain in interfaces[interface_tmp]:
														if interface_tmp in interfaces[chain]:
															if len(interfaces[interface_tmp][chain]['binding_surface_interface'].replace(".","")) < len(interfaces[chain][interface_tmp]['binding_surface_interface'].replace(".","")):
																self.data["chain_details"][chain]["motif_present"] = False

												if self.data["chain_details"][chain]["motif_present"]:
													self.data["structure"]["motifs"].append(chain)
													self.data["structure"]["motif_present"] = True

											else:
												self.data["chain_details"][chain]["motif_present"] = False
										else:
											self.data["chain_details"][chain]["motif_present"] = False

										if self.data["structure"]["motif_present"]:

											self.data["chain_details"][chain]["peptide"] = ""
											self.data["chain_details"][chain]["n_flank"] = ""
											self.data["chain_details"][chain]["c_flank"] = ""

											self.data["chain_details"][chain]["pmids"] = self.data["structure"]["pmid"]
											self.data["chain_details"][chain]["methods"] = self.data["structure"]["experimental_method"]
											self.data["chain_details"][chain]["elm_instance_accession"] = ""
											self.data["chain_details"][chain]["elm_instance_logic"] = ""

										if self.data["chain_details"][chain]["start"] != "":
											self.data["chain_details"][chain]["n_flank"] = self.data["chain_details"][chain]["sequence"][
											                                  max(0, self.data["chain_details"][chain]["start"] - 6):
											                                  self.data["chain_details"][chain]["start"] - 1]
											self.data["chain_details"][chain]["c_flank"] = self.data["chain_details"][chain]["sequence"][
											                                  self.data["chain_details"][chain]["end"]:min(
												                                  len(self.data["chain_details"][chain]["sequence"]),
												                                  self.data["chain_details"][chain]["end"] + 5)]

										if "mapped_sequence" in self.data["chain_details"][chain]:
											self.data["chain_details"][chain]["peptide"] = self.data["chain_details"][chain]["mapped_sequence"]
										elif "chain_construct" in self.data["chain_details"][chain]:
											self.data["chain_details"][chain]["peptide"] = self.data["chain_details"][chain]["chain_construct"]
										else:
											self.data["chain_details"][chain]["peptide"] = ""


										if self.data["chain_details"][chain]["motif_present"]:
											self.data["structure"]["pocket_instances"][chain] = {}

											merged_data = {
												"protein_acc": [],
												"domain_acc": [],
												"region_start": [],
												"region_end": [],
												"pdb_region_start": [],
												"pdb_region_end": [],
												"region_sequence": [],
												"region_pocket": [],
												"region_pocket_residues": [],
												"structure_pdb_id": [],
												"structure_pocket_chain": []
											}

											binding_region_data = {}
											genes = []

											for interface in interfaces[chain]:
												#print interfaces[interface][chain]
												if interface in interfaces:
													if chain in interfaces[interface]:
														motif_interface = interfaces[interface][chain]

														merged_data["protein_acc"].append(self.data["chain_details"][interface]["accession"])

														genes.append(self.data["chain_details"][interface]["gene_name"])

														if self.data["chain_details"][interface]["polymer_type"] in ["rna","dna"]:
															binding_region_data[self.data["chain_details"][interface]["polymer_type"]] = self.data["chain_details"][interface]["polymer_type"].upper()
															merged_data["domain_acc"].append(self.data["chain_details"][interface]["polymer_type"])
															merged_data["pdb_region_start"].append(-1)
															merged_data["pdb_region_end"].append(-1)
															self.data["structure"]['domains'] = [self.data["chain_details"][interface]["polymer_type"]]
														else:
															if len(motif_interface['binding_surface_interface_details']) != 0:
																merged_data["domain_acc"].append(motif_interface['binding_surface_interface_details']['pfamAcc'])
																merged_data["pdb_region_start"].append(motif_interface['binding_surface_interface_details']['pdbResNumEnd'])
																merged_data["pdb_region_end"].append(motif_interface['binding_surface_interface_details']['pdbResNumStart'])
																for i in range(0, len(motif_interface['binding_surface_interface_details']['pfamAcc'])):
																	binding_region_data[motif_interface['binding_surface_interface_details']['pfamAcc'][i]] = motif_interface['binding_surface_interface_details']['pfamDesc'][i]

																	if motif_interface['binding_surface_interface_details']['pfamAcc'][i] not in self.data["structure"]['domains']:
																		self.data["structure"]['domains'].append(motif_interface['binding_surface_interface_details']['pfamAcc'][i])
															else:
																merged_data["domain_acc"].append(["Region in " + self.data["chain_details"][interface]["gene_name"]])
																merged_data["pdb_region_start"].append([])
																merged_data["pdb_region_end"].append([])

														merged_data["region_sequence"].append(motif_interface['binding_surface_sequence'])
														merged_data["region_pocket"].append(motif_interface['binding_surface_interface'])
														merged_data["region_pocket_residues"].append(motif_interface['interacting_residues'])
														merged_data["structure_pocket_chain"].append(interface)

														try:
															[offset, score] = self.alignStrings(self.data["chain_details"][interface]["sequence"],motif_interface['binding_surface_sequence'], True)
														except:
															offset = 'False'

														if offset != 'False':
															merged_data["region_start"].append(offset)
															merged_data["region_end"].append(offset + len(motif_interface['binding_surface_sequence']))

											merged_data["motif_acc"] = self.data["chain_details"][chain]['accession']
											merged_data["motif_start"] = self.data["chain_details"][chain]['start']
											merged_data["motif_end"] = self.data["chain_details"][chain]['end']
											merged_data["motif_sequence"] = self.data["chain_details"][chain]['chain_construct']
											merged_data["motif_buried_residues"] = self.data["chain_details"][chain]['buried_residues']
											merged_data["motif_ss"] = self.data["chain_details"][chain]['ss']
											merged_data["structure_pdb_id"] = chain.split(".")[0]
											merged_data["structure_motif_chain"] = chain

											synonyms = []
											if self.data["chain_details"][chain]['fragment_description'].strip() == "Unknown":
												synonyms = []
											elif self.data["chain_details"][chain]['fragment_description'].lower().count("unp residues") > 0 and len(self.data["chain_details"][chain]['fragment_description'].lower().split("unp residues")[0].strip()) == 0:
												synonyms = []
											elif self.data["chain_details"][chain]['fragment_description'].lower().count("unp residues") > 0 and len(self.data["chain_details"][chain]['fragment_description'].lower().split("unp residues")[0].strip()) > 0:
												synonyms = [self.data["chain_details"][chain]['fragment_description'].lower().split("unp residues")[0].strip(" ,(")]
											elif self.data["chain_details"][chain]['fragment_description'].lower().count("residues") > 0 and len(self.data["chain_details"][chain]['fragment_description'].lower().split("residues")[0].strip()) == 0:
												synonyms = []
											elif self.data["chain_details"][chain]['fragment_description'].lower().count("residues") > 0 and len(self.data["chain_details"][chain]['fragment_description'].lower().split("residues")[0].strip()) > 0:
												synonyms = [self.data["chain_details"][chain]['fragment_description'].lower().split("residues")[0].strip(" ,(")]
											else:
												synonyms = [self.data["chain_details"][chain]['fragment_description'].lower()]


											if len(binding_region_data) != 0:
												merged_data["common_name"] = "/".join(list(set(binding_region_data.values()))) + " binding motif"
												merged_data["synonyms"] = ["/".join(list(set(binding_region_data.values()))) + " binding motif"] + synonyms
											else:
												if len(synonyms) == 0:
													merged_data["common_name"] = "Unknown binding motif"
													merged_data["synonyms"] = []
												else:
													merged_data["common_name"] = synonyms[0]
													merged_data["synonyms"] = synonyms

											merged_data["motif_type"] = ""
											merged_data["motif_type_id"] = ""

											merged_data["elm_class_id"] = ""
											merged_data["elm_class_accession"] = ""
											merged_data["elm_motif"] = ""

											merged_data["domain_id"] = []
											merged_data["domain_acc"] = []

											for domain in binding_region_data:
												merged_data["domain_acc"].append(domain)
												merged_data["domain_id"].append(binding_region_data[domain])

											merged_data["domain_acc"] = list(set(merged_data["domain_acc"]))
											merged_data["domain_id"] = list(set(merged_data["domain_id"]))
											merged_data["domains_shortcode"] = "-".join(merged_data["domain_acc"])

											#
											#

											if merged_data["domain_acc"] == []:
												merged_data["domain_acc"] = ["None"]
												merged_data["domain_id"] = ["None"]
												merged_data["domains_shortcode"] = "None"

											if merged_data["motif_acc"] == "":
												merged_data["motif_acc"] = "Unmapped"

											if merged_data["motif_start"] == "":
												merged_data["motif_start"] = 1

											if merged_data["motif_end"] == "":
												merged_data["motif_end"] = len(merged_data["motif_sequence"])

											#
											#

											self.data["structure"]["pocket_instances"][chain] = merged_data

										#
										# pprint.pprint(self.data["chain_details"][chain])
										#
										#####
										#####
										#####
					else:
						response = self.data["structure"]
						open(pdb_interfaces_filename, "w").write(pickle.dumps(response))
						return response

					self.data["structure"]["chains"] = self.data["chain_details"]
					self.data["structure"]["pocket"] = {}
					self.data["structure"]["interface_instances"] = {}
					self.data["structure"]["specificity_class"] = {}
					self.data["structure"]["motif_instances"] = {}

					if "motifs" in self.data["structure"]:
						for motif in self.data["structure"]["motifs"]:
							self.data["structure"]["pocket"][motif] = {}
							self.data["structure"]["specificity_class"][motif] = {}
							self.data["structure"]["interface_instances"][motif] = {}
							self.data["structure"]["motif_instances"][motif] = {}

							pocket = ["common_name", "synonyms", "domain_id", "domain_acc", "domains_shortcode"]
							for pocket_header in pocket:
								self.data["structure"]["pocket"][motif][pocket_header] = self.data["structure"]["pocket_instances"][motif][pocket_header]

							interface_instances = ["protein_acc", "domain_acc", "domains_shortcode", "region_start",
							                       "region_end", "region_pocket", "region_pocket_residues",
							                       "region_sequence", "motif_acc", "motif_start", "motif_end",
							                       "motif_sequence", "motif_buried_residues", "motif_ss",
							                       "structure_pdb_id", "structure_motif_chain",
							                       "structure_pocket_chain"]

							for interface_instance_header in interface_instances:
								self.data["structure"]["interface_instances"][motif][interface_instance_header] = self.data["structure"]["pocket_instances"][motif][interface_instance_header]

							specificity_class = ["common_name", "synonyms", "domains_shortcode", "motif_type",
							                     "motif_type_id", "elm_class_id", "elm_class_accession", "elm_motif",
							                     "domain_id"]

							for specificity_class_header in specificity_class:
								self.data["structure"]["specificity_class"][motif][specificity_class_header] = \
									self.data["structure"]["pocket_instances"][motif][specificity_class_header]

							motif_instances = ["accession", "start", "end", "ss", "peptide", "c_flank", "n_flank",
							                   "pmids", "elm_instance_accession", "elm_instance_logic", "methods"]
							for motif_instance_header in motif_instances:
								self.data["structure"]["motif_instances"][motif][motif_instance_header] = \
									self.data["structure"]['chains'][motif][motif_instance_header]

							motif_instances = ["motif_acc", "motif_start", "motif_end", "motif_sequence",
							                   "motif_buried_residues", "motif_ss"]
							for motif_instance_header in motif_instances:
								self.data["structure"]["motif_instances"][motif][motif_instance_header] = \
									self.data["structure"]["pocket_instances"][motif][motif_instance_header]

						# if self.data["structure"]["motif_present"]:
						#	pprint.pprint(self.data["structure"]["motif_instances"])

						self.data["structure"]["proteins"] = list(set(self.data["structure"]["proteins"]))

					del self.data["structure"]["pocket_instances"]

					response = {"status": "found", "data": self.data["structure"]}
				else:
					response = {"status": "Error", "error_type": "File not found"}

				open(pdb_interfaces_filename, "w").write(pickle.dumps(response))
			else:
				response = pickle.loads(open(pdb_interfaces_filename).read())

			return response

		except Exception as e:
			error_type = sys.exc_info()[0]
			error_value = sys.exc_info()[1]
			error_traceback = traceback.extract_tb(sys.exc_info()[2])

			sys.stderr.write("\n")
			sys.stderr.write('Error in routine ' + pdb + '\n')
			sys.stderr.write('Error Type       : ' + str(error_type) + '\n')
			sys.stderr.write('Error Value      : ' + str(error_value) + '\n')
			sys.stderr.write('File             : ' + str(error_traceback[-1][0]) + '\n')
			sys.stderr.write('Method           : ' + str(error_traceback[-1][2]) + '\n')
			sys.stderr.write('Line             : ' + str(error_traceback[-1][1]) + '\n')
			sys.stderr.write('Error            : ' + str(error_traceback[-1][3]) + '\n')

			if self.options["debug"]:
				raise

			return {"status": "Error", "error_type": str(e)}

"""

Not cross compatible python2/3

class testStructureRest():
	def __init__(self):
		
		url = "http://localhost:6543/resource/login?login=Norman&password=Norman"
		login_response = urllib.request.urlopen(url).read()
		login_response = json.loads(login_response)

		self.headers = login_response["headers"]
		self.added = {
			"pdbs": [],
			"pmids": [],
			"pfamids": [],
			"update_pdb_pfamids": [],
			"uniprotids": []
		}

	# --------------------------#
	# --------------------------#

	def restRequest(self, urlRequest):
		opener = urllib.request.build_opener()

		for header in self.headers:
			opener.addheaders.append(('Cookie', str(header[1])))

		f = opener.open(urlRequest).read()
		return json.loads(f)

	# --------------------------#
	# --------------------------#

	def addStructure(self, structure):
		if structure not in self.added["pdbs"]:
			response = self.restRequest("http://localhost:6543/resource/add/structure/" + structure)

			if response["status"] == "Added" or response["status"] == "Present":
				self.added["pdbs"].append(structure)

			return response
		else:
			return {"status": "Present"}

"""

if __name__ == "__main__":

	dataDownloaderObj = pdbDownloader()
	dataDownloaderObj.options["remake"] = True

	uniprotDownloaderObj = uniprotDownloader.uniprotDownloader()


	pfam_id = "PF00498"
	response = dataDownloaderObj.parsePDBPfamxml(pfam_id)
	print(",".join(list(response.keys())))
	sys.exit()

	uniprot_accession = "P06400"
	print(uniprot_accession)

	protein_data = uniprotDownloaderObj.parseBasic(uniprot_accession)
	protein_structure_data = uniprotDownloaderObj.parsePDBs(uniprot_accession)
	pprint.pprint(protein_structure_data)


	if 'data' in protein_structure_data:
		for pdb_id in ['1GUX']:
			for chain in protein_structure_data['data'][pdb_id]:
				print(pdb_id,chain,uniprot_accession)
				region_start = protein_structure_data['data'][pdb_id][chain]['start']
				region_end = protein_structure_data['data'][pdb_id][chain]['end']
				dataDownloaderObj.parsePDB(pdb_id)
				dataDownloaderObj.grabPDBStructure()
				dataDownloaderObj.grabDSSP()
				dssp_data = dataDownloaderObj.parseDSSP()

			#	print dssp_data['chains']['B']
				#
				response = dataDownloaderObj.stripsequenceDSSP(dssp_data,chain)

				sequence = protein_data['data']['sequence']

				response = dataDownloaderObj.mapDSSP(dssp_data,chain,uniprot_accession,1,len(sequence),sequence)

				print(response)


	motif_count = 0
	pdb_count = 0

	failed = []

	for pdb in ['1GUX']:
		try:
			pdb_count += 1
			response = dataDownloaderObj.parsePDB(pdb)

			if response['data']['motif_present']:
				motif_count += 1

				for instance in response['data']['motif_instances']:
					print(instance,motif_count, "\t",str(pdb_count) + "/" + str(len(pdb_ids)), "\t")
					print(",".join(response['data']['pocket'][instance]['domain_acc']),"\t")
					print(response['data']['motif_instances'][instance]['accession'],"\t")
					print(response['data']['motif_instances'][instance]['motif_start'],"\t")
					print(response['data']['motif_instances'][instance]['motif_end'],"\t")
					print(response['data']['motif_instances'][instance]['pmids'],"\t")
					print(response['data']['motif_instances'][instance]['motif_buried_residues'],"\t")
					print(response['data']['motif_instances'][instance]['motif_ss'],"\t")
					print(response['data']['pocket'][instance]['common_name'],"\t")
					print(response['data']['title'])


		except:
			failed.append(pdb)
			raise

	print(failed)
