import os,re,time,inspect,sys

from xml.dom import minidom
from xml.etree import cElementTree as elementtree

file_path = os.path.dirname(inspect.stack()[0][1])
sys.path.append(os.path.join(file_path,"../"))
sys.path.append(os.path.join(file_path,"../utilities/"))
import utilities_downloader
import config_reader

class pfamDownloader():

	def __init__(self):
		self.options = {}
		self.options["wait_time"] = 0.01
		self.options["data_path"] = ""
		self.options["remake_age"] = 1800

		config_options = config_reader.load_configeration_options(sections=["general","structure_reader"])

		for config_option in config_options:
			self.options[config_option] = config_options[config_option]

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def check_directory(self):
		if not os.path.exists(os.path.join(self.options["data_path"], "pfam")):
			os.mkdir(os.path.join(self.options["data_path"], "pfam"))

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def grabPfam(self,pfam_id):
		self.check_directory()

		url = 'https://pfam.xfam.org/family/' + pfam_id + '?output=xml';
		out_path = os.path.join(self.options["data_path"], "pfam",pfam_id + ".xml")
		sessionDownloaderObj = utilities_downloader.sessionDownloader()
		status = sessionDownloaderObj.download_file(url,out_path,method="GET",replace_empty=True)


	def parsePfam(self,pfam_id=None):
		if pfam_id == None:
			if 'pfam_id' in self.options:pfam_id = self.options['pfam_id']
			else: return {}

		self.grabPfam(pfam_id)

		pfamHit_data = {}
		xml_path = os.path.join(self.options["data_path"] , "pfam",pfam_id + ".xml")
		error_pattern = re.compile("<error>.+</error>")

		try:
			if os.path.exists(xml_path):

				errors = error_pattern.findall(open(xml_path).read())
				if len(errors) > 0:
					return  {"status":"Error","error_type":errors[0].split(">")[1].split("<")[0]}

				tree = elementtree.parse(xml_path)
				root = tree.getroot()

				pfam_data = {"pfam_acc":"","domain_name":"","domain_description":"","clan_name":"","clan_id":""}

				pfam_data["pfam_id"] = pfam_id

				if root.find('{https://pfam.xfam.org/}entry'):
					pfam_data["pfam_acc"]  = root.find('{https://pfam.xfam.org/}entry').attrib['id']
					for child in root.find('{https://pfam.xfam.org/}entry'):
						if child.tag.split("}")[1] == "description":
							pfam_data["domain_name"] =  child.text.strip()
						if child.tag.split("}")[1] == "comment":
							pfam_data["domain_description"] =  child.text.strip()
						if child.tag.split("}")[1] == "clan_membership":
							if 'clan_acc' in child.attrib:
								pfam_data['clan_id'] = child.attrib['clan_acc'].strip()
							if 'clan_id' in child.attrib:
								pfam_data['clan_name'] = child.attrib['clan_id'].strip()

				return {"status":"Added","data":pfam_data}
			else:
				return {"status":"Error","error_type":"File not found"}

		except Exception as e:
			return {"status":"Error","error_type":str(e)}
