import os,json, pprint,inspect,sys

file_path = os.path.dirname(inspect.stack()[0][1])
sys.path.append(os.path.join(file_path,"../"))
sys.path.append(os.path.join(file_path,"../utilities/"))

import utilities_downloader
import utilities_basicReader
import config_reader

class pharosDownloader():

	def __init__(self):
		self.options = {}

		self.options.update(config_reader.load_configeration_options(sections=["general","structure_reader"]))

		self.options['pharos_file_name'] = "TCRDv6.1.0_ALLexp.csv"

		self.options['pharos_data_path'] = os.path.join(self.options["data_path"], "pharos")
		self.options['json_path'] = os.path.join(self.options["pharos_data_path"],  ".".join(self.options['pharos_file_name'].split(".")[0:-1]) + ".json")
		self.options['tdt_path'] = os.path.join(self.options["pharos_data_path"], self.options['pharos_file_name'] )
		self.options['url'] = 'http://juniper.health.unm.edu/tcrd/download/' + self.options['pharos_file_name'] 
		
		self.data = {}

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def check_directory(self):
		if not os.path.exists(os.path.join(self.options["pharos_data_path"])):
			os.mkdir(os.path.join(self.options["pharos_data_path"]))

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def grab_pharos(self):
		self.check_directory()
		
		sessionDownloaderObj = utilities_downloader.sessionDownloader()
		status = sessionDownloaderObj.download_file(self.options['url'], self.options['tdt_path'] ,method="GET",replace_empty=True)
		
		self.parse_pharos()

	def parse_pharos(self):
		self.data  = utilities_basicReader.readTableFile(self.options['tdt_path'],key='UniProt', delimiter=",", stripQuotes=True, byColumn=False)
		with open(self.options['json_path'], 'w') as outfile:
			json.dump(self.data, outfile)

	def load_pharos(self):
		if not os.path.exists(self.options['tdt_path']):
			self.grab_pharos()
		
		if os.path.exists(self.options['json_path']):
			self.data = json.loads(open(self.options['json_path'], 'r').read())
		else:
			self.parse_pharos()		

	def parse_pharos_data(self):
		self.load_pharos()

		pharos_data = {}
		for accession in self.options['accession']:
			if accession in self.data:
				pharos_data[accession] = self.data[accession]
		
		return pharos_data

	def parse_pharos_druggability_data(self):
		pharos_data = self.parse_pharos_data()
		pharos_druggability_data = {}
		pharos_druggability_data_tags = [
			'TDL',
			'DrugCentral Activity Count',
			'DTO Class',
 			'DTO ID',
			'TIN-X Novelty',
			'OMIM Phenotype(s)',
			'Top 5 TIN-X Importance(s)',
			'Top 5 Text-Mining DISEASES',
			'eRAM Diseases',
			'OMIM Phenotype Count',
			'GWAS Count',
 			'GWAS Phenotype(s)'
			]

		for accession in pharos_data:
			pharos_druggability_data[accession] = {}
			for tag in pharos_druggability_data_tags:
				pharos_druggability_data[accession][tag] = pharos_data[accession][tag]

		return pharos_druggability_data

	def parse_pharos_publication_data(self):
		pharos_data = self.parse_pharos_data()
		pharos_publication_data = {}
		pharos_publication_data_tags = [
			 'JensenLab PubMed Score',
			 'PubTator Score', 
			 'NCBI Gene PubMed Count'
		]

		for accession in pharos_data:
			pharos_publication_data[accession] = {}
			for tag in pharos_publication_data_tags:
				pharos_publication_data[accession][tag] = pharos_data[accession][tag]

		return pharos_publication_data

	def parse_target_development_level(self):
		self.load_pharos()
		
		pharos_data = {}
		for accession in self.options['accession']:
			if accession in self.data:
				pharos_data[accession] = self.data[accession]['TDL']
		
		return pharos_data


if __name__ == "__main__":
	pharosDownloaderObj = pharosDownloader()
	pharosDownloaderObj.options['accession'] = ["P11802"]
	pprint.pprint(pharosDownloaderObj.parse_pharos_data())
	pprint.pprint(pharosDownloaderObj.parse_pharos_druggability_data())
	pprint.pprint(pharosDownloaderObj.parse_pharos_publication_data())
	pprint.pprint(pharosDownloaderObj.parse_target_development_level())
	