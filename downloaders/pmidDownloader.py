import os,re,sys,string, json,inspect, hashlib

from xml.dom import minidom
from xml.etree import cElementTree as elementtree


file_path = os.path.dirname(inspect.stack()[0][1])
sys.path.append(os.path.join(file_path,"../utilities/"))
import utilities_downloader

import uniprotDownloader

class pmidDownloader():

	def __init__(self):
		self.options = {}
		self.options["wait_time"] = 0.01
		self.options["data_path"] = ""
		self.options["remake_age"] = 1800

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def check_directory(self):
		if not os.path.exists(os.path.join(self.options["data_path"], "pmid")):
			os.mkdir(os.path.join(self.options["data_path"], "pmid"))

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def grabSearchPubmedResults(self,search_term,index_of_first_papers=0,no_of_papers=5,force_download=False):
		url = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&term=' + str(search_term) +'&retmode=json&retstart=' + str(index_of_first_papers) + '&retmax=' + str(no_of_papers)
		url_hash = str(hashlib.md5(url.encode()).hexdigest())
		out_path = os.path.join(self.options["data_path"] + "/pmid/",url_hash + ".search.json")
		
		sessionDownloaderObj = utilities_downloader.sessionDownloader()
		status = sessionDownloaderObj.download_file(url,out_path,method="GET",replace_empty=True,remake_age=28)
		
		return out_path

	def searchPubmed(self, search_term,index_of_first_papers=0,no_of_papers=5):
		search_results_path = self.grabSearchPubmedResults(search_term,index_of_first_papers=0,no_of_papers=5)
		
		if os.path.exists(search_results_path):

			with open(search_results_path) as outfile:
				try:
					search_response = json.load(outfile)

					if 'esearchresult' in search_response:
						search_data = search_response['esearchresult']
						search_data['article_details'] = {}
						for pmid in search_data['idlist']:
							article_details = self.parsePMID(pmid,get_proteins=False,simple=True)

							if 'data' in article_details:
								search_data['article_details'][pmid] = article_details['data']

						return {"status":"Success","data":search_data}

				except Exception as e:
					return {"status":"Error","error_type":str(e)}
		else:
			return {"status":"Error","error_type":"Unknown"}

	##------------------------------------------------------------------##

	def grabPMID(self,pmid,force_download=False):
		self.check_directory()

		url = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&id=' + pmid + '&report=xml';
		out_path = os.path.join(self.options["data_path"] + "/pmid/",pmid + ".xml")
		
		sessionDownloaderObj = utilities_downloader.sessionDownloader()
		status = sessionDownloaderObj.download_file(url,out_path,method="GET",replace_empty=True)

	##------------------------------------------------------------------##

	def parsePMIDs(self,pmids,get_proteins=False):
		articles_data = {}

		for pmid in pmids:
			response  = self.parsePMID(pmid,get_proteins)

			if response["status"] == "Error":
				articles_data[pmid] = response
			else:
				articles_data[pmid] = response["data"]
			
		return articles_data

	##------------------------------------------------------------------##

	def parsePMID(self,pmid,get_proteins=True,simple=False):
		article_data = {"pmid":pmid}

		header_mapping = {
		'ArticleTitle':"title",
		'Authors':"author_list",
		'ISOAbbreviation':"journal",
		'Volume':"article_volume",
		'Issue':"article_issue",
		'MedlinePgn':"article_page",
		'ArticleId':"article_details",
		'AbstractText':"abstract_text",
		'PDB':"pdb",
		'Year':"article_year",
		'Month':"article_month",
		}

		try:
			self.grabPMID(pmid)

			xml_path = os.path.join(self.options["data_path"] + "/pmid/",pmid + ".xml")
			#print(open(xml_path).read())
			print(xml_path)

			error_pattern = re.compile("<error>.+</error>")
			abstract_pattern = re.compile("<AbstractText[^>]*>.+</AbstractText>")
			article_title_pattern = re.compile("<ArticleTitle>.+</ArticleTitle>")

			if os.path.exists(xml_path):

				errors = error_pattern.findall(open(xml_path).read())
				xml_text = open(xml_path).read()

				if len(xml_text.strip()) == 0:
					print(("File empty:" + xml_path))
					self.grabPMID(pmid,force_download=True)
					xml_text = open(xml_path).read()

				abstractText = abstract_pattern.findall(xml_text)
				articleTitle = article_title_pattern.findall(xml_text)
				
				article_data[header_mapping["ArticleTitle"]] = articleTitle[0].replace('<ArticleTitle>','').replace('</ArticleTitle>','')#.decode('utf-8')

				if len(abstractText) > 0:
					abstractTextProcessed = ""
					for abstractTextChunk in abstractText:

						abstractTextChunk = re.sub('<AbstractText [^>]+>', '', abstractTextChunk)
						abstractTextProcessed += abstractTextChunk.replace('<AbstractText>','').replace('</AbstractText>','')#.decode('utf-8')

					article_data[header_mapping["AbstractText"]] = abstractTextProcessed
				else:
					article_data[header_mapping["AbstractText"]] = ""

				if len(errors) > 0:
					return  {"status":"Error","error_type":errors[0].split(">")[1].split("<")[0]}

				tree = elementtree.parse(xml_path)
				root = tree.getroot()

				for tag_name in ['ISOAbbreviation','Volume','Issue','MedlinePgn']:
					article_data[header_mapping[tag_name]] = ""
					for elem in root.iter(tag_name):
						try:
							article_data[header_mapping[tag_name]] = "".join([x for x in elem.text if x in string.printable])
						except:
							pass



				authors = []
				for elem in tree.iter('Author'):
					authors_dict = {"ForeName":"", "LastName":"","Initials":""}
					try:
						authors_dict["ForeName"] = elem.find("ForeName").text
					except:
						pass

					try:
						authors_dict["LastName"] = elem.find("LastName").text
					except:
						pass

					try:
						authors_dict["Initials"] = elem.find("Initials").text
					except:
						pass

					authors.append(authors_dict)

				authors_short = ""
				authors_long = ""

				try:
					if len(authors) == 1:
						authors_short = authors[0]['LastName'] + " "  +authors[0]['Initials']
					else:
						authors_short = authors[0]['LastName']  + " et al"

					if len(authors) == 1:
						authors_long = authors[0]['LastName'] + " "  +authors[0]['Initials']
					else:
						authors_long = ""
						authors_tmp = []

						for author in authors:
							authors_tmp += [author['LastName'] + " " + author['Initials']]

						if len(authors_tmp) > 1:
							authors_long = ", ".join(authors_tmp[:-1]) + " and " + authors_tmp[-1]
						elif len(authors_tmp) == 1:
							authors_long = authors_short
				except:
					pass

				article_data["short_author"] = authors_short
				article_data["long_author"] = authors_long
				article_data["author_list"] = authors
				article_data["article_month"] = ""
				article_data["article_year"] = ""

				mesh_terms = []
				for elem in tree.iter('MeshHeading'):
					try:
						descriptorName = elem.find("DescriptorName").text
						mesh_terms.append(descriptorName)
					except:
						pass

				article_data["mesh_terms"] = mesh_terms

				for elem in tree.iter('PubDate'):
					for dates in elem.getchildren():
						try:
							article_data[header_mapping[dates.tag]] = dates.text
						except:
							pass

				article_data["pdbs"] = []
				for elem in tree.iter('DataBank'):
					if elem.find("DataBankName").text == "PDB":
						for databank in elem.find("AccessionNumberList").getchildren():
							article_data["pdbs"].append(databank.text)

				try:
					article_data["article_details"] = article_data['article_volume'] + '(' + article_data['article_issue']  + '):' + article_data['article_page']
				except:
					article_data["article_details"] = ""

				if get_proteins:
					try:
						article_data["proteins"] = self.parseUniProtByPMID(pmid,True)['data']
					except:
						article_data["proteins"] = {}


				#print article_data

				if simple:
					simple_article_data = {}
					for tag in ['article_details','article_year','journal','long_author','short_author','title']:
						simple_article_data[tag] = article_data[tag]

					return {"status":"Success","data":simple_article_data}
				else:
					return {"status":"Success","data":article_data}
			else:
				return {"status":"Error","error_type":"File not found"}
		except Exception as e:
			return {"status":"Error","error_type":str(e)}


	##------------------------------------------------------------------##

	def grabUniProtByPMID(self,pmid):

		self.check_directory()

		url = "http://www.uniprot.org/uniprot/?sort=&desc=&query=(citation:" + pmid + ")&fil=&force=no&format=tab&columns=id,entry%20name,reviewed,protein%20names,genes,organism,length,sequence"
		out_path = os.path.join(self.options["data_path"] + "/pmid/",pmid + ".uniprot.tdt")

		sessionDownloaderObj = utilities_downloader.sessionDownloader()
		status = sessionDownloaderObj.download_file(url,out_path,method="GET",replace_empty=True)

	##------------------------------------------------------------------##

	def grabAnnotationByPMID(self,pmid):
		self.check_directory()

		url = "https://www.ebi.ac.uk/europepmc/annotations_api/annotationsByArticleIds?articleIds=MED%3A" + pmid + "&type=Gene_Proteins&format=JSON"
		out_path = os.path.join(self.options["data_path"] + "/pmid/",pmid + ".annotation.json")
		sessionDownloaderObj = utilities_downloader.sessionDownloader()
		status = sessionDownloaderObj.download_file(url,out_path,method="GET",replace_empty=True)


	##------------------------------------------------------------------##

	def parseAnnotationByPMID(self,pmid,detailed=False):
		self.grabAnnotationByPMID(pmid)

		json_path = os.path.join(self.options["data_path"] + "/pmid/",pmid + ".annotation.json")

		annotation_json_data = {}
		annotation_data = {}

		if pmid == "":
			return {"status":"Error","error_type":"File not found","data":{}}

		if detailed:
			uniprotDownloaderObj = uniprotDownloader.uniprotDownloader()
			uniprotDownloaderObj.options["data_path"] = self.options["data_path"]

		if os.path.exists(json_path):

			with open(json_path) as outfile:
				try:
					annotation_data = json.load(outfile)

					if 'annotations' in annotation_data[0]:
						for annotation in annotation_data[0]['annotations' ]:
							annotation_json_data[annotation['tags'][0]['uri'].split("/")[-1]] = annotation['tags'][0]['name']

						if detailed:
							for accession in annotation_json_data:
								try:
									try:
										protein_data = uniprotDownloaderObj.parseBasic(accession)['data']
									except:
										protein_data = {}

									tag = annotation_json_data[accession]
									annotation_json_data[accession] = protein_data
									annotation_json_data[accession]['tag'] = tag
								except Exception as e:
									print(("Error",accession,pmid,e))
				except:
					print(json_path)
					return {"status":"Success","data":{}}

			return {"status":"Success","data":annotation_json_data}
		else:
			return {"status":"Error","error_type":"File not found"}


	##------------------------------------------------------------------##

	def parseUniProtByPMID(self,pmid,detailed=False):
		self.grabUniProtByPMID(pmid)

		tdt_path = os.path.join(self.options["data_path"] + "/pmid/",pmid + ".uniprot.tdt")

		if os.path.exists(tdt_path):
			if detailed:
				uniprotDownloaderObj = uniprotDownloader.uniprotDownloader()
				uniprotDownloaderObj.options["data_path"] = self.options["data_path"]

			tdt_data = open(tdt_path).read().strip().split("\n")

			headers = tdt_data[0].split("\t")

			proteins = []
			for protein in tdt_data[1:]:
				accession = protein.split("\t")[0]
				proteins.append(accession)

			if len(proteins) > 2000: return {"status":"Failed - Too many proteins","data":{}}

			if detailed:
				annotation_json_data = {}
				for accession in proteins:

					protein_data = uniprotDownloaderObj.parseBasic(accession)

					if protein_data['status'] == "Success":
						annotation_json_data[accession] = protein_data['data']

				return {"status":"Success","data":annotation_json_data}
			else:
				return proteins
		else:
			return proteins

	##------------------------------------------------------------------##

	def parseFeaturesByPMID(self,pmid,features="all"):
		try:
			uniprotDownloaderObj = uniprotDownloader.uniprotDownloader()
			uniprotDownloaderObj.options["data_path"] = self.options["data_path"]

			accessions = self.parseUniProtByPMID(pmid)
		
			feature_data = {}

			for accession in accessions:
				protein_data = uniprotDownloaderObj.parseFeatures(accession)

				if 'features' in protein_data['data']:
					for feature_type in protein_data['data']['features']:
						if feature_type in features or features == "all":
							for feature_instance in protein_data['data']['features'][feature_type]:
								if pmid in feature_instance['pmid']:
									if accession not in feature_data:
										feature_data[accession] = {}
									if feature_type not in feature_data[accession]:
										feature_data[accession][feature_type] = []

									feature_data[accession][feature_type].append(feature_instance)

			return {"status":"Success","data":feature_data}
		except Exception as e:
			return {"status":"Error","error_type":str(e)}

	##------------------------------------------------------------------##

	def parsePDBByPMID(self,pmid):
		article_data = {"pmid":pmid}

		header_mapping = {
		'ArticleTitle':"title",
		'Authors':"author_list",
		'ISOAbbreviation':"journal",
		'Volume':"article_volume",
		'Issue':"article_issue",
		'MedlinePgn':"article_page",
		'ArticleId':"article_details",
		'AbstractText':"abstract_text",
		'PDB':"pdb",
		'Year':"article_year",
		'Month':"article_month",
		}

		try:
			self.grabPMID(pmid)

			xml_path = os.path.join(self.options["data_path"] + "/pmid/",pmid + ".xml")

			error_pattern = re.compile("<error>.+</error>")

			if os.path.exists(xml_path):
				errors = error_pattern.findall(open(xml_path).read())

				if len(errors) > 0:
					return  {"status":"Error","error_type":errors[0].split(">")[1].split("<")[0]}

				tree = elementtree.parse(xml_path)

				article_data["pdbs"] = []
				for elem in tree.iter('DataBank'):
					if elem.find("DataBankName").text == "PDB":
						for databank in elem.find("AccessionNumberList").getchildren():
							article_data["pdbs"].append(databank.text)

				return {"status":"Success","data":article_data}
			else:
				return {"status":"Error","error_type":"File not found"}

		except Exception as e:
			return {"status":"Error","error_type":str(e)}

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

if __name__ == "__main__":
	import pprint
	pmidDownloaderObj = pmidDownloader()
	pmidDownloaderObj.options["data_path"] = "/Users/adminndavey/Documents/Work/data/"
	pprint.pprint(pmidDownloaderObj.parsePMID("9632783"))

	pmids = "23707760,21460798,25669885,25505175,25669885,27522463,27509861,27509861,27522463,25287299,25669885,23707760,18519589,18596038,18498748,23707760,18596038,18519589,18498748,16855400,16855400,23816140,16040610,16040610,9036857,16195415,24206843,16204042,20471943,20471943,24871945,24871945,27522463,27509861,8885231,1846030,11285279,10733526,23708605,11179223,10559878,10733526,8885231,1846030,12185076,8895572,1846030,8885231,21118994,9560342,9482731,9560342,9482731,16153703,12679038,17679094,22045811,11163211,11694576,23707760,11719221,17620341,9312055,9312055,17314514,16415207,9836745,7588612,9836745,15004270,7588612,10594027,25287299,25287299,20471943,21325626,19805045,11073992,17869113,17869113,17638867,19160489,18757745,22854063,16921029,23708605,21725316,17173039,18758239,18758239,18758239,20972601,20972601,23469016,22844260,9635433,21186356,21186356,24163370,20471943,20471943,12853486,12853486,11562348,23708605,16810178,16810178,12471060,12471060,24751481,24510915,17369399,20526282,24972868,18331722,22434193,11073992,11742988,19900895,17403670,21562221,20471943,16195415,22801552,23707760,11553328,23707760,12724352,23328397,14734534,10411507,11179223,23091007,22167059,18701670,21505434,25236599,19015261,19015261,23786411,23786411,11741538,15014503,15014502,22086178,23707760,17493939,22405274,23049888,20064487,20729194,22430208,10930472,22426463,17889668,21562221,21562221,22205987,22205987,22844260,23288039,11742988,22167059,18498748,18519589,18596038,23707760,23707760,16855400,16855400,23816140,20921411,20921411,24206843,11964384,12208850,15923616,23029325,23029325,17158872,17158872,22193957,23091007,20016069,17369399,21118994,10733526,12234927,17599046,11842186,16153703,12679038,12679038,15837422,20053638,11694576,17376772,24260314,8020094,23776205,9312055,17314514,20471943,15899874,11073992,21059905,22854063,18498748,20972601,22844260,23195958,10733526,20471943,11562348,24751481,17726374,23707760,12773557,17406666,17369399,17406666,18331722,10733526,11742988,17403670,17618083,20471943,21628590,24169697,19448625,12724352,23091007,11179223,22167059,18426916,12655059,17681132,19015261,22416277,24485834,14701726,23049888,16287863,21596315,20526282,23708001,23708605,22580462,22178396,22178396,24361936,17942599,24019759,11742988,15923616,15536123,27509861,16878123"

	for pmid in pmids.split(","):
		data = pmidDownloaderObj.parsePMID(pmid)

		row = [
			pmid,
			data['data']['title'].encode('utf-8'),
			data['data']['long_author'].encode('utf-8'),
			data['data']['journal'] + " " + data['data']['article_details'] + " " + data['data']['article_year']
		]

		print(("\t".join(row)))
