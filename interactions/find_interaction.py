import sys
import json
import urllib.request, urllib.parse, urllib.error
import urllib.request, urllib.error, urllib.parse
import pprint
import os
import operator
import inspect
import time


file_path = os.path.dirname(inspect.stack()[0][1])
sys.path.append(os.path.join(file_path,"../downloaders"))
sys.path.append(os.path.join(file_path,"../"))

import config_reader
import interactionDownloader


option_list  = """
		TASKS
		- load_cytoscape_style:

		- load_cytoscape_example:

		- get_complex_data:
			accession

		- collapse_by_complex:
			accession - comma separated list

		- get_shared_interactions:
			accession - comma separated list
			number_of_interactors

		- get_shared_interactions:
			accession - comma separated list

		- get_extended_network:
			accession - comma separated list

		- get_interactions_by_pmid:
			pmids - comma separated list

		- get_domain_interactions:
			accession
			pfam_id

		- get_scaffolds:
			bait
			accession - comma separated list

		- get_localisation_by_interactions:
			accession - comma separated list

		- get_localisation_interactions:
			accession
			localisation - comma separated list

		- get_interaction_existence:
			bait
			prey
	"""

class findInteractions():

	def __init__(self):
		self.options = {}


	def check_options(self,rest_options,required_options):
		check_json = {"status":"Passed","missing":[]}
		for required_option in required_options:
			if required_option in rest_options:
				pass
			else:
				check_json["status"] = "Failed"
				check_json["message"] = "Missing option"
				check_json["missing"].append(required_option)

		return check_json

	def load_cytoscape_example(self):
		with open(os.path.join(file_path,"cytoscape","data.json")) as data_file:
			return json.load(data_file)

	def load_cytoscape_style(self):
		with open(os.path.join(file_path,"cytoscape","cy-style.json")) as data_file:
			return json.load(data_file)

	def findInteractionsRest(self,rest_options):
		data = {}
		status = "Success"
		error = {"status":"Error"}
		try:
			if rest_options["task"] == "load_cytoscape_style":
				data = self.load_cytoscape_style()

			elif rest_options["task"] == "load_cytoscape_example":
				data = self.load_cytoscape_example()
			else:
				self.interactionDownloaderObj = interactionDownloader.interactionDownloader(use_hippie=True,use_string=False)

				if "cutoff" in rest_options:
					self.interactionDownloaderObj.options["cutoff"] = float(rest_options["cutoff"])

				if "bioplex" in rest_options:
					self.interactionDownloaderObj.options["bioplex"] = rest_options["bioplex"] == "True"

				if "number_of_interactors" in rest_options:
					self.interactionDownloaderObj.options["number_of_interactors"] = int(rest_options["number_of_interactors"])

				###----###
				###----###

				if rest_options["task"] == "help":
					data = option_list


				###----###
				###----###

				if rest_options["task"] == "get_interactions":
					data = self.interactionDownloaderObj.getInteractions(rest_options["accession"].split(","),"accession")

				if rest_options["task"] == "get_complex_data":
					data = self.interactionDownloaderObj.getComplexData(rest_options["accession"])

				###----###
				###----###

				if rest_options["task"] == "collapse_by_complex":
					data = self.interactionDownloaderObj.collapseByComplex(rest_options["accession"].split(","))

				if rest_options["task"] == "get_shared_interactions":
					data = self.interactionDownloaderObj.getSharedInteractions(rest_options["accession"],number_of_interactors = rest_options["number_of_interactors"])


				if rest_options["task"] == "get_extended_network":
					data = self.interactionDownloaderObj.getExtendedNetwork(rest_options["accession"].split(","))

				if rest_options["task"] == "get_interactions_by_pmid":
					data = self.interactionDownloaderObj.getInteractions(rest_options["pmid"].split(","),"pmid")

				if rest_options["task"] == "get_domain_interactions":
					data = self.interactionDownloaderObj.getDomainInteractions(rest_options["accession"],rest_options["pfam_id"])

				if rest_options["task"] == "get_scaffolds":
					data = self.interactionDownloaderObj.getScaffolds(rest_options["bait"].split(","),rest_options["accession"].split(","))

				if rest_options["task"] == "get_localisation_by_interactions":
					data = self.interactionDownloaderObj.getLocalisationByInteractions(rest_options["accession"].split(","))

				if rest_options["task"] == "get_localisation_interactions":
					check_json = self.check_options(rest_options,["accession","localisation"])
					if check_json['status'] == "Failed":
						status = "Error"
						error = check_json
					else:
						data = self.interactionDownloaderObj.getLocalisationInteractions(rest_options["accession"],rest_options["localisation"].split())

				if rest_options["task"] == "get_interaction_existence":
					data = self.interactionDownloaderObj.existsInteractions(rest_options["bait"],rest_options["prey"])

				if rest_options["task"] == "make_cytoscape_json":
					data = self.interactionDownloaderObj.makeCytoscapeJson(rest_options["accession"])

				if rest_options["task"] == "get_interactions_table":
					data = self.interactionDownloaderObj.getInteractionsTable(rest_options["accession"])

				### REQUIRES NETWORKX
				if rest_options["task"] == "get_subnetwork_plot":
					data = self.interactionDownloaderObj.getSubNetworkPlot(rest_options["accession"])

			###----###
			###----###

			if status == "Error":
				return error
			else:
				return {
				"status": status,
				"options":rest_options,
				"data":data
				}
		except Exception as e:
			raise
			return {
			"status":"Error",
			"error_type":str(e)
			}

if __name__ == "__main__":

	findInteractionsObj = findInteractions()

	############################
	############################
	############################

	test_accession = "O75400"

	test_accessions = ["Q9NYY8","Q58FF8","Q8N183","P57740","Q9Y3P9"]


	############################
	############################
	############################

	print("# get_interactions_table #",sys.argv[1])
	test_rest_options = {}
	test_rest_options['accession'] = [sys.argv[1]]
	test_rest_options['task'] = "get_interactions_table"

	response = findInteractionsObj.findInteractionsRest(test_rest_options)
	print((response['data']['data']))


	sys.exit()
	############################
	############################
	############################

	print("# get_interactions #")
	test_rest_options = {}
	test_rest_options['accession'] = test_accession
	test_rest_options['task'] = "get_interactions"

	response = findInteractionsObj.findInteractionsRest(test_rest_options)
	pprint.pprint(response)

	############################
	############################
	############################

	print("# make_cytoscape_json #")
	test_rest_options = {}
	test_rest_options['task'] = "make_cytoscape_json"
	test_rest_options['accession'] = "O95400"
	response = findInteractionsObj.findInteractionsRest(test_rest_options)
	pprint.pprint(response)


	############################
	############################
	############################

	print("# get_localisation_interactions #")
	test_rest_options = {}
	test_rest_options['accession'] = "P20248"
	test_rest_options['localisation'] =  'Endoplasmic reticulum,Nucleus'
	test_rest_options['task'] = "get_localisation_interactions"

	response = findInteractionsObj.findInteractionsRest(test_rest_options)
	pprint.pprint(response)

	############################
	############################
	############################


	print("# get_localisation_by_interactions #")
	test_rest_options = {}
	test_rest_options['accession'] = "P20248"
	test_rest_options['task'] = "get_localisation_by_interactions"

	response = findInteractionsObj.findInteractionsRest(test_rest_options)
	pprint.pprint(response)

	############################
	############################
	############################


	print("# load_cytoscape_style #")
	test_rest_options = {}
	test_rest_options['task'] = "load_cytoscape_style"
	response = findInteractionsObj.findInteractionsRest(test_rest_options)
	pprint.pprint(response)

	############################
	############################
	############################

	print("# load_cytoscape_example #")
	test_rest_options = {}
	test_rest_options['task'] = "load_cytoscape_example"
	response = findInteractionsObj.findInteractionsRest(test_rest_options)
	pprint.pprint(response)

	############################
	############################
	############################

	print("# get_complex_data #")
	test_rest_options = {}
	test_rest_options['task'] = "load_cytoscape_style"

	response = findInteractionsObj.findInteractionsRest(test_rest_options)
	pprint.pprint(response)

	############################
	############################
	############################



	print("# get_complex_data #")
	test_rest_options = {}
	test_rest_options['accession'] = test_accession
	test_rest_options['task'] = "get_complex_data"

	response = findInteractionsObj.findInteractionsRest(test_rest_options)
	pprint.pprint(response)

	############################
	############################
	############################

	print("# collapse_by_complex #")
	test_rest_options = {}
	test_rest_options['accession'] = test_accession
	test_rest_options['task'] = "collapse_by_complex"

	response = findInteractionsObj.findInteractionsRest(test_rest_options)
	pprint.pprint(response)


	############################
	############################
	############################

	print("# get_domain_interactions #")
	test_rest_options = {}
	test_rest_options['accession'] =  "Q99814"
	test_rest_options['pfam_id'] = "PF01847"
	test_rest_options['task'] = "get_domain_interactions"

	response = findInteractionsObj.findInteractionsRest(test_rest_options)
	pprint.pprint(response)

	############################
	############################
	############################

	print("# get_extended_network #")
	test_rest_options = {}
	test_rest_options['accession'] = test_accession
	test_rest_options['task'] = "get_extended_network"

	response = findInteractionsObj.findInteractionsRest(test_rest_options)
	pprint.pprint(response)

	############################
	############################
	############################

	print("# get_interaction_existence #")
	test_rest_options = {}
	test_rest_options['bait'] = "Q8N3Y1"
	test_rest_options['prey'] = "Q9H0W5"
	test_rest_options['task'] = "get_interaction_existence"

	response = findInteractionsObj.findInteractionsRest(test_rest_options)
	pprint.pprint(response)

	############################
	############################
	############################

	print("# get_interactions_by_pmid #")
	test_rest_options = {}
	test_rest_options['pmid'] = "10531037"
	test_rest_options['task'] = "get_interactions_by_pmid"

	response = findInteractionsObj.findInteractionsRest(test_rest_options)
	pprint.pprint(response)


	############################
	############################
	############################

	print("# get_scaffolds #")
	test_rest_options = {}
	test_rest_options['bait'] = "Q15172,Q15173,Q14738,Q13362,Q16537"
	test_rest_options['accession'] ='Q6T4R5,Q00537,Q14814,Q69YN4,Q86VQ1'
	test_rest_options['task'] = "get_scaffolds"

	response = findInteractionsObj.findInteractionsRest(test_rest_options)
	pprint.pprint(response)

	############################
	############################
	############################

	print("# get_shared_interactions #")
	test_rest_options = {}
	test_rest_options['accession'] = test_accessions
	test_rest_options['number_of_interactors'] = 30
	test_rest_options['task'] = "get_shared_interactions"

	response = findInteractionsObj.findInteractionsRest(test_rest_options)
	pprint.pprint(response)

	############################
	############################
	############################

	#print "# get_subnetwork_plot #"
	#test_rest_options = {}
	#test_rest_options['accession'] = test_accessions
	#test_rest_options['task'] = "get_subnetwork_plot"

	#response = findInteractionsObj.findInteractionsRest(test_rest_options)
	#pprint.pprint(response)

	############################
	############################
	############################
