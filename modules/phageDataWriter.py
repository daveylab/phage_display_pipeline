#!/usr/bin/python3
#
# Program:      ProP-PD Data Analysis Pipeline
# Class:        phageDataWriter
# Description:  Analysis pipeline to process peptides returned from replicated ProP-PD selections
# Version:      1.0.0
# Last Edit:    1/1/2021
# Citation:     Under Preparation
# Author contact: Norman E. Davey <norman.davey@icr.ac.uk> Institute Of Cancer Research, Chelsea, SW3 6JB, London, United Kingdom.
# 
# License:
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http:#www.gnu.org/licenses/>.
#
# Copyright (C) 2020  Norman E. Davey


__doc__ = """
# ProP-PD Data Analysis Pipeline
### Code for writing output files for the pipeline
"""

## TODO:

####################################################################################################################################################################################
####################################################################################################################################################################################	

import os
import sys
import json
import logging

sys.path.append(os.path.join(os.path.dirname(__file__),"../../utilities"))
import utilities_error

sys.path.append(os.path.join(os.path.dirname(__file__),"../../data_management"))
import queryRunner

#-----
logger = logging.getLogger(__name__)
#-----

####################################################################################################################################################################################
####################################################################################################################################################################################
	
class phageDataWriter():

	def __init__(self):

		self.headers = [
		'overlap',
		'Hit',
		'mutant',
		'SeqStart',
		'SeqStop',
		'ProteinAcc',
		'GeneName',
		'ProteinName',
		'Virus',
		"motifs",
		'Region',
		'SNP',
		'Mutagenesis'
		]

	def createPeptideOutputJSON(self,library,bait,peptide_data):
		"""
		Creates JSON output of peptide data for screen

		Args:
			library (str): library identifier of library for processing
			bait (str): bait identifier of bait for processing
		"""

		out_file_json = os.path.join(self.options["peptools_data_path"],"json",  bait + "." +  library + ".peptides.output.json")

		with open(out_file_json, 'w') as outfile:
			logger.info("Writing: " + out_file_json)
			json.dump(peptide_data, outfile)

	###---------------------------------------------------------###

	def setTagStr(self):
		tags = []

		if self.options["remove_duplicates"]:tags.append("duplicates")
		if self.options["require_highcount"]:tags.append("highcount" + "_" + str(self.options["count_cutoff"]))
		if self.options["require_interaction"]:tags.append("interactor")
		if self.options["require_motifs"]:tags.append("motif")
		if self.options["require_overlap"]:tags.append("overlap")
		if self.options["require_replicate"]:tags.append("replicate")
		if self.options['require_mutagenesis']:tags.append("mutagenesis")
		if self.options['require_snp']:tags.append("snp")
		if self.options['require_region']:tags.append("region")

		if len(tags) > 0:
			self.tag_str = "." + ".".join(tags)
		else:
			self.tag_str = ""

	def createPeptideOutputTDT(self,library,bait,peptide_data):

		"""
		Creates TDT output of peptide data for screen

		Args:
			library (str): library identifier of library for processing
			bait (str): bait identifier of bait for processing
		"""
		hits = []

		for hit_id in peptide_data:
			try:
				hit_str_peptide = peptide_data[hit_id]['confidence_level_text'] + "\t"
				hit_str_peptide += str(peptide_data[hit_id]['primary_duplicate']) + "\t"
				hit_str_peptide += peptide_data[hit_id]['interactor_str'] + "\t"
				hit_str_peptide += peptide_data[hit_id]['motifs'] + "\t"
				hit_str_peptide += peptide_data[hit_id]["consensus_match"] + "\t"
				hit_str_peptide += "|".join(peptide_data[hit_id]['peptide_screen']) + "\t"
				hit_str_peptide += "|".join(peptide_data[hit_id]['peptide_counts']) + "\t"
				hit_str_peptide += str(peptide_data[hit_id]['replicates']) + "\t"
				hit_str_peptide += str(peptide_data[hit_id]['promiscuity']) + "\t"
				hit_str_peptide += str(peptide_data[hit_id]['max_counts']) + "\t"
				hit_str_peptide += str(peptide_data[hit_id]['mean_counts']) + "\t"
				hit_str_peptide += str(peptide_data[hit_id]['overlap_masked_peptide']) + "\t"

				for header in self.headers:
					if header in peptide_data[hit_id]:
						hit_str_peptide += str(peptide_data[hit_id][header]) + "\t"
					else:
						hit_str_peptide += "\t"

				hits.append(hit_str_peptide)
			except:
				pass

		hits.sort()

		header_str = "\t".join(["confidence","primary_duplicate","validated_interactor","validated_motif","consensus_match","screens","screen_counts","replicates",'promiscuity',"max_counts","mean_counts","overlap_masked_peptide"] + self.headers + ['taxonomy']) + "\n"
		hit_str = "\n".join(hits)

		
		if self.options["make_output_file"]:
			self.setTagStr()
			out_file = os.path.join(self.options["peptools_data_path"],"annotated",library + "__"  + bait + self.tag_str + ".annotation.tdt")
			logger.info("Writing: " + out_file)
			open(out_file,"w").write(header_str + hit_str)

		if self.options["show_results"]:
			hit_str = hit_str.strip()

			if len(hit_str) != 0:
				logger.info(self.data[bait][library]['info_str']  + header_str + hit_str)

	###---------------------------------------------------------###

	def createMetadataOutput(self,library,bait,stats_json,bait_data,selection_day_replicate_mapping):
		
		"""
		Creates JSON output of screen details

		Args:
			library (str): library identifier of library for processing
			bait (str): bait identifier of bait for processing

		TODO Update scripts to read json_use data
		
		"""

		json_basic_output = {}
		json_use = [
			'screens',
			'elm_class',
			'structure_elm_class',
			'structure_details',
			'elm_interaction_details',
			'elm_instance_details',
			'domain_name',
			'pfam_id',
			'pfam_accession',
			'clan_name',
			'domain_description',
			'domain_start',
			'domain_end',
			'peptide_scores_SLiMFinder'
			'SLiMFinder_motif'
		]

		#for tag in json_use:
		#	if tag in self.data[bait][library]:
		#		json_basic_output[tag] = copy.deepcopy(self.data[bait][library][tag])

		json_basic_output['statistics'] = stats_json
		json_basic_output['replicate_details'] = selection_day_replicate_mapping
		json_basic_output['protein_details'] = bait_data['protein']
		json_basic_output['domain_details'] = bait_data['domain']
		json_basic_output['targetting_compounds'] = bait_data['targetting_compounds']
		json_basic_output['druggability'] = bait_data['druggability']
		
		
		if 'consensus_details' in json_basic_output['statistics']:
			for motif in json_basic_output['statistics']['consensus_details']:
				json_basic_output['statistics']['consensus_details'][motif] = {'re':json_basic_output['statistics']['consensus_details'][motif]['re']}

		out_file_json = os.path.join(self.options["peptools_data_path"],"json",  bait + "." +  library + ".details.output.json")

		with open(out_file_json, 'w') as outfile:
			json.dump(json_basic_output, outfile)

		del json_basic_output
		logger.info("Writing: " + out_file_json)

	###---------------------------------------------------------###

	def createStatsOutput(self,stats_json):
		"""
		Creates JSON output of stats for screen
		"""

		if self.options['bait'] == None:
			if self.options['library'] == None:
				
				tag = "all"
				library_tag = ""
				
				if self.options['analysis_bait_list_file'] != None:
					tag = os.path.basename(self.options['analysis_bait_list_file']).split(".")[0]
					library_tag = tag + "."

				out_file_json = os.path.join(self.options["peptools_data_path"],"metadata",  tag + ".screen.stats.json")

				with open(out_file_json, 'w') as outfile:
					json.dump(stats_json, outfile)

				libraries = []
				for bait in stats_json:
					for library in stats_json[bait]:
						if library not in libraries:
							libraries.append(library)

				for library in libraries:
					out_json = {}

					for bait in stats_json:
						if library in stats_json[bait]:
							out_json[bait] = {}
							out_json[bait][library] = stats_json[bait][library]

					out_file_json = os.path.join(self.options["peptools_data_path"],"metadata", library_tag + library + ".screen.stats.json")
					logger.info("Writing: " + out_file_json)
					with open(out_file_json, 'w') as outfile:
						json.dump(out_json, outfile)

			else:
				out_file_json = os.path.join(self.options["peptools_data_path"],"metadata", self.options['library'] + ".screen.stats.json")
				logger.info("Writing: " + out_file_json)

				with open(out_file_json, 'w') as outfile:
					json.dump(self.stats, outfile)

	###---------------------------------------------------------###

	def createProteinsOutputJSON(self):
		"""
		Creates protein JSON outputs
		"""

		if self.options['bait'] == None and self.options['analysis_bait_list_file'] == None:

			protein_json = {}
			protein_summary_json = {}

			use_peptide_data = ['Hit',
								"Library_Peptide",
								'overlap_masked_peptide',
								'SeqStop',
								'SeqStart',
								'confidence',
								'consensus_match',
								'confidence_level_text',
								'motif_check',
								'structure_check',
								'interaction_check',
								'interactor',
								'consensus_check',
								'elm_check',
								'elm_check_details',
								'in_crapome',
								'promiscuity',
								'colocalisation_best',
								'shared_function_best',
								'shared_process_best',
								'colocalisation_score',
								'shared_function_score',
								'shared_process_score',
								'overlap',
								'replicates',
								'peptide_screen',
								'peptide_counts',
								'max_counts',
								'mean_counts',
								'slimfinder_score',
								'slimfinder_score_p',
								'disease_mutants',
								'disease_mutant_check'
								]

			use_protein_info = ['gene_name',
								'id',
								'protein_name',
								'species_common',
								'species_scientific',
								'taxonomy']
		
			#####

			for bait in self.data:
				for library in self.data[bait]:
					if library not in protein_summary_json:
						protein_summary_json[library] = {}

					if 'phage_peptools_data' not in self.data[bait][library]: continue

					for hit_id in self.data[bait][library]['phage_peptools_data']:
						try:
								proteinAccession = self.data[bait][library]['phage_peptools_data'][hit_id]['ProteinAcc']

								if proteinAccession not in protein_json:
									try:
										options = {'accession':proteinAccession}
										data = queryRunner.queryRunner("uniprot","parse_basic",options).run()
										protein_info = data['data']
									except:
										protein_info = {}

									protein_json[proteinAccession] = {
									'info':protein_info,
									'libraries':{}
									}


								if proteinAccession not in protein_summary_json[library]:
									protein_summary_json[library][proteinAccession] = {
									'peptides':0,
									'specific_peptides':0,
									'specific_replicated_peptides':0,
									'similarity_peptides':0,
									'replicated_peptides':0,
									'highcount_peptides':0,
									'motif_peptides':0,
									'overlap_peptides':0,
									'specific_overlap_peptides':0,
									'structure_peptides':0,
									'bait':[]
									}

									for header in use_protein_info:
										if header in protein_info:
											protein_summary_json[library][proteinAccession][header] = protein_info[header]
										else:
											protein_summary_json[library][proteinAccession][header] = 'n/d'


								protein_summary_json[library][proteinAccession]['peptides'] += 1

								if 'specific_check' in self.data[bait][library]['phage_peptools_data'][hit_id]:
									if self.data[bait][library]['phage_peptools_data'][hit_id]['specific_check']:
										protein_summary_json[library][proteinAccession]['specific_peptides'] += 1

								if 'specific_replicate_check' in self.data[bait][library]['phage_peptools_data'][hit_id]:
									if self.data[bait][library]['phage_peptools_data'][hit_id]['specific_replicate_check']:
										protein_summary_json[library][proteinAccession]['specific_replicated_peptides'] += 1

								if 'specific_overlap_check' in self.data[bait][library]['phage_peptools_data'][hit_id]:
									if self.data[bait][library]['phage_peptools_data'][hit_id]['specific_overlap_check']:
										protein_summary_json[library][proteinAccession]['specific_overlap_peptides'] += 1

								if 'replicate_check' in self.data[bait][library]['phage_peptools_data'][hit_id]:
									if self.data[bait][library]['phage_peptools_data'][hit_id]['replicate_check']:
										protein_summary_json[library][proteinAccession]['replicated_peptides'] += 1

								if 'highcount_check' in self.data[bait][library]['phage_peptools_data'][hit_id]:
									if self.data[bait][library]['phage_peptools_data'][hit_id]['highcount_check'] :
										protein_summary_json[library][proteinAccession]['highcount_peptides'] += 1

								if 'motif_check' in self.data[bait][library]['phage_peptools_data'][hit_id]:
									if self.data[bait][library]['phage_peptools_data'][hit_id]['motif_check']:
										protein_summary_json[library][proteinAccession]['motif_peptides'] += 1

								if 'structure_check' in self.data[bait][library]['phage_peptools_data'][hit_id]:
									if self.data[bait][library]['phage_peptools_data'][hit_id]['structure_check']:
										protein_summary_json[library][proteinAccession]['structure_peptides'] += 1

								if 'overlap_check' in self.data[bait][library]['phage_peptools_data'][hit_id]:
									if self.data[bait][library]['phage_peptools_data'][hit_id]['overlap_check']:
										protein_summary_json[library][proteinAccession]['overlap_peptides'] += 1

								if 'replicates' in protein_summary_json[library][proteinAccession]:
									if bait not in protein_summary_json[library][proteinAccession]['bait']:
										protein_summary_json[library][proteinAccession]['bait'].append(bait)

								if library not in protein_json[proteinAccession]['libraries']:
									protein_json[proteinAccession]['libraries'][library] = {}

								####################################################################################

								try:
									bait_domain_name = list(self.bait_data[bait]['domain'].values())[0]['domain_name']
									bait_clan_name = list(self.bait_data[bait]['domain'].values())[0]['clan_name']
									bait_pfam_accession = list(self.bait_data[bait]['domain'].values())[0]['pfam_accession']
									bait_pfam_id = list(self.bait_data[bait]['domain'].values())[0]['pfam_id']
								except:
									bait_domain_name = "n\d"
									bait_clan_name = "n\d"
									bait_pfam_id = "n\d"
									bait_pfam_accession = "n\d"
								#

								if bait not in protein_json[proteinAccession]['libraries'][library]:
									protein_json[proteinAccession]['libraries'][library][bait] = {}

								protein_json[proteinAccession]['libraries'][library][bait][hit_id] = {
									'bait':bait,
									'bait_domain_name':bait_domain_name,
									'bait_clan_name':bait_clan_name,
									'bait_pfam_id':bait_pfam_id,
									'bait_pfam_accession': bait_pfam_accession,
									'bait_domain_start':bait.split("_")[-2],
									'bait_domain_end':bait.split("_")[-1],
									'bait_accession':self.bait_data[bait]['protein']['accession'],
									'bait_protein_name':self.bait_data[bait]['protein']['protein_name'],
									'bait_gene_name':self.bait_data[bait]['protein']['gene_name'],
									'bait_species_common':self.bait_data[bait]['protein']['species_common'],
									'bait_species_scientific':self.bait_data[bait]['protein']['species_scientific'],
									'bait_taxonomy':self.bait_data[bait]['protein']['taxonomy'],
								}

								for header in use_peptide_data:
									try:
										if header in self.data[bait][library]['phage_peptools_data'][hit_id]:
											protein_json[proteinAccession]['libraries'][library][bait][hit_id][header] = self.data[bait][library]['phage_peptools_data'][hit_id][header]
										else:
											protein_json[proteinAccession]['libraries'][library][bait][hit_id][header] = "n\d"
									except Exception as e:
										pass

						except Exception as e:
							logger.error("processPeptides: " + library + " - " + bait)
							utilities_error.printError()

			#####

			for library in protein_summary_json:
				try:
					out_file_json = os.path.join(self.options["peptools_data_path"],"proteins", library + ".summary.json")
					logger.info("Writing: " + out_file_json)
					with open(out_file_json, 'w') as outfile:
						json.dump(protein_summary_json[library], outfile)

				except Exception as e:
					logger.error(str(e))
							
			#####

			if self.options['library'] == None:
				for proteinAccession in protein_json:
					try:
						out_file_json = os.path.join(self.options["peptools_data_path"],"proteins", proteinAccession + ".json")
						logger.info("Writing: " + out_file_json)
						with open(out_file_json, 'w') as outfile:
							json.dump(protein_json[proteinAccession], outfile)

					except Exception as e:
						logger.error(str(e))

	###---------------------------------------------------------###

	def showScreens(self):
		"""
		Creates, print and save screen overview data
		"""

		baits = list(self.data.keys())
		baits.sort()

		self.screens_details = {}

		for bait in baits:
			try:
				self.screens_details[bait] = {}
				self.screens_details[bait]['bait'] = bait

				accession = bait.split("_")[-3]
				start = bait.split("_")[-2]
				end = bait.split("_")[-1]

				self.screens_details[bait]['accession'] = accession
				self.screens_details[bait]['start'] = start
				self.screens_details[bait]['end'] = end

				bait_str = ""
				bait_str += bait + "\t"
				bait_str +=  accession + "\t"
				bait_str +=  start + "\t"
				bait_str +=  end + "\t"

				for tag in ['gene_name','protein_name']:
					cell = []

					self.screens_details[bait][tag] = self.bait_data[bait]['protein'][tag]

					cell.append(self.bait_data[bait]['protein'][tag])

					bait_str +=  ",".join(cell) + "\t"

				for tag in ['pfam_accession','clan_name', 'pfam_id','domain_name',]:
					cell = []

					self.screens_details[bait][tag] = ""

					for domain in self.bait_data[bait]['domain']:
						if tag in self.bait_data[bait]['domain'][domain]:
							cell.append(self.bait_data[bait]['domain'][domain][tag])
							self.screens_details[bait][tag] = self.bait_data[bait]['domain'][domain][tag]

					bait_str +=  ",".join(list(set(cell))) + "\t"

				bait_str +=  ",".join(self.bait_data[bait]['motif']) + "\t"
				self.screens_details[bait]['motif'] = ",".join(self.bait_data[bait]['motif'])


				for library in self.libraries:
					if library in list(self.data[bait].keys()):
						if self.options['show_screens_collapse'] :
							logger.info(library + ":" + str(len(self.data[bait][library]['screens'])))
						else:
							logger.info(library + "\t" + str(len(self.data[bait][library]['screens'])) + "\t" + bait_str)

						self.screens_details[bait][library + '_screen_count'] = len(self.data[bait][library]['screens'])
					else:
						if self.options['show_screens_collapse'] :logger.info("\t", end=' ')

						self.screens_details[bait][library +'_screen_count'] = 0


				if self.options['show_screens_collapse']:logger.info(bait_str)

			except Exception as e:
				logger.error("Error:" + bait)

		if self.options['library'] == None and self.options['bait'] == None:
			out_file_json = os.path.join(self.options["peptools_data_path"],"metadata",  "all.screen.details.json")

			with open(out_file_json, 'w') as outfile:
				json.dump(self.screens_details, outfile)

############################################################################################################
############################################################################################################