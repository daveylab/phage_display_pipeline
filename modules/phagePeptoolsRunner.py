#!/usr/bin/python3
#
# Program:      ProP-PD Data Analysis Pipeline
# Class:        phagePeptoolsRunner
# Description:  Analysis pipeline to process peptides returned from replicated ProP-PD selections
# Version:      1.0.0
# Last Edit:    1/1/2021
# Citation:     Under Preparation
# Author contact: Norman E. Davey <norman.davey@icr.ac.uk> Institute Of Cancer Research, Chelsea, SW3 6JB, London, United Kingdom.
# 
# License:
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http:#www.gnu.org/licenses/>.
#
# Copyright (C) 2020  Norman E. Davey


__doc__ = """
# ProP-PD Data Analysis Pipeline
### Code for the analysis tools for the ProP-PD data

"""

import sys,os,json,pprint

from datetime import datetime

#-----

sys.path.append(os.path.join(os.path.dirname(__file__),"../../utilities"))

#-----

import utilities_rest
import phageDataUtilities

#-----

class phagePeptoolsRunner():

	def __init__(self):
		self.options = {}

		self.phageDataUtilitiesObj = phageDataUtilities.phageDataUtilities()
		self.phageDataUtilitiesObj.options = self.options

	####################################
	def processMutantPetides(self,peptides):
		self.mutant_peptides = []

		self.parse_mutations_files()

		peptides_tmp = []
		for peptide in peptides:

			if peptide in self.mutations_re_mapping_info:
				self.mutant_peptides.append(self.mutations_re_mapping_info[peptide]['peptools_peptide'])
				peptides_tmp.append(self.mutations_re_mapping_info[peptide]['peptools_peptide'])

		return peptides_tmp

	####################################

	def parse_mutations_files(self):

		lines = open(os.path.abspath(self.options["mutant"])).read().strip().split("\n")
		headers = lines[0].split("\t")

		self.mutant_comparison = {}
		self.mutants_present = {}
		self.mutations_re_mapping_info = {}
		self.mutations_re_reverse_mapping_info = {}

		for line in lines[1:]:
			mutations_info_tmp = {}
			line_bits = line.split("\t")
			if len(line_bits) == len(headers):
				if line_bits[headers.index('wt_or_mut')] == "wt":
					wildtype_peptide = line_bits[headers.index('designed_peptide')]
					self.mutants_present[wildtype_peptide] = {}

		for line in lines[1:]:
			mutations_info_tmp = {}
			line_bits = line.split("\t")
			if len(line_bits) == len(headers):
				for i in range(0,len(headers)):
					mutations_info_tmp[headers[i]] = line_bits[i]

				library_peptide  = mutations_info_tmp['library_peptide']
				designed_peptide = mutations_info_tmp['designed_peptide']

				if mutations_info_tmp['wt_or_mut'] == "mut":
					mutated_position = int(mutations_info_tmp['mutated_position'])
					wildtype_residue = mutations_info_tmp['wild_type']
					mutated_residue = mutations_info_tmp['mutation']

					wildtype_peptide_tmp = list(designed_peptide)
					re_peptide_tmp = list(designed_peptide)

					if wildtype_peptide_tmp[mutated_position] == mutated_residue:
						wildtype_peptide_tmp[mutated_position] = wildtype_residue
						re_peptide_tmp[mutated_position] = "."
					else:
						print("ERROR",wildtype_peptide_tmp[mutated_position],wildtype_residue,mutated_residue,designed_peptide,library_peptide,designed_peptide == library_peptide)

					wildtype_peptide = "".join(wildtype_peptide_tmp)
					peptools_peptide = "".join(re_peptide_tmp).replace("C","A").replace(".","A")
					re_peptide = "".join(re_peptide_tmp).replace("C",".").replace("A",".")

					mutations_info_tmp['peptools_peptide'] = peptools_peptide
					mutations_info_tmp['wildtype_peptide'] = wildtype_peptide
					mutations_info_tmp['designed_peptide'] = designed_peptide
					mutations_info_tmp["library_peptide"] = library_peptide

					self.mutants_present[wildtype_peptide][library_peptide] = {"wt":wildtype_residue,"mut":mutated_residue,"offset":mutated_position,}
					self.mutations_re_mapping_info[library_peptide] = mutations_info_tmp
					self.mutations_re_reverse_mapping_info[re_peptide] = mutations_info_tmp

				else:
					wildtype_peptide = line_bits[headers.index('designed_peptide')]

					self.mutations_re_mapping_info[library_peptide] = {
					"wildtype_peptide":wildtype_peptide,
					"peptools_peptide":library_peptide,
					"designed_peptide":designed_peptide
					}
			else:
				print(("Line length mismatch",len(line_bits),len(headers),line))

	def runPeptools(self,peptides,taxon_id,bait_uniprot,domains,motif):

		self.restSubmitter = utilities_rest.RestSubmission()
		self.restSubmitter.server_url = self.options['peptools_server_url']

		peptides = list(set(peptides))
		peptides.sort()

		###-----------------------------------###
		# Replace the mutant peptides with the correct peptides so they can be mapped to the human proteome
		###-----------------------------------###

		if self.options["mutant"] != None:
			peptides = self.processMutantPetides(peptides)

		###-----------------------------------###
		### Set PepTools Options
		###-----------------------------------###

		params = {
		"search_type": "peptides",
		"peptides": "\n".join(["Peptide"] + peptides),
		"taxon_id": taxon_id,
		"replaceLetter": True,
		"letter2replace": "A",
		"iupred": 0,
		"flank": 0,
		"numResults":len(peptides) + 1,
		}

		try:
			if bait_uniprot.count(",") > 0:
				bait_uniprot = bait_uniprot.split(",")
			else:
				bait_uniprot = [bait_uniprot]

			params["shared_annots"] = bait_uniprot
			params["shared_pval_cutoff"] = 1
		except:
			pass
		
		if len(domains) > 0 and self.options['peptools_domain_shared_annotation']:
			params["interacting_domains"] = ",".join(domains)
		
		params["motif"] = motif
		
		###-----------------------------------###
		### Run PepTools
		###-----------------------------------###s

		params_hash = self.phageDataUtilitiesObj.params_to_hash(params)

		self.options["hits_file"] = os.path.join(self.options["peptools_data_path"], "peptools",params_hash + ".pssm.json")
		self.options["hits_id_file"] = os.path.join(self.options["peptools_data_path"],"peptools", params_hash + ".job.id")
		job_id = ""
	
		if (not os.path.exists(self.options["hits_file"]) or self.options['remake']) and self.options['run_peptools']:
			try:
				if self.options["verbose"]:print("Running Peptools",self.restSubmitter.server_url,self.options["hits_file"],"(",len(peptides),"peptides)")
				if self.options["debug"]:pprint.pprint(params)
				if not os.path.exists(self.options["hits_id_file"]) or self.options['remake']:

					search_data = self.restSubmitter.run_query('/search/',json.dumps(params), "POST")
					if 'job_id' in search_data:
						job_id = search_data['job_id']

						if self.options["verbose"]:
							print("\n------------------------------------------------------")
							print("JobId:", str(job_id))
							print(search_data)

						open(self.options["hits_id_file"],"w").write(job_id)
					else:
						print(search_data)
						if 'status' in search_data:
							search_data['status'] = 'Error'
				else:
					job_id = open(self.options["hits_id_file"]).read()
					print("Job exist:"+job_id)


				print("Downloading Peptools",self.restSubmitter.server_url,job_id, " -> ",self.options["hits_file"])
				
				params = {
				"jobid": job_id,
				 "fileFormat": "json"
				 }

				search_data = self.restSubmitter.run_query("/downloadInstances/",json.dumps(params), "POST")

				isRunning = True
				while isRunning:

					now = datetime.now()
					print("#Job running -",job_id,"-",now.strftime("%H:%M:%S"))

					search_data = self.restSubmitter.run_query("/downloadInstances/",json.dumps(params), "POST")
					
					if "status" in search_data:
						isRunning = self.restSubmitter.waiter(search_data['status'])
					else:
						isRunning = False

				save = True
				if "status" in search_data:
					if "error" in search_data["status"]:
						print("Error running PepTools")
						save = False

				if save:
					print("Writing Peptools",job_id, " -> ",self.options["hits_file"])

					with open(self.options["hits_file"], 'w') as outfile:
						json.dump(search_data, outfile)

			except Exception as e:
				print(e)

		else:
			if os.path.exists(self.options["hits_id_file"]):
				job_id = open(self.options["hits_id_file"]).read()

		del self.restSubmitter 

		return {"job_id":job_id,"peptools_hits_file":self.options["hits_file"]}
