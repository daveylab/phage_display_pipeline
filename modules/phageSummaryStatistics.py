import os, json
import pandas as pd

class phageSummaryStatistics:
  def summarizeScreenData(self, bait, library, peptides_data):
    data_dict = {
        'Conservation_score': [],
        'overlap': [],
        'mean_count_proportion': [],
        'promiscuity': [],
        'colocalisation_score': [],
        'shared_function_score': [],
        'shared_process_score': [],
        'replicates': [],
        'mean_counts': [],
        'max_counts': [],
        'slimfinder_score_p': [],
        'confidence': [],
        'curated_motifs': []
    }

    #iterating all peptides and filling data_dict
    for peptide in peptides_data:
      for key in data_dict.keys():
        try:
          if key == 'Conservation_score':
            data_dict[key].append(peptides_data[peptide]['Conservation']['score'])
          elif key == 'curated_motifs':
            if peptides_data[peptide][key]:
              data_dict[key].append(1)
            else:
              data_dict[key].append(0)
          else:
            data_dict[key].append(peptides_data[peptide][key])
        except:
          data_dict[key].append(None)

    df = pd.DataFrame(data_dict)

    #saving summary statistics in json file
    json_output = df.describe().to_json(orient="columns")
    json_output = json.loads(json_output)
    out_file_json = os.path.join(self.options["peptools_data_path"],"json",  bait + "." +  library + ".summary_statistics.json")
    with open(out_file_json, 'w') as outfile:
      json.dump(json_output, outfile)

    #saving summary mean in tsv file
    out_file_tsv = os.path.join(self.options["peptools_data_path"],"json",  "summary_statistics.tsv")
    means = df.mean().to_frame()
    means.columns = [bait + "." +  library]
    means = means.T
    if(os.path.isfile(out_file_tsv)):
      ts_output = means.to_csv(out_file_tsv, sep ='\t', mode='a', header=False)
    else:
      ts_output = means.to_csv(out_file_tsv, sep ='\t')

  def summarizeScreenJson(self, bait, library, peptides_output_json):
    with open(peptides_output_json) as file:
      data = json.load(file)
      self.summarizeScreenData(bait, library, data)

  def summarizeScreens(self):
    for f in os.listdir(os.path.join(self.options["peptools_data_path"],"json")):
      if f.endswith('.peptides.output.json'):
        f_name = f.split('.')
        self.summarizeScreenJson(f_name[0], f_name[1], os.path.join(self.options["peptools_data_path"],"json", f))

if __name__ == "__main__":
  try:
    phageSummaryStatisticsObj = phageSummaryStatistics()
    phageSummaryStatisticsObj.options = {'peptools_data_path': '/home/ec2-user/environment/data/phage_display_data/'}
    phageSummaryStatisticsObj.summarizeScreens()
  except Exception as e:
    print(e)
    raise