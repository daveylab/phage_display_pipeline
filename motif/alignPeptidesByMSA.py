import os,time,json,pprint,sys,optparse,hashlib,shutil,re,inspect

file_path = os.path.dirname(inspect.stack()[0][1])

sys.path.append(os.path.join(file_path,"../pssm/"))
sys.path.append(os.path.join(file_path,"../"))

import psiblastPSSM
import alignByPSSM

flexible_pattern = re.compile("[^A-Z\[\]]+")
positions_pattern = re.compile("[A-Z]|\[[A-Z]+\]|[^A-Z\[\]]+")

sys.path.append("../")
import config_reader
import option_reader

class alignPeptidesByMSA():
	def __init__(self):

		self.options = {
		"peptides":"",
		"elm_class":"",
		"remake":False,
		"show_elm_classes":False,
		"evalue":"100",
		'data_path':"/Users/normandavey/Documents/Work/Websites/slimdb/data/",
		'clustalo_path': '/Users/normandavey/Documents/Work/Tools/clustalo/clustalo',
		'mafft_path': 'not_set',
		'muscle_path': '/Users/normandavey/Documents/Work/Tools/muscle/muscle3.8.31_i86darwin64',
		'fsa_path': 'not_set',
		'probcons_path': 'not_set',
		'clustalw_path':'/Users/normandavey/Documents/Work/Tools/clustalw/clustalw2'
		}

		self.options.update(config_reader.load_configeration_options(sections=['align_peptides','general']))
		self.options["peptides"] = ""

		self.hits = {}
		self.data = {"peptides":{},"peptides_details":{},"peptides_id_mapping":{}}

	def setup_paths(self):

		self.options["peptide_data_path"] = os.path.join(self.options["data_path"] ,"peptide")
		self.options['resources_data_path'] = self.options["peptide_data_path"]

		if not os.path.exists(self.options["peptide_data_path"]):
			os.mkdir(self.options["peptide_data_path"])

		for hmm_dir in ["results","hmms","fasta","databases","peptides","pssm"]:
			if not os.path.exists(os.path.join(self.options["peptide_data_path"],hmm_dir)):
				print("Making",os.path.join(self.options["peptide_data_path"],hmm_dir))
				os.mkdir(os.path.join(self.options["peptide_data_path"],hmm_dir))

		self.paths = {}

		for alignment_algorithm in ["muscle", "clustalo", "clustalw", "fsa", "probcons", "mafft"]:
			if alignment_algorithm + "_path" in self.options:
				self.paths[alignment_algorithm] = self.options[alignment_algorithm + "_path"]

	################################################
	################################################

	def parse_peptides(self):
		fasta_str = ""

		peptides_lengths = []
		counter = 0

		if isinstance(self.options["peptides"], str):
			peptides = self.options["peptides"].split(",")

		if isinstance(self.options["peptides"], list):
			peptides = self.options["peptides"]

		for peptide in peptides:
			fasta_str += ">peptide" + str(counter) + "\n"

			if peptide == "":
				fasta_str +=  "A\n"
			else:
				fasta_str += peptide.replace("-","") + "\n"

			self.data["peptides_id_mapping"]["peptide" + str(counter)] = peptide
			self.data["peptides"][peptide] = peptide
			self.data["peptides_details"][peptide] = {
			'original_n_flank':"",
			'original_c_flank':"",
			'original_start':1,
			'original_end':len(peptide),
			'original_peptide':peptide
			}

			counter += 1

		self.options["motif_dataset"] = str(hashlib.md5(fasta_str.encode()).hexdigest())
		print(self.options["motif_dataset"])
		self.paths["fasta"] = os.path.join(self.options["peptide_data_path"],"fasta" , self.options["motif_dataset"]  +  ".fasta")


		open(self.paths["fasta"],"w").write(fasta_str)

	################################################
	################################################

	def alignPeptides(self):
		if not os.path.exists(self.paths["alignment"] ) or self.options["remake"]:
			alignmentAlgorithm = self.options["alignmentAlgorithm"]

			if not os.path.exists(self.paths[alignmentAlgorithm]):
				return  {"status":"Error","type":"Alignment algorithm does not appear to exist"}

			if alignmentAlgorithm == "muscle":
				cmd = self.paths["muscle"] + " -in " + self.paths["fasta"] + " -out " + self.paths["alignment"]  + ""

			if alignmentAlgorithm == "clustalo":
				cmd = self.paths["clustalo"] + " --force --infile=" + self.paths["fasta"] + " --outfile=" + self.paths["alignment"]  + ""

			if alignmentAlgorithm == "clustalw":
				cmd = self.paths["clustalw"] + " -gapopen=1000 -ALIGN -INFILE=" + self.paths["fasta"] + " -OUTFILE=" + self.paths["alignment"]  + " -OUTPUT=FASTA -OUTORDER=INPUT"

			if alignmentAlgorithm == "fsa":
				cmd = self.paths["fsa"] + " " + self.paths["fasta"] + " > " + self.paths["alignment"]  + ""

			if alignmentAlgorithm == "mafft":
				cmd = self.paths["mafft"] + " --quiet --anysymbol --auto " + self.paths["fasta"] + " > " + self.paths["alignment"]  + ""

			if alignmentAlgorithm == "probcons":
				cmd = self.paths["probcons"] +  " " + self.paths["fasta"] + " > " + self.paths["alignment"]  + ""

			if len(cmd) > 0:
				os.popen(cmd).read()
			else:
				print("Can't run",alignment_path)

			return {"status":"Success"}
		else:
			return {"status":"Exists"}

	################################################
	################################################

	def parseAlignment(self):
		try:
			fasta = open(self.paths["alignment"]).read()

			#pprint.pprint(self.data["peptides_id_mapping"])
			peptides = []
			details = {}

			for line in fasta.split(">"):
				line_bits = line.strip().split("\n")

				if len(line_bits) > 1:
					peptides.append(line_bits[1])

					if line_bits[0] in self.data["peptides_id_mapping"]:
						details[self.data["peptides_id_mapping"][line_bits[0]]] = "".join(line_bits[1:])
					else:
						details[line_bits[0]] = "".join(line_bits[1:])

			self.data["aligned_peptides"] = details

			json_data = {
			"fasta":fasta,
			"peptides":peptides,
			"details":details
			}

			return json_data
		except:
			return {}

	def stripGaps(self,peptides,gap_proportion):
		columns = {}
		for i in range(0,len(peptides[0])):
			columns[i] = []
			for j in range(0,len(peptides)):
				columns[i].append(peptides[j][i])

		use_columns = []

		for i in range(0,len(columns)):
			if float(columns[i].count("-"))/len(columns[i]) < gap_proportion:
				use_columns.append(i)

		peptides_degapped = []
		for i in range(0,len(peptides)):
			peptides_degapped.append(peptides[i][min(use_columns):max(use_columns)+1])

		return peptides_degapped


	def alignByPSSM(self):
		alignByPSSMObj = alignByPSSM.alignByPSSM()

		alignByPSSMObj.option['data_path'] = self.options['data_path']
		#alignByPSSMObj.option['pssm_score_scheme'] = "psi_blast IC"
		alignByPSSMObj.option['pssm_score_scheme'] = "binomial log"

		alignByPSSMObj.data["aligned_peptides"] = list(self.data["aligned_peptides"].values())
		alignByPSSMObj.data['peptides'] = list(self.data["aligned_peptides"].values())

		#print "\n".join(self.data["aligned_peptides"].values())
		#print alignByPSSMObj.data['peptides']
		#alignByPSSMObj.get_PSSM()
		#alignByPSSMObj.interatively_align()

		alignByPSSMObj.data["pssm_peptides"] = None
		alignByPSSMObj.get_PSSM()
		aligned_peptides = alignByPSSMObj.split_align()

		self.data["pssm_aligned_peptides"] = {}

		for peptide in aligned_peptides["aligned_peptides"]:
			self.data["pssm_aligned_peptides"][aligned_peptides["aligned_peptides"][peptide]['peptide']] = aligned_peptides["aligned_peptides"][peptide]

		self.data["pssm_aligned_peptides_motif"] = alignByPSSMObj.data["motif"]

	################################################
	################################################

	def runAlignPeptides(self):

		if self.options['elm_class'] == "" and self.options['peptides'] == "":
			return  {"status":"Error","status_details":"Set elm_class or peptides"}
		else:
			if self.options['elm_class'] != "":
				motif_datasets = self.get_elm_classes()

				self.options["motif_dataset"] = self.options['elm_class']
				runSearch = False

				self.paths["elm_instances"] = os.path.join(self.options["data_path"],"elm" , self.options["motif_dataset"]  +  ".json")
				self.paths["fasta"] = os.path.join(self.options["peptide_data_path"],"fasta" , self.options["motif_dataset"]  +  ".fasta")

				if self.options['elm_class'] in motif_datasets['data']:
					self.get_elm_instances()
					self.parse_elm_instances()
					runSearch = True
				else:
					return  {"status":"Error","status_details":self.options['elm_class'] + "does not exist"}
			else:
				self.parse_peptides()
				runSearch = True

			if runSearch:
				try:
					self.paths["fasta"] = os.path.join(self.options["peptide_data_path"], "fasta", self.options["motif_dataset"]  +  ".fasta")
					self.paths["alignment"] = os.path.join(self.options["peptide_data_path"], "fasta", self.options["motif_dataset"]  + "." + self.options["alignmentAlgorithm"] + ".aln.fasta")

					if len(self.data["peptides"]) > 1:
						status = self.alignPeptides()
						self.parseAlignment()
						self.alignByPSSM()
					else:
						status = {"status":"Error","status_details":"Only one peptide"}

					return status

				except Exception as e:
					print(e)
					raise
					return {"status":"Error","status_details":str(e)}


	################################################
	################################################

if __name__ == "__main__":

	op = optparse.OptionParser()

	op.add_option("--peptides",
		  action="store",
		  dest="peptides",
		  default="",
		  help="Comma seperated list of aligned peptides used to make HMM.")
	op.add_option("--elm_class",
		  action="store",
		  dest="elm_class",
		  default="",
		  help="ELM class used to make HMM.")
	op.add_option("--evalue",
		  action="store",
		  dest="evalue",
		  default="100",
		  help="Set HMMer e value cuoff.")
	op.add_option("--remake",
		  action="store",
		  dest="remake",
		  default=False,
		  help="Remake all files. [True|False]")
	op.add_option("--show_elm_classes",
		  action="store",
		  dest="show_elm_classes",
		  default=False,
		  help="Print list of ELM classes.")

	opts, args = op.parse_args()

	alignPeptideObj = alignPeptidesByMSA()

	for option in alignPeptideObj.options:
		try:
			alignPeptideObj.options[option] = getattr(opts, option)
		except:
			pass

	alignPeptideObj.setup_paths()
	#motif_datasets = alignPeptideObj.get_elm_classes()

	#if  alignPeptideObj.options["show_elm_classes"]:
	#	pprint.pprint(motif_datasets['data'])
	#	sys.exit()


	#alignPeptideObj.options["task"] = "search"
	#alignPeptideObj.options["type"] = "elm"
	#alignPeptideObj.options["peptides"] ='RTPEVNPMDAEGLSGL,SALLLVLAGWLFHVKG,MFLRLIDDNGIVLNSI,LSKSVNELQHTLQALR,RQTGRQPTPLSPPLRN,ETIARLVDDPLFPTAW,ALQTTSIPSQYGNTTV,AASERERLKQEVEDMV,WTSSGSSTASAGNVTG,DVKDKGWSLLAPITAY,EWETISNSSLGRAGAY,ERAMGRVADTIARGPS,QDSLGVSGSSITTLSS,TRRRSAVFGGEGDPVG,KPRNFPVAQIPQGLTP,RRRTPRKTKTHSSSAS,AEAM,ELAGVVAKLQQEVTEL,PTTRRSFGVEPSGSGH,GAGAFGPGFTPPHGGL,GSGSPGRRHSQASYPS,PSNTTVTSTTSISTSN,KRGHNRKTGDLQGGMG,DGYVSGEPWPGAGPPP,MDCEGTDGEGLGCTGW,NALQQDSTTQGCLGAE,TVTSTTSISTSN,VFDPVHRLVSEQTGTP,SLKHCHWFFNNYLRKR,NSRELLCQQQSEESEQ,NKQEHDDIESSVVSLV,KGSQHTNNTHHHKRNL,WAILPSVVGFWITLQY'
	alignPeptideObj.options["peptides"] ='KLITYDNPAYEGIDVD,FLTHPSSLVTFDNPAY,PSSFVTFDNPAFEPVD,LVTFDNPAFEPGDETI,LDNPTTLISADNPVFE,PSSLVTFDNPAYEPLD,TTPTKLITYDNPAYEG,NPSTFVTFDNPAYEPI,FVTHPSSFVTFDNPAF,RPTSLVTFDNPAFEPG,FVTFDNPAYEPIDETL,LITYDNPAFESFDPED,DRPADLVTFDNPVYDP,LSSPQRLITFDNPAYE,LVTYDNPAFEGFNPED,ERPADLVTFDNPVYDP,QRLITFDNPAYEGEDV,PSSFVTFDNPAFEPGD'
	alignPeptideObj.options["remake"] = True
	alignPeptideObj.options["alignmentAlgorithm"] = 'clustalw'
	alignPeptideObj.options['blast_path'] = '/Users/normandavey/Documents/Work/Tools/blast+/bin'
	alignPeptideObj.options['slimfinder_path'] = '/Users/normandavey/Documents/Work/Tools/scripts/data_management/tools/slimfinder.py'

	#for path in ["clustalo_path","mafft_path","muscle_path","fsa_path","probcons_path","clustalw_path"]:
	#	if path in options:
	#		alignPeptideObj.paths[path.split("_")[0]] = options[path]

	result = alignPeptideObj.runAlignPeptides()
	pprint.pprint(alignPeptideObj.data)

	"""
	##
	elm_classes = ["CLV_C14_Caspase3-7","CLV_MEL_PAP_1","CLV_NRD_NRD_1","CLV_PCSK_FUR_1","CLV_PCSK_KEX2_1","CLV_PCSK_PC1ET2_1","CLV_PCSK_PC7_1","CLV_PCSK_SKI1_1","CLV_Separin_Fungi","CLV_Separin_Metazoa","CLV_TASPASE1","DEG_APCC_DBOX_1","DEG_APCC_KENBOX_2","DEG_APCC_TPR_1","DEG_COP1_1","DEG_CRL4_CDT2_1","DEG_CRL4_CDT2_2","DEG_Kelch_actinfilin_1","DEG_Kelch_Keap1_1","DEG_Kelch_Keap1_2","DEG_Kelch_KLHL3_1","DEG_MDM2_SWIB_1","DEG_Nend_UBRbox_4","DEG_ODPH_VHL_1","DEG_SCF_COI1_1","DEG_SCF_FBW7_1","DEG_SCF_FBW7_2","DEG_SCF_SKP2-CKS1_1","DEG_SCF_TIR1_1","DEG_SCF_TRCP1_1","DEG_SIAH_1","DEG_SPOP_SBC_1","DOC_AGCK_PIF_1","DOC_AGCK_PIF_2","DOC_AGCK_PIF_3","DOC_ANK_TNKS_1","DOC_CKS1_1","DOC_CYCLIN_1","DOC_GSK3_Axin_1","DOC_MAPK_DCC_7","DOC_MAPK_FxFP_2","DOC_MAPK_gen_1","DOC_MAPK_HePTP_8","DOC_MAPK_JIP1_4","DOC_MAPK_MEF2A_6","DOC_MAPK_NFAT4_5","DOC_MAPK_RevD_3","DOC_PIKK_1","DOC_PP1_MyPhoNE_1","DOC_PP1_RVXF_1","DOC_PP1_SILK_1","DOC_PP2A_B56_1","DOC_PP2B_LxvP_1","DOC_PP2B_PxIxI_1","DOC_SPAK_OSR1_1","DOC_USP7_MATH_1","DOC_USP7_MATH_2","DOC_WD40_RPTOR_TOS_1","DOC_WW_Pin1_4","LIG_14-3-3_CanoR_1","LIG_14-3-3_ChREBP_3","LIG_14-3-3_CterR_2","LIG_Actin_RPEL_3","LIG_Actin_WH2_1","LIG_Actin_WH2_2","LIG_ANK_PxLPxL_1","LIG_AP_GAE_1","LIG_AP2alpha_1","LIG_AP2alpha_2","LIG_APCC_ABBA_1","LIG_APCC_ABBAyCdc20_2","LIG_APCC_Cbox_1","LIG_APCC_Cbox_2","LIG_BH_BH3_1","LIG_BIR_III_1","LIG_BIR_III_2","LIG_BIR_III_3","LIG_BIR_III_4","LIG_BRCT_BRCA1_1","LIG_BRCT_BRCA1_2","LIG_BRCT_MDC1_1","LIG_CaM_IQ_9","LIG_CaMK_CASK_1","LIG_CAP-Gly_1","LIG_CAP-Gly_2","LIG_CID_NIM_1","LIG_Clathr_ClatBox_1","LIG_Clathr_ClatBox_2","LIG_CNOT1_NIM_1","LIG_CORNRBOX","LIG_CSK_EPIYA_1","LIG_CSL_BTD_1","LIG_CtBP_PxDLS_1","LIG_DCNL_PONY_1","LIG_Dynein_DLC8_1","LIG_EABR_CEP55_1","LIG_EF_ALG2_ABM_1","LIG_EF_ALG2_ABM_2","LIG_EH_1","LIG_EH1_1","LIG_eIF4E_1","LIG_eIF4E_2","LIG_EVH1_1","LIG_EVH1_2","LIG_EVH1_3","LIG_FAT_LD_1","LIG_FHA_1","LIG_FHA_2","LIG_G3BP_FGDF_1","LIG_GBD_Chelix_1","LIG_GLEBS_BUB3_1","LIG_GSK3_LRP6_1","LIG_GYF","LIG_HCF-1_HBM_1","LIG_HOMEOBOX","LIG_HP1_1","LIG_Integrin_isoDGR_1","LIG_IRF3_LxIS_1","LIG_KEPE_1","LIG_KEPE_2","LIG_KEPE_3","LIG_KLC1_WD_1","LIG_LIR_Apic_2","LIG_LIR_Gen_1","LIG_LIR_LC3C_4","LIG_LIR_Nem_3","LIG_LRP6_Inhibitor_1","LIG_LYPXL_L_2","LIG_LYPXL_S_1","LIG_MAD2","LIG_Mtr4_Air2_1","LIG_Mtr4_Trf4_1","LIG_Mtr4_Trf4_2","LIG_MYND_1","LIG_MYND_2","LIG_MYND_3","LIG_NBox_RRM_1","LIG_NRBOX","LIG_OCRL_FandH_1","LIG_PALB2_WD40_1","LIG_PAM2_1","LIG_PAM2_2","LIG_PCNA_PIPBox_1","LIG_PDZ_Class_1","LIG_PDZ_Class_2","LIG_PDZ_Class_3","LIG_Pex14_1","LIG_Pex14_2","LIG_Pex14_3","LIG_Pex3_1","LIG_PTAP_UEV_1","LIG_PTB_Apo_2","LIG_PTB_Phospho_1","LIG_Rb_LxCxE_1","LIG_Rb_pABgroove_1","LIG_RGD","LIG_RPA_C_Fungi","LIG_RPA_C_Vert","LIG_RRM_PRI_1","LIG_SH2_GRB2","LIG_SH2_PTP2","LIG_SH2_SRC","LIG_SH2_STAT3","LIG_SH2_STAT5","LIG_SH2_STAT6","LIG_SH3_1","LIG_SH3_2","LIG_SH3_3","LIG_SH3_4","LIG_SH3_5","LIG_Sin3_1","LIG_Sin3_2","LIG_Sin3_3","LIG_SPRY_1","LIG_SUFU_1","LIG_SUMO_SIM_anti_2","LIG_SUMO_SIM_par_1","LIG_SxIP_EBH_1","LIG_TPR","LIG_TRAF2_1","LIG_TRAF2_2","LIG_TRAF6","LIG_TRFH_1","LIG_TYR_ITAM","LIG_TYR_ITIM","LIG_TYR_ITSM","LIG_UBA3_1","LIG_UFM1_UFIM_1","LIG_ULM_U2AF65_1","LIG_Vh1_VBS_1","LIG_WD40_WDR5_VDV_1","LIG_WD40_WDR5_VDV_2","LIG_WD40_WDR5_WIN_1","LIG_WD40_WDR5_WIN_2","LIG_WD40_WDR5_WIN_3","LIG_WH1","LIG_WRPW_1","LIG_WRPW_2","LIG_WW_1","LIG_WW_2","LIG_WW_3","MOD_ASX_betaOH_EGF","MOD_CAAXbox","MOD_CDK_SPK_2","MOD_CDK_SPxK_1","MOD_CDK_SPxxK_3","MOD_CK1_1","MOD_CK2_1","MOD_CMANNOS","MOD_GlcNHglycan","MOD_GSK3_1","MOD_LATS_1","MOD_N-GLC_1","MOD_N-GLC_2","MOD_NEK2_1","MOD_NMyristoyl","MOD_OFUCOSY","MOD_OGLYCOS","MOD_PIKK_1","MOD_PK_1","MOD_PKA_1","MOD_PKA_2","MOD_PKB_1","MOD_Plk_1","MOD_Plk_2-3","MOD_Plk_4","MOD_ProDKin_1","MOD_SPalmitoyl_2","MOD_SPalmitoyl_4","MOD_SUMO_for_1","MOD_SUMO_rev_2","MOD_TYR_CSK","MOD_TYR_DYR","MOD_WntLipid","TRG_AP2beta_CARGO_1","TRG_Cilium_Arf4_1","TRG_Cilium_RVxP_2","TRG_ENDOCYTIC_2","TRG_ER_diArg_1","TRG_ER_diLys_1","TRG_ER_FFAT_1","TRG_ER_KDEL_1","TRG_Golgi_diPhe_1","TRG_LysEnd_APsAcLL_1","TRG_LysEnd_APsAcLL_3","TRG_LysEnd_GGAAcLL_1","TRG_LysEnd_GGAAcLL_2","TRG_NES_CRM1_1","TRG_NLS_Bipartite_1","TRG_NLS_MonoCore_2","TRG_NLS_MonoExtC_3","TRG_NLS_MonoExtN_4","TRG_PTS1","TRG_PTS2"]
	# ["LIG_EF_ALG2_ABM_1","LIG_SxIP_EBH_1","LIG_CaM_IQ_9","MOD_ProDKin_1","TRG_NLS_MonoCore_2","LIG_Rb_LxCxE_1","LIG_EF_ALG2_ABM_1","DOC_WW_Pin1_4","TRG_NES_CRM1_1", "LIG_SH3_3","MOD_OFUCOSY"]:
	#elm_classes = ["MOD_N-GLC_1"]
	for elm_class in elm_classes:

		print "#",elm_class
		alignPeptideObj.options["elm_class"] = elm_class
		try:
			result = alignPeptideObj.runAlignPeptides()
		except:
			print "ERROR",elm_class
			raise

	#pprint.pprint(result)

	##
	#pprint.pprint(result)

	##
	"""
