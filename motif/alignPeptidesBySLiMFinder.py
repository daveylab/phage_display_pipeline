import os,time,json,pprint,sys,optparse,hashlib,shutil,re, inspect

file_path = os.path.dirname(inspect.stack()[0][1])

sys.path.append(os.path.join(file_path,"../pssm/"))
sys.path.append(os.path.join(file_path,"../"))

import psiblastPSSM
import config_reader
import option_reader

flexible_pattern = re.compile("[^A-Z\[\]]+")
positions_pattern = re.compile("[A-Z]|\[[A-Z]+\]|[^A-Z\[\]]+")
positions_gaps_pattern = re.compile("\.|[A-Z]|\[[A-Z]+\]|[^A-Z\[\]]+")

class alignPeptidesBySLiMFinder():
	def __init__(self):

		self.input_options = {
		"peptides":"",
		"input_file":"",
		"output_file":"",
		"output_type":"tdt",
		"make_output":True,
		"use_peptide_aa_freqs":False,
		"task":"motif",
		"topranks":None,
		"output_rank":1,
		"termini":False,
		"slimlen":4,
		"walltime":10.0,
		"slimfinder_background_frequencies":0.5,
		"probcut":0.01,
		"slimfinder_equiv":"ILMVFWY,ILMVF,ILMV,ILMF,DE,KRH,KR,AGSTCVP,STGA,ST,ILV,FHYW,FWY,AGSC,AP,NQ,DEST,DEN,ST,STNQ", # Original - AGS,ILMV,IVMLFAP,IVMLFWHYA,FYWH,KRH,ST,STNQ,DEST
		"require_motif":False,
		"remake":False,
		"variant":"fast_heuristic",
		"ambiguity_heuristic":True,
		"run_cluster_peptides":True,
		"cluster_type":"exact_residues",
		"clustering_type":"DBSCAN",
		"variable_length_gaps":False,
		"aafreq_file":None
		}

		self.input_options_help = {
		"peptides":"Comma seperated list of peptides",
		"input_file":"File containing a list of peptides as fasta or newline separated",
		"output_file":"File to output aligned peptides",
		"output_file_type":"File to output aligned peptides",
		"task":"Whether to find motifs or align by found motifs [motif|align]",
		"require_motif":"Whether the SLiMFinder motif must be present to retain peptide in the aligned peptides output",
		"remake":"Remake all files. [True|False]",
		"variant":"fast_heuristic | exhaustive_heuristic | exhaustive (very slow)"
		}

		self.options = self.input_options
		self.options.update(config_reader.load_configeration_options(sections=['align_peptides','general']))

	def setup_paths(self):

		self.hits = {}
		self.data = {"peptides":{},"peptides_details":{},"peptides_id_mapping":{}}
		self.aligned_peptide_data = {}

		#------------------------------------------------------------------------------------#

		self.options["peptide_data_path"] = os.path.join(self.options["data_path"] ,"peptides","peptide")
		self.options['resources_data_path'] = self.options["peptide_data_path"]

		if not os.path.exists(os.path.join(self.options["data_path"])):
			os.mkdir(self.options["data_path"])

		if not os.path.exists(os.path.join(self.options["data_path"] ,"peptides")):
			os.mkdir(os.path.join(self.options["data_path"] ,"peptides"))

		if not os.path.exists(self.options["peptide_data_path"]):
			os.mkdir(self.options["peptide_data_path"])

		for hmm_dir in ["results","hmms","fasta","databases","peptides","pssm"]:
			if not os.path.exists(os.path.join(self.options["peptide_data_path"],hmm_dir)):
				print(("Making",os.path.join(self.options["peptide_data_path"],hmm_dir)))
				os.mkdir(os.path.join(self.options["peptide_data_path"],hmm_dir))

		self.paths = {}

		self.paths["muscle"] = ""
		self.paths["clustalo"] = ""
		self.paths["clustalw"] = ""
		self.paths["fsa"] = ""
		self.paths["probcons"] = ""
		self.paths["mafft"] = ""

		self.paths["elm_classes"] = os.path.join(self.options["data_path"],"elm" ,"classes.json")

		if self.options["aafreq_file"] != None:
			self.paths["aafreq_file"] = self.options["aafreq_file"] 
		else:
			self.paths["aafreq_file"] = os.path.join(os.path.dirname(__file__),"aa_freq_true_" + str(self.options['slimfinder_background_frequencies']) + ".tdt")


	################################################
	################################################

	def parse_elm_instances(self):
		if not os.path.exists(self.paths["elm_instances"]):
			self.get_elm_instances(motif_dataset)

		try:
			with open(self.paths["elm_instances"]) as data_file:
				data = json.load(data_file)

			fasta_str = ""
			for instance in data['data']:
				fasta_str += ">" + instance + "\n"
				fasta_str += data['data'][instance]['extended_peptide'] + "\n"
				self.data["peptides"][instance] = data['data'][instance]['extended_peptide']
				self.data["peptides_details"][instance] = {
				'original_n_flank':"",
				'original_c_flank':"",
				'original_start':1,
				'original_end':len(data['data'][instance]['extended_peptide']),
				'original_peptide':data['data'][instance]['extended_peptide']
				}

			open(self.paths["fasta"],"w").write(fasta_str)
		except:
			print(("Error",self.paths["elm_instances"]))

	################################################
	################################################

	def use_peptide_aa_freqs(self):
		concat_peptides = "".join(list(self.data["peptides"].values()))

		aa_freqs_str = "AA\tFREQ\n"
		for aa in ["A","C","D","E","F","G","H","I","K","L","M","N","P","Q","R","S","T","V","W","Y"]:
			aa_freqs_str += aa + "\t" + "%1.6f"%(float(concat_peptides.count(aa))/len(concat_peptides)) + "\n"

		self.paths["aafreq_file"] =  self.paths["fasta"] + ".freq.tdt"
		open(self.paths["aafreq_file"],"w").write(aa_freqs_str.strip())

	################################################
	################################################

	def parse_peptides(self):
		fasta_str = ""

		counter = 0

		if not isinstance(self.options["peptides"],(list)):
			peptides = self.options["peptides"].split(",")
		else:
			peptides = self.options["peptides"]

		peptides.sort()

		for peptide in peptides:
			peptide = peptide.replace("-","")

			fasta_str += ">peptide" + str(counter) + "\n"

			if peptide == "":
				fasta_str +=  "A\n"
			else:
				fasta_str += peptide + "\n"

			self.data["peptides_id_mapping"]["peptide" + str(counter)] = peptide
			self.data["peptides"][peptide] = peptide
			self.data["peptides_details"][peptide] = {
			'original_n_flank':"",
			'original_c_flank':"",
			'original_start':1,
			'original_end':len(peptide),
			'original_peptide':peptide
			}

			counter += 1

		self.options["motif_dataset"] = str(hashlib.md5(fasta_str.encode()).hexdigest())
	
		self.paths["fasta"] = os.path.join(self.options["peptide_data_path"],"fasta" , self.options["motif_dataset"]  +  ".fasta")

		open(self.paths["fasta"],"w").write(fasta_str)

	################################################
	################################################

	def parseAlignment(self):
		try:

			input = open(self.options["input_file"]).read()
			self.options["motif_dataset"] = str(hashlib.md5(input.encode()).hexdigest())
		
			counter = 0
			fasta_str = ""

			if input[0] == ">":

				for line in input.split(">"):

					line_bits = line.strip().split("\n")

					if len(line_bits) > 1:
						peptide = "".join(line_bits[1:]).strip()
						if peptide == "": continue

						fasta_str += ">peptide" + str(counter) + "\n"
						fasta_str += peptide + "\n"

						self.data["peptides_id_mapping"]["peptide" + str(counter)] = peptide
						self.data["peptides"][peptide] = peptide
						self.data["peptides_details"][peptide] = {
						'original_n_flank':"",
						'original_c_flank':"",
						'original_start':1,
						'original_end':len(peptide),
						'original_peptide':peptide
						}

						counter += 1
			else:
				for line in input.split("\n"):

					peptide = line.strip()
					if peptide == "": continue

					fasta_str += ">peptide" + str(counter) + "\n"
					fasta_str += peptide + "\n"

					self.data["peptides_id_mapping"]["peptide" + str(counter)] = peptide
					self.data["peptides"][peptide] = peptide
					self.data["peptides_details"][peptide] = {
					'original_n_flank':"",
					'original_c_flank':"",
					'original_start':1,
					'original_end':len(peptide),
					'original_peptide':peptide
					}

					counter += 1

			self.paths["fasta"] = os.path.join(self.options["peptide_data_path"],"fasta" , self.options["motif_dataset"]  +  ".fasta")
			open(self.paths["fasta"],"w").write(fasta_str)

		except:
			raise
	################################################
	################################################

	def run_slimfinder(self):

		if self.options['use_peptide_aa_freqs']:
			self.use_peptide_aa_freqs()

		slimfinder_option = [
			"blast+path=" + self.options['blastplus_path'],
			"masking=F",
			"termini=F",
			"extras=0",
			"slimlen=" + str(self.options['slimlen']),
			"minic=1.1",
			"blaste=0.0001",
			'metmask=F',
			'posmask=""',
			"clusters=-1",
			"clouds=-1",
			"maxgapx=3",
			"savespace=3",
			"compmask=0,0",
			"probcut=" + str(self.options["probcut"]),
			"topranks=500",
			"maxwild=5",
			"aafreq=" + self.paths["aafreq_file"],
			"i=-1",
			"v=1",
			"seqin=" + self.paths["fasta"],
			"resdir=" + os.path.join(self.options['resources_data_path'], "SLiMFinder",  self.options['motif_dataset']),
			"walltime=" + str(self.options['walltime']),
			"variant" + str(self.options['variant'])
			]

		if self.options['termini']:
			slimfinder_option.append("termini=T")

		if self.options['variable_length_gaps']:
			slimfinder_option.append("wildvar=T")
		else:
			slimfinder_option.append("wildvar=F")

		if self.options['variant'] == 'fast_heuristic':
			slimfinder_option.append("equiv=AGS,ILMV,IVMLFAP,IVMLFWHYA,FYWH,KRH,ST,STNQ,DEST")
		elif self.options['variant'] == 'exhaustive_heuristic':
			slimfinder_option.append("minocc=0.51")
			slimfinder_option.append("probcut=0.05")
			slimfinder_option.append("equiv=" + self.options['slimfinder_equiv'])
		elif self.options['variant'] == 'exhaustive':
			slimfinder_option.append("minocc=0.51")
			slimfinder_option.append("probcut=0.05")
			slimfinder_option.append("equiv=" + self.options['slimfinder_equiv'])
			slimfinder_option.append('ambocc=1')
			slimfinder_option.append('absminamb=1')
		else:
			print("Variant must be one of: fast_heuristic | exhaustive_heuristic | exhaustive (very slow)")
			sys.exit()

		slimfinder_option.sort()

		self.options["slimfinder_tag"] = str(hashlib.md5((" ".join(slimfinder_option) + "peptides=" ",".join(self.data["peptides"])).encode()).hexdigest())
		self.paths["slimfinder_results"] = os.path.join(self.options['resources_data_path'], "SLiMFinder",  self.options['motif_dataset'],self.options["slimfinder_tag"] + ".slimfinder.out.tdt")
		self.options['slimfinder_instances_results'] =  os.path.join(self.options['resources_data_path'], "SLiMFinder",  self.options['motif_dataset'],self.options["slimfinder_tag"] + ".slimfinder.out.occ.tdt" )

		slimfinder_option.append("resfile=" + self.paths["slimfinder_results"])

		###
		
		if not os.path.exists(self.paths["slimfinder_results"]) or self.options["remake"]:

			if os.path.exists(self.paths["slimfinder_results"]):
				print(("Removing:",self.paths["slimfinder_results"]))
				os.remove(self.paths["slimfinder_results"])

			if os.path.exists(self.options['slimfinder_instances_results']):
				print(("Removing:",self.options["slimfinder_instances_results"]))
				os.remove(self.options['slimfinder_instances_results'])

			cmd = "python " + self.options['slimfinder_path'] + " " + " ".join(slimfinder_option)
			print(cmd)
			print((os.popen(cmd).read()))
			#print output

		###

	def parse_slimfinder_motifs(self):
		motifs = open(self.paths["slimfinder_results"]).read().strip().split("\n")

		if "motifs" not in self.data:
			self.data["motifs"] = {}

		if "motif_rank" not in self.data:
			self.data["motif_rank"] = {}

		for motif in motifs[1:]:
			motif_data = motif.split("\t")

			pattern_re = re.compile(motif_data[12])
			instances = 0
			support = 0
			instance_peptides = []
			for peptide in self.data["peptides"]:
				matches = pattern_re.findall(peptide)
				instances += len(matches)
				if len(matches) > 0:
					support += 1
					instance_peptides += [peptide]

			#print motif_data[12], len(self.data["peptides"]),instances,len(list(set(instance_peptides)))
			#print instance_peptides
			if motif_data[12] == "-": continue

			try:
				if motif_data[12] not in self.data["motif_rank"]:
					self.data["motif_rank"][motif_data[12]] = len(self.data["motif_rank"]) + 1

				self.data["motifs"][self.data["motif_rank"][motif_data[12]] ] = {
				"rank": self.data["motif_rank"][motif_data[12]],
				"sig": motif_data[11],
				"p": motif_data[18],
				"instances":instances,
				"instance_peptides":",".join(instance_peptides),
				"proteins":len(list(set(instance_peptides))),
				"proteins coverage": "%1.2f"%(float(support)/int(motif_data[6])),
				"search instances": motif_data[14],
				"search proteins": motif_data[15],
				"search proteins coverage": "%1.2f"%(float(motif_data[15])/int(motif_data[6])),
				"upcs": motif_data[16],
				"upcs coverage":"%1.2f"%( float(motif_data[16])/int(motif_data[7])),
				"expected upcs": motif_data[17],
				"peptide upc count": motif_data[7],
				"peptide count": motif_data[7],
				"motif": motif_data[12]
				}
			except:
				pass

	def parse_slimfinder_instances(self):
		instances = open(self.options['slimfinder_instances_results']).read().strip().split("\n")

		for instance in instances[1:]:
			instance_data = instance.split("\t")

			if self.data["motif_rank"][instance_data[3]] not in self.data["motif_instances"]:
				self.data["motif_instances"][self.data["motif_rank"][instance_data[3]]] = []

			protein_data = instance_data[5].split("_")[0].split("-")
			peptide_id = protein_data[0]
			#print instance_data[5]
			peptide = self.data["peptides"][self.data["peptides_id_mapping"][peptide_id]]
			#protein = self.data["peptides_details"][protein_data[0]]

			tmpDict = {
			"motif": instance_data[3],
			"start": int(instance_data[6]),
			"end": int(instance_data[7]),
			"protein_start": 1,
			"protein_end": len(peptide),
			"length": int(instance_data[8]),
			"match": instance_data[9],
			'variant': instance_data[10],
			"peptide_id": peptide_id,
			"peptide": peptide# + protein['c_flank']#protein_data[0] + "-" + str(int(protein_data[1]) + int(instance_data[6]))  + "-" + str(int(protein_data[2]) + int(instance_data[7]))
			#"peptide": protein['peptide']#protein_data[0] + "-" + str(int(protein_data[1]) + int(instance_data[6]))  + "-" + str(int(protein_data[2]) + int(instance_data[7]))
			}

			if tmpDict not in self.data["motif_instances"][self.data["motif_rank"][instance_data[3]]]:
				self.data["motif_instances"][self.data["motif_rank"][instance_data[3]]].append(tmpDict)

	def grab_all_rank(self):
		peptide_alignments = {}
		for rank in self.data["motifs"]:
			self.options['rank'] = rank
			peptide_alignment = self.grab_rank()
			self.data["motifs"][rank]["peptide_alignments"] = peptide_alignment

	def calculate_clusters(self):
		for rank in self.data["motifs"]:
			self.options['rank'] = rank
			peptide_alignment = self.grab_rank()
			self.data["motifs"][rank]["peptide_alignments"] = peptide_alignment
		
		if self.options['topranks'] != None:
			toprank = int(self.options['topranks'])
		else:
			toprank = len(list(self.data["motifs"].keys()))
		
		try:

			clusters = {}
			clusters_association = {}
					
			rankList = list(self.data["motifs"].keys())
			if len(rankList) == 0: 
				pass
			else:
				if self.options['clustering_type'] == 'pairwise':
					
					if rankList:
						rankList.remove(1)

					clusters_association[1] = 1

					for rank_query in list(self.data["motifs"].keys())[:toprank]:
						removeList = []
						clusters[rank_query] = []

						for rank in rankList:
							overlap = False

							for pos_1 in list(self.data["cluster"][rank_query].keys()):
								for pos_2 in list(self.data["cluster"][rank].keys()):
									pos_1_set = set(self.data["cluster"][rank_query][pos_1])
									pos_2_set = set(self.data["cluster"][rank][pos_2])
									pos_intersection = set(pos_1_set).intersection(pos_2_set)

								#	print rank_query,rank,float(len(pos_intersection))/len(pos_1_set),float(len(pos_intersection))/len(pos_2_set)
									if float(len(pos_intersection))/len(pos_1_set) > 0.5 or float(len(pos_intersection))/len(pos_2_set) > 0.5:
										removeList.append(rank)

										if rank_query in clusters_association:
											clusters[clusters_association[rank_query]].append(rank)
											clusters_association[rank] = clusters_association[rank_query]
										else:
											clusters_association[rank] = rank_query

										overlap = True

									if overlap == True:
										break

								if overlap == True:
									break

						if len(clusters[rank_query]) == 0:
							del clusters[rank_query]

						for removeRank in removeList:
							rankList.remove(removeRank)

				if self.options['clustering_type'] == 'DBSCAN':
					import numpy as np
					from sklearn.cluster import DBSCAN

					clusters = {}
					distance_matrix = []

					for motif_a in list(self.data["motifs"].keys())[:toprank]:
						distance_matrix_row = []
						for motif_b in list(self.data["motifs"].keys())[:toprank]:
							pos_1_set = set(self.data["cluster"][motif_a][1])
							pos_2_set = set(self.data["cluster"][motif_b][1])
							pos_intersection = set(pos_1_set).intersection(pos_2_set)

							#distance = min(float(len(list(pos_intersection)))/len(pos_1_set),float(len(pos_intersection))/len(pos_2_set))
							distance = max(float(len(list(pos_intersection)))/len(pos_1_set),float(len(pos_intersection))/len(pos_2_set))
							
							distance_matrix_row.append(1 - (distance))

							
						distance_matrix.append(distance_matrix_row)

					dist = np.array(distance_matrix)

					db = DBSCAN(eps=0.5, min_samples=1, metric='precomputed')

					db.fit(dist)
					labels = db.labels_
					no_clusters = len(set(labels)) - (1 if -1 in labels else 0)
					cluster_labels = set(labels)
					#print(labels)

					cluster_index = 0
					for cluster_label in cluster_labels:
						cluster_index += 1
						clusters[int(min(list(np.nonzero(labels == cluster_label)[0])) + 1)] = [int(x + 1) for x in list(np.nonzero(labels == cluster_label)[0])]
						
						#clusters[min(list(np.nonzero(labels == cluster_label)[0])) + 1] = [x + 1 for x in list(np.nonzero(labels == cluster_label)[0])]
						
		except Exception as e:
			print(("#ERROR with clustering:" + str(e)))

		#-----

		self.data["clusters"] = {}
		self.data["cluster_peptides"] = []

		clusters_keys_sorted = list(clusters.keys())
		clusters_keys_sorted.sort()

		clusters_index = 0
		for cluster in clusters_keys_sorted:
			clusters_index += 1
			self.data["clusters"][clusters_index] = {}
			self.data["clusters"][clusters_index]["members"] = clusters[cluster]
			self.data["clusters"][clusters_index]["member_motif"] = [self.data["motifs"][member]["motif"] for member in self.data["clusters"][clusters_index]["members"]]
			self.data["clusters"][clusters_index]["motif"] = self.data["motifs"][cluster]["motif"]
			self.data["clusters"][clusters_index]["sig"] = float(self.data["motifs"][cluster]["sig"])

			self.data["motifs"][clusters_index]["cluster"] = int(cluster)
			self.data["motifs"][clusters_index]["cluster_motif"] = self.data["motifs"][cluster]["motif"]

			for member in clusters[cluster]:
				self.data["motifs"][member]["cluster"] = int(clusters_index)
				self.data["motifs"][member]["cluster_motif"] = self.data["motifs"][cluster]["motif"]

				for instance in self.data["motif_instances"][member]:
					self.data["cluster_peptides"].append(instance['peptide'])

		self.data["outliers"] = list(set(self.data["peptides"]).difference(self.data["cluster_peptides"]))


	def grab_rank(self,runPSSMAlignment=False):
		rank = int(self.options['rank'])

		if rank in self.data["motifs"]:
			slimfinder_peptides = {}
			max_min = [0,0]

			self.data["motifs"][rank]['motif_positions']  = positions_gaps_pattern.findall(self.data["motifs"][rank]['motif'])


			self.data["cluster"][rank] = {}

			for instance in self.data["motif_instances"][rank]:

				accession = instance["peptide_id"]

				if instance["start"] - 1 > max_min[0]:
					max_min[0] = instance["start"] - 1

				if instance["length"] - instance["end"] > max_min[1]:
					max_min[1] = instance["length"] - instance["end"]


				peptide = instance['peptide']

				variant_positions = positions_pattern.findall(instance["variant"])
				offset = 0

				#-----

				for position in range(0,len(variant_positions)):

					if self.options["cluster_type"] == "exact_residues":
						if variant_positions[position][0] == ".":
							offset += len(variant_positions[position])
						else:
							offset += 1

							if 1 not in self.data["cluster"][rank]:
								self.data["cluster"][rank][1] = []

							self.data["cluster"][rank][1].append(str(accession) + "-" + str(instance['start'] + offset))

					if self.options["cluster_type"] == "exact_residues_position":
						if variant_positions[position][0] == ".":
							offset += len(variant_positions[position])
						else:
							offset += 1

							if position not in self.data["cluster"][rank]:
								self.data["cluster"][rank][position] = []

							self.data["cluster"][rank][position].append(str(accession) + "-" + str(instance['start'] + offset))

					if self.options["cluster_type"] == "overlap":
						if 1 not in self.data["cluster"][rank]:
							self.data["cluster"][rank][1] = []

						self.data["cluster"][rank][1].append(str(accession) + "-" + str(instance['start'] + position))

				#pprint.pprint(self.data["cluster"][rank])
				#-----

				if instance["motif"].count("{") > 0:
					try:
						gap_lengths = re.split("[A-Z]",instance['variant'])[1:-1]
						positions = positions_pattern.findall(instance["motif"])

						last = "gap"
						max_gap_lengths = []

						for i in range(0,len(positions)):
							if positions[i][0] == ".":
								if positions[i].count(".") == len(positions[i]):
									max_gap_lengths.append(len(positions[i]))
								else:
									max_gap_lengths.append(int(positions[i][-2]))
								last = "gap"

							else:
								if last == "aa":
									max_gap_lengths.append(0)

								last = "aa"

						gaps_used = 0
						pos = 0
						for i in range(0,len(gap_lengths)):

							gaps_use = max_gap_lengths[i] - len(gap_lengths[i])

							offset = pos + instance['start']
							peptide = self.insert_sequence(peptide,"."*gaps_use,offset)

							pos += max_gap_lengths[i] + 1 #- len(gap_lengths[i])
					except:
						raise

				#-----

				peptideStart = instance['start']
				peptideEnd = instance['end']

				proteinStart = instance['protein_start']
				proteinEnd = instance['protein_end']

				unique_id = str(instance["peptide_id"]) #+ ":" + str(proteinStart) + "-" + str(proteinEnd)

				slimfinder_peptides[unique_id] = {
				"peptide":peptide,
				"start":peptideStart,
				"end":peptideEnd,
				"protein_start":proteinStart,
				"protein_end":proteinEnd,
				"length":instance["length"],
				}

			#--
			#--

			self.aligned_peptide_data = {"motif":self.data["motifs"],"aligned_by_motif":{},"aligned_by_pssm":{}}
			motif_aligned_peptides = {}
			pssm_aligned_peptides = {}

			for peptide_id in slimfinder_peptides:

				self.options["flank_length"] = 5

				#original_peptide = self.data["peptides_details"][peptide_id]
				original_peptide = self.data["peptides_details"][slimfinder_peptides[peptide_id]['peptide']]

				annotated_n_flank = original_peptide['original_n_flank'].rjust(self.options["flank_length"], '-')
				annotated_c_flank =  original_peptide['original_c_flank'].ljust(self.options["flank_length"], '-')

				n_flank = ""
				n_flank_offset = slimfinder_peptides[peptide_id]["start"]-(self.options["flank_length"]+1)
				n_flank_check = False

				c_flank = ""
				c_flank_offset = self.options["flank_length"] - (len(original_peptide['original_peptide']) - slimfinder_peptides[peptide_id]["end"])
				c_flank_check = False

				core_peptide = ""

				if n_flank_offset < 0:
					 n_flank = annotated_n_flank[n_flank_offset:]
					 n_flank_check = True

				if  c_flank_offset  > 0:
					c_flank = annotated_c_flank[:self.options["flank_length"] - (len(original_peptide['original_peptide']) - slimfinder_peptides[peptide_id]["end"])]
					c_flank_check = True

				if  n_flank_check and c_flank_check:
					core_peptide = original_peptide['original_peptide']

				if n_flank_check == False and c_flank_check == True:
					core_peptide = original_peptide['original_peptide'][n_flank_offset:]
				elif n_flank_check == True and c_flank_check == False :
					core_peptide = original_peptide['original_peptide'][:len(original_peptide['original_peptide']) + c_flank_offset]
				elif  n_flank_check == False and c_flank_check == False :
					core_peptide = original_peptide['original_peptide'][n_flank_offset:len(original_peptide['original_peptide']) + c_flank_offset]

				aligned_peptide = n_flank + core_peptide + c_flank


				motif_aligned_peptides[peptide_id] = aligned_peptide

				self.aligned_peptide_data["aligned_by_motif"][original_peptide['original_peptide']] = {}
				self.aligned_peptide_data["aligned_by_motif"][original_peptide['original_peptide']]["aligned_peptide"] = aligned_peptide
				self.aligned_peptide_data["aligned_by_motif"][original_peptide['original_peptide']]["aligned_motif"] = aligned_peptide[self.options["flank_length"]:-self.options["flank_length"]]
				self.aligned_peptide_data["aligned_by_motif"][original_peptide['original_peptide']]["aligned_n_flank"]= aligned_peptide[:self.options["flank_length"]]
				self.aligned_peptide_data["aligned_by_motif"][original_peptide['original_peptide']]["aligned_c_flank"] = aligned_peptide[-self.options["flank_length"]:]
				self.aligned_peptide_data["aligned_by_motif"][original_peptide['original_peptide']]["aligned_motif_start"] = original_peptide['original_start']+slimfinder_peptides[peptide_id]["start"]
				self.aligned_peptide_data["aligned_by_motif"][original_peptide['original_peptide']]["aligned_motif_end"] = self.aligned_peptide_data["aligned_by_motif"][original_peptide['original_peptide']]["aligned_motif_start"]  + len(core_peptide) - 1

			#
			# PSSM PSSM PSSM PSSM PSSM PSSM PSSM
			# PSSM PSSM PSSM PSSM PSSM PSSM PSSM
			#

			if runPSSMAlignment:

				self.aligned_peptide_data["motif_aligned_peptides"] = motif_aligned_peptides

				pssm = psiblastPSSM.psiblastPSSM(self.stripGaps([motif_aligned_peptide.replace("X","-") for motif_aligned_peptide in list(motif_aligned_peptides.values())],0.5))
				pssm.profiles()

				pssm_aligned_peptides = pssm.score_peptides(self.data["peptides"])

				for peptide_id in list(pssm_aligned_peptides.keys()):

					self.aligned_peptide_data["aligned_by_pssm"][peptide_id] = {}

					original_peptide = self.data["peptides_details"][peptide_id]['original_n_flank'] + self.data["peptides_details"][peptide_id]['original_peptide'] + self.data["peptides_details"][peptide_id]['original_c_flank']

					offset = original_peptide.find(pssm_aligned_peptides[peptide_id]['peptide'].replace("-",""))

					original_peptide_start = self.data["peptides_details"][peptide_id]["original_start"] - len(self.data["peptides_details"][peptide_id]['original_n_flank'])
					original_peptide_end = self.data["peptides_details"][peptide_id]["original_end"] + len(self.data["peptides_details"][peptide_id]['original_c_flank']) - 1

					pssm_aligned_peptide = pssm_aligned_peptides[peptide_id]['peptide']

					self.aligned_peptide_data["aligned_by_pssm"][peptide_id]["aligned_peptide"] = pssm_aligned_peptide
					self.aligned_peptide_data["aligned_by_pssm"][peptide_id]["aligned_motif"] = pssm_aligned_peptide[self.options["flank_length"]:-self.options["flank_length"]]

					self.aligned_peptide_data["aligned_by_pssm"][peptide_id]["aligned_n_flank"] = original_peptide.split(pssm_aligned_peptide.strip('-'))[0]
					self.aligned_peptide_data["aligned_by_pssm"][peptide_id]["aligned_c_flank"] = original_peptide.split(pssm_aligned_peptide.strip('-'))[1]
					self.aligned_peptide_data["aligned_by_pssm"][peptide_id]["aligned_motif_start"] = original_peptide_start + offset
					self.aligned_peptide_data["aligned_by_pssm"][peptide_id]["aligned_motif_end"] = self.aligned_peptide_data["aligned_by_pssm"][peptide_id]["aligned_motif_start"]  + len(pssm_aligned_peptide.replace("-","")) - 1
					self.aligned_peptide_data["aligned_by_pssm"][peptide_id]["motif_present"] = re.subn(self.data["motifs"][rank]['motif'], '',pssm_aligned_peptide)[1] > 0
					self.aligned_peptide_data["aligned_by_pssm"][peptide_id]["pssm_score"] =  pssm_aligned_peptides[peptide_id]['score']
					self.aligned_peptide_data["aligned_by_pssm"][peptide_id]["pssm_score_p"] =  pssm_aligned_peptides[peptide_id]['score_p']

					pssm_aligned_peptides[peptide_id] =  pssm_aligned_peptides[peptide_id]['peptide']

				#--
				#--

				aligned_peptides_fasta = ""
				aligned_peptides = []
				sig_aligned_peptides = []
				aligned_peptides_details = {}

				for peptide_id in pssm_aligned_peptides:
					if self.options["require_motif"]:
						if not self.aligned_peptide_data["aligned_by_pssm"][peptide_id]["motif_present"]: continue

					aligned_peptides_fasta += ">Peptide_" + peptide_id + " PSSM_P:" + "%1.3g"%(self.aligned_peptide_data["aligned_by_pssm"][peptide_id]["pssm_score_p"]) + " MOTIF_PRESENT: " + self.data["motifs"][rank]['motif'] + " - " + str(self.aligned_peptide_data["aligned_by_pssm"][peptide_id]["motif_present"]) + "\n"
					aligned_peptides_fasta +=  pssm_aligned_peptides[peptide_id] + "\n"
					aligned_peptides.append(pssm_aligned_peptides[peptide_id])

					if self.aligned_peptide_data["aligned_by_pssm"][peptide_id]["pssm_score_p"] < 0.0001:
						sig_aligned_peptides.append(pssm_aligned_peptides[peptide_id])

					aligned_peptides_details[peptide_id] = pssm_aligned_peptides[peptide_id]

				#--

				self.aligned_peptide_data["fasta"] = aligned_peptides_fasta
				self.aligned_peptide_data["peptides"] = aligned_peptides
				self.aligned_peptide_data["sig_aligned_peptides"] = sig_aligned_peptides
				self.aligned_peptide_data["details"] = aligned_peptides_details


				#PSSM align iterator
				for i in range(0,1):
					pssm = psiblastPSSM.psiblastPSSM(self.stripGaps(self.aligned_peptide_data["sig_aligned_peptides"],0.5))
					pssm.profiles()
					pssm_aligned_peptides = pssm.score_peptides(self.data["peptides"])

					self.aligned_peptide_data["sig_aligned_peptides"] = []
					for peptide_id in list(pssm_aligned_peptides.keys()):

						if self.aligned_peptide_data["aligned_by_pssm"][peptide_id]["pssm_score_p"] < 0.001:
							self.aligned_peptide_data["sig_aligned_peptides"].append(pssm_aligned_peptides[peptide_id]['peptide'])

			#--
			#--

	################################################
	################################################

	def findMotifs(self):

		self.data['motifs'] = {}
		self.data["motif_rank"] = {}
		self.data["motif_instances"] = {}
		self.data["cluster"] = {}

		self.run_slimfinder()

		self.parse_slimfinder_motifs()
		self.parse_slimfinder_instances()
		self.calculate_clusters()

		if self.options['topranks'] != None:
			tmpMotifDict = {}
			sorted_keys = list(self.data['motifs'].keys())
			sorted_keys.sort()

			for rank in sorted_keys[:self.options['topranks']]:
				tmpMotifDict[rank] = self.data['motifs'][rank]

			data = tmpMotifDict
		else:
			data = self.data["motifs"]


		if 1 in self.data["motifs"]:
			return {"status":"Success","data":data}
		else:
			return {"status":"Failed","status_details":"No motif found"}

	################################################
	################################################

	def findClusters(self):

		self.data['motifs'] = {}
		self.data["motif_rank"] = {}
		self.data["motif_instances"] = {}
		self.data["cluster"] = {}

		self.run_slimfinder()

		self.parse_slimfinder_motifs()
		self.parse_slimfinder_instances()
		self.calculate_clusters()

		if 1 in self.data["motifs"]:
			return {"status":"Success","data":{"clusters":self.data["clusters"],"outliers":self.data["outliers"]}}
		else:
			return {"status":"Failed","status_details":"No motif found"}

	################################################
	################################################

	def alignPeptides(self):

		self.data['motifs'] = {}
		self.data["motif_rank"] = {}
		self.data["motif_instances"] = {}
		self.data["cluster"] = {}

		self.run_slimfinder()

		self.parse_slimfinder_motifs()
		self.parse_slimfinder_instances()

		if self.options["task"] == "motif":self.calculate_clusters()
		self.options['rank'] = self.options['output_rank']
		self.grab_rank(runPSSMAlignment=True)

		#print self.aligned_peptide_data["motif_aligned_peptides"]
		#print self.data["motifs"][alignPeptideObj.options["rank"]]['instance_peptides']

		if 1 in self.data["motifs"]:
			return {"status":"Success","data":self.aligned_peptide_data}
		else:
			return {"status":"Failed","status_details":"No motif found"}

	################################################
	################################################

	def stripGaps(self,peptides,gap_proportion):
		columns = {}
		for i in range(0,len(peptides[0])):
			columns[i] = []
			for j in range(0,len(peptides)):
				columns[i].append(peptides[j][i])

		use_columns = []

		for i in range(0,len(columns)):
			if float(columns[i].count("-"))/len(columns[i]) < gap_proportion:
				use_columns.append(i)

		peptides_degapped = []
		for i in range(0,len(peptides)):
			peptides_degapped.append(peptides[i][min(use_columns):max(use_columns)+1])

		return peptides_degapped

	################################################
	################################################

	def make_output(self):

		if self.options["task"] == "motif":
			if self.options["output_type"] == "tdt":
				if self.options["output_file"] != "":
					if self.options["output_file"] == True:
						print(("Output to:",self.paths["slimfinder_results"]))
					else:
						print("Functionality not finished")
				else:
					headers = ['rank',
								'p',
								'sig',
								'expected upcs',
								'instances',
								'proteins',
								'proteins coverage',
								'peptide count',
								'peptide upc count',
								'motif',
								'search instances',
								'search proteins',
								'search proteins coverage',
								'rank',
								'upcs',
								'upcs coverage',
								#'instance_peptides'
								]

					if self.options['run_cluster_peptides']:
						headers = headers + ['cluster_motif']

					out_str = "\t".join(headers) + "\n"

					if self.options['topranks'] == None:self.options['topranks'] = 5
					for motif in list(self.data['motifs'].keys())[:self.options['topranks']]:
						out_str += "\t".join([str(self.data['motifs'][motif][header]) for header in headers]) + "\n"

			elif self.options["output_type"] == "json":
				if self.options["output_file"] != "":
					if self.options["output_file"] == True:
						output_file = os.path.join(self.options["peptide_data_path"], "fasta", self.options["motif_dataset"]  +  ".motif.output.json")
						with open(output_file, 'w') as outfile:
							json.dump(self.data, outfile)
						print(("Output to:",output_file))
					else:
						print("Functionality not finished")
				else:
					pprint.pprint(self.data)
			else:
				print('output_type [json|tdt]')

		elif self.options["task"] == "align":
			if self.options["output_type"] == "fasta":
				if self.options["output_file"] != "":
					if self.options["output_file"] == True:
						output_file = os.path.join(self.options["peptide_data_path"], "fasta", self.options["motif_dataset"]  +  ".align.output.fasta")
						print(("Output to:",output_file))
						open(output_file,"w").write(self.aligned_peptide_data["fasta"] )
					else:
						open(self.options["output_file"],"w").write(self.aligned_peptide_data["fasta"] )
				else:
					print((self.aligned_peptide_data["fasta"]))

			elif self.options["output_type"] == "tdt":
				if self.options["output_file"] != "":
					if self.options["output_file"] == True:
						output_file = os.path.join(self.options["peptide_data_path"], "fasta", self.options["motif_dataset"]  +  ".align.output.tdt")
						print(("Output to:",output_file))
						open(output_file,"w").write("\n".join(self.aligned_peptide_data["sig_aligned_peptides"]))
					else:
						open(self.options["output_file"],"w").write("\n".join(self.aligned_peptide_data["sig_aligned_peptides"]))
				else:
					print("# Sig_PSSM Aligned Peptides")
					print(("\n".join(self.aligned_peptide_data["sig_aligned_peptides"])))

				return self.aligned_peptide_data["peptides"]

			elif self.options["output_type"] == "json":
				if self.options["output_file"] != "":
					if self.options["output_file"] == True:
						output_file = os.path.join(self.options["peptide_data_path"], "fasta", self.options["motif_dataset"]  +  ".align.output.json")
						print(("Output to:",output_file))
						with open(output_file, 'w') as outfile:
							json.dump(self.aligned_peptide_data, outfile)
					else:
						with open(self.options["output_file"], 'w') as outfile:
							json.dump(self.aligned_peptide_data, outfile)
				else:
					print(("\n".join(self.aligned_peptide_data["peptides"])))

				return self.aligned_peptide_data
			else:
				print('output_type [json|fasta|tdt]')

	################################################
	################################################

	def runAlignPeptides(self):

		if self.options['input_file'] == "" and self.options['peptides'] == "":
			return  {"status":"Error","status_details":"Set elm_class or peptides"}
		else:
			try:
				if self.options['input_file'] != "":
					self.parseAlignment()

				if self.options['peptides'] != "":
					self.parse_peptides()

				self.paths["fasta"] = os.path.join(self.options["peptide_data_path"], "fasta", self.options["motif_dataset"]  +  ".fasta")
				self.paths["alignment"] = os.path.join(self.options["peptide_data_path"], "fasta", self.options["motif_dataset"]  + ".slimfinder.aln.fasta")

				if len(self.data["peptides"]) > 1:
					status = self.alignPeptides()
				else:
					status = {"status":"Error","status_details":"Only one peptide"}
				
				if self.options["make_output"]:
					self.make_output()

				return status

			except Exception as e:
				print(e)
				raise
				return {"status":"Error","status_details":str(e)}

	################################################
	################################################

	def runFindMotifs(self):

		if self.options['input_file'] == "" and self.options['peptides'] == "":
			return  {"status":"Error","status_details":"Set elm_class or peptides"}
		else:
			try:
				if self.options['input_file'] != "":
					self.parseAlignment()

				if self.options['peptides'] != "":
					self.parse_peptides()
				
				self.paths["fasta"] = os.path.join(self.options["peptide_data_path"], "fasta", self.options["motif_dataset"]  +  ".fasta")
				self.paths["alignment"] = os.path.join(self.options["peptide_data_path"], "fasta", self.options["motif_dataset"]  + ".slimfinder.aln.fasta")

				if len(self.data["peptides"]) > 1:
					status = self.findMotifs()
				else:
					status = {"status":"Error","status_details":"Only one peptide"}
				
				if self.options["make_output"]:
					self.make_output()

				return status

			except Exception as e:
				print(e)

				print(self.options['slimfinder_instances_results'] )
				raise
				return {"status":"Error","status_details":str(e)}

	################################################
	################################################

	def runFindClusters(self):

		if self.options['input_file'] == "" and self.options['peptides'] == "":
			return  {"status":"Error","status_details":"Set elm_class or peptides"}
		else:
			try:
				if self.options['input_file'] != "":
					self.parseAlignment()

				if self.options['peptides'] != "":
					self.parse_peptides()

				self.paths["fasta"] = os.path.join(self.options["peptide_data_path"], "fasta", self.options["motif_dataset"]  +  ".fasta")
				self.paths["alignment"] = os.path.join(self.options["peptide_data_path"], "fasta", self.options["motif_dataset"]  + ".slimfinder.aln.fasta")

				if len(self.data["peptides"]) > 1:
					status = self.findClusters()
				else:
					status = {"status":"Error","status_details":"Only one peptide"}
				
				if self.options["make_output"]:
					self.make_output()

				return status

			except Exception as e:
				print(e)
				raise
				return {"status":"Error","status_details":str(e)}


	################################################
	################################################

if __name__ == "__main__":

	alignPeptideObj = alignPeptidesBySLiMFinder()
	alignPeptideObj.options.update(option_reader.load_commandline_options(alignPeptideObj.input_options,alignPeptideObj.input_options_help))

	if alignPeptideObj.options["input_file"] != "":
		alignPeptideObj.options["input_files"] = []
		if os.path.isdir(alignPeptideObj.options["input_file"]):
			for input_file in os.listdir(alignPeptideObj.options["input_file"]):
				if input_file[0] != ".":
					alignPeptideObj.options["input_files"].append(os.path.join(alignPeptideObj.options["input_file"],input_file))
		else:
			alignPeptideObj.options["input_files"] = [alignPeptideObj.options["input_file"]]

		for input_file in alignPeptideObj.options["input_files"]:
			print(("#Analysing:",input_file))

			alignPeptideObj.options["peptides"] = ""
			alignPeptideObj.options["input_file"] = input_file
			alignPeptideObj.setup_paths()
			alignPeptideObj.runAlignPeptides()

	elif alignPeptideObj.options["peptides"] != "":
		alignPeptideObj.options["peptides"] = alignPeptideObj.options["peptides"]
		alignPeptideObj.setup_paths()
		alignPeptideObj.runAlignPeptides()

	#------------------------------------------------------------------------------------#
