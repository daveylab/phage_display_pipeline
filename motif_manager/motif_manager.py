import sys,re,os,json,inspect,copy
import gspread,pprint
import dataset, sqlalchemy

from oauth2client.service_account import ServiceAccountCredentials

# Watch https://www.youtube.com/watch?v=7I2s81TsCnc
# pip install gspread
# pip install oauth2client
# pip install pyopenssl

# SHARE WITH viral-motifs@titanium-scope-115610.iam.gserviceaccount.com

file_path = os.path.dirname(inspect.stack()[0][1])

sys.path.append(os.path.join(file_path,"../"))
sys.path.append(os.path.join(file_path,"../downloaders"))
sys.path.append(os.path.join(file_path,"../data_management"))
sys.path.append(os.path.join(file_path,"../utilities"))

import queryManager
import utilities_downloader
import utilities_error

import config_reader
import option_reader

import utilities_google
import utilities_error
import interactionDownloader
import pmidDownloader
import pdbDownloader
import uniprotDownloader
import elmManager
import pfamDownloader

output_tags = [
	"source",
	"instance id",
	"pmid",
	"motif start residue number",
	"motif end residue number",
	"motif sequence",
	'motif protein uniprot accession',
	'motif protein name',
	'motif protein family',
	'motif gene name',
	'motif taxon identifier',
	'motif species scientific',
	'motif species common',
	'motif species',
	'motif taxonomy',
	"domain start residue number",
	"domain end residue number",
	'domain protein uniprot accession',
	'domain protein name',
	'domain protein family',
	'domain gene name',
	'domain pfam accession',
	'domain pfam long names',
	'domain pfam names',
	'domain taxon identifier',
	'domain species scientific',
	'domain species common',
	'domain taxonomy',
	'reference pmid',
	'reference title',
	'reference authors',
	'reference authors short',
	'reference article details',
	'elm class',
	'interaction pdb',
	'task',
	'username',
	'interface type',
	'curation required'
	]

curation_output_tags = [
	"instance id",
	"motif type",
	"task",
	"curator in progress",
	"curator names",
	"pmid",
	"publication title",
	"publication authors",
	"publication details",
	"motif protein uniprot accession",
	"motif gene name",
	"motif protein name",
	"motif protein family",
	"motif protein species",
	"motif start residue number",
	"motif end residue number",
	"motif sequence",
	"domain protein uniprot accession",
	"domain gene name",
	"domain protein name",
	"domain protein family",
	"domain protein species",
	"domain start residue number",
	"domain end residue number",
	"domain pfam name",
	"domain pfam accession",
	"domain polymer type",
	"interaction pdb",
	"interaction kd",
	"elm class",
	"evidence classes",
	"evidence methods",
	"evidence logic",
	"reliabilities",
	"curator's instance assessment",
	"conditional motif",
	"conditional motif description",
	"cell state",
	"other notes",
	"interface type",
	"chain",
]

print_tags = [
	'motif protein uniprot accession',
	'motif start residue number',
	'motif end residue number',
	'motif gene name',
	'motif protein name',
	'motif species scientific',
	'domain protein uniprot accession',
	'domain gene name',
	'domain protein name',
	'domain pfam names',
	'elm class',
	'interaction pdb',
	'interface type',
	'pmid'
	]

formats ={
	'accession':"\A([OPQ][0-9][A-Z0-9]{3}[0-9]|[A-NR-Z][0-9]([A-Z][A-Z0-9]{2}[0-9]){1,2})\-{0,1}[0-9]*\Z",
	'pmid':'[0-9]+',
	'pdb_id':'[A-Z0-9]{4}',
	'pfam_id':'PF[0-9]{5}',
	'integer':'[0-9]+',
	'sequence':'[ACDEFGHIKLNMPQRSTVWY]+'
}

required = {
	'domain protein uniprot accession':{"format":"accession"},
	'domain gene name':{},
	'domain start residue number':{"format":"integer"},
	'domain end residue number':{"format":"integer"},
	"domain pfam accession":{"format":"pfam_id"},
	'motif protein uniprot accession':{"format":"accession"},
	'motif gene name':{},
	'motif start residue number':{"format":"integer"},
	'motif end residue number':{"format":"integer"},
	'motif sequence':{"format":"sequence"},
	'pmid':{"format":"pmid"}
	}



class motifLoader():

	def __init__(self):
		self.options = {
			'data_sources':None,
			'update':False,
			'remake':False,
			'addELM':False,
			'addPDB':False,
			'addIntAct':False,
			'filter_motifs':True,
			"collapse_lists":False,
			"collapse_key_data":True,
			"use_compact_output_tags_list":True,
			'filter_motifs_types':["Complete", "In ELM","Example"]
			}

		self.options.update(config_reader.load_configeration_options())
		self.options.update(option_reader.load_commandline_options(self.options,self.options))

		self.output_tags  = output_tags
		self.print_tags  = print_tags
		self.curation_output_tags = curation_output_tags

		self.curation = {}
		self.annotation= {}
		self.clusters = {}
		self.motif_dataset = []

		if not os.path.exists(os.path.join(self.options["data_path"],'sqlite',"motifs")):
			os.makedirs(os.path.join(self.options["data_path"],'sqlite',"motifs"))

		self.read_users()

	############################################################################

	def read_users(self):

		try:
			url = "http://localhost/users/rest/users/get/"
			out_path = os.path.join(self.options["data_path"], "webservices","users.json")

			sessionDownloaderObj = utilities_downloader.sessionDownloader()
			status = sessionDownloaderObj.download_file(url,out_path,JSON=True,replace_empty=True)

			self.users = {}
			with open(out_path, "r") as data_file:
				self.users = json.load( data_file)
		except:
			self.users = {}

	############################################################################
	############################################################################
	############################################################################

	def load_pdb_data(self):
		pdb_motif_dataset = []
		motifs_output_path = os.path.join(self.options["data_path"],"interfaces","motifs")


		if not os.path.exists(motifs_output_path):
			os.makedirs(motifs_output_path)

		for file in os.listdir(motifs_output_path):
			if file.split(".")[-2] == "curation" and file.split(".")[-1] == "json":

				with open(os.path.join(motifs_output_path,file)) as data_file:
					try:
						motifs = json.load(data_file)

						if len(motifs) > 0:
							if 'status' in motifs:
								#if motifs['data'][0]['domain protein uniprot accession']  not in ['Q03164'] and  motifs['data'][0]['motif protein uniprot accession']  not in ['Q03164']:
								#	continue
								#else:
								#	pprint.pprint(motifs['data'])

								if motifs['status'] == "Success":
									if len(motifs['data']) > 0:
										#print(motifs)

										#print len(pdb_motif_dataset), motifs['data'][0]['interface type']
										instances = motifs['data']

										for instance in instances:
											instance["instance id"] = "PDB_" + instance['chain'].replace(".","_")
											instance["source_type"] = "structure_mining"
											instance["curation required"] = "curator_check"

											print("#Adding PDB chain",instance['chain'])
											#if instance["domain protein uniprot accession"] == "P30307" or  instance["motif protein uniprot accession"] == "P30307":
											pdb_motif_dataset.append(instance)

						#if len(pdb_motif_dataset) > 100:
						#	break
					except:
						#if self.options['verbose']:print("Cannot read",file)
						print("rm " + os.path.join(motifs_output_path,file))
						#raise

		return pdb_motif_dataset

	############################################################################
	############################################################################
	############################################################################

	def load_intact_data(self):

		intact_rules = {
			"pmid":{
				"htp": [
					"31585087",
					"24728074",
					"17474147",
					"16606443",
					"30126976"
				],
				"motif_class":
					{
						"31585087":"Unknown phospho-Y",
						"24728074":"LIG_SH2",
						"17474147":"LIG_SH3",
						"16606443":"LIG_WW",
						"30126976":"LIG_PDZ"
					},
				"domain":{
						"24728074":["PF00017"],
						"17474147":["PF00018"],
						"16606443":["PF00397"],
						"30126976":["PF00595"]
					},
				"skip":[
					"20195357",
					"18614564",
					"26514267"
					]
				}
			}

		#-----#

		intact_motif_dataset = []

		motifs_output_path = os.path.join(self.options["data_path"],"interfaces","motifs")

		queryManagerObj = queryManager.queryManager()
		queryManagerObj.options['dataset_type'] = "interaction"
		queryManagerObj.options['task'] = "get_intact_interfaces"

		queryManagerObj.options['verbose'] = False

		interfaces = queryManagerObj.main()
		interface_ids = list(interfaces['data'].keys())

		counter = 0
		for interface_id in interface_ids:
			for interface in interfaces['data'][interface_id]:

				bait_length = 10000
				prey_length = 10000

				#----------------------------------------------------------------------------------#

				if interface['pmid'] in intact_rules["pmid"]["skip"]:
					if self.options['verbose']:print("Skipping:",interface['pmid'])
					continue

				if interface['pmid'] in intact_rules["pmid"]["htp"]:
					interface["htp"] = True

				#----------------------------------------------------------------------------------#

				if interface['bait_start'] != "" and interface['bait_end'] != "":
					bait_length = int(interface['bait_end']) - int(interface['bait_start'])

				if interface['prey_start'] != "" and interface['prey_end'] != "":
					prey_length = int(interface['prey_end']) - int(interface['prey_start'])

				if bait_length < 40 or prey_length < 40:
					if bait_length < 40:
						motif_side = "bait"
						domain_side = "prey"

					if prey_length < 40:
						motif_side = "prey"
						domain_side = "bait"

					interface_formatted = {
					"source":"intact",
					"instance id":"INTACT_" + interface_id,
					"pmid":interface['pmid'],
					"motif start residue number":interface[motif_side + '_start'],
					"motif end residue number":interface[motif_side + '_end'],
					"motif sequence":"",
					'motif protein uniprot accession':interface[motif_side + '_accession'],
					'motif protein name':interface[motif_side + '_protein_name'],
					'motif gene name':interface[motif_side + '_gene_name'],
					'motif taxon identifier':interface[motif_side + '_taxon_id'],
					"domain start residue number":interface[domain_side + '_start'],
					"domain end residue number":interface[domain_side + '_end'],
					'domain protein uniprot accession':interface[domain_side + '_accession'],
					'domain protein name':interface[domain_side + '_protein_name'],
					'domain gene name':interface[domain_side + '_gene_name'],
					'domain pfam accession':interface[domain_side + '_region_domains_accessions'],
					'domain pfam long names':"",
					'domain pfam names':interface[domain_side + '_region_domains_ids'],
					'domain taxon identifier':interface[domain_side + '_taxon_id'],
					'evidence methods':interface['method'],
					'username':'ndavey',
					'source_type':'database_mining',
					'confidence':interface['confidence'],
					'curation required':'curator_check',
					'interface type':'uncertain'
					}

					if interface['pmid'] in intact_rules["pmid"]["motif_class"]:
						if interface['pmid'] in intact_rules["pmid"]["motif_class"]:
							if self.options['verbose']:print("Adding:",interface['pmid'],intact_rules["pmid"]["motif_class"][interface['pmid']])
							interface["elm class"] = intact_rules["pmid"]["motif_class"][interface['pmid']]

					if interface['pmid'] in intact_rules["pmid"]["domain"]:
						if interface['pmid'] in intact_rules["pmid"]["domain"]:
							if self.options['verbose']:print("Adding:",interface['pmid'],intact_rules["pmid"]["domain"][interface['pmid']])
							interface["domain pfam accession"] = intact_rules["pmid"]["domain"][interface['pmid']]

				#	if interface[domain_side + '_accession'] == "P30307" or  interface[motif_side + '_accession'] == "P30307":
					intact_motif_dataset.append(interface_formatted)

					#if len(intact_motif_dataset) > 1000:
					#	break

		return intact_motif_dataset

	############################################################################
	############################################################################
	############################################################################

	def load_elm_data(self):
		sys.argv = sys.argv[:1]

		elmManagerObj = elmManager.elmManager()

		elmManagerObj.options['task'] = 'get_elm_instances'
		elmManagerObj.options['format'] ='curation'
		elmManagerObj.options['flanks'] = 0
		elm_instances = elmManagerObj.main()

		print("#ELM loaded")

		instances = []
		identifier_list = {}
		# print("elm instances", elm_instances)

		for instance in elm_instances["data"]:
			if instance['source'] == "ELM":
				instance["instance id"] = instance['source_id'] + "_" + instance['pmid']

				if instance['source_id'] not in identifier_list:
					identifier_list[instance['source_id']] = []

				source_id_index = len(identifier_list[instance['source_id']]) + 1
				source_identifier = instance['source_id'] + "." + str(source_id_index)

				identifier_list[instance['source_id']].append(source_identifier)

				instance["instance id"] = source_identifier

				#if instance["domain protein uniprot accession"] == "P30307" or  instance["motif protein uniprot accession"] == "P30307":
				instances.append(instance)

				#if len(instances) > 100:
				#	break


		return instances

	############################################################################
	############################################################################
	############################################################################

	def read_motif_annotation(self):
		screenPermissionsObj = utilities_google.googlesheetReader()
		screenPermissionsObj.options['update'] = self.options['update']
		screenPermissionsObj.options['table_name'] = self.options['table_name']
		screenPermissionsObj.options['sheet_name'] = self.options['sheet_name']

		screenPermissionsObj.setup_access()
		screenPermissionsObj.get_sheet()

		pprint.pprint(screenPermissionsObj.data)

		self.preprocess_sheet(screenPermissionsObj.data)

	############################################################################

	def preprocess_sheet(self,instances):
		self.curation = []

		for instance in instances:
			instance_row = {}
			for key in instance:
				if isinstance( instance[key],(str)):
					instance_row[key.lower().strip()] = instance[key].strip()
				else:
					instance_row[key.lower().strip()] = instance[key]

			self.curation.append(instance_row)

	############################################################################
	############################################################################
	############################################################################

	def preprocess_instance(self,instance):

		instance["pmid"] = str(instance["pmid"]).strip().replace(" ",",")
		instance["references"] = {}
		instance["domain protein domains"] = []
		instance["domain protein data"] = {}
		instance["motif protein data"] = {}
		instance["motif protein domains"] = []
		instance["domain pfam data"] = {}

		if not isinstance(instance["domain protein uniprot accession"],(list)):
			instance["domain protein uniprot accession"] = instance["domain protein uniprot accession"].split(",")

		if not isinstance(instance["domain pfam accession"],(list)):
			instance["domain pfam accession"] = str(instance["domain pfam accession"]).split(",")

		return instance

	def validate_instance(self,instance):

		data_validation = {}

		uniprotDownloaderObj = uniprotDownloader.uniprotDownloader()
		uniprotDownloaderObj.options["data_path"] = self.options["data_path"]

		#------

		for tag in list(required.keys()):
			if instance[tag] in ["","n/a",111111]:
				data_validation[tag] =  {"status":"incomplete"}

			elif 'format' in required[tag]:
				if isinstance(instance[tag],(list)):
					for val in instance[tag]:
						if not re.match(formats[required[tag]['format']], str(val)):
							data_validation[tag] =  {"status":"incorrect format","status_help": "Required format for " + tag + ": " + str(val) + " is " + str(formats[required[tag]['format']])}

				else:
					if not re.match(formats[required[tag]['format']], str(instance[tag])):
						data_validation[tag] =  {"status":"incorrect format","status_help": "Required format for " + tag + ": " + str(instance[tag]) + " is " + str(formats[required[tag]['format']])}

		#------

		if len(instance["domain protein uniprot accession"]) > 0 and instance["domain protein uniprot accession"] != "n/a":
			for domain_accession in instance["domain protein uniprot accession"]:
				domain_accession = domain_accession.strip()

				if domain_accession not in ['',"NA","N","A"]:
					protein_details = uniprotDownloaderObj.parseBasic(domain_accession)

					try:
						instance["domain protein data"][domain_accession] = protein_details['data']
						instance["domain protein domains"] += uniprotDownloaderObj.parseDomains(domain_accession)['data']
					except:
						print(("####Error",domain_accession))
				else:
					data_validation["domain protein uniprot accession"] = {"status":"Missing data","status_help": "Domain protein uniprot accession" + ",".join(instance["domain protein uniprot accession"]) + ") is not correctly" }

			#------
			### FIX LATTER

			if len(instance["domain pfam accession"]) > 0 and instance["domain pfam accession"] != "":
				for pfam_accession in instance["domain pfam accession"]:
					if "domain protein domains" in instance and "domain protein domains" not in data_validation and "domain protein uniprot accession" not in data_validation:
						if pfam_accession not in instance["domain protein domains"]:
							data_validation["domain pfam accession"] = {
								"status":"incorrect format",
								"status_help": "Selected domain (" + pfam_accession + ") is not in the select domain proteins (" + "|".join(instance["domain protein uniprot accession"]) + ")",
								"binding protein domains": instance["domain protein domains"]
								}
		else:
			data_validation["domain protein uniprot accession"] = {"status":"incomplete","status_help": "Domain protein uniprot accession" + instance["domain protein uniprot accession"] + ") is not set" }

		return data_validation

	def validate_motif_position(self,user_motif_sequence,user_motif_start,user_motif_end,uniprot_protein_sequence):

		uniprot_motif_sequence = uniprot_protein_sequence[int(user_motif_start) - 1:int(user_motif_end)]

		data_validation = {}
		uniprot_motif_start = -1
		uniprot_motif_end = -1

		if uniprot_motif_sequence == user_motif_sequence:
			uniprot_motif_start = user_motif_start
			uniprot_motif_end = user_motif_end
		else:
			protein_bits = [m.start() for m in re.finditer(user_motif_sequence,uniprot_protein_sequence)]

			if len(protein_bits) > 1:
				data_validation["Motif Sequence"] = {"status":"Incorrect peptide start and stop","status_help": user_motif_sequence + " found at multiple positions in sequence:" + ", ".join([str(x) for x in protein_bits])}

			elif len(protein_bits) > 0:
				uniprot_motif_start = int(protein_bits[0]) + 1
				uniprot_motif_end = uniprot_motif_start + len(user_motif_sequence) - 1
				uniprot_motif_sequence = uniprot_protein_sequence[int(uniprot_motif_start) - 1:int(uniprot_motif_end)]

				if uniprot_motif_start != user_motif_start and user_motif_end != uniprot_motif_end:
					data_validation["Motif Sequence"] = {"status":"Incorrect peptide start and stop","status_help": user_motif_sequence + " found at residues" + str(uniprot_motif_start) + " - " + str(uniprot_motif_end)}
				elif uniprot_motif_start != user_motif_start:
					data_validation["Motif Sequence"] = {"status":"Incorrect peptide start","status_help": user_motif_sequence + " found at residues" + str(uniprot_motif_start) + " - " + str(uniprot_motif_end)}
				elif user_motif_end != uniprot_motif_end:
					data_validation["Motif Sequence"] = {"status":"Incorrect peptide end","status_help": user_motif_sequence + " found at residues" + str(uniprot_motif_start) + " - " + str(uniprot_motif_end)}

			else:
				data_validation["Motif Sequence"] = {"status":"incorrect peptide" + user_motif_sequence + " not found in protein sequence defined for motif accession - check sequence and accession"}

		if user_motif_sequence != uniprot_motif_sequence or str(user_motif_start) != str(uniprot_motif_start) or str(user_motif_end) != str(uniprot_motif_end):
			print(user_motif_sequence.strip(),"\t", end=' ')
			print(user_motif_start,"\t", end=' ')
			print(user_motif_end,"\t", end=' ')
			print(user_motif_sequence,"\t", end=' ')
			print(uniprot_motif_sequence,"\t", end=' ')
			print(uniprot_motif_start,"\t", end=' ')
			print(uniprot_motif_end,"\t", end=' ')
			print(user_motif_sequence == uniprot_motif_sequence,"\t", end=' ')
			print(str(user_motif_start) == str(uniprot_motif_start),"\t", end=' ')
			print(str(user_motif_end) == str(uniprot_motif_end))

		return {
			"uniprot_motif_sequence":uniprot_motif_sequence,
			"uniprot_motif_start":uniprot_motif_start,
			"uniprot_motif_end":uniprot_motif_end,
			"data_validation":data_validation
			}

	def get_key_data(self):

		pfamDownloaderObj = pfamDownloader.pfamDownloader()

		uniprotDownloaderObj = uniprotDownloader.uniprotDownloader()
		uniprotDownloaderObj.options["data_path"] = self.options["data_path"]

		pmidDownloaderObj = pmidDownloader.pmidDownloader()
		pmidDownloaderObj.options["data_path"] = self.options["data_path"]

		instance_counter = 2

		for instance in self.curation:
			try:

				if 'task' in instance:
					if instance["task"] not in self.options['filter_motifs_types'] and self.options['filter_motifs']: continue

				#------
				instance = self.preprocess_instance(instance)

				data_validation = self.validate_instance(instance)

				#------

				minimal_data = {}
				protein_data = {}

				#------

				if "htp" not in instance:
					instance["htp"] = False

				if "interface type" in instance:
					if instance["interface type"] == "":
						instance["interface type"] = "SLiM"
				else:
					instance["interface type"] = "SLiM"

				if 'username' in instance:

					username = instance['username']

					user_tags = ['affiliation_address_city',
						'firstname',
						'lastname',
						'affiliation_address_country',
						'affiliation_address_state',
						'affiliation',
						'affiliation_department']

					user_details = {}
					for user_tag in user_tags:
						if instance['username'] in self.users:
							user_details[user_tag] = self.users[username][0][user_tag]
						else:
							user_details[user_tag] = ""
							user_details['firstname'] = username

					#username_affiliation = user_details['firstname'] + ' ' + user_details['lastname']
					#username_affiliation+= " (" + user_details['affiliation'] + "," + user_details['affiliation_address_country'] + ")"
					#instance['username_full'] = username_affiliation
					instance['username'] = {username: user_details}
				else:
					instance['username'] = {}


				"""
				if 'pmid' not in data_validation:
					for pmid in instance["pmid"]:
						if pmid != "":
							instance["references"][pmid]  = pmidDownloaderObj.parsePMID(str(instance["pmid"]),get_proteins=False)['data']
				"""
				#------
				if 'motif protein uniprot accession' not in data_validation:
					motif_accession = instance['motif protein uniprot accession']
					instance["motif protein data"] = uniprotDownloaderObj.parseBasic(motif_accession)['data']
					instance["motif protein domains"] = uniprotDownloaderObj.parseDomains(motif_accession)['data']

				#------

				if 'domain protein uniprot accession' not in data_validation:
					for domain_accession in instance["domain protein uniprot accession"]:
						if domain_accession != "":
							instance["domain protein data"][domain_accession] = uniprotDownloaderObj.parseBasic(domain_accession)['data']
							instance["domain protein domains"] += uniprotDownloaderObj.parseDomains(domain_accession)['data']
				else:
					if instance["domain start residue number"] != "" and instance["domain end residue number"] != "":
						print(data_validation['domain protein uniprot accession'])
						print(instance["domain protein uniprot accession"], instance["domain start residue number"], instance["domain end residue number"])
						print(uniprotDownloaderObj.parseUniProtPfam(domain_accession, instance["domain start residue number"], instance["domain end residue number"]))

				#------
				if len(instance["domain pfam accession"]) > 0:
					for pfam_accession in instance["domain pfam accession"]:
						pfam_instance_data = pfamDownloaderObj.parsePfam(pfam_accession)
						instance["domain pfam data"][pfam_accession] = pfam_instance_data

				instance["curation required"] = data_validation

				if "instance id" not in instance or len(instance["instance id"]) == 0:
					print((' "instance id" not in ' + instance['motif protein uniprot accession'] +  ' ' + instance['pmid']))

				if len(instance["pmid"].split(",")) > 1:
					pmids = instance["pmid"]
					for pmid in pmids.split(","):
						instance["pmid"] = pmid
						#instance["instance id"] = len(self.annotation)
						self.annotation[instance["instance id"]] = copy.deepcopy(instance)
				else:
					#instance["instance id"] = len(self.annotation)
					self.annotation[instance["instance id"]] = instance

			except Exception as e:
				utilities_error.printError()
				pprint.pprint(instance)

		print("Instances:",len(self.annotation))

	def organise_key_data(self):
		for instance in self.annotation:
			try:
				tmp_list = [str(instance)]

				motif_gene_names = []
				motif_protein_names = []
				motif_protein_family = []

				source = self.options['data_source']

				if source in self.data_sources:
					source = self.data_sources[source]['full_name']

				self.annotation[instance]['source'] = source

				if len(self.annotation[instance]["motif protein data"]) > 0:
					if 'gene_name' in self.annotation[instance]["motif protein data"]:
						motif_gene_names.append(self.annotation[instance]["motif protein data"]['gene_name'])
					if 'protein_name' in self.annotation[instance]["motif protein data"]:
						motif_protein_names.append(self.annotation[instance]["motif protein data"]['protein_name'])
					if 'family' in self.annotation[instance]["motif protein data"]:
						motif_protein_family.append(self.annotation[instance]["motif protein data"]['family'])

				domain_gene_names = []
				domain_protein_names = []
				domain_protein_family = []

				if len(self.annotation[instance]["domain protein data"]) > 0:
					for accession in self.annotation[instance]["domain protein data"]:
						if 'gene_name' in self.annotation[instance]["domain protein data"][accession]:
							domain_gene_names.append(self.annotation[instance]["domain protein data"][accession]['gene_name'])
						if 'protein_name' in self.annotation[instance]["domain protein data"][accession]:
							domain_protein_names.append(self.annotation[instance]["domain protein data"][accession]['protein_name'])
						if 'family' in self.annotation[instance]["domain protein data"][accession]:
							domain_protein_family.append(self.annotation[instance]["domain protein data"][accession]['family'])
						if 'species_common' in self.annotation[instance]["domain protein data"][accession]:
							self.annotation[instance]["domain species common"] = self.annotation[instance]["domain protein data"][accession]['species_common']
						if 'species_scientific' in self.annotation[instance]["domain protein data"][accession]:
							self.annotation[instance]["domain species scientific"] = self.annotation[instance]["domain protein data"][accession]['species_scientific']
						if 'taxonomy' in self.annotation[instance]["domain protein data"][accession]:
							self.annotation[instance]["domain taxonomy"] = self.annotation[instance]["domain protein data"][accession]['taxonomy']
						if 'taxon_id' in self.annotation[instance]["domain protein data"][accession]:
							self.annotation[instance]["domain taxon identifier"] = self.annotation[instance]["domain protein data"][accession]['taxon_id']

				self.annotation[instance]["domain pfam accessions"] = list(self.annotation[instance]["domain pfam data"].keys())
				self.annotation[instance]["domain pfam names"] = []
				self.annotation[instance]["domain pfam long names"] = []

				for pfam_id in list(self.annotation[instance]["domain pfam data"].keys()):
					if 'data' in self.annotation[instance]["domain pfam data"][pfam_id]:
						self.annotation[instance]["domain pfam names"].append(self.annotation[instance]["domain pfam data"][pfam_id]['data']['pfam_acc'])
						self.annotation[instance]["domain pfam long names"].append(self.annotation[instance]["domain pfam data"][pfam_id]['data']['domain_name'])

				try:
					if 'sequence' in self.annotation[instance]["motif protein data"]:
						user_motif_sequence = self.annotation[instance]["motif sequence"].strip()
						uniprot_protein_sequence = self.annotation[instance]["motif protein data"]['sequence']
						user_motif_start = self.annotation[instance]["motif start residue number"]
						user_motif_end = self.annotation[instance]["motif end residue number"]

						if user_motif_sequence == "":
							self.annotation[instance]["motif sequence"] = uniprot_protein_sequence[int(user_motif_start)- 1:int(user_motif_end)]
							user_motif_sequence = self.annotation[instance]["motif sequence"].strip()

						uniprot_protein_sequence = self.annotation[instance]["motif protein data"]['sequence']
						user_motif_start = self.annotation[instance]["motif start residue number"]
						user_motif_end = self.annotation[instance]["motif end residue number"]

						validate_motif_position = self.validate_motif_position(user_motif_sequence,user_motif_start,user_motif_end,uniprot_protein_sequence)

						self.annotation[instance]["uniprot_motif_sequence"] = validate_motif_position['uniprot_motif_sequence']
						self.annotation[instance]["uniprot_motif_start"] = validate_motif_position['uniprot_motif_start']
						self.annotation[instance]["uniprot_motif_end"] = validate_motif_position['uniprot_motif_end']
						self.annotation[instance]["curation required"].update(validate_motif_position['data_validation'])


				except Exception as e:
					print('#ERROR validate_motif_position',e)


				if self.options["collapse_lists"]:
					self.annotation[instance]["motif gene name"] = "|".join(motif_gene_names)
					self.annotation[instance]["motif protein name"] = "|".join(motif_protein_names)
					self.annotation[instance]["motif protein family"] = "|".join(motif_protein_family)

					self.annotation[instance]["domain gene name"] = "|".join(domain_gene_names)
					self.annotation[instance]["domain protein name"] = "|".join(domain_protein_names)
					self.annotation[instance]["domain protein family"] = "|".join(domain_protein_family)
				else:
					self.annotation[instance]["motif gene name"] = motif_gene_names
					self.annotation[instance]["motif protein name"] = motif_protein_names
					self.annotation[instance]["motif protein family"] = motif_protein_family

					self.annotation[instance]["domain gene name"] = domain_gene_names
					self.annotation[instance]["domain protein name"] = domain_protein_names
					self.annotation[instance]["domain protein family"] = domain_protein_family

				if len(list(self.annotation[instance]["motif protein data"].keys())) > 0:

					if 'species_common' in self.annotation[instance]["motif protein data"]:
						self.annotation[instance]["motif species common"] = self.annotation[instance]["motif protein data"]['species_common']

					if 'species_scientific' in self.annotation[instance]["motif protein data"]:
						self.annotation[instance]["motif species scientific"] = self.annotation[instance]["motif protein data"]['species_scientific']

					if 'taxonomy' in self.annotation[instance]["motif protein data"]:
						self.annotation[instance]["motif taxonomy"] = self.annotation[instance]["motif protein data"]['taxonomy']
						self.annotation[instance]["motif species"] = self.annotation[instance]["motif protein data"]['taxonomy'].split("|")[-1]

					if 'taxon_id' in self.annotation[instance]["motif protein data"]:
						self.annotation[instance]["motif taxon identifier"] = self.annotation[instance]["motif protein data"]['taxon_id']

				"""
				reference = self.annotation[instance]["references"].values()
				print reference

				self.annotation[instance]["reference pmid"] = reference[0]['pmid']
				self.annotation[instance]["reference title"] = reference[0]['title']
				self.annotation[instance]["reference authors"] = reference[0]['long_author']
				self.annotation[instance]["reference authors short"] = reference[0]['short_author']
				self.annotation[instance]["reference article details"] = reference[0]['journal']  + reference[0]['article_details'] + " (" + reference[0]['article_year'] + ")"
				"""
				cleaned_instance = {}

				if self.options['use_compact_output_tags_list']:
					use_output_tags = output_tags
				else:
					use_output_tags = self.annotation[instance].keys()

				for tag in use_output_tags:
					if tag not in self.annotation[instance]:
						cleaned_instance[tag] = ""
					else:
						try:
							cleaned_instance[tag] = self.annotation[instance][tag].strip()
						except:
							cleaned_instance[tag] = self.annotation[instance][tag]

				pprint.pprint(cleaned_instance)
				self.annotation[instance] = cleaned_instance

			except Exception as e:
				print("####ERROR",instance,e)

	############################################################################

	def get_data(self):
		self.options["curation_path"] = os.path.join(self.options["data_path"],'sqlite',"motifs",self.options['sheet_name'].lower().replace(" ","_") + '.curation.json')

		print("#read_motif_annotation")
		self.read_motif_annotation()

		with open(self.options["curation_path"], "w") as data_file:
			json.dump(self.curation, data_file)

		return "Data Updated"

	############################################################################
	def collapse_key_data(self):

		residues = {}
		self.clusters = {
			'overlap':{
				'cluster_members':{},
				'cluster_map':{}
				},
			'domain':{
				'cluster_members':{},
				'cluster_map':{}
				},
			'domain_region':{
				'cluster_members':{},
				'cluster_map':{}
				},
			'elm_class':{
				'cluster_members':{},
				'cluster_map':{}
				},
			'protein':{
				'cluster_members':{},
				'cluster_map':{}
				},
			'any_binding':{
				'cluster_members':{},
				'cluster_map':{}
				},
			'any':{
				'cluster_members':{},
				'cluster_map':{}
				}
		}

		motif_instance_ids = list(self.annotation.keys())
		motif_instance_ids.sort()

		for motif_instance_id in motif_instance_ids:
			try:
				if 'motif start residue number' in self.annotation[motif_instance_id]['curation required']: continue
				if 'motif end residue number' in self.annotation[motif_instance_id]['curation required']: continue
				if 'motif protein uniprot accession' in self.annotation[motif_instance_id]['curation required']: continue

				motif_protein_uniprot_accession = self.annotation[motif_instance_id]['motif protein uniprot accession']
				motif_start_residue_number = int(self.annotation[motif_instance_id]['motif start residue number'])
				motif_end_residue_number = int(self.annotation[motif_instance_id]['motif end residue number'])

				motif_residues = list(range(motif_start_residue_number,motif_end_residue_number))
				motif_cluster_member = None

				motif_cluster_member = {
					'overlap':None,
					'domain':None,
					'domain_region':None,
					'elm_class':None,
					'protein':None,
					'any_binding':None,
					'any':None
					}

				if motif_protein_uniprot_accession in residues:
					compare_motif_instance_ids = list(residues[motif_protein_uniprot_accession].keys())
					compare_motif_instance_ids.sort()

					for compare_motif_instance_id in compare_motif_instance_ids:

						shared_binding_protein = False
						shared_binding_domain = False
						shared_elm_class = False
						overlapping_binding_domain = False
						overlapping_region = False

						motif_residues = motif_residues
						compare_motif_residues = residues[motif_protein_uniprot_accession][compare_motif_instance_id]
						overlap = list(set(motif_residues).intersection(compare_motif_residues))

						if len(overlap) > 0:
							overlapping_binding_domain_length = 0
							overlapping_region = True

							###----------------------###

							self.annotation[motif_instance_id]['domain protein uniprot accession'] = [x for x in self.annotation[motif_instance_id]['domain protein uniprot accession'] if x]
							self.annotation[compare_motif_instance_id]['domain protein uniprot accession'] = [x for x in self.annotation[compare_motif_instance_id]['domain protein uniprot accession'] if x]
							self.annotation[motif_instance_id]['domain pfam accession'] = [x for x in self.annotation[motif_instance_id]['domain pfam accession'] if x]
							self.annotation[compare_motif_instance_id]['domain pfam accession'] = [x for x in self.annotation[compare_motif_instance_id]['domain pfam accession'] if x]

							if isinstance(self.annotation[motif_instance_id]['elm class'],(list)):
								self.annotation[motif_instance_id]['elm class'] = [x for x in self.annotation[motif_instance_id]['elm class'] if x]

							if isinstance(self.annotation[compare_motif_instance_id]['elm class'],(list)):
								self.annotation[compare_motif_instance_id]['elm class'] = [x for x in self.annotation[compare_motif_instance_id]['elm class'] if x]

							if len(self.annotation[motif_instance_id]['domain protein uniprot accession']) != 0 and len(self.annotation[compare_motif_instance_id]['domain protein uniprot accession']) != 0:
								if self.annotation[motif_instance_id]['domain protein uniprot accession'] == self.annotation[compare_motif_instance_id]['domain protein uniprot accession']:
									shared_binding_protein = True
									binding_region_data_available = True

									if self.annotation[motif_instance_id]['domain start residue number'] == "": binding_region_data_available = False
									if self.annotation[motif_instance_id]['domain end residue number'] == "": binding_region_data_available = False
									if self.annotation[compare_motif_instance_id]['domain start residue number'] == "": binding_region_data_available = False
									if self.annotation[compare_motif_instance_id]['domain end residue number'] == "": binding_region_data_available = False

									if binding_region_data_available:
										domain_overlap = []
										try:
											motif_instance_domain_region = list(range(int(self.annotation[motif_instance_id]['domain start residue number']),int(self.annotation[motif_instance_id]['domain end residue number'])))
											compare_motif_instance_domain_region = list(range(int(self.annotation[compare_motif_instance_id]['domain start residue number']),int(self.annotation[compare_motif_instance_id]['domain end residue number'] )))
											domain_overlap = list(set(motif_instance_domain_region).intersection(compare_motif_instance_domain_region))
										except:
											pass

										if len(domain_overlap) > 0:
											overlapping_binding_domain = True
											overlapping_binding_domain_length = len(domain_overlap)

							###----------------------###

							if len(self.annotation[motif_instance_id]['domain protein uniprot accession']) != 0 and len(self.annotation[compare_motif_instance_id]['domain protein uniprot accession']) != 0:
								if self.annotation[motif_instance_id]['domain protein uniprot accession'] == self.annotation[compare_motif_instance_id]['domain protein uniprot accession']:
									shared_binding_protein = True

							if len(self.annotation[motif_instance_id]['domain pfam accession']) != 0 and len(self.annotation[compare_motif_instance_id]['domain pfam accession']) != 0:
								if self.annotation[motif_instance_id]['domain pfam accession'] == self.annotation[compare_motif_instance_id]['domain pfam accession']:
									shared_binding_domain = True

							if len(self.annotation[motif_instance_id]['elm class']) != 0 and len(self.annotation[compare_motif_instance_id]['elm class']) != 0:
								if self.annotation[motif_instance_id]['elm class'] == self.annotation[compare_motif_instance_id]['elm class']:
									shared_elm_class = True

							###----------------------###

							#print("\t".join([str(x) for x in [overlapping_region, shared_binding_protein, shared_binding_domain, shared_elm_class, overlapping_binding_domain]]))

							if overlapping_region:
								motif_cluster_member['overlap'] = self.clusters['overlap']['cluster_map'][compare_motif_instance_id]

							if  shared_binding_domain:
								motif_cluster_member['domain'] = self.clusters['domain']['cluster_map'][compare_motif_instance_id]

							if  overlapping_binding_domain:
								motif_cluster_member['domain_region'] = self.clusters['domain_region']['cluster_map'][compare_motif_instance_id]

							if shared_elm_class:
								motif_cluster_member['elm_class'] = self.clusters['elm_class']['cluster_map'][compare_motif_instance_id]

							if shared_binding_protein:
								motif_cluster_member['protein'] = self.clusters['protein']['cluster_map'][compare_motif_instance_id]

							if shared_binding_protein or shared_binding_domain or shared_elm_class or overlapping_binding_domain:
								motif_cluster_member['any_binding'] = self.clusters['any_binding']['cluster_map'][compare_motif_instance_id]

							if overlapping_region or shared_binding_protein or shared_binding_domain or shared_elm_class or overlapping_binding_domain:
								motif_cluster_member['any'] = self.clusters['any']['cluster_map'][compare_motif_instance_id]

							###----------------------###

				for cluster_type in motif_cluster_member:
					row = []
					if motif_cluster_member[cluster_type] != None:
						motif_cluster_member_id = motif_cluster_member[cluster_type]

						self.clusters[cluster_type]['cluster_members'][motif_cluster_member_id].append(motif_instance_id)
						self.clusters[cluster_type]['cluster_map'][motif_instance_id] = motif_cluster_member_id
						motif_cluster_members = self.clusters[cluster_type]['cluster_members'][motif_cluster_member_id]

						row = [
						motif_instance_id,
						compare_motif_instance_id,
						motif_cluster_member_id,
						motif_cluster_members,
						motif_protein_uniprot_accession,
						self.annotation[motif_instance_id]['elm class'],
						motif_start_residue_number,
						motif_end_residue_number,
						shared_binding_protein,
						shared_binding_domain,
						shared_elm_class,
						]

					else:
						self.clusters[cluster_type]['cluster_members'][motif_instance_id] = [motif_instance_id]
						self.clusters[cluster_type]['cluster_map'][motif_instance_id] = motif_instance_id

						row = [
						motif_instance_id,
						motif_instance_id,
						self.clusters[cluster_type]['cluster_map'][motif_instance_id] ,
						self.clusters[cluster_type]['cluster_members'][motif_instance_id]
						]

					#if self.options['verbose']:
					#	print "\t".join([str(x) for x in row])

				#######-------------------------------------------------#########

				if motif_protein_uniprot_accession not in residues:
					residues[motif_protein_uniprot_accession] = {}

				residues[motif_protein_uniprot_accession][motif_instance_id] = motif_residues

				#######-------------------------------------------------#########

			except:
				utilities_error.printError()

		######

		view_cluster_type = self.options['merge_by']

		self.merged_annotation = {}

		cluster_ids = list(self.clusters[view_cluster_type]['cluster_members'].keys())
		cluster_ids.sort()

		for cluster in cluster_ids:
			cluster_members = self.clusters[view_cluster_type]['cluster_members'][cluster]
			cluster_members.sort()

			motif_ranges = []
			for cluster_member in cluster_members:
				motif_ranges.append(list(range(int(self.annotation[cluster_member]['motif start residue number']),int(self.annotation[cluster_member]['motif end residue number'])+1)))

			minimal_region = list(set.intersection(*list(map(set,motif_ranges))))

			if len(minimal_region) > 0:
				min_minimal_region = min(minimal_region)
				max_minimal_region = max(minimal_region)
			else:
				min_minimal_region = -1
				max_minimal_region = -1

			merged_instance_tags = [
				'domain start residue number',
				'domain end residue number',
				'domain protein uniprot accession',
				'domain gene name',
				'domain pfam accession',
				'domain pfam long names',
				'domain pfam names',
				'domain protein family',
				'domain protein name',
				'domain species common',
				'domain species scientific',
				'domain taxon identifier',
				'domain taxonomy',
				'interaction pdb',
				'pmid',
				'elm class',
				'username',
				'source',
				'interface type']

			merged_instances = {}
			for merged_instance_tag in merged_instance_tags:
				merged_instances[merged_instance_tag] = []

			for cluster_member in cluster_members:
				"""
				row = [
					cluster,
					cluster_member,
					str(len(cluster_members)),
					self.annotation[cluster_member]['motif protein uniprot accession'],
					self.annotation[cluster_member]['motif gene name'],
					self.annotation[cluster_member]['motif start residue number'],
					self.annotation[cluster_member]['motif end residue number'],
					min_minimal_region,
					max_minimal_region,
					#",".join(self.annotation[cluster_member]['domain pfam accession']),
					self.annotation[cluster_member]['domain start residue number'],
					self.annotation[cluster_member]['domain end residue number'],
					",".join(self.annotation[cluster_member]['domain protein uniprot accession']),
					self.annotation[cluster_member]['domain gene name'],
					self.annotation[cluster_member]['domain protein family'],
					",".join(self.annotation[cluster_member]['domain pfam names']),
					self.annotation[cluster_member]['elm class']
				]
				"""

				merged_instances['motif start residue number'] = min_minimal_region
				merged_instances['motif end residue number'] = max_minimal_region

				for merged_instance_tag in merged_instance_tags:
					if merged_instance_tag in self.annotation[cluster_member]:
						if isinstance(self.annotation[cluster_member][merged_instance_tag],(list)):
							merged_instances[merged_instance_tag] += self.annotation[cluster_member][merged_instance_tag]
						else:
							if self.annotation[cluster_member][merged_instance_tag]:
								merged_instances[merged_instance_tag].append(self.annotation[cluster_member][merged_instance_tag])
					else:
						merged_instances[merged_instance_tag] = []
				#for cluster_type in motif_cluster_member:
				#	row.append(self.clusters[cluster_type]['cluster_map'][cluster_member])

			for merged_instance_tag in merged_instance_tags:
				try:
					if merged_instance_tag == "username":
						merged_instance_tag_dict = {}
						for data_point in merged_instances[merged_instance_tag]:

							for username in data_point:
								merged_instance_tag_dict[username] = data_point[username]

						merged_instances[merged_instance_tag] = merged_instance_tag_dict

					else:
						merged_instances[merged_instance_tag] = list(set(merged_instances[merged_instance_tag]))
				except:
					print("Error @ MERGING")
					print(merged_instances)
					raise

			merged_annotation_instance = copy.deepcopy(self.annotation[cluster_member])
			merged_annotation_instance.update(merged_instances)

			self.merged_annotation[cluster_member] = merged_annotation_instance

			if self.options['verbose']:
				row = []
				for print_tag in print_tags:

					if print_tag in self.merged_annotation[cluster_member]:
						if isinstance(self.merged_annotation[cluster_member][print_tag],(list)):
							row.append(",".join([str(x) for x in self.merged_annotation[cluster_member][print_tag]]))
						else:
							row.append(self.merged_annotation[cluster_member][print_tag])
					else:
						row.append("")

				print("\t".join([str(x) for x in row]))

		self.annotation = self.merged_annotation

		if self.options['verbose']:
			print("cluster_members",len(self.clusters[view_cluster_type]['cluster_members']))

	############################################################################

	def load_remote_data(self):
		self.options["curation_path"] = os.path.join(self.options["data_path"],'sqlite',"motifs",self.options['sheet_name'].lower().replace(" ","_") + '.curation.json')
		self.options["annotation_path"] = os.path.join(self.options["data_path"],'sqlite',"motifs",self.options['sheet_name'].lower().replace(" ","_") + '.annotation.json')

		if not os.path.exists(self.options["annotation_path"]) or self.options['remake'] or self.options['update']:
			self.annotation= {}

			if not os.path.exists(self.options["curation_path"]) or self.options['update']:
				self.get_data()
			else:
				with open(self.options["curation_path"] ) as data_file:
					self.curation = json.load(data_file)

			if self.options['verbose']: print("#process_data")
			self.process_data()

			with open(self.options["annotation_path"], "w") as data_file:
				json.dump(self.annotation, data_file)
		else:
			with open(self.options["annotation_path"] ) as data_file:
				self.annotation = json.load(data_file)

		return "Data Loaded"

	############################################################################

	def process_data(self):
		if self.options['verbose']: print("#get_key_data")
		self.get_key_data()
		if self.options['verbose']: print("#organise_key_data")
		self.organise_key_data()

	############################################################################
	############################################################################
	############################################################################

	def load_data(self):

		with open(os.path.join(os.path.join(file_path, "motif_sources.json"))) as data_file:
			self.data_sources = json.load(data_file)


		if len(self.options['data_sources']) == 0:
			if self.options['verbose']:
				print("data_sources not set")
			sys.exit()

		if self.options['data_sources'][0] == "complete":
			self.options['data_sources'] = list(self.data_sources.keys())

		self.options['data_sources'].sort()
		self.options["motif_dataset_path"] = os.path.join(self.options["data_path"], 'sqlite', "motifs", "_".join(
			self.options['data_sources']).lower() + '.motif_dataset.json')

		self.annotations = {}

		if not os.path.exists(self.options["motif_dataset_path"]) or self.options['remake'] or self.options['update']:
			for dataSource in self.options['data_sources']:
				if self.options['verbose']:
					print((dataSource),self.options['data_sources'])

				self.options['data_source'] = dataSource

				if dataSource == "":
					pass
				elif dataSource == "ELM":
					self.options['filter_motifs'] = False
					self.options['data_source'] = "ELM"
					elm_dataset = self.load_elm_data()
					self.curation = elm_dataset
					self.process_data()
				elif dataSource == "PDB":
					self.options['filter_motifs'] = False
					self.options['data_source'] = "PDB"
					pdb_dataset = self.load_pdb_data()
					self.curation = pdb_dataset
					self.process_data()
				elif dataSource == "IntAct":
					self.options['filter_motifs'] = False
					self.options['data_source'] = "intact"
					intact_dataset = self.load_intact_data()
					self.curation = intact_dataset
					self.process_data()
				elif dataSource == "transcription":
					pass
				else:
					if dataSource in self.data_sources:
						try:
							self.options['table_name'] = self.data_sources[dataSource]['table_name']
							self.options['sheet_name'] = self.data_sources[dataSource]['sheet_name']
							self.options['filter_motifs'] = self.data_sources[dataSource]['filter_motifs']
							self.load_remote_data()
						except:
							print(("ERROR @load_remote_data: ", self.data_sources[dataSource]['table_name'],self.data_sources[dataSource]['sheet_name'],dataSource + " - Table data missing"))
							raise
					else:
						print(("ERROR: " + dataSource + " - Table missing"))

				self.annotations.update(self.annotation)

				print(dataSource,len(self.annotations))
				self.annotation = {}
				self.curation = {}

			if self.options['verbose']: print("#collapse_key_data")

			self.annotation = self.annotations

			if self.options['collapse_key_data']:
				self.collapse_key_data()

			self.motif_dataset += list(self.annotation.values())

			#####
			if self.options['verbose']: print(("Writing: " + self.options["motif_dataset_path"]))
			with open(self.options["motif_dataset_path"], "w") as data_file:
				json.dump(self.motif_dataset, data_file)
		else:
			with open(self.options["motif_dataset_path"]) as data_file:
				self.motif_dataset = json.load(data_file)

	def load_raw_database(self):
		# print("here")
		with open(os.path.join(os.path.join(file_path, "motif_sources.json"))) as data_file:
			self.data_sources = json.load(data_file)


		if len(self.options['data_sources']) == 0:
			if self.options['verbose']:
				print("data_sources not set")
			sys.exit()

		self.options["motif_dataset_path"] = os.path.join(self.options["data_path"], 'sqlite', "motifs", "_".join(
			self.options['data_sources']).lower() + '.motif_dataset.json')
		# print("motif_dataset_path",self.options["motif_dataset_path"])
		with open(self.options["motif_dataset_path"]) as data_file:
			self.motif_dataset = json.load(data_file)
		return self.motif_dataset
