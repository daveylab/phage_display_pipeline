import sys,os,inspect,pprint,json,copy

file_path = os.path.dirname(inspect.stack()[0][1])

sys.path.append(os.path.join(file_path,"../"))
sys.path.append(os.path.abspath(os.path.join(file_path,"../utilities")))

import utilities_error
import utilities_downloader

import config_reader
import option_reader
import motif_manager

#  python3 queryManager.py  --verbose True --task get_instances_by_elm_class --dataset_type motif --database_name complete --elm_class MOD_CDK_SPxK_1 --filter_by "source" --filter_term ELM

class motifQueryManager():
	
	#####------------------------------------------------------------------#####
	
	def __init__(self):
		self.options = {
			"task":"get_instances",
			"print_output":False,
			"print_instance_table":False,
			"data_sources":"",
			"filter_by":"",
			"filter_term":"",
			"filter_terms":"",
			"filter_logic":"",
			"filter_logic_default":"contains",
			"group_by":"",
			"merge_by":"any_binding",
			"data_type":"",
			"statistics_detailed":False,
			"case_sensitive":False,
			'remake':False,
			"collapse_lists":False,
			"protein_type":"both",
			"use_columns":""
			}
			
		##
		## Check motif_sources.json for more details
		##
		
		self.databases = {}
		with open(os.path.join(os.path.join(file_path, "motif_databases.json"))) as data_file:
			self.databases = json.load(data_file)
		
		self.options.update(config_reader.load_configeration_options())
		self.options['data_path'] = option_reader.load_commandline_options(self.options,self.options,purge_commandline=True)['data_path']
			
		motifs_path = os.path.join(self.options["data_path"],"motifs")
		if not os.path.exists(motifs_path):
			os.mkdir(motifs_path)
		
		self.motifLoaderObj = motif_manager.motifLoader()
		self.process_options()

	############################################################################
	############################################################################
	############################################################################

	###
	### MOVE MOVE MOVE
	###
	
	def make_database(self):
		if self.options['verbose']: print("#make_database")
		
		motif_databases_path = os.path.join(self.options["data_path"],"motifs","databases")
		if not os.path.exists(motif_databases_path):
			os.mkdir(motif_databases_path)
			
		self.options["database_path"] = os.path.join(motif_databases_path,self.options["database_name"] + '.database.json')
		
		if os.path.exists(self.options["database_path"]):	
			"Database " + self.options["database_name"] + '.database.json'  + " exists. Overwriting"
				
		if self.options["database_name"] in self.databases:
			database_options = self.databases[self.options["database_name"]]["options"]
			
			self.options.update(database_options)
			self.motifLoaderObj.options.update(database_options)
	
		self.motifLoaderObj.load_data()
		self.motif_dataset = self.motifLoaderObj.motif_dataset

		if self.options['filter_terms'] != "":
			self.filter_instances_by_term_list()
		else:
			self.filter_instances_by_list()
		
		with open(self.options["database_path"], "w") as data_file:
			json.dump(self.motif_dataset, data_file)
			
		if self.options['verbose']: print("Writing:",self.options["database_path"]) 
		status = {"status":"Success","data":self.get_statistics(),"message":"Database " + self.options["database_name"] + '.database.json'  + " created"}
		return status
	
	def load_intact_data(self):
		return self.motifLoaderObj.load_intact_data()

	def load_elm_data(self):
		return self.motifLoaderObj.load_elm_data()

	def load_pdb_data(self):
		return self.motifLoaderObj.load_pdb_data()

	############################################################################
	

	def process_options(self):
		
		split_tags = ["group_by","filter_by","filter_term","filter_logic","data_type","data_sources"]

		for split_tag in split_tags:
			if self.options[split_tag] == "":
				self.options[split_tag] = []
			else:
				self.options[split_tag] = self.options[split_tag].split(",")
		
		self.motifLoaderObj.options.update(self.options)
	
	#####------------------------------------------------------------------#####
		
	def get_index(self):

		group_member_data = {}

		for group in self.options['data_type']:
			group_member_data[group] = []

		for instance in self.motif_dataset:
			for group in self.options['data_type']:
				if instance[group] not in group_member_data[group]:
					group_member_data[group].append(instance[group])

		return group_member_data
	
	#####------------------------------------------------------------------#####
	
	def get_statistics(self):

		group_details = self.get_index()
		
		statistics = {"instances":len(self.motif_dataset)}
		
		for group in self.options['data_type']:
			
			if self.options["statistics_detailed"]:
				statistics[group] = {
					"total":len(group_details[group]),
					"detailed":{}
				}
				
				for sub_group in group_details[group]:
					self.options['group_by'] = group
					sub_group_data = self.group_instances()
					
					
					if isinstance(sub_group,(list)):
						for sub_group_instance in sub_group:
							statistics[group]["detailed"][sub_group_instance] = len(sub_group_data[sub_group_instance])
							
					else:
						statistics[group]["detailed"][sub_group] = len(sub_group_data[sub_group])
			else:
				statistics[group] = {"total":len(group_details[group])}
				
		return statistics
	
	#####------------------------------------------------------------------#####
	
	def group_instances(self):
		
		grouped_motif_data = {}

		for instance in self.motif_dataset:
			if isinstance(instance[self.options['group_by']],(list)):
				for instance_tag in instance[self.options['group_by']]:
					if instance_tag not in grouped_motif_data:
						grouped_motif_data[instance_tag] = []
					
					grouped_motif_data[instance_tag].append(instance) #.append(self.annotation[instance])

			else:
				if instance[self.options['group_by']] not in grouped_motif_data:
					grouped_motif_data[instance[self.options['group_by']]] = []

			
				grouped_motif_data[instance[self.options['group_by']]].append(instance) #.append(self.annotation[instance])

		return grouped_motif_data
	
	#####------------------------------------------------------------------#####
	
	def filter_instances_by_list(self):
		
		filtered_motif_data = []
		if len(self.options["filter_by"]) > 0:
			for i in range(0,len(self.options["filter_by"])):
				filter_by = self.options["filter_by"][i]
				filter_term = self.options["filter_term"][i]
				
				if len(self.options["filter_logic"]) > 0:
					filter_logic = self.options["filter_logic"][i]
				else:
					filter_logic = self.options["filter_logic_default"]
				
				filtered_motif_data += self.filter_instances(filter_by,filter_term,filter_logic,update_motif_dataset=False)
				
			self.motif_dataset = filtered_motif_data

		return {"status":"Success"}

	#####------------------------------------------------------------------#####
	
	def filter_instances_by_term_list(self):
		
		filtered_motif_data = []
		if len(self.options["filter_by"]) > 0:
			for i in range(0,len(self.options["filter_terms"])):
				filter_by = self.options["filter_by"][0]
				filter_term = self.options["filter_terms"][i]
				
				if len(self.options["filter_logic"]) > 0:
					filter_logic = self.options["filter_logic"][i]
				else:
					filter_logic = self.options["filter_logic_default"]
				
				filtered_motif_data += self.filter_instances(filter_by,filter_term,filter_logic,update_motif_dataset=False)
				
			print(filtered_motif_data)
			self.motif_dataset = filtered_motif_data

		return {"status":"Success"}
		
	#####------------------------------------------------------------------#####
	
	def filter_instances(self,filter_by,filter_by_value,filter_type,update_motif_dataset=True):

		filtered_motif_data = []
		
		if isinstance(filter_by_value,(list)):
			filter_by_values = copy.copy(filter_by_value)
		else:
			filter_by_values =  copy.copy([filter_by_value])
		
		if filter_type == "equals" or filter_type == "contains":
			for i in range(0,len(filter_by_values)):
				if self.options['case_sensitive'] == False:
					filter_by_values[i] = str(filter_by_values[i]).lower()
				else:
					filter_by_values[i] = str(filter_by_values[i])
					
		for instance in self.motif_dataset:
			keep = False
			
			for filter_by_value in filter_by_values:
				
				if isinstance(instance[filter_by],(list)):
					filter_by_compares =  instance[filter_by]
				else:
					filter_by_compares = [instance[filter_by]]
					
				for filter_by_compare in filter_by_compares:
					if self.options['case_sensitive'] == False:
						filter_by_compare = str(filter_by_compare).lower()
					else:
						filter_by_compare = str(filter_by_compare)
						
					if filter_type == "equals":
						if filter_by_compare == filter_by_value:
							keep = True
							
					if filter_type == "contains":
						if filter_by_compare.count(filter_by_value) > 0:
							keep = True
						
					if filter_type == "list":
						if filter_by_value in filter_by_compares:
							keep = True
			if keep:
				filtered_motif_data.append(instance)
	
		if update_motif_dataset:
			self.motif_dataset = filtered_motif_data
	
		return filtered_motif_data

	#####------------------------------------------------------------------#####

	def filter_columns(self):

		filtered_motif_data = {}
		
		if len(self.options["use_columns"]) == 0 or self.options["use_columns"] == "":
			return
			
		if not isinstance(self.options["use_columns"],(list)):
			self.options["use_columns"] = self.options["use_columns"].split(",")
			
		for instance in self.motif_dataset:
			keep = False
			
			instance_filtered = {}
			instance_filtered_tag = []
			for use_column in self.options["use_columns"]:
				if use_column in instance:
					if isinstance(instance[use_column],(list)):
						if self.options["collapse_lists"]:
							instance_filtered[use_column] = "|".join(instance[use_column])
						else:
							instance_filtered[use_column] = instance[use_column]

						instance_filtered_tag.append("|".join(instance[use_column]))
					else:
						instance_filtered[use_column] = instance[use_column]
						instance_filtered_tag.append(instance[use_column])
			
			instance_filtered_tag = "_".join(instance_filtered_tag)
			
			if instance_filtered_tag not in filtered_motif_data:
				instance_filtered['count'] = 1
				filtered_motif_data[instance_filtered_tag] = instance_filtered
			else:
				filtered_motif_data[instance_filtered_tag]['count'] += 1
				
		filtered_motif_data_sorted = []
		filtered_motif_data_sorted_keys = list(filtered_motif_data.keys())

		filtered_motif_data_sorted_keys.sort()

		for filtered_motif_data_sorted_key in filtered_motif_data_sorted_keys:
			filtered_motif_data_sorted.append(filtered_motif_data[filtered_motif_data_sorted_key])

		self.motif_dataset = filtered_motif_data_sorted

		return filtered_motif_data
	#####------------------------------------------------------------------#####	
	
	def make_instance_table(self,instances = None):
		counter = 0
		
		if instances == None:
			instances =  self.motif_dataset	
			
		make_instance_table = ["\t".join(self.motifLoaderObj.curation_output_tags)]
		
		for motif in self.motif_dataset:
			counter += 1
			try:
				row = [str(counter)]
				
				for tag in self.motifLoaderObj.curation_output_tags:
					try:
						if isinstance(motif[tag],(dict)):
							row.append("|".join(list(motif[tag].keys())))
						elif isinstance(motif[tag],(list)):
							row.append("|".join(motif[tag]))
						else:
							row.append(str(motif[tag]))
					except:
						row.append("")
				
				make_instance_table.append("\t".join(row))
			except:
				pass
		
		make_instance_table_str = "\n".join(make_instance_table)
		if self.options['verbose']:
			print(make_instance_table_str)

		return make_instance_table_str	
		 
	#####------------------------------------------------------------------#####
		
	def check_query(self):
		
		if len(self.options["filter_by"]) != len(self.options["filter_term"]) and self.options["filter_terms"] == "":
			print(self.options["filter_by"],self.options["filter_term"],len(self.options["filter_by"]), len(self.options["filter_term"]))
			return {"status":"Error","error_type":"filter_by and filter_term length are not equal length"}
		
		if len(self.options["filter_logic"]) > 0:
			if len(self.options["filter_by"]) != len(self.options["filter_logic"]):
				return {"status":"Error","error_type":"filter_by and filter_logic length are not equal length"}
		
		if self.options["task"] == "get_grouped_instances":
			if len(self.options["group_by"]) == 0:
				return {"status":"Error","error_type":"group_by not set for get_grouped_instances"}
		
		if self.options["task"] in ["get_index", "get_statistics"]:
			if len(self.options["data_type"]) == 0:
				return {"status":"Error","error_type":"data_type not set"}
		
		if len(self.options["data_type"]) > 0:
			for data_type in self.options["data_type"]:
				if data_type not in  self.motifLoaderObj.output_tags: return {"status":"Error","error_type":data_type + " data_type does not exist"}

		if len(self.options["filter_by"]) > 0:
			for filter_by in self.options["filter_by"]:
				if filter_by not in  self.motifLoaderObj.output_tags: return {"status":"Error","error_type":filter_by + " filter_by does not exist"}

		if len(self.options["group_by"]) > 0:
			if self.options["group_by"] not in  self.motifLoaderObj.output_tags: return {"status":"Error","error_type":self.options["group_by"] + " group_by does not exist"}

		return {"status":"Success"}
		
	#####------------------------------------------------------------------#####
	
	def get_instances(self):
		return self.motif_dataset
	
	#####------------------------------------------------------------------#####
		
	def load_database(self):
		if self.options['verbose']: print("#load_database")
	
		if "database_name" not in self.options:
			status = {"status":"Error","error_type":"Database [--database_name] not set!"}
			return status
		elif self.options["database_name"] == None:
			status = {"status":"Error","error_type":"Database [--database_name] not set!"}
			return status
		else:
			motif_databases_path = os.path.join(self.options["data_path"],"motifs","databases")
			self.options["database_path"] = os.path.join(motif_databases_path,self.options["database_name"] + '.database.json')
			
			if not os.path.exists(self.options["database_path"]):	
				status = {"status":"Error","error_type":"Database " + self.options["database_name"] + " does not exist - check motif_database.json!"}
				if self.options['verbose']: print(status)
				return status
			else:
				with open(self.options["database_path"]) as data_file:
					self.motif_dataset = json.load(data_file)
				
				if self.options['verbose']: print("#loaded_database",self.options["database_path"])
				status = {"status":"Success"}
				return status
		
	#####------------------------------------------------------------------#####
	
	def load_data(self):
		if self.options["database_name"] != "":
			 status = self.load_database()
			 
			 if status['status'] == "Error":
				 return status
		else:
			 self.motifLoaderObj.load_data()
			 self.motif_dataset = self.motifLoaderObj.motif_dataset
		 
		if self.options['filter_terms'] != "":
			self.filter_instances_by_term_list()
		else:
			self.filter_instances_by_list()

		self.filter_columns()
		 
	#####------------------------------------------------------------------#####
	#####------------------------------------------------------------------#####
	#####------------------------------------------------------------------#####
	
	def get_instances_by(self,filter_by_motif,filter_by_domain,filter_term):
		self.options['filter_by'] = []
		self.options['filter_term'] = []
		
		if self.options['protein_type'] == "motif" or self.options['protein_type'] == "both":
			self.options['filter_by'].append(filter_by_motif)
			self.options['filter_term'].append(filter_term)
		
		if self.options['protein_type'] == "domain" or self.options['protein_type'] == "both":
			self.options['filter_by'].append(filter_by_domain)
			self.options['filter_term'].append(filter_term)
		
		self.filter_instances_by_list()
		
		return self.motif_dataset
	
	#####------------------------------------------------------------------#####	

	def get_instances_by_gene_name(self):
		return self.get_instances_by('motif gene name','domain gene name',self.options['gene_name'])
		
	def get_instances_by_protein_name(self):
		return self.get_instances_by('motif protein name','domain protein name',self.options['protein_name'])
		
	def get_instances_by_accession(self):
		return self.get_instances_by('motif protein uniprot accession','domain protein uniprot accession',self.options['accession'])
		
	def get_instances_by_taxon_id(self):
		return self.get_instances_by('motif taxon identifier','domain taxon identifier',self.options['taxon_id'])
				
	def get_instances_by_protein_family(self):
		return self.get_instances_by('motif protein family','domain protein family',self.options['protein_family'])
		
	def get_instances_by_taxonomy(self):
		return self.get_instances_by('motif taxonomy','domain taxonomy',self.options['taxonomy'])
		
	#####------------------------------------------------------------------#####
	
	def get_instances_by_motif_accession(self):
		self.options['filter_by'] =  ['motif protein uniprot accession']
		self.options['filter_term'] = [self.options['accession']]
		self.filter_instances_by_list()
		
		return  self.motif_dataset

	def get_instances_by_domain_accession(self):
		self.options['filter_by'] =  ['domain protein uniprot accession']
		self.options['filter_term'] = [self.options['accession']]
		self.filter_instances_by_list()
		
		return  self.motif_dataset

	def get_instances_by_motif_protein_family(self):
		self.options['filter_by'] =  ['motif protein family']
		self.options['filter_term'] = [self.options['protein_family']]
		self.filter_instances_by_list()
		
		return  self.motif_dataset

	def get_instances_by_domain_protein_family(self):
		self.options['filter_by'] =  ['domain protein family']
		self.options['filter_term'] = [self.options['protein_family']]
		self.filter_instances_by_list()
		
		return  self.motif_dataset

	def get_instances_by_elm_class(self):
		self.options['filter_by'] =  ['elm class']
		self.options['filter_term'] = [self.options['elm_class']]
		self.filter_instances_by_list()
		
		return  self.motif_dataset
	
	def get_instances_by_username(self):
		self.options['filter_by'] =  ['username']
		self.options['filter_term'] = [self.options['username']]
		self.filter_instances_by_list()
		
		return  self.motif_dataset

	def get_instances_by_pfam_id(self):
	
		self.options['filter_by'] = []
		self.options['filter_term'] = []
		
		self.options['filter_by'].append('domain pfam accession')
		self.options['filter_term'].append(self.options['pfam_id'])
		
		self.filter_instances_by_list()
		
		return self.motif_dataset
		
	def get_instances_by_pmid(self):
		self.options['filter_by'] = []
		self.options['filter_term'] = []
		
		self.options['filter_by'].append('pmid')
		self.options['filter_term'].append(self.options['pmid'])

		self.filter_instances_by_list()
		
		return self.motif_dataset
		
	#####------------------------------------------------------------------#####	

	def get_indexes_by_gene_name(self):
		self.get_instances_by_gene_name()
		return self.get_statistics()
		
	def get_indexes_by_protein_name(self):
		self.get_instances_by_protein_name()
		return self.get_statistics()
		
	def get_indexes_by_accession(self):
		self.get_instances_by_accession()
		return self.get_statistics()
		
	def get_indexes_by_taxon_id(self):
		self.get_instances_by_taxon_id()
		return self.get_statistics()
				
	def get_indexes_by_protein_family(self):
		self.get_instances_by_protein_family()
		return self.get_statistics()
		
	def get_indexes_by_taxonomy(self):
		self.get_instances_by_taxonomy()
		return self.get_statistics()
	
	def get_indexes_by_elm_class(self):
		self.get_instances_by_elm_class()
		return self.get_statistics()
		
	def get_indexes_by_pfam_id(self):
		self.get_instances_by_pfam_id()
		return self.get_statistics()
		
	def get_indexes_by_pmid(self):
		self.get_instances_by_pmid()
		return self.get_statistics()
		
	#####------------------------------------------------------------------#####
	
	def get_motif_protein_index(self):
		self.options['use_columns'] = ['motif protein uniprot accession','motif protein name','motif gene name',"motif species common","motif species scientific"]
		self.filter_columns()
		return self.motif_dataset
		
	def get_domain_protein_index(self):
		self.options['use_columns'] = ['domain protein uniprot accession','domain protein name','domain gene name',"domain species common","domain species scientific"]
		self.filter_columns()
		return self.motif_dataset

	def get_species_index(self):
		self.options['use_columns'] = ["motif species common","motif species scientific"]
		self.filter_columns()
		return self.motif_dataset

	def get_family_index(self):
		self.options['data_type'] = ["motif protein family","domain protein family"]
		self.options['statistics_detailed'] = True
		statistics = self.get_statistics()
		
		merged_dataset = {}
		for data_type in self.options['data_type']:
			
			for family in statistics[data_type]['detailed']:
				if family == "": continue

				if family not in merged_dataset:
					merged_dataset[family] = {
						"protein family":family,
						"motif protein family":0,
						"domain protein family":0
						} 
				
				merged_dataset[family][data_type] = statistics[data_type]['detailed'][family]

		return list(merged_dataset.values())
		
	#####------------------------------------------------------------------#####
	#####------------------------------------------------------------------#####
	#####------------------------------------------------------------------#####
