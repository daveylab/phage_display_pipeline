# ProP-PD Data Analysis Pipeline
### Code for the analysis tools for the ProP-PD data

#### The major functionalities of the pipeline are:
- Read NGS peptide count files
- Read NGS selection information files
- Define screen data from library, bait, replicate, selection and condition information
- Create screen metadata
- Create crapome of promiscuous peptides
- Map peptides to human proteome using the PepTools server (see http://slim.icr.ac.uk/tools/peptools/)
- Annotate peptides using the PepTools server (mutations, PTMs, etc - see http://slim.icr.ac.uk/tools/peptools/help#instancesAnnotations)
- Annotate peptides with ELM consensuses, curated motifs, bait interaction data and bait shared significant ontology
- Set peptides quality checks
- Define the replicated, overlapping, high NGS count and significant specificity score peptides
- Define the peptide consensus confidence level
- Create outputs 

#### Data Management: 
The ProP-PD Data Analysis Pipeline downloads and produces a large quantity of data during the analysis of the the ProP-PD sccreens. The downloaded data will be stored in a data structure in a directory defined by the data_path parameter. 

- Downloaded and processed data will be stored a defined data structure in the directory defined by *--data_path*

##### Required Input Data:
- The input datasets, ProP-PD peptide count and screen info files, **must** be stored at:
	```
	[--data_path]/phage_display_data/peptide_counts/
	```
- The elm_screen_mapping.tdt **must** be stored at:
	```
	[--data_path]/phage_display_data/curation/
	```
##### Output Data:
- ProP-PD processed data will be stored at:
	```
	[--data_path]/phage_display_data/
	```
- All external data is retrieved from data source APIs

*NB:* *--data_path* parameter needs to be set in the *../config_local.ini* file

#### Commandline: 
The ProP-PD Data Analysis Pipeline can be run as follows:

- Process all data. Processes all peptide count data in the peptide counts directory *[--data_path]/phage_display_data/peptide_counts/*

	```
	python3 phageAnalysisPipeline.py --verbose True
	```

- Process a specific dataset(s). Can be refered to by bait identifier, domain name, gene name or UniProt accession

	```
	python3 phageAnalysisPipeline.py --bait OXSR1___O95747_434_527 --verbose True
	```

- Process a specific library

	```
	python3 phageAnalysisPipeline.py --library HD2 --verbose True
	```

- Process a specific subset of files

	```
	python3 phageAnalysisPipeline.py --analysis_bait_list_file [bait_list_file_path] --verbose True
	```

	The bait list file path is formatted like:
	
	```
	ANKRA2_Ank__Q9H9E1_142_313	HD2,p3_HD2		
	KPNA4_Arm__O00629_68_487	HD2,p3_HD2		
	AP2B1_adaptin__P63010_700_937	HD2,p3_HD2		
	CALM1_EF-hand__P0DP23_2_149	HD2,p3_HD2		
	```

#### Options:

###### Dataset	
**```--library  [STRING]:```** *library identifier of library for processing*

**```--bait  [STRING]:```**  *bait identifier of bait for processing*

**```--analysis_bait_list_file [PATH]:```** *File containing the library and baits for processing*

###### View Data
**```--show_screens BOOL [False]:```** *Show the screens that are available as one line per screen*

**```--show_screens_collapse BOOL [False]```** *Collapse the available screens to one line per bait [True]*

###### Tasks	
**```--create_library_metadata BOOL [False]:```** *Run functions to create library metadata data*

**```--run_create_crapome BOOL [False]:```** *Run functions to create library crapome of peptide promiscuity data*

**```--run_library_screens BOOL [False]:```** *Run the whole library NGS sequencing datasets*

**```--run_peptools BOOL [True]:```** *Run PepTools to annotate screen peptides*

**```--run_slimfinder BOOL [True]:```** *Run SLiMFinder to find enriched specificity determinants in the screen peptides*

**```--slimfinder_only BOOL [False]:```** *Run SLiMFinder Only*

**```--run_create_protein_output BOOL [False]:```** *Run functions to create protein-centric data*

###### Paths					
**```--blast_path [PATH]```** *Path to the blast library bin folder (e.g. /home/tools/blast/bin/)*

**```--blastplus_path [PATH]```** *Path to the blast+ library bin folder (e.g. /home/tools/blast+/bin/)*

**```--clustalw_path [PATH]```** *Path to the clustalw executable*

**```--slimfinder_path [PATH]```** *Path to the SLiMFinder python script*

**```--slimsuite_path [PATH]```** *Path to the SLiMSuite directory*

###### Data Management	
**```--data_path [PATH]```** *Path to the data directory*

**```--remake BOOL [False]```** *Remake the data overwriting preprocessed data*

**```--remake_age INT [180]:```** *Age to remake old data overwriting preprocessed data*

**```--retry_after_peptools_error BOOL [False]```** *Whether to retry after PepTools after an error*

###### Cutoffs
**```--mean_count_proportion_cutoff FLOAT [0.0005]:```** *Confidence level cutoff for the mean count proportion*

**```--promiscuity_cutoff INT [3]```** *Maximum number of baits returning a peptide for a specific peptide. Peptides above the cut-off are defined as promiscuous*

**```--prefilter_count_cutoff INT [1]```** *Minimum number of peptide*

**```--complexity_cutoff INT [3]```** *Minimum number of unique amino acids for filtering low complexity peptides*

###### Motifs
**```--consensus RE: ```** *Annotate the peptides with a consensus*

**```--merge_mismatched_peptides BOOL [False]```** *Merge non-designed peptides with single point mutations*

**```--remove_duplicates BOOL [False]```** *Remove all but one peptides for peptides that are mapped to multiple proteins*

###### Filters
**```--require_highcount BOOL [False]```** *Filter peptides based on high NGS counts*

**```--require_interaction BOOL [False]```** *Filter peptides based on known interactions between bait and peptide containing protein*

**```--require_motifs BOOL [False]```** *Filter peptides based on on overlap with curated motifs*

**```--require_mutagenesis BOOL [False]```** *Filter peptides based on overlap with mutagenesis*

**```--require_region BOOL [False]```** *Filter peptides based on overlap with functional region*

**```--require_snp BOOL [False]```** *Filter peptides based on overlap with SNPs*

**```--require_overlap BOOL [False]```** *Filter peptides based on overlapping peptid*

**```--require_specific BOOL [False]```** *Filter peptides based on specific (non-promiscuous) peptides*

**```--require_replicate BOOL [False]```** *Filter peptides based on replicated peptides*

**```--require_specific_replicate BOOL [False]```** *Filter peptides based on specific replicates*

**```--require_structure BOOL [False]```** *Filter peptides based on overlap with motif structure*

###### Peptools				
**```--use_selection_day_replicates BOOL [True]```** *Whether to use all selection day replicates [True] or the last selection day replicate [False]*

#### Requires: 
##### Software:

- SLiMTools library for data retrieval and management, motif analysis, PSSM definition, etc.

- SLiMFinder software as part of the SLIMSuite library (included in the SLiMTools library or at http://slimsuite.blogspot.com/)

- BLAST+ as a requirement for SLiMFinder (ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/LATEST/)

- ClustalW as a requirement for SLiMFinder (http://www.clustal.org/clustal2/)

- chEMBL REST library to download drug annotation (https://pypi.org/project/chembl-webresource-client/) ```pip install chembl_webresource_client```

- dataset library for databases (https://pypi.org/project/dataset/) ```pip install dataset```

- SQLAlchemy for databases (https://pypi.org/project/SQLAlchemy/) ```pip install SQLAlchemy```

- BioPython for databases (https://pypi.org/project/biopython/) ```pip install biopython```

*NB:* paths needs to be set in the *../config.ini* file

##### Classes:
The pipeline is split into several classes.
- phageDataManager

- phageDataExternalDatasetsLoader 

- phageDataProteinAnnotator

- phageDataUtilities

- phagePeptoolsRunner

- phageEnrichedMotifFinder 

- phageDataReader


#### Important infomation, definitions and functionality*:

**PepTools peptide mapping and annotation**
Selected peptides matching the designed peptide length (16 amino acids) were mapped to the human proteome using PepTools. Alanine residues were permitted to be variable in the PepTools peptide mapping to allow for remapping of cysteine residues converted to alanine in the ProP-PD library design. Peptides were annotated with structural, evolutionary, functional, genomic and proteomic data. For each bait, the peptides for each replicate were compared to define replicated peptides and overlapping peptides. Overlapping peptides were mapped using their protein mapping (rather than sequence) and the overlapping peptides can be present in different replicates for the same bait.

**Motif enrichment, specificity determinant score and motif-containing peptides**
The replicated peptides and overlapping regions of overlapping peptides for each bait were analyzed for enriched motifs using the SLiMFinder motif discovery tool. SLiMFinder was run using minic (Minimum information content for returned motifs) of 1.1, equiv (list of groupings of physicochemically similar residues to use for ambiguous positions) ofAGS,ILMV,IVMLFAP,IVMLFWHYA,FYWH,KRH,ST,STNQ,DEST and maxwild (the maximum number of consecutive wildcard positions toallow) of 5. The amino acid frequencies of the input peptides were used as background amino acid frequencies. The topranked SLiMFinder motif from the motif enrichment step was used to align the motif-containing peptides and a PSI-BLASTPSSM was built from the aligned peptides using the PSSMSearch motif discovery tool 21. The PSSM was used to calculate a specificity determinant score, a metric to define the similarity of a peptide to the enriched specificitydeterminants, for the selected peptides for a bait. The PSSM specificity determinant score was calculated using the probabilistic scoring method of the PSSMSearch motif discovery tool on a background model obtained from scanning thehuman proteome with the reversed variant of the PSSM. 

**Normalized peptide sequencing count**
The NGS sequencing counts for each selection day were normalized by dividing the peptide sequencing counts by the sum of the sequencing counts for all peptides in the selection to create the normalized peptide sequencing count. Peptides for each selection day for a replicate were pooled and the mean normalized peptide sequencing count for each replicate was calculated for each peptide. Peptides selected in 3 or more bait screens were annotated as non-specific promiscuous binders and filtered from the results.

**Peptide consensus confidence assignment**
Optimal cut-offs for each of the peptide metrics were calculated using Youden's J statistic to maximize the true positive rate and minimize false positive rate: (i) replicated peptide (the peptide is observed in two or more replicates); (ii) overlapped peptide (the peptide has one or more overlapping peptides in any replicate); (iii) specificity determinant match (the peptide has a SLiMFinder-derived PSSM match with a p-value < 0.0001); and (iv) high normalized peptide count (the peptide has a mean normalized peptide count > 0.0005). The four binary confidence criteria were combined for each peptide to create a single metric ‘Confidence level’ which has four categories (‘High’, ‘Medium’, ‘Low’ and ‘Filtered’). The ‘Confidence level’ of ‘High’ is assigned to instances matching all four confidence criteria, ‘Medium’ is assigned to instances matching two or three of the criteria and ‘Low’ is assigned to instances matching one metric criteria. Peptides that do not match any of the metric criteria are defined as ‘Filtered’ and discarded from the results. 

**text taken from draft HD2 pilot ProP-PD article*

#### Contributors:
Norman E. Davey <norman.davey@icr.ac.uk> Institute Of Cancer Research, Chelsea, SW3 6JB, London, United Kingdom.

#### Reference:
In Preparation