#!/usr/bin/python3
#
# Program:      ProP-PD Data Analysis Pipeline
# Class:        phageDataWriter
# Description:  Analysis pipeline to process peptides returned from replicated ProP-PD selections
# Version:      1.0.0
# Last Edit:    1/1/2021
# Citation:     Under Preparation
# Author contact: Norman E. Davey <norman.davey@icr.ac.uk> Institute Of Cancer Research, Chelsea, SW3 6JB, London, United Kingdom.
# 
# License:
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http:#www.gnu.org/licenses/>.
#
# Copyright (C) 2020  Norman E. Davey


__doc__ = """
# ProP-PD Data Analysis Pipeline
### Code for writing output files for the pipeline
"""

## TODO:

import os,sys,re,logging

#-----

sys.path.append(os.path.join(os.path.dirname(__file__),"../../utilities"))
import utilities_error

sys.path.append(os.path.join(os.path.dirname(__file__),"../../data_management"))
import queryManager

#-----
logger = logging.getLogger(__name__)
#-----

class phageDataExternalDatasetsLoader():
	
	def grabELMData(self):
		"""
		Get ELM curated data 
		"""
		
		logger.debug("Function Init")

		queryManagerObj = queryManager.queryManager()
		queryManagerObj.options['verbose'] = False
		queryManagerObj.options['dataset_type'] = "elm"
		queryManagerObj.options['task'] = "get_domain_dict"
		elm_domains = queryManagerObj.main()['data']

		queryManagerObj.options['task'] = "get_interactions_list"	
		elm_interactions = queryManagerObj.main()['data']

		queryManagerObj.options['task'] = "get_elm_classes"	
		queryManagerObj.options['identifier'] = "all"	
		queryManagerObj.options['identifier_type'] = "details"	
		queryManagerObj.options['output_type'] = "details"	

		elm_consensus_details = queryManagerObj.main()['data']

		queryManagerObj.options['task'] = "get_protein_dict"	
		elm_instance_details = queryManagerObj.main()['data']
		
		elm_manual_curation_file = os.path.join(self.options["peptools_data_path"], "curation","elm_screen_mapping.tdt")
		elm_manual_curation = {}

		if os.path.exists(elm_manual_curation_file):
			
			for line in open(elm_manual_curation_file).read().strip().split('\n'):
				bait = line.split('\t')[0]
				if len(bait.split("_")) == 5:
					bait = "_".join(bait.split("_")[0:2] + [""] + bait.split("_")[2:5])

				elm_list = line.split('\t')[1]

				if bait not in elm_manual_curation:
					elm_manual_curation[bait] = {}

				for elm_class in elm_list.split(","):
					elm_class = elm_class.strip()
					if len(elm_class) > 1:
						if elm_class in elm_consensus_details:
							elm_manual_curation[bait][elm_class] = {"re":elm_consensus_details[elm_class][0]['Regex'],"re_compiled":re.compile(elm_consensus_details[elm_class][0]['Regex'].strip("."))}
						else:
							elm_manual_curation[bait][elm_class] = []
		else:
			print(elm_manual_curation_file,"does not exist")

		return {
			"elm_domains":elm_domains,
			"elm_interactions":elm_interactions,
			"elm_instance_details":elm_instance_details,
			"elm_consensus_details":elm_consensus_details,
			"elm_manual_curation":elm_manual_curation
		}

	#--------------------------------------------------------------------

	def grabCuratedELMData(self):
		"""
		Get ELM curated motif data 
		"""

		logger.debug("Function Init")

		elm_data = self.grabELMData()
		return elm_data["elm_manual_curation"]

	#--------------------------------------------------------------------

	def grabCuratedMotifData(self,bait_data):
		"""
		Get in house curated motif data 
		"""
		
		logger.debug("Function Init")

		motif_manual_curation = {}
		queryManagerObj = queryManager.queryManager()
		queryManagerObj.options['dataset_type'] = "motif"
		queryManagerObj.options['database_name'] = "complete"		

		for bait in bait_data:
			motif_manual_curation[bait] = {}
			
			try:
				motif_instances = []
				bait_accession = bait_data[bait]['protein']['accession']
				bait_domain_pfam_ids = bait_data[bait]['domain'].keys()

				#Get protein motif partners
				queryManagerObj.options['task'] = "get_instances_by_domain_accession"
				queryManagerObj.options['accession'] = bait_accession		
				motif_instance_response = queryManagerObj.main()

				if 'data' in motif_instance_response:
					motif_instances += motif_instance_response['data']

				#Get domain motif partners
				for bait_domain_pfam_id in bait_domain_pfam_ids:
					queryManagerObj.options['task'] = "get_instances_by_pfam_id"
					queryManagerObj.options['pfam_id'] = bait_domain_pfam_id	
					motif_instance_response = queryManagerObj.main()
					
					if 'data' in motif_instance_response:
						motif_instances += motif_instance_response['data']

				bait_region_start = int(bait_data[bait]['region_start'])
				bait_region_end = int(bait_data[bait]['region_end'])
				bait_region_range = range(bait_region_start,bait_region_end)
				bait_domain = list(bait_data[bait]['domain'].keys())
				bait_elm_classes = bait_data[bait]['motif']
			
				for motif_instance_data in motif_instances:
					try:
						motif_protein_accession = motif_instance_data['motif protein uniprot accession']
						bait_protein_check = bait_accession in motif_instance_data['domain protein uniprot accession']

						bait_region_overlap_proportion = 0

						try:
							curated_domain_start = max([int(x) for x in motif_instance_data['domain start residue number']])
							curated_domain_end = min([int(x) for x in motif_instance_data['domain end residue number']])
							curated_domain_range = list(range(curated_domain_start,curated_domain_end))
							bait_region_overlap = list(set(bait_region_range).intersection(curated_domain_range))
							bait_region_overlap_proportion = float(len(bait_region_overlap))/len(curated_domain_range)
							if bait_protein_check:
								bait_region_overlap_check = bait_region_overlap_proportion > 0.5
							else:
								bait_region_overlap_check = False
						except:
							curated_domain_start = "-1"
							curated_domain_end = "-1"
							bait_region_overlap_check = False

						bait_domain_overlap = list(set(bait_domain).intersection(motif_instance_data['domain pfam accession']))
						bait_elm_class_overlap = list(set(bait_elm_classes).intersection(motif_instance_data['elm class']))

						bait_domain_overlap_check = len(bait_domain_overlap) > 0
						bait_elm_class_overlap_check = len(bait_elm_class_overlap) > 0

						if motif_protein_accession not in motif_manual_curation[bait]:
							motif_manual_curation[bait][motif_protein_accession] = []

						motif_instance_data['bait_protein_check'] = bait_protein_check
						motif_instance_data['bait_domain_overlap_check'] = bait_domain_overlap_check
						motif_instance_data['bait_elm_class_overlap_check'] = bait_elm_class_overlap_check
						motif_instance_data['bait_region_overlap_check'] = bait_region_overlap_check
						motif_instance_data['bait_domain_overlap'] = bait_domain_overlap
						motif_instance_data['bait_elm_class_overlap_'] = bait_elm_class_overlap
						motif_instance_data['bait_region_overlap'] = bait_region_overlap_proportion

						motif_manual_curation[bait][motif_protein_accession].append(motif_instance_data)
					except:
						logger.error(motif_instance_data)
				
			except:
				logger.error("ERROR@grabCuratedMotifData: " + bait)
				utilities_error.printError()

		return motif_manual_curation

	#--------------------------------------------------------------------

	def getInteractions(self,bait_data):
		"""
		Get validated interactions of the bait proteins
		"""
		
		logger.debug("Function Init")

		proteins = []
		for bait in bait_data:
			if bait_data[bait]['protein']['accession'] not in proteins:
				proteins.append(bait_data[bait]['protein']['accession'])

		logger.info("Grabbing " + str(len(proteins)) + " interaction networks")
	
		queryManagerObj = queryManager.queryManager()
		queryManagerObj.options['dataset_type'] = "interaction"
		queryManagerObj.options['task'] = "get_interactions"
		queryManagerObj.options["accession"] = proteins
		queryManagerObj.options["cutoff"] = 0.0

		interactions = queryManagerObj.main()['data']
		
		logger.info("Completed")

		return interactions

	####################################