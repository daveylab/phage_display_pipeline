#!/usr/bin/python3
#
# Program:      ProP-PD Data Analysis Pipeline
# Class:        phageDataManager
# Description:  Analysis pipeline to process peptides returned from replicated ProP-PD selections
# Version:      1.0.0
# Last Edit:    1/1/2021
# Citation:     Under Preparation
# Author contact: Norman E. Davey <norman.davey@icr.ac.uk> Institute Of Cancer Research, Chelsea, SW3 6JB, London, United Kingdom.
# 
# License:
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http:#www.gnu.org/licenses/>.
#
# Copyright (C) 2020  Norman E. Davey


__doc__ = """
# ProP-PD Data Analysis Pipeline
### Code for the analysis tools for the ProP-PD data

"""

## TODO
##	[X] Make extra main file
##	[X] Remove uniprotDownloader and replace with queryRunner
##	[] Remove psiblastPSSM and replace with queryRunner

####################################################################################################################################################################################
####################################################################################################################################################################################	

import os
import sys
import random
import json
import re
import math
import logging

#-----

sys.path.append(os.path.join(os.path.dirname(__file__),"../../data_management"))
import queryRunner

sys.path.append(os.path.join(os.path.dirname(__file__),"../../pssm"))
import psiblastPSSM

sys.path.append(os.path.join(os.path.dirname(__file__),"../../utilities"))
import utilities_error

from phageEnrichedMotifFinder import phageEnrichedMotifFinder
from phageDataReader import phageDataReader
from phageDataWriter import phageDataWriter

#-----
logger = logging.getLogger(__name__)
#-----

####################################################################################################################################################################################
####################################################################################################################################################################################
	
class phageDataManager(phageDataReader):

	####################################################################################################################################################################################
	####################################################################################################################################################################################
	### ANNOTATE ANNOTATE ANNOTATE ANNOTATE ANNOTATE ANNOTATE ANNOTATE ANNOTATE ANNOTATE ANNOTATE ANNOTATE ANNOTATE ANNOTATE ANNOTATE ANNOTATE ANNOTATE ANNOTATE ANNOTATE ANNOTATE #####
	### ANNOTATE ANNOTATE ANNOTATE ANNOTATE ANNOTATE ANNOTATE ANNOTATE ANNOTATE ANNOTATE ANNOTATE ANNOTATE ANNOTATE ANNOTATE ANNOTATE ANNOTATE ANNOTATE ANNOTATE ANNOTATE ANNOTATE #####
	####################################################################################################################################################################################
	####################################################################################################################################################################################

	def annotatePeptidesScreen(self,library,bait):
		"""
		Takes peptides returned by a ProP-PD screen and annotates them using PepTools. 
		Also initialises the data structures required for processing with PepTools data.

		Args:
			library (str): library identifier of library for processing
			bait (str): bait identifier of bait for processing
		"""
	
		try:
			peptides = self.getScreenPeptides(library,bait)
			self.data[bait][library]['interacting_peptides'] = peptides

			if library in  self.library_info['taxon_library_mapping']:
				taxon_id =  self.library_info['taxon_library_mapping'][library]
			else:
				logger.info('library not in self.taxon_library_mapping' + library)
				taxon_id = "9606"
				self.library_info['taxon_library_mapping'][library] = "9606"
				self.library_info['library_shortnames'].append(library)

			################################################################
			################################################################
			################################################################

			self.data[bait][library]['peptide_scores_SLiMFinder'] = None
			self.data[bait][library]['sequence_logo_peptides'] = []
			self.data[bait][library]['SLiMFinder_motif'] = {}
			self.data[bait][library]['overlap_masked_peptide'] = ""

			################################################################

			[peptools_hits_data,missing_peptools_hits_data] = self.getPepToolsResultsChunks(library,bait,peptides,taxon_id)

			self.peptools_hits_overlaps = {}
			self.peptools_hits_peptides = {}

			found_peptools_hits_data = []
			for peptools_hit in peptools_hits_data:
				###
				try:
					hit = self.initialiseHit(peptools_hit)
					
					if hit['incorrect_mapping']: continue
				
					found_peptools_hits_data.append(hit['Hit'])
					self.update_phage_peptools_data_dictionary(library,bait,hit)

				except:
					logger.error("Error@annotatePeptides",library,bait,peptools_hit)
				
					utilities_error.printError()
					#raise

			if self.options["merge_mismatched_peptides"]:
				self.data[bait][library]['unmapped_peptide_mapping'] = self.phageDataUtilitiesObj.similarity(found_peptools_hits_data,missing_peptools_hits_data)
		
		except Exception as e:
			logger.error(library + " " + bait)
			utilities_error.printError()

	####################################

	def annotatePeptidesRand(self,library,bait):
		"""
		Takes peptides returned by a Combinattorial Phage Display screen and initialises them using a dummy Object. 

		Args:
			library (str): library identifier of library for processing
			bait (str): bait identifier of bait for processing
		"""

		self.data[bait][library]['phage_peptools_data'] = {}

		for replicate in self.selection_day_replicate_mapping[bait][library]:
			for screen_id in self.selection_day_replicate_mapping[bait][library][replicate]:
				for peptide in list(self.data[bait][library]['screens'][screen_id]['phage_peptide_data'].keys()):
					hit_id = "Random_0_" + peptide
					self.data[bait][library]['phage_peptools_data'][hit_id] = {
						"Hit":peptide,
						'SeqStart':"1",
						'SeqStop':"16",
						'ProteinAcc':"None",
						'GeneName':peptide,
						'ProteinName':"Random Peptide",
						'Virus':"Synthetic",
						"Designed_Mutant":False,
						"Library_Peptide":peptide,
						'overlap_masked_peptide':""
						}
	
	####################################################################################################################################################################################	
	####################################################################################################################################################################################
	### CONFIDENCE SCORE CONFIDENCE SCORE CONFIDENCE SCORE CONFIDENCE SCORE CONFIDENCE SCORE CONFIDENCE SCORE CONFIDENCE SCORE CONFIDENCE SCORE CONFIDENCE SCORE CONFIDENCE SCORE ###### 
	### CONFIDENCE SCORE CONFIDENCE SCORE CONFIDENCE SCORE CONFIDENCE SCORE CONFIDENCE SCORE CONFIDENCE SCORE CONFIDENCE SCORE CONFIDENCE SCORE CONFIDENCE SCORE CONFIDENCE SCORE ###### 
	####################################################################################################################################################################################
	####################################################################################################################################################################################

	####################################################################################################################################################################################
	###
	###  CONFIDENCE SCORE - Overlaps
	###
	####################################################################################################################################################################################

	def setPeptidesOverlaps(self,library,bait):
		"""
		Set the Number of peptide overlaps for the peptides in the screen.
		Overlaps are calculated based on the mapping to uniprot proteins.
		Uses a data structure - self.peptools_hits_overlaps - of proteins and 
		peptide starts.

		Args:
			library (str): library identifier of library for processing
			bait (str): bait identifier of bait for processing
		"""

		if library in ["Rand"]:
			## If the analysis is a combinsttorial library
			for hit_id in self.data[bait][library]['phage_peptools_data']:
				self.data[bait][library]['phage_peptools_data'][hit_id]['overlap'] = 0
		else:
			## Iterate through the peptide containing proteins
			for accession in self.peptools_hits_overlaps:
				if len(self.peptools_hits_overlaps[accession]) < 2: continue

				## Iterate through the peptides in the protein
				for peptide_start_a in self.peptools_hits_overlaps[accession]:
					self.data[bait][library]['phage_peptools_data'][self.peptools_hits_overlaps[accession][peptide_start_a]]['overlapping_peptides'] = []
					peptide_a = self.peptools_hits_overlaps[accession][peptide_start_a].split("_")[2] # Get peptide sequence
					overlap_positions = [0]*len(peptide_a) # Initialise the overlap list

					## Compare them to all other peptides in the protein
					for peptide_start_b in self.peptools_hits_overlaps[accession]:
						if peptide_start_a != peptide_start_b: # Skip if its the same peptide
							if abs(peptide_start_a-peptide_start_b) <= 16: # Do they overlap?

								## add peptide to overlapping peptide list for query peptide
								self.data[bait][library]['phage_peptools_data'][self.peptools_hits_overlaps[accession][peptide_start_a]]['overlapping_peptides'].append(self.peptools_hits_overlaps[accession][peptide_start_b].split("_")[2])
								
								## add peptide to overlapping peptide list for query peptide
								overlap_start = max(0,peptide_start_b - peptide_start_a)
								overlap_end = min(16,peptide_start_b - peptide_start_a + 16)

								## update the number of overlapping peptides for each residue in the peptide
								for i in range(overlap_start,overlap_end):
									overlap_positions[i] += 1

					## Create a peptide mask of residues that are overlapped
					overlap_masked_peptide = ""
					if sum(overlap_positions) == 0:
						overlap_masked_peptide = ""
					else:
						for i in range(0,len(peptide_a)):
							if overlap_positions[i] > 0:
								overlap_masked_peptide += self.data[bait][library]['phage_peptools_data'][self.peptools_hits_overlaps[accession][peptide_start_a]]['Hit'][i]
							else:
								overlap_masked_peptide += " "
					
					## Push data to dictionary
					# max(overlap_positions) will the number of peptides overlapping the most overlapped residue
					self.data[bait][library]['phage_peptools_data'][self.peptools_hits_overlaps[accession][peptide_start_a]]['overlap'] = max(overlap_positions) 
					self.data[bait][library]['phage_peptools_data'][self.peptools_hits_overlaps[accession][peptide_start_a]]['overlap_positions'] = len(peptide_a) - overlap_positions.count(0)
					self.data[bait][library]['phage_peptools_data'][self.peptools_hits_overlaps[accession][peptide_start_a]]['overlap_masked_peptide'] = overlap_masked_peptide

	####################################################################################################################################################################################	
	###
	###  CONFIDENCE SCORE - Normalised Peptides Frequencies
	###
	####################################################################################################################################################################################

	def setPeptidesNormalisedPeptideFrequencies(self,library,bait):
		"""
		Set the Normalised Peptides Frequencies for the peptides in the screen
		
		Args:
			library (str): library identifier of library for processing
			bait (str): bait identifier of bait for processing
		"""

		try:
			screen_peptide_counts_sums = {}
			screen_peptide_counts = {}

			## iterate through the peptides in the screen
			for hit_id in self.data[bait][library]['phage_peptools_data']:
				libraryPeptide = self.data[bait][library]['phage_peptools_data'][hit_id]["Library_Peptide"]

				## initialise data
				self.data[bait][library]['phage_peptools_data'][hit_id]['screen_counts'] = {}
				self.data[bait][library]['phage_peptools_data'][hit_id]['mean_count_proportion'] = 0
				self.data[bait][library]['phage_peptools_data'][hit_id]['mean_count_proportion_std'] = 0
				self.data[bait][library]['phage_peptools_data'][hit_id]['peptide_counts_proportion'] = []

				## count peptides in the NGS data
				for replicate in self.selection_day_replicate_mapping[bait][library]:
					self.data[bait][library]['phage_peptools_data'][hit_id]['screen_counts'][replicate] = {}
					for screen_id in self.selection_day_replicate_mapping[bait][library][replicate]:
						replicate_id = " ".join([replicate, "Day " + str(self.data[bait][library]['screens'][screen_id]['details']['Selection Day'])])
					
						if replicate_id not in screen_peptide_counts: screen_peptide_counts[replicate_id] = {}
						if libraryPeptide in self.data[bait][library]['screens'][screen_id]['phage_peptide_data']:
							screen_peptide_counts[replicate_id][libraryPeptide] = int(self.data[bait][library]['screens'][screen_id]['phage_peptide_data'][libraryPeptide])

			## calculate the summ of the NGS peptide counts for each replicate
			for replicate_id in screen_peptide_counts:
				screen_peptide_counts_sums[replicate_id] = sum(screen_peptide_counts[replicate_id].values())
				
			## iterate through the peptides in the screen
			for hit_id in self.data[bait][library]['phage_peptools_data']:
				libraryPeptide = self.data[bait][library]['phage_peptools_data'][hit_id]["Library_Peptide"]
				row = [libraryPeptide]

				replicate_selection_count_proportions = []

				## iterate through the replicates of the screen
				for replicate in self.selection_day_replicate_mapping[bait][library]:
					
					screen_selection_count_proportions = []

					## iterate through the selection days of the replicate 
					for screen_id in self.selection_day_replicate_mapping[bait][library][replicate]:
						replicate_id = " ".join([replicate, "Day " + str(self.data[bait][library]['screens'][screen_id]['details']['Selection Day'])])
					
						if libraryPeptide in self.data[bait][library]['screens'][screen_id]['phage_peptide_data']:
							row.append(str(self.data[bait][library]['screens'][screen_id]['phage_peptide_data'][libraryPeptide]))
							row.append("%1.3f"%(float(self.data[bait][library]['screens'][screen_id]['phage_peptide_data'][libraryPeptide])/screen_peptide_counts_sums[replicate_id]))
							screen_count_proportion = float(self.data[bait][library]['screens'][screen_id]['phage_peptide_data'][libraryPeptide])/screen_peptide_counts_sums[replicate_id]
							
							self.data[bait][library]['phage_peptools_data'][hit_id]['screen_counts'][replicate][self.data[bait][library]['screens'][screen_id]['details']['Selection Day']] = {
								"count":self.data[bait][library]['screens'][screen_id]['phage_peptide_data'][libraryPeptide],
								"proportion":screen_count_proportion
							}
						else:
							screen_count_proportion = 0

							self.data[bait][library]['phage_peptools_data'][hit_id]['screen_counts'][replicate][self.data[bait][library]['screens'][screen_id]['details']['Selection Day']] = {
								"count":0,
								"proportion":screen_count_proportion
							}
							row.append("")
							row.append("")

						screen_selection_count_proportions.append(screen_count_proportion)

					if len(screen_selection_count_proportions) == 0:
						replicate_selection_count_proportions.append(0)
					else:
						replicate_selection_count_proportions.append(sum(screen_selection_count_proportions)/len(screen_selection_count_proportions))
				
				## calculate the mean Normalised Peptides Frequencies 
				mean = sum(replicate_selection_count_proportions)/len(replicate_selection_count_proportions)
				stddev = math.sqrt(sum((x - mean)**2 for x in replicate_selection_count_proportions) / len(replicate_selection_count_proportions))
				
				self.data[bait][library]['phage_peptools_data'][hit_id]['mean_count_proportion'] = mean
				self.data[bait][library]['phage_peptools_data'][hit_id]['mean_count_proportion_std'] = stddev
				self.data[bait][library]['phage_peptools_data'][hit_id]['peptide_counts_proportion'] = replicate_selection_count_proportions

				row.append("%1.3f"%mean)
				row.append("%1.3f"%stddev)

				row.append("|".join(["%1.3f"%x for x in replicate_selection_count_proportions]))

				logger.debug("\t".join(row))
		except Exception as e:
				logger.error(library + " - " + bait)

	####################################################################################################################################################################################
	###
	###  CONFIDENCE SCORE - Replicates
	###
	####################################################################################################################################################################################

	def setPeptidesReplicateCount(self,library,bait):
		"""
		Set the replicate count for the peptides in the screen
		
		Args:
			library (str): library identifier of library for processing
			bait (str): bait identifier of bait for processing
		"""

		for hit_id in self.data[bait][library]['phage_peptools_data']:
			peptide_counts = []
			peptide_screen = []
			peptide_count_summer = []

			peptide_selection_data = {}

			## iterate through the replicates of the screen
			for replicate in self.selection_day_replicate_mapping[bait][library]:
				peptide_count_summer_tmp = []
				screen_ids_tmp = []

				peptide_selection_data[replicate] = {}

				## iterate through the selection days of the replicate 
				for screen_id in self.selection_day_replicate_mapping[bait][library][replicate]:

					########

					if self.options["merge_mismatched_peptides"]:
						logger.info("Functionality incomplete")
						hit = self.data[bait][library]['phage_peptools_data'][hit_id]['Hit']
						for mismatch_count in self.data[bait][library]['unmapped_peptide_mapping'][hit]:
							for mismatch_peptide in self.data[bait][library]['unmapped_peptide_mapping'][hit][mismatch_count]:
								#print mismatch_peptide
								if hit_id.split("_")[2] in self.data[bait][library]['screens'][screen_id]['phage_peptide_data'] and mismatch_peptide in self.data[bait][library]['screens'][screen_id]['phage_peptide_data']:
									logger.info(int(self.data[bait][library]['screens'][screen_id]['phage_peptide_data'][hit_id.split("_")[2]]) + "\t",mismatch_count,"\t",hit,"\t",mismatch_peptide,"\t",self.data[bait][library]['screens'][screen_id]['phage_peptide_data'][mismatch_peptide])

					if self.data[bait][library]['phage_peptools_data'][hit_id]["Designed_Mutant"]:
						regex = self.data[bait][library]['phage_peptools_data'][hit_id]["Regex"]

						#print "wildtype_peptide ",self.mutations_re_reverse_mapping_info[regex]['wildtype_peptide'],self.mutations_re_reverse_mapping_info[regex]['wildtype_peptide'] in self.data[bait][library]['screens'][screen_id]['phage_peptide_data']
						#print "designed_peptide ",self.mutations_re_reverse_mapping_info[regex]['designed_peptide'],self.mutations_re_reverse_mapping_info[regex]['designed_peptide'] in self.data[bait][library]['screens'][screen_id]['phage_peptide_data']
						#print "peptools_peptide ",self.mutations_re_reverse_mapping_info[regex]['peptools_peptide'],self.mutations_re_reverse_mapping_info[regex]['peptools_peptide'] in self.data[bait][library]['screens'][screen_id]['phage_peptide_data']
						#print "library_peptide  ",self.mutations_re_reverse_mapping_info[regex]['library_peptide'],self.mutations_re_reverse_mapping_info[regex]['library_peptide'] in self.data[bait][library]['screens'][screen_id]['phage_peptide_data']
						#screen_peptide = self.mutations_re_reverse_mapping_info[regex]['library_peptide']

						screen_peptide = self.mutations_re_reverse_mapping_info[regex]['library_peptide']

					else:
						screen_peptide = self.data[bait][library]['phage_peptools_data'][hit_id]["Library_Peptide"]

					if library in self.library_info['terminal_libraries']:
						screen_peptide = screen_peptide + "$"

					if screen_peptide in self.data[bait][library]['screens'][screen_id]['phage_peptide_data']:
						peptide_selection_data[replicate][screen_id] = int(self.data[bait][library]['screens'][screen_id]['phage_peptide_data'][screen_peptide])

						peptide_count_summer_tmp.append(int(self.data[bait][library]['screens'][screen_id]['phage_peptide_data'][screen_peptide]))
						screen_ids_tmp.append(replicate + "-" + str(self.data[bait][library]['screens'][screen_id]['details']['Selection Day']))
					else:
						peptide_selection_data[replicate][screen_id] = 0

				if len(peptide_count_summer_tmp) > 0:
					peptide_count_summer.append(sum(peptide_count_summer_tmp))
					peptide_counts.append(":".join([str(x) for x in peptide_count_summer_tmp]))
				else:
					peptide_counts.append("-")

				peptide_screen.append(":".join(screen_ids_tmp))

			self.data[bait][library]['phage_peptools_data'][hit_id]['replicates'] = len(peptide_count_summer)
			self.data[bait][library]['phage_peptools_data'][hit_id]['peptide_selection_data'] = len(peptide_selection_data)
			self.data[bait][library]['phage_peptools_data'][hit_id]['peptide_screen'] = peptide_screen
			self.data[bait][library]['phage_peptools_data'][hit_id]['peptide_counts'] = peptide_counts
			self.data[bait][library]['phage_peptools_data'][hit_id]['mean_counts'] = sum(peptide_count_summer)/len(peptide_counts)

			if len(peptide_count_summer) > 0:
				self.data[bait][library]['phage_peptools_data'][hit_id]['max_counts'] = max(peptide_count_summer)
			else:
				self.data[bait][library]['phage_peptools_data'][hit_id]['max_counts'] = 0
	
	###################################################################################################################################################################################
	###
	###  CONFIDENCE SCORE - SpecificityDeterminantScore
	###
	###################################################################################################################################################################################

	def createSLiMFinderInputPeptideList(self,library,bait):
		"""
		Create peptide list for analysis by SLiMFinder for enriched motifs
		
		Args:
			library (str): library identifier of library for processing
			bait (str): bait identifier of bait for processing
		"""
		
		slimfinder_input_peptides = []

		if library in ["Rand"]:
			slimfinder_input_peptides = self.ranked_peptides(library,bait,200)
		else:
			for hit_id in self.data[bait][library]['phage_peptools_data']:
				if 'specific_check' not in self.data[bait][library]['phage_peptools_data'][hit_id]: continue
				if self.data[bait][library]['phage_peptools_data'][hit_id]['specific_check']:
					replicate_check = self.data[bait][library]['phage_peptools_data'][hit_id]['replicate_check']
					overlap_check = self.data[bait][library]['phage_peptools_data'][hit_id]['overlap_check']
					specific_check = self.data[bait][library]['phage_peptools_data'][hit_id]['specific_check']
					
					if replicate_check or overlap_check and specific_check:
						if self.options['slimfinder_use_overlap']:
							if 'overlap_masked_peptide' in self.data[bait][library]['phage_peptools_data'][hit_id]:
								if  len(self.data[bait][library]['phage_peptools_data'][hit_id]['overlap_masked_peptide']) > 0:
									slimfinder_input_peptides.append(self.data[bait][library]['phage_peptools_data'][hit_id]['overlap_masked_peptide'].strip())
								else:
									slimfinder_input_peptides.append(self.data[bait][library]['phage_peptools_data'][hit_id]['Hit'])
							else:
								slimfinder_input_peptides.append(self.data[bait][library]['phage_peptools_data'][hit_id]['Hit'])
						else:
							slimfinder_input_peptides.append(self.data[bait][library]['phage_peptools_data'][hit_id]['Hit'])
		
		return slimfinder_input_peptides
		
	def findEnrichedMotif(self,library,bait):
		"""
		Run SLiMFinder analysis on screen
		
		Args:
			library (str): library identifier of library for processing
			bait (str): bait identifier of bait for processing
		"""
		
		logger.info("#"*80)

		slimfinder_input_peptides = self.createSLiMFinderInputPeptideList(library,bait)

		phageEnrichedMotifFinderObj = phageEnrichedMotifFinder()
		phageEnrichedMotifFinderObj.options = self.options
		phageEnrichedMotifFinderObj.library_info = self.library_info
		phageEnrichedMotifFinderObj.elm_manual_curation = self.elm_manual_curation
		enriched_motif_data = phageEnrichedMotifFinderObj.findMotif(library,bait,slimfinder_input_peptides)

		out_file_json = os.path.join(self.options["peptools_data_path"],"json",  bait + "." +  library + ".slimfinder.json")
		with open(out_file_json, 'w') as outfile:
			json.dump(enriched_motif_data, outfile)

		self.data[bait][library].update(enriched_motif_data)
	
		logger.info("SLiMFinder - Completed")
		logger.info("SLiMFinder path: " + out_file_json)
		logger.info("#"*80)
		
		
	def createSLiMFinderAlignmentPSSM(self,library,bait):
		"""
		Create PSSM from SLiMFinder aligned peptides
		
		Args:
			library (str): library identifier of library for processing
			bait (str): bait identifier of bait for processing
		"""
		
		options = {'peptides':",".join(self.data[bait][library]['peptide_scores_SLiMFinder']['sig_aligned_peptides'])}
		data = queryRunner.queryRunner("pssm","make_pssm",options).run()

		out_file_json = os.path.join(self.options["peptools_data_path"],"specificity_determinants",  bait + "." +  library + ".pssm.json")

		if 'data' in data:
			data['data']['source'] = "ProP-PD"
			data['data']['library'] = library
			data['data']['bait'] = bait
			data['data']['alignment_methods'] = "Peptides realigned with SLiMFinder motif-derived PSSM" 

		with open(out_file_json, 'w') as outfile:
			json.dump(data, outfile)
		
		logger.info("Writing: " + out_file_json)
	
	def annotatePeptidesWithSLiMFinderSpecificityDeterminants(self,library,bait):
		"""
		Annotate and score peptide based on SLiMFinder returned motif using a PSI-BLAST PSSM.
		
		Args:
			library (str): library identifier of library for processing
			bait (str): bait identifier of bait for processing
		"""
	
		## SCORE PEPTIDE BASED ON SLIMFINDER RETURNED MOTIF USING PSI-BLAST PSSM

		## Create PSSM
		pssm = psiblastPSSM.psiblastPSSM(self.data[bait][library]['peptide_scores_SLiMFinder']['sig_aligned_peptides'])
		pssm.profiles()

		## Grab all peptides in screen
		input = {}
		for hit_id in self.data[bait][library]['phage_peptools_data']:
			input[hit_id] = self.data[bait][library]['phage_peptools_data'][hit_id]["Hit"]

		## Score all peptides in screen with PSSM
		pssm_aligned_peptides = pssm.score_peptides(input,gap_penalty=0)
		
		## UPDATE CONFIDENCE BASED ON SPECIFICITY DETERMINANT MATCHING

		## Grab thte most significant enriched SLiMFinder motif in peptides
		motif =  self.data[bait][library]['peptide_scores_SLiMFinder']['motif'][1]['motif']
		motif_compiled = re.compile(motif)

		for hit_id in self.data[bait][library]['phage_peptools_data']:
			## Annotate peptides with significant enriched SLiMFinder PSSM scores
		
			for header in pssm_aligned_peptides[hit_id]:
				self.data[bait][library]['phage_peptools_data'][hit_id]['slimfinder_' + header] = pssm_aligned_peptides[hit_id][header]
			
			if self.data[bait][library]['phage_peptools_data'][hit_id]['slimfinder_score_p'] < 0.0001:
				self.data[bait][library]['phage_peptools_data'][hit_id]["specificity_determinant_check"] = True
			
			peptide = self.data[bait][library]['phage_peptools_data'][hit_id]["Hit"]
			
			match_start = -1
			match_end = -1
			match_sequence = ""
			
			## Annotate peptides with significant enriched SLiMFinder motif matches
		
			for m in motif_compiled.finditer(peptide):
				match_start = m.start()
				match_end = m.end()
				match_sequence = m.group()
				
			self.data[bait][library]['phage_peptools_data'][hit_id]['slimfinder_match_start'] = match_start
			self.data[bait][library]['phage_peptools_data'][hit_id]['slimfinder_match_end'] = match_end
			self.data[bait][library]['phage_peptools_data'][hit_id]['slimfinder_match_sequence'] = match_sequence		
			self.data[bait][library]['phage_peptools_data'][hit_id]['slimfinder_use_peptide_aa_freqs'] = self.options['use_peptide_aa_freqs'] 	

	def setPeptidesSpecificityDeterminantScore(self,library,bait):
		"""
		Set the Peptides Specificity Determinant Scores for the peptides in the screen
		
		Args:
			library (str): library identifier of library for processing
			bait (str): bait identifier of bait for processing
		"""

		if self.options['run_slimfinder']:
			try:
				self.findEnrichedMotif(library,bait)

				if 'sig_aligned_peptides' in self.data[bait][library]['peptide_scores_SLiMFinder']:
					self.createSLiMFinderAlignmentPSSM(library,bait)
					self.annotatePeptidesWithSLiMFinderSpecificityDeterminants(library,bait)
						
			except Exception as e:
				self.data[bait][library]['peptide_scores_SLiMFinder'] = {}
				utilities_error.printError()
				logger.info("# Motif discovery failed",library,bait,e)

	####################################################################################################################################################################################
	####################################################################################################################################################################################
	### SET PEPTIDE DATA  SET PEPTIDE DATA  SET PEPTIDE DATA  SET PEPTIDE DATA  SET PEPTIDE DATA  SET PEPTIDE DATA  SET PEPTIDE DATA  SET PEPTIDE DATA  SET PEPTIDE DATA  SET PEPT #####
	####################################################################################################################################################################################
	####################################################################################################################################################################################

	###################################################################################################################################################################################
	###
	###  SET PEPTIDE ATTRIBUTES
	###
	###################################################################################################################################################################################

	def setPeptidePromiscuity(self, library,bait,hit_id):
		"""
		Set the promiscuity of the peptide

		Args:
			library (str): library identifier of library for processing
			bait (str): bait identifier of bait for processing
			hit_id (str): unique peptide identifier
		"""
		try:
			self.data[bait][library]['phage_peptools_data'][hit_id]["in_crapome"] = []

			if library in ["Rand"]:
				if self.data[bait][library]['phage_peptools_data'][hit_id]["Hit"] in self.crapome[library]:
					self.data[bait][library]['phage_peptools_data'][hit_id]["in_crapome"] = self.crapome[library][self.data[bait][library]['phage_peptools_data'][hit_id]["Hit"]]
			else:
				try:
					self.data[bait][library]['phage_peptools_data'][hit_id]["Hit_Phage_Variant"] = self.data[bait][library]['phage_peptools_data'][hit_id]["Library_Peptide"]

					if self.data[bait][library]['phage_peptools_data'][hit_id]["Hit_Phage_Variant"] in self.crapome[library]:
						self.data[bait][library]['phage_peptools_data'][hit_id]["in_crapome"] = self.crapome[library][self.data[bait][library]['phage_peptools_data'][hit_id]["Hit_Phage_Variant"]]
				except:
					logger.error(library + ":" + bait + ":" + hit_id)

			library = library.split("-")[0]  ### CHECK THIS - this is to allow the conditional library screens

			try:
				self.data[bait][library]['phage_peptools_data'][hit_id]["promiscuity"] = len(self.data[bait][library]['phage_peptools_data'][hit_id]["in_crapome"])
			except:
				self.data[bait][library]['phage_peptools_data'][hit_id]["promiscuity"] = 1
		except:
			self.data[bait][library]['phage_peptools_data'][hit_id]["promiscuity"] = 1

	####################################

	def setPeptideConsensusMatches(self, library,bait,hit_id):
		"""
		Set the ELM consensus matches in the peptide
		
		Args:
			library (str): library identifier of library for processing
			bait (str): bait identifier of bait for processing
			hit_id (str): unique peptide identifier
		"""

		self.data[bait][library]['phage_peptools_data'][hit_id]["consensus_match"] = "n/d"

		if self.options["consensus"] != "":
			consensus_re = re.compile(self.options["consensus"])
			self.data[bait][library]['phage_peptools_data'][hit_id]["consensus_match"] = "|".join([":".join(x) for x in consensus_re.findall(self.data[bait][library]['phage_peptools_data'][hit_id]["Hit"])])
			
		else:
			self.data[bait][library]['phage_peptools_data'][hit_id]["consensus_match_details"] = {}
					
			for elm_class in self.elm_manual_curation[bait]:
				if self.data[bait][library]['phage_peptools_data'][hit_id]["consensus_match"] == "n/d":
					self.data[bait][library]['phage_peptools_data'][hit_id]["consensus_match"] = []

				try:
					
					for m in self.elm_manual_curation[bait][elm_class]["re_compiled"].finditer(self.data[bait][library]['phage_peptools_data'][hit_id]["Hit"]):
						if elm_class not in self.data[bait][library]['phage_peptools_data'][hit_id]["consensus_match_details"]:
							self.data[bait][library]['phage_peptools_data'][hit_id]["consensus_match_details"][elm_class] = []
						
						match = {
							"match_start":m.start(),
							"match_end":m.end(),
							"match_sequence":m.group()
						}

						self.data[bait][library]['phage_peptools_data'][hit_id]["consensus_match_details"][elm_class].append(match)

						if elm_class not in self.data[bait][library]['phage_peptools_data'][hit_id]["consensus_match"]:
							self.data[bait][library]['phage_peptools_data'][hit_id]["consensus_match"].append(elm_class)
				except:
					logger.critical("re_compiled not found")
					self.data[bait][library]['phage_peptools_data'][hit_id]["consensus_match"] = []

			if self.data[bait][library]['phage_peptools_data'][hit_id]["consensus_match"] != "n/d":
				self.data[bait][library]['phage_peptools_data'][hit_id]["consensus_match"] = "|".join(self.data[bait][library]['phage_peptools_data'][hit_id]["consensus_match"])

	####################################

	def setPeptideRelatedProteins(self, library,bait,hit_id):
		"""
		Set the related proteins/protietn families of the protein containing the peptide
		
		Args:
			library (str): library identifier of library for processing
			bait (str): bait identifier of bait for processing
			hit_id (str): unique peptide identifier
		"""

		proteinAccession = self.data[bait][library]['phage_peptools_data'][hit_id]['ProteinAcc']

		self.data[bait][library]['phage_peptools_data'][hit_id]['uniref50'] = "?"
		self.data[bait][library]['phage_peptools_data'][hit_id]['uniref90'] = "?"
		self.data[bait][library]['phage_peptools_data'][hit_id]['uniref100'] = "?"
		self.data[bait][library]['phage_peptools_data'][hit_id]['uniref50_index'] = "?"
		self.data[bait][library]['phage_peptools_data'][hit_id]['uniref90_index'] = "?"
		self.data[bait][library]['phage_peptools_data'][hit_id]['uniref100_index'] = "?"
		self.data[bait][library]['phage_peptools_data'][hit_id]['description'] = "N/A"
		self.data[bait][library]['phage_peptools_data'][hit_id]['taxonomy'] = "N/A"
		self.data[bait][library]['phage_peptools_data'][hit_id]['family'] = "N/A"

		if self.options["add_uniref_mapping"] and proteinAccession != "None":
			try:
				options = {'accession':proteinAccession}
				protein_data = queryRunner.queryRunner("uniprot","parse_basic",options).run()

				if protein_data['status'] != "Error":
					self.data[bait][library]['phage_peptools_data'][hit_id]['description'] = protein_data['data']['description']
					self.data[bait][library]['phage_peptools_data'][hit_id]['family'] = protein_data['data']['family']
					self.data[bait][library]['phage_peptools_data'][hit_id]['taxonomy'] = protein_data['data']['taxonomy']
			except:
				logger.error("Can't load protein " + hit_id)

			"""
			TODO: 
			FIX THIS AND UNIREF DATA TO DATA STRUCTURE

			try:
				if self.options["add_uniref_mapping"]:
					uniref50_data = uniprotDownloaderObj.getUniProtUniref(proteinAccession)
					options = {'accession':proteinAccession}
					protein_data = queryRunner.queryRunner("uniprot","parse_basic",options).run()


					self.data[bait][library]['phage_peptools_data'][hit_id]['uniref50'] = uniref50_data[0]
					self.data[bait][library]['phage_peptools_data'][hit_id]['uniref50_index'] = uniref50_data.index(proteinAccession)

					uniref90_data = uniprotDownloaderObj.getUniProtUniref(proteinAccession,0.9)
					self.data[bait][library]['phage_peptools_data'][hit_id]['uniref90'] =  uniref90_data[0]
					self.data[bait][library]['phage_peptools_data'][hit_id]['uniref90_index'] = uniref90_data.index(proteinAccession)

					uniref100_data = uniprotDownloaderObj.getUniProtUniref(proteinAccession,1.0)
					self.data[bait][library]['phage_peptools_data'][hit_id]['uniref100'] = uniref100_data[0]
					self.data[bait][library]['phage_peptools_data'][hit_id]['uniref100_index'] = uniref100_data.index(proteinAccession)
			except:
				print("Can't load uniref",hit_id)

			"""

	####################################

	def setPeptideInteractorPeptools(self,library,bait,hit_id):
		"""
		Set the interactors the protein containing the peptide from peptools
		
		Args:
			library (str): library identifier of library for processing
			bait (str): bait identifier of bait for processing
			hit_id (str): unique peptide identifier
		"""

		if 'interactor' in self.data[bait][library]['phage_peptools_data'][hit_id]:
			if self.data[bait][library]['phage_peptools_data'][hit_id]['interactor']['confidence'] > 0:
				#interactors_list.append(str(self.data[bait][library]['phage_peptools_data'][hit_id]['GeneName']))
				interactor = str(self.data[bait][library]['phage_peptools_data'][hit_id]['interactor']['confidence'])  + "|"
				interactor += ",".join(self.data[bait][library]['phage_peptools_data'][hit_id]['interactor']['pmids'])  + "|"
				interactor += ",".join(self.data[bait][library]['phage_peptools_data'][hit_id]['interactor']['method_details'])
			else:
				interactor = "False"
		else:
			interactor = "False"
		
		self.data[bait][library]['phage_peptools_data'][hit_id]['interactor_str'] = interactor

	####################################

	def setPeptideSharedSignificantOntology(self,library,bait,hit_id):
		"""
		Set the Shared Significant Ontologies between the bait and protein containing the peptide. 
		
		Args:
			library (str): library identifier of library for processing
			bait (str): bait identifier of bait for processing
			hit_id (str): unique peptide identifier
		"""
		
		self.data[bait][library]['phage_peptools_data'][hit_id]['colocalisation_score'] = 1
		self.data[bait][library]['phage_peptools_data'][hit_id]['colocalisation_best'] = ""

		try:
			if len(self.data[bait][library]['phage_peptools_data'][hit_id]['shared_ontology']['localisation']['best']) > 0:
				self.data[bait][library]['phage_peptools_data'][hit_id]['colocalisation_score'] = self.data[bait][library]['phage_peptools_data'][hit_id]['shared_ontology']['localisation']['best'][0]['probs']['uniref90']
				self.data[bait][library]['phage_peptools_data'][hit_id]['colocalisation_best'] =  self.data[bait][library]['phage_peptools_data'][hit_id]['shared_ontology']['localisation']['best'][0]['name']
		except:
			pass
	
		self.data[bait][library]['phage_peptools_data'][hit_id]['shared_function_score'] = 1
		self.data[bait][library]['phage_peptools_data'][hit_id]['shared_function_best'] = ""

		try:
			if len(self.data[bait][library]['phage_peptools_data'][hit_id]['shared_ontology']['function']['best']) > 0:
				self.data[bait][library]['phage_peptools_data'][hit_id]['shared_function_score'] = self.data[bait][library]['phage_peptools_data'][hit_id]['shared_ontology']['function']['best'][0]['probs']['uniref90']
				self.data[bait][library]['phage_peptools_data'][hit_id]['shared_function_best'] =  self.data[bait][library]['phage_peptools_data'][hit_id]['shared_ontology']['function']['best'][0]['name']
		except:
			pass

		####

		self.data[bait][library]['phage_peptools_data'][hit_id]['shared_process_score'] = 1
		self.data[bait][library]['phage_peptools_data'][hit_id]['shared_process_best'] = ""

		try:
			if len(self.data[bait][library]['phage_peptools_data'][hit_id]['shared_ontology']['process']['best']) > 0:
				self.data[bait][library]['phage_peptools_data'][hit_id]['shared_process_score'] = self.data[bait][library]['phage_peptools_data'][hit_id]['shared_ontology']['process']['best'][0]['probs']['uniref90']
				self.data[bait][library]['phage_peptools_data'][hit_id]['shared_process_best'] =  self.data[bait][library]['phage_peptools_data'][hit_id]['shared_ontology']['process']['best'][0]['name']
		except:
			pass
	
	####################################

	def setPeptidePrimaryDuplicate(self,library,bait,hit_id):
		"""
		Set the primary duplicte status of the peptide. 
		
		Args:
			library (str): library identifier of library for processing
			bait (str): bait identifier of bait for processing
			hit_id (str): unique peptide identifier
		"""
		self.data[bait][library]['phage_peptools_data'][hit_id]['primary_duplicate'] = self.duplicates[self.data[bait][library]['phage_peptools_data'][hit_id]['Hit']][hit_id] == min(self.duplicates[self.data[bait][library]['phage_peptools_data'][hit_id]['Hit']].values())
					
	####################################
	
	def setPeptideInteractorLocal(self,library,bait,hit_id):
		"""
		Set the primary duplicte status of the peptide from local data
		
		Args:
			library (str): library identifier of library for processing
			bait (str): bait identifier of bait for processing
			hit_id (str): unique peptide identifier
		"""

		bait_uniprot = bait.split("_")[-3]
		hit = self.data[bait][library]['phage_peptools_data'][hit_id]

		if bait_uniprot in self.interactions:
			if hit['ProteinAcc'] in self.interactions[bait_uniprot]:

				method_details = []
				for pmid in self.interactions[bait_uniprot][hit['ProteinAcc']]['pmids']:
					
					method_details += self.interactions[bait_uniprot][hit['ProteinAcc']]['pmids'][pmid]['method'].split("|")

				method_details = list(set(method_details))

				self.data[bait][library]['phage_peptools_data'][hit_id]['interactor'] = {
				'confidence':float(self.interactions[bait_uniprot][hit['ProteinAcc']]['confidence']),
				'pmids':list(self.interactions[bait_uniprot][hit['ProteinAcc']]['pmids'].keys()),
				'method_details':method_details
				}

	####################################

	def setPeptideCuratedMotifs(self,library,bait,hit_id):
		hit = self.data[bait][library]['phage_peptools_data'][hit_id]
		peptide_range = list(range(hit['SeqStart'], hit['SeqStop']))
		
		self.data[bait][library]['phage_peptools_data'][hit_id]['curated_motifs'] = []

		if hit['ProteinAcc'] in self.motif_manual_curation[bait]:
			for motif_instance in self.motif_manual_curation[bait][hit['ProteinAcc']]:
				motif_range = list(range(motif_instance['motif start residue number'], motif_instance['motif end residue number']))
				
				if len(set(peptide_range).intersection(motif_range)) > 0:
					self.data[bait][library]['phage_peptools_data'][hit_id]['curated_motifs'].append(motif_instance)
					
	####################################

	def setPeptideSNPsData(self,library,bait,hit_id):
		hit = self.data[bait][library]['phage_peptools_data'][hit_id]

		self.data[bait][library]['phage_peptools_data'][hit_id]['SNP'] = []
		disease_mutations = {"snp_str":[],"snps":[],"pmids":[]}

		####
		
		if 'SNP' in hit:
			try:
				for snp in hit['SNP']:
					if snp["name"].count("missense") > 0:
						snp_str_list = []
						for tag in ['mutation','start','Disease']:
							if tag in snp['description']:
								snp_str_list.append(str(snp['description'][tag]))
							else:
								snp_str_list.append("-")
						
						snp_str_list.append(snp["name"])

						if "url" in snp:
							if snp["url"].count('pubmed') > 0:
								disease_mutations["pmids"] += snp["url"].split("/")[-1].split(",") 

						disease_mutations['snp_str'].append("|".join(snp_str_list))
						disease_mutations['snps'].append(snp)

				disease_mutations['snp_str'] = "; ".join(disease_mutations['snp_str'])
				
				self.data[bait][library]['phage_peptools_data'][hit_id]['SNP'] = disease_mutations
				
			except:
				logger.error("AnnotatePeptides annotating mutations: " + hit_id)
				utilities_error.printError()
			
		else:
			self.data[bait][library]['phage_peptools_data'][hit_id]['SNP'] = disease_mutations

		####

		self.data[bait][library]['phage_peptools_data'][hit_id]['disease_mutants'] = ""
		self.data[bait][library]['phage_peptools_data'][hit_id]['disease_mutant_check'] = False

		try:
			if library not in ["Rand"]:
				if "snps" in self.data[bait][library]['phage_peptools_data'][hit_id]['SNP']:
					if len(self.data[bait][library]['phage_peptools_data'][hit_id]['SNP']["snps"]) > 0:
						self.data[bait][library]['phage_peptools_data'][hit_id]['disease_mutants'] = self.data[bait][library]['phage_peptools_data'][hit_id]['SNP']
						self.data[bait][library]['phage_peptools_data'][hit_id]['disease_mutant_check'] = True
		except:
			utilities_error.printError()

	####################################
	
	def setPeptideQualityChecks(self,library,bait,hit_id):
		
		quality_checks = {
		"specific_check":False,
		"specific_replicate_check":False,
		"specificity_determinant_check":False,
		"continue_check":False,
		"overlap_check":False,
		"specific_overlap_check":False,
		"replicate_check":False,
		"highcount_check":False,
		"duplicates_check":False,
		"motif_check":False,
		"interaction_check":False,
		"structure_check":False,
		"consensus_check":False
		}

		if self.data[bait][library]['phage_peptools_data'][hit_id]['replicates'] <= 1:
			if self.options['require_replicate']:
				quality_checks['continue_check'] = True
		else:
			quality_checks['replicate_check'] = True

		if int(self.data[bait][library]['phage_peptools_data'][hit_id]['overlap']) == 0:
			if self.options['require_overlap']:
				quality_checks['continue_check'] = True
		else:
			quality_checks['overlap_check'] = True

		if 'promiscuity' not in self.data[bait][library]['phage_peptools_data'][hit_id]:
			self.data[bait][library]['phage_peptools_data'][hit_id]['promiscuity'] = 1

		if self.data[bait][library]['phage_peptools_data'][hit_id]['promiscuity'] > self.options["promiscuity_cutoff"]:
			if self.options['require_specific']:
					quality_checks['continue_check'] = True
		else:
			if self.options['require_specific']:
				quality_checks['continue_check'] = True

			quality_checks['specific_check'] = True

			if quality_checks['replicate_check']:
				quality_checks['specific_replicate_check'] = True
			else:
				if self.options['require_specific_replicate']:
					quality_checks['continue_check'] = True

			if quality_checks['overlap_check']:
				quality_checks['specific_overlap_check'] = True

		if self.data[bait][library]['phage_peptools_data'][hit_id]['mean_count_proportion'] < self.options['mean_count_proportion_cutoff']:
			if self.options['require_highcount']:
				quality_checks['continue_check'] = True
		else:
			quality_checks['highcount_check'] = True

		if library not in ["Rand"]:
			if not self.data[bait][library]['phage_peptools_data'][hit_id]['primary_duplicate']:
				if self.options['remove_duplicates'] and len(self.data[bait][library]['phage_peptools_data'][hit_id]['curated_motifs']) > 0:
					quality_checks['continue_check'] = True
			else:
				quality_checks['duplicates_check'] = True
		else:
			quality_checks['duplicates_check'] = True

		if library not in ["Rand"]:
			if len(self.data[bait][library.split("-")[0]]['phage_peptools_data'][hit_id]['curated_motifs']) == 0:
				if self.options['require_motifs']:
					quality_checks['continue_check']  = True
			else:
				quality_checks['motif_check']  = True
		
		if library not in ["Rand"]:
			if self.data[bait][library]['phage_peptools_data'][hit_id]['interactor_str'] == "False":
				if self.options['require_interaction']:
					quality_checks['continue_check'] = True
			else:
				quality_checks['interaction_check'] = True

		if "solved_structure" in self.data[bait][library]['phage_peptools_data'][hit_id]:
			if self.data[bait][library]['phage_peptools_data'][hit_id]['solved_structure'] == "False":
				if self.options['require_structure']:
					quality_checks['continue_check ']= True
			else:
				quality_checks['structure_check'] = True

		if self.options['require_mutagenesis']:
			if 'Mutagenesis' not in self.data[bait][library]['phage_peptools_data'][hit_id]:
				quality_checks['continue_check'] = True
				if len(self.data[bait][library]['phage_peptools_data'][hit_id]['Mutagenesis']) == 0:
					quality_checks['continue_check'] = True

		if self.options['require_snp']:
			if 'SNP' not in self.data[bait][library]['phage_peptools_data'][hit_id]:
				quality_checks['continue_check'] = True

				if len(self.data[bait][library]['phage_peptools_data'][hit_id]['SNP']) == 0:
					quality_checks['continue_check'] = True

		if self.options['require_region']:
			if 'Region' not in self.data[bait][library]['phage_peptools_data'][hit_id]:
				quality_checks['continue_check'] = True
				if len(self.data[bait][library]['phage_peptools_data'][hit_id]['Region']) == 0:
					quality_checks['continue_check'] = True

		if self.options['protein_accession'] == None:
			pass
		else:
			if self.data[bait][library]['phage_peptools_data'][hit_id]['ProteinAcc'] not in self.options['protein_accession']: 
				quality_checks['continue_check'] = True

		if self.data[bait][library]['phage_peptools_data'][hit_id]["consensus_match"] not in ["n/d",""]:
			quality_checks['consensus_check'] = True

		self.data[bait][library]['phage_peptools_data'][hit_id].update(quality_checks)

		self.bait_stats['specific'].append(quality_checks['specific_check'])
		self.bait_stats['specific_replicate'].append(quality_checks['specific_replicate_check'])
		self.bait_stats['overlap'].append(quality_checks['overlap_check'])
		self.bait_stats['specific_overlap'].append(quality_checks['specific_overlap_check'])
		self.bait_stats['replicate'].append(quality_checks['replicate_check'])
		self.bait_stats['highcount'].append(quality_checks['highcount_check'])
		self.bait_stats['duplicates'].append(quality_checks['duplicates_check'])
		self.bait_stats['motifs'].append(quality_checks['motif_check'])
		self.bait_stats['interaction'].append(quality_checks['interaction_check'])
		self.bait_stats['structure'].append(quality_checks['structure_check'])

		if len(self.elm_manual_curation[bait]) > 0: #Only count if there's a motif consensus defined
			self.bait_stats['consensus'].append(quality_checks['consensus_check'])

	####################################

	def setPeptideConsensusConfidence(self,library,bait,hit_id):
		confidence_level = 0
		confidence_level_texts = []

		try:
			if (self.data[bait][library]['phage_peptools_data'][hit_id]['replicate_check']):
				confidence_level += 1
				confidence_level_texts.append('replicated')

			if (self.data[bait][library]['phage_peptools_data'][hit_id]['overlap_check']):
				confidence_level += 1
				confidence_level_texts.append('overlapped')

			if (self.data[bait][library]['phage_peptools_data'][hit_id]['specificity_determinant_check']):
				confidence_level += 1
				confidence_level_texts.append('motif')

			if (self.data[bait][library]['phage_peptools_data'][hit_id]['highcount_check']):
				confidence_level += 1
				confidence_level_texts.append('highcount')

			if (self.data[bait][library]['phage_peptools_data'][hit_id]['specific_check']) == False:
				confidence_level = -confidence_level
				confidence_level_texts.append('promiscuous')
		except:
			pass
		
		confidence_level_texts = "|".join(confidence_level_texts)
		self.data[bait][library]['phage_peptools_data'][hit_id]['confidence'] = confidence_level
		self.data[bait][library]['phage_peptools_data'][hit_id]['confidence_level_text'] =  confidence_level_texts
		logger.debug(hit_id + "\t" + str(confidence_level) + "\t" + confidence_level_texts)

		return [confidence_level,confidence_level_texts]

	###################################################################################################################################################################################
	###  CALCULATE PRIMARY DUPLICATES
	###################################################################################################################################################################################

	def setPeptidesPrimaryDuplicate(self,library,bait):
		"""
		Set primary duplicates for all peptides in a screen

		Args:
			library (str): library identifier of library for processing
			bait (str): bait identifier of bait for processing
		"""

		for hit_id in self.use_hits:
			try:
				self.setPeptidePrimaryDuplicate(library,bait,hit_id)
			except Exception as e:
				logger.debug("setPeptidesPrimaryDuplicate failed for " + library + " - " + bait + " - " +hit_id)
				utilities_error.printError()

	###---------------------------------------------------------###

	def findDuplicatedPeptides(self,library,bait):
		"""
		Find duplicate peptides in a screen

		Args:
			library (str): library identifier of library for processing
			bait (str): bait identifier of bait for processing
		"""

		sorted_hit_ids = self.use_hits
		sorted_hit_ids.sort()

		for hit_id in sorted_hit_ids:
			try:
				if self.data[bait][library]['phage_peptools_data'][hit_id]['Hit'] not in self.duplicates:
					self.duplicates[self.data[bait][library]['phage_peptools_data'][hit_id]['Hit']] = {}

				if 'uniref50_index' in self.data[bait][library]['phage_peptools_data'][hit_id]:
					if self.data[bait][library]['phage_peptools_data'][hit_id]['uniref50_index'] == "?":
						self.duplicates[self.data[bait][library]['phage_peptools_data'][hit_id]['Hit']][hit_id] = 10000 + random.random()
					elif library in ["Rand"]:
						self.duplicates[self.data[bait][library]['phage_peptools_data'][hit_id]['Hit']][hit_id] = 0
					else:
						self.duplicates[self.data[bait][library]['phage_peptools_data'][hit_id]['Hit']][hit_id] = len(self.duplicates[self.data[bait][library]['phage_peptools_data'][hit_id]['Hit']])
						logger.debug("findDuplicatedPeptides failed for " + library + " - " + bait + " - " +hit_id)
				else:
					self.duplicates[self.data[bait][library]['phage_peptools_data'][hit_id]['Hit']][hit_id] = len(self.duplicates[self.data[bait][library]['phage_peptools_data'][hit_id]['Hit']])
					logger.debug("findDuplicatedPeptides failed for " + library + " - " + bait + " - " +hit_id)
				
			except Exception as e:
				logger.error("findDuplicatedPeptides failed for " + library + " - " + bait + " - " +hit_id)
				utilities_error.printError()

	###################################################################################################################################################################################
	###  FILTER LOW COMPLEXITY PEPTIDES
	###################################################################################################################################################################################

	def filterPeptidesByComplexity(self,library,bait):
		"""
		Filter peptides below a complexity cut-off from downstream part of the analysis. Sets self.use_hits as the peptide identifiers that have been retained.

		Args:
			library (str): library identifier of library for processing
			bait (str): bait identifier of bait for processing
		"""

		sorted_hit_ids = list(self.data[bait][library]['phage_peptools_data'].keys())
		sorted_hit_ids.sort()

		for hit_id in sorted_hit_ids:
			try:
				if self.phageDataUtilitiesObj.complexity(self.data[bait][library]['phage_peptools_data'][hit_id]['Hit']):
					self.use_hits.append(hit_id)
				else:
					logger.info("Skipping " + self.data[bait][library]['phage_peptools_data'][hit_id]['Hit'])
			except Exception as e:
				logger.debug("filterPeptidesByComplexity failed for " + library + " - " + bait + " - " + hit_id)
				
				utilities_error.printError()

		self.use_hits.sort()

	###################################################################################################################################################################################
	###  SET TARGET DEVELOPMENT LEVEL
	###################################################################################################################################################################################

	def annotateTargetDevelopmentLevel(self,library,bait):
		accessions = []
				
		for hit_id in list(self.data[bait][library]['phage_peptools_data'].keys()):
			if 'ProteinAcc' in self.data[bait][library]['phage_peptools_data'][hit_id]:
				proteinAccession = self.data[bait][library]['phage_peptools_data'][hit_id]['ProteinAcc']
				accessions.append(proteinAccession)

		options = {'accession':accessions}
		pharos_response = queryRunner.queryRunner("druggability","parse_target_development_level",options).run()

		for hit_id in list(self.data[bait][library]['phage_peptools_data'].keys()):
			if 'ProteinAcc' in self.data[bait][library]['phage_peptools_data'][hit_id]:
				proteinAccession = self.data[bait][library]['phage_peptools_data'][hit_id]['ProteinAcc']

				if proteinAccession in pharos_response['data']:
					self.data[bait][library]['phage_peptools_data'][hit_id]['target_development_level'] = pharos_response['data'][proteinAccession]

	###################################################################################################################################################################################
	###  SET BINARY CHECKS FOR PEPTIDE
	###################################################################################################################################################################################

	def setPeptidesQualityChecks(self,library,bait):
		"""
		Sets the set binary checks for all peptides in the screen
		
		Args:
			library (str): library identifier of library for processing
			bait (str): bait identifier of bait for processing
		"""
		
		for hit_id in self.use_hits:
			try:
				self.setPeptideQualityChecks(library,bait,hit_id)
			except Exception as e:
				logger.error("setPeptidesQualityChecks failed for " + library + " - " + bait + " - " + hit_id)
				utilities_error.printError()
		
	####################################################################################################################################################################################
	####################################################################################################################################################################################
	### MUTANT MUTANT MUTANT MUTANT MUTANT MUTANT MUTANT MUTANT MUTANT MUTANT MUTANT MUTANT MUTANT MUTANT MUTANT MUTANT MUTANT MUTANT MUTANT MUTANT MUTANT MUTANT MUTANT MUTANT ########
	### MUTANT MUTANT MUTANT MUTANT MUTANT MUTANT MUTANT MUTANT MUTANT MUTANT MUTANT MUTANT MUTANT MUTANT MUTANT MUTANT MUTANT MUTANT MUTANT MUTANT MUTANT MUTANT MUTANT MUTANT ########
	####################################################################################################################################################################################
	####################################################################################################################################################################################

	def makeMutationComparsion(self,library,bait):
		"""
		Calculates the wildtype/mutant comparison metrics for screens of library which have designed mutations

		Args:
			library (str): library identifier of library for processing
			bait (str): bait identifier of bait for processing
		"""

		self.createMutantComparisonData()

		for peptide in self.mutant_comparison:
			try:
				for mutant_peptide in self.mutants_present[peptide]:
					mutant_peptide = self.mutations_re_mapping_info[mutant_peptide]['designed_peptide']
					if 'counts' in self.mutant_comparison[peptide]['wildtype']:
						peptide_selections = list(self.mutant_comparison[peptide]['wildtype']['counts']['peptide_selection_data'].keys())
					elif mutant_peptide in self.mutant_comparison[peptide]['mutants']:
						peptide_selections = list(self.mutant_comparison[peptide]['mutants'][mutant_peptide]['counts']['peptide_selection_data'].keys())
					else:
						logger.debug("Mutant peptide not found: " +  peptide + " ->" + mutant_peptide)
						continue

					peptide_selections.sort()

					count_checks = []
					wildtype_selections_counts = []
					mutants_selections_counts = []
					delta_selections_counts = []
					wildtype_counts_sum = 0
					mutants_counts_sum = 0

					for peptide_selection in peptide_selections:
						if 'counts' in self.mutant_comparison[peptide]['wildtype']:
							wildtype_counts = list(self.mutant_comparison[peptide]['wildtype']['counts']['peptide_selection_data'][peptide_selection].values())
						else:
							wildtype_counts = [0]

						mutants_counts = [0]
						if mutant_peptide in self.mutant_comparison[peptide]['mutants']:
							if 'counts' in self.mutant_comparison[peptide]['mutants'][mutant_peptide]:
								mutants_counts = list(self.mutant_comparison[peptide]['mutants'][mutant_peptide]['counts']['peptide_selection_data'][peptide_selection].values())

						wildtype_counts_sum =  sum(wildtype_counts)
						mutants_counts_sum =  sum(mutants_counts)

						wildtype_selections_counts.append(wildtype_counts_sum)
						mutants_selections_counts.append(mutants_counts_sum)
						delta_selections_counts.append(sum(wildtype_counts) - sum(mutants_counts))

					if wildtype_counts_sum == 0 and mutants_counts_sum == 0:
						count_checks.append(0)
					elif wildtype_counts_sum > 0 and mutants_counts_sum > 0:
						count_checks.append(0)
					elif wildtype_counts_sum > 0 and mutants_counts_sum == 0:
						count_checks.append(-1)
					elif wildtype_counts_sum == 0 and mutants_counts_sum > 0:
						count_checks.append(1)

					if (sum(wildtype_selections_counts) + sum(mutants_selections_counts)) > 0:
						selections_ratio = float(sum(delta_selections_counts))/(sum(wildtype_selections_counts) + sum(mutants_selections_counts))
					else:
						selections_ratio = 0.0

					if mutant_peptide in self.mutant_comparison[peptide]['mutants']:
						mutant_hit_id = self.mutant_comparison[peptide]['mutants'][mutant_peptide]['hit_id']

						self.data[bait][library]['phage_peptools_data'][mutant_hit_id]['selections_ratio'] = selections_ratio
						self.data[bait][library]['phage_peptools_data'][mutant_hit_id]['wildtype_selections_counts'] = wildtype_selections_counts
						self.data[bait][library]['phage_peptools_data'][mutant_hit_id]['mutants_selections_counts'] = mutants_selections_counts
						self.data[bait][library]['phage_peptools_data'][mutant_hit_id]['delta_selections_counts'] = delta_selections_counts
						self.data[bait][library]['phage_peptools_data'][mutant_hit_id]['count_checks'] = count_checks

			except Exception as e:
				logger.error(peptide + " " + mutant_peptide + " " + e)
		
	###---------------------------------------------------------###
						
	def createMutantComparisonData(self,library,bait):
		"""
		Makes and populates the wildtype/mutant comparison data structure for screens of library which have designed mutations

		Args:
			library (str): library identifier of library for processing
			bait (str): bait identifier of bait for processing
		"""
	
		self.mutant_comparison = {}

		for hit_id in self.use_hits:
			if self.options['mutant'] != None:
				wildtype_peptide = self.data[bait][library]['phage_peptools_data'][hit_id]["Hit"]
				library_peptide = self.data[bait][library]['phage_peptools_data'][hit_id]["Library_Peptide"]
				mutant_peptide = library_peptide

				if wildtype_peptide not in self.mutant_comparison:
					self.mutant_comparison[wildtype_peptide] = {'wildtype':{"hit_id":hit_id},"mutants":{}}

				if self.data[bait][library]['phage_peptools_data'][hit_id]["Designed_Mutant"]:
					self.mutant_comparison[wildtype_peptide]['mutants'][mutant_peptide] = {
						"hit_id":hit_id, 
						"counts":{
							"peptide_count_summer":self.data[bait][library]['phage_peptools_data'][hit_id]['peptide_counts'] ,
							"peptide_selection_data":self.data[bait][library]['phage_peptools_data'][hit_id]['peptide_selection_data'] 
							}
						}
				else:
					self.mutant_comparison[wildtype_peptide]['wildtype']["counts"] = {
						"peptide_count_summer":self.data[bait][library]['phage_peptools_data'][hit_id]['peptide_counts'] ,
						"peptide_selection_data":self.data[bait][library]['phage_peptools_data'][hit_id]['peptide_selection_data'] 
						}

					if len(self.data[bait][library]['phage_peptools_data'][hit_id]['peptide_counts'] ) == 0:
						logger.info("peptide_count_summer is empty")
						sys.exit()

	###---------------------------------------------------------###

	def checkMutantMapping(self,hit_id):
		"""
		Check whether a mutant peptide is in the designed library

		Args:
			hit_id (str): unique peptide identifier

		Returns:
			bool: True/False - whether a mutant peptide is in the designed library
		"""
		if self.options['mutant'] != None:
			if self.data[bait][library]['phage_peptools_data'][hit_id]["Library_Peptide"] not in self.mutations_re_mapping_info:
				logger.info(self.data[bait][library]['phage_peptools_data'][hit_id]["Library_Peptide"] + " - " + self.data[bait][library]['phage_peptools_data'][hit_id]["Hit"] + " PEPTIDE NOT IN LIBRARY")
				
				return True
		
		return False

	####################################################################################################################################################################################
	####################################################################################################################################################################################
	### SLIMFINDER ONLY  SLIMFINDER ONLY  SLIMFINDER ONLY  SLIMFINDER ONLY  SLIMFINDER ONLY  SLIMFINDER ONLY  SLIMFINDER ONLY  SLIMFINDER ONLY  SLIMFINDER ONLY  SLIMFINDER ONLY  ######
	### SLIMFINDER ONLY  SLIMFINDER ONLY  SLIMFINDER ONLY  SLIMFINDER ONLY  SLIMFINDER ONLY  SLIMFINDER ONLY  SLIMFINDER ONLY  SLIMFINDER ONLY  SLIMFINDER ONLY  SLIMFINDER ONLY  ######
	####################################################################################################################################################################################
	####################################################################################################################################################################################

	def runSLiMFinderOnlyAnalysis(self):
		"""
		Endpoint analysis that runs only SLiMFinder on the selected screens
		"""
		phageEnrichedMotifFinderObj = phageEnrichedMotifFinder.phageEnrichedMotifFinder()	
		phageEnrichedMotifFinderObj.options = self.options
		phageEnrichedMotifFinderObj.data = self.data
		phageEnrichedMotifFinderObj.libraries = self.libraries
		phageEnrichedMotifFinderObj.selection_day_replicate_mapping = self.selection_day_replicate_mapping
		phageEnrichedMotifFinderObj.runSLiMFinderOnlyAnalysis()	

	####################################################################################################################################################################################
	####################################################################################################################################################################################
	### PROCESS PROCESS PROCESS PROCESS PROCESS PROCESS PROCESS PROCESS PROCESS PROCESS PROCESS PROCESS PROCESS PROCESS PROCESS PROCESS PROCESS PROCESS PROCESS PROCESS PROCESS ######## 
	### PROCESS PROCESS PROCESS PROCESS PROCESS PROCESS PROCESS PROCESS PROCESS PROCESS PROCESS PROCESS PROCESS PROCESS PROCESS PROCESS PROCESS PROCESS PROCESS PROCESS PROCESS ######## 
	####################################################################################################################################################################################
	####################################################################################################################################################################################

	def initialiseScreenDataStructures(self):
		"""
		Sets up the required data structures for the screen processing.
		"""		

		self.use_hits = []
		self.use_peptides = []
		self.duplicates = {}
		self.bait_stats = {
			'specific':[],
			'specific_replicate':[],
			'overlap':[],
			'specific_overlap':[],
			'replicate':[],
			'highcount':[],
			'duplicates':[],
			'motifs':[],
			'interaction':[],
			'structure':[],
			'consensus':[],
			'elm_interaction':[],
			'complex_structure':[]
		}

	###---------------------------------------------------------###

	def processScreens(self):
		"""
		Iterates through and processes each screen in the analysis. 
		Sets up the required data structures, annotates the peptide mapping, 
		filters the peptides and process the returned peptides
		"""
	
		sorted_baits = list(self.data.keys())
		sorted_baits.sort()
		bait_counter = 0

		for bait in sorted_baits:
			if bait not in self.elm_manual_curation: self.elm_manual_curation[bait] = {}
			bait_counter +=1
			library_counter = 0
			for library in self.data[bait]:
				library_counter += 1
				
				logger.info("#"*80)
				logger.info("Process Screen")
				logger.info("Bait: " + bait + " - " + str(bait_counter) +  "/" + str(len(sorted_baits)))
				logger.info("Library: " + library + " - " + str(library_counter) + "/" + str(len(self.data[bait])))
				logger.info("#"*80)
				logger.info("#"*80)
				logger.info("Annotate Screens: " + library + " - " + bait)
				
				###---------------------------------------------------------###
				### ANNOTATE SCREEN FOR BAIT
				###---------------------------------------------------------###
				
				if not self.data[bait][library]['status']: continue

				if self.options['use_selection_day_replicates']:
					if bait not in self.selection_day_replicate_mapping:
						logger.error("Bait not in selection_day_replicate_mapping: "  + library + " - " + bait)
				
						continue

					if library not in self.selection_day_replicate_mapping[bait]:
						logger.error("Library not in selection_day_replicate_mapping: "  + library + " - " + bait)
						continue
				
				if library == "Rand":
					try:
						self.annotatePeptidesRand(library,bait)
					except Exception as e:
						logger.error("AnnotatePeptidesRand: "  + library + " - " + bait)
						
				else:
					try:
						self.annotatePeptidesScreen(library,bait)
					except Exception as e:
						logger.error("AnnotatePeptidesScreen: "  + library + " - " + bait)
						
						

				###---------------------------------------------------------###
				### PROCESS SCREEN FOR BAIT
				###---------------------------------------------------------###
				
				try:
					### INITIALISE DATA STRUCTURES FOR SCREEN
					self.initialiseScreenDataStructures()
	
					###---------------------------------------------------------###

					if not self.data[bait][library]['status']:
						continue

					if 'phage_peptools_data' not in self.data[bait][library]: 
						continue

					if self.data[bait][library]['replicates_count'] < 1:
						logger.info("Skipping bait. Only one screen found")
						continue

					###---------------------------------------------------------###
					
					self.data[bait][library]['screens_count'] = len(self.data[bait][library]['screens'])

					### ANNOTATE MAPPED AND UNMAPPED PEPTIDES
					self.annotateScreenInteractingPeptides(library,bait)

					### FILTER LOW COMPLEXITY PEPTIDES
					self.filterPeptidesByComplexity(library,bait)

					### PROCESS SCREEN PEPTIDES
					self.processPeptidesScreen(library,bait)

					## SET SCREEN STATS
					self.setScreenMetadata(library,bait)
				except Exception as e:
					logger.error("initialiseScreenDataStructures: "  + library + " - " + bait)
					utilities_error.printError()

				logger.info("#"*80)
				
				###---------------------------------------------------------###
				### WRITE SCREEN OUTPUT FILES
				###---------------------------------------------------------###
				try:
					self.createScreenOuputs(library,bait)
				except Exception as e:
					logger.error("createScreenOuputs: "  + library + " - " + bait)
				
					utilities_error.printError()

		###---------------------------------------------------------###
		### ANALYSIS METADATA
		###---------------------------------------------------------###

		self.createStatsOutput()

	###---------------------------------------------------------###

	def processPeptidesScreen(self,library,bait):
		"""
		Process peptides returned by the screen defined by library and bait. Wrapper for multiple other functions. All data is updated in the self.data dictionary.

		Args:
			library (str): library identifier of library for processing
			bait (str): bait identifier of bait for processing
		"""
	
		###---------------------------------------------------------###
		### SET CONFIDENCE METRICS - Overlaps, Replicates, Normalised Peptides Frequencies
		###---------------------------------------------------------###
		
		self.setPeptidesNormalisedPeptideFrequencies(library,bait)
		self.setPeptidesOverlaps(library,bait)	
		self.setPeptidesReplicateCount(library,bait)
		
		###---------------------------------------------------------###

		for hit_id in self.use_hits:
			try:
				logger.debug(library + " - " + bait + " - " + hit_id)
				
				### Check if the peptide is a designed mutated peptide
				if self.checkMutantMapping(hit_id):   
					continue
			
				### PROMICUITY CHECK ####
				self.setPeptidePromiscuity(library,bait,hit_id)

				### CONSENSUS MATCH CHECK ####
				self.setPeptideConsensusMatches(library,bait,hit_id)

				if library in ["Rand"]:
					continue

				### ADD SNPS ###
				self.setPeptideSNPsData(library,bait,hit_id)

				### ADD MOTIFS ###
				self.setPeptideCuratedMotifs(library,bait,hit_id)
				
				### SET INTERACTOR
				self.setPeptideInteractorPeptools(library,bait,hit_id)
				
				### SET SHARED SIGNIFICANT ONTOLOGY
				self.setPeptideSharedSignificantOntology(library,bait,hit_id)

				### RELATED PROTEINS CHECK ####
				self.setPeptideRelatedProteins(library,bait,hit_id)

			except Exception as e:
				logger.error("ProcessPeptidesScreen: "  + library + " - " + bait)
				utilities_error.printError()

		if library not in ["Rand"]:
			### FIND DUPLICATES
			self.findDuplicatedPeptides(library,bait)

			### SET PRIMARY DUPLICATE STATUS
			self.setPeptidesPrimaryDuplicate(library,bait)

			### SET PRIMARY DUPLICATE STATUS
			self.annotateTargetDevelopmentLevel(library,bait)
				
		### SET BINARY CHECKS FOR THE PEPTIDE
		self.setPeptidesQualityChecks(library,bait)
			
		### MAKE DATA FOR MUTATION LIBRARY
		if self.options['mutant'] != None:
			self.makeMutationComparsion(library,bait)
			
		###---------------------------------------------------------###
		### SET FINAL CONFIDENCE METRICS - Specificity Determinant Score
		###---------------------------------------------------------###

		self.setPeptidesSpecificityDeterminantScore(library,bait)

		###---------------------------------------------------------###
		### SET CONSENSUS CONFIDENCE LEVEL
		###---------------------------------------------------------###

		for hit_id in self.data[bait][library]['phage_peptools_data']:
			self.setPeptideConsensusConfidence(library,bait,hit_id)
			
	####################################################################################################################################################################################
	####################################################################################################################################################################################
	### OUTPUTS OUTPUTS OUTPUTS OUTPUTS OUTPUTS OUTPUTS OUTPUTS OUTPUTS OUTPUTS OUTPUTS OUTPUTS OUTPUTS OUTPUTS OUTPUTS OUTPUTS OUTPUTS OUTPUTS OUTPUTS OUTPUTS OUTPUTS OUTPUTS ########
	####################################################################################################################################################################################
	####################################################################################################################################################################################
	
	def createScreenOuputs(self,library,bait):
		"""
		Write screen output files
		"""
		
		phageDataWriterObj = phageDataWriter()
		phageDataWriterObj.options = self.options

		## MAKE JSON PEPTIDES OUTPUT FILE
		peptide_data = self.data[bait][library]['phage_peptools_data']
		phageDataWriterObj.createPeptideOutputJSON(library,bait,peptide_data)

		## MAKE TDT PEPTIDES OUTPUT FILE
		peptide_data = self.data[bait][library]['phage_peptools_data']
		phageDataWriterObj.createPeptideOutputTDT(library,bait,peptide_data)

		## MAKE JSON DETAILS OUTPUT FILE
		stats_json = self.stats[bait][library]
		selection_day_replicate_mapping = self.selection_day_replicate_mapping[bait][library]
		bait_data = self.bait_data[bait]

		phageDataWriterObj.createMetadataOutput(library,bait,stats_json,bait_data,selection_day_replicate_mapping)
		del phageDataWriterObj
	
	###---------------------------------------------------------###

	def createStatsOutput(self):
		phageDataWriterObj = phageDataWriter()
		phageDataWriterObj.options = self.options
		phageDataWriterObj.createStatsOutput(self.stats)
		del phageDataWriterObj

	###---------------------------------------------------------###

	def createProteinOutput(self):
		phageDataWriterObj = phageDataWriter()
		phageDataWriterObj.options = self.options
		phageDataWriterObj.data = self.data
		phageDataWriterObj.bait_data = self.bait_data
		phageDataWriterObj.createProteinsOutputJSON()
		del phageDataWriterObj

	###---------------------------------------------------------###

	def showScreens(self):
		phageDataWriterObj = phageDataWriter()
		phageDataWriterObj.options = self.options
		phageDataWriterObj.data = self.data
		phageDataWriterObj.bait_data = self.bait_data
		phageDataWriterObj.libraries = self.libraries
		phageDataWriterObj.showScreens()
		del phageDataWriterObj


	####################################################################################################################################################################################
	####################################################################################################################################################################################
	### RUN RUN RUN RUN RUN RUN RUN RUN RUN RUN RUN RUN RUN RUN RUN RUN RUN RUN RUN RUN RUN RUN RUN RUN RUN RUN RUN RUN RUN RUN RUN RUN RUN RUN RUN RUN RUN RUN RUN RUN RUN RUN RUN ####
	####################################################################################################################################################################################
	####################################################################################################################################################################################

	def main(self):
		"""
		Entrypoint to run the analysis
		"""

		try:
			################################################################
			self.process_screen_files()

			if self.options['create_library_metadata']:
				self.setLibraryMetadata()
				sys.exit()
			elif self.options['slimfinder_only']:
				self.runSLiMFinderOnlyAnalysis()
				sys.exit()
			elif self.options['show_screens']:
				logger.info("Show Screens")
				self.showScreens()
				sys.exit()
			elif self.options['run_create_crapome']:
				logger.info("Create Crapome")
				self.showScreens()
				self.createCrapome()
				sys.exit()
			else:
				self.showScreens()

			################################################################

			logger.info("Create Crapome")
			self.createCrapome()

			################################################################

			logger.info("Process Screens")
			self.processScreens()

			################################################################

			if self.options['run_create_protein_output']:
				logger.info("create Protein Output")
				self.createProteinOutput()
			
			################################################################

		except Exception as e:
			logger.error(e)
			raise

############################################################################################################
############################################################################################################