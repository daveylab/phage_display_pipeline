#!/usr/bin/python3
#
# Program:      ProP-PD Data Analysis Pipeline
# Class:        phageDataProteinAnnotator
# Description:  Analysis pipeline to process peptides returned from replicated ProP-PD selections
# Version:      1.0.0
# Last Edit:    1/1/2021
# Citation:     Under Preparation
# Author contact: Norman E. Davey <norman.davey@icr.ac.uk> Institute Of Cancer Research, Chelsea, SW3 6JB, London, United Kingdom.
# 
# License:
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http:#www.gnu.org/licenses/>.
#
# Copyright (C) 2020  Norman E. Davey


__doc__ = """
# ProP-PD Data Analysis Pipeline
### Code for the analysis tools for the ProP-PD data

"""

import os
import sys

#-----
import logging
logger = logging.getLogger(__name__)
#-----

sys.path.append(os.path.join(os.path.dirname(__file__),"../../data_management"))
import queryManager

sys.path.append(os.path.join(os.path.dirname(__file__),"../../utilities"))
import utilities_error

class phageDataProteinAnnotator():

	####-----------####
	
	def __init__(self):
		pass
		
	####################################

	def get_protein_details(self,accession,start,end):
		protein_details = {
			'targetting_compounds':{},
			'druggability': "N/A",
			'domain':{}
		}
		try:
			if start == "" or end == "":
				start=None
				end=None

			protein_details['region_start'] = start
			protein_details['region_end'] = end
			protein_details['protein'] = self.get_protein_uniprot_details(accession)
			protein_details['domain'] = self.get_domain_details(accession,start,end)
			protein_details['motif'] = self.get_elm_details(protein_details)
			protein_details['druggability'] = self.get_druggability_details(accession)
			protein_details['targetting_compounds'] = self.get_targetting_compounds_details(accession)
			
		except:
			logger.error("Bait data retrieval failed: " + accession)
			utilities_error.printError()
			
		return protein_details

	####################################

	def get_protein_uniprot_details(self,accession):
		uniprot_details = {}
		try:
			queryManagerObj = queryManager.queryManager()
			queryManagerObj.options['dataset_type'] = "uniprot"
			queryManagerObj.options['task'] = "parse_basic"
			queryManagerObj.options['accession'] = accession					
			uniprot_details = queryManagerObj.main()['data']
			del queryManagerObj
		except:
			logger.error("Bait data retrieval failed: " + accession)
			utilities_error.printError()

		return uniprot_details

	####################################

	def get_domain_details(self,accession,start,end):
		pfam_details = {}
		try:
			queryManagerObj = queryManager.queryManager()
			queryManagerObj.options['dataset_type'] = "uniprot"
			queryManagerObj.options['task'] = "parse_uniprot_pfam"
			queryManagerObj.options['region_start'] = start
			queryManagerObj.options['region_end'] = end
			queryManagerObj.options['accession'] = accession				
			pfam_details = queryManagerObj.main()['data']
			del queryManagerObj
		except:
			logger.error("Bait data retrieval failed: " + accession)
			utilities_error.printError()

		return pfam_details

	####################################

	def get_elm_details(self,protein_details):
		elm = []
		try:
			for domain_id in protein_details['domain']:
					for tag in ['domain_name', 'clan_name',  'pfam_id']:
						if tag in protein_details['domain'][domain_id]:
							if protein_details['domain'][domain_id][tag] in self.elm_curation['elm_domains']:
								elm += self.elm_curation['elm_domains'][protein_details['domain'][domain_id][tag]]
		except:
			logger.error("Bait data retrieval failed" + accession)
			utilities_error.printError()
			

		return elm

	####################################

	def get_druggability_details(self,accession):
		druggability = "N/A"

		try:
				queryManagerObj = queryManager.queryManager()
				queryManagerObj.options['dataset_type'] = "druggability"
				queryManagerObj.options['task'] = "parse_target_development_level"
				queryManagerObj.options['accession'] = accession					
				pharos_response = queryManagerObj.main()

				druggability = "N/A"
				if 'data' in pharos_response:
					if accession in pharos_response['data']:
						druggability = pharos_response['data'][accession]

				del queryManagerObj
		except:
			logger.error("Bait data retrieval failed" + accession)
			utilities_error.printError()

		return druggability

	####################################

	def get_targetting_compounds_details(self,accession):
		targetting_compounds = {}

		try:
			queryManagerObj = queryManager.queryManager()
			queryManagerObj.options['dataset_type'] = "druggability"
			queryManagerObj.options['task'] = "parse_chembl_protein_active_compounds"
			queryManagerObj.options['accession'] = accession			
			chembl_response = queryManagerObj.main()

			if 'data' in chembl_response:
				targetting_compounds = chembl_response['data']
			else:
				targetting_compounds = {}
			
			del queryManagerObj
		except:
			logger.error("Bait data retrieval failed" + accession)
			utilities_error.printError()

		return targetting_compounds