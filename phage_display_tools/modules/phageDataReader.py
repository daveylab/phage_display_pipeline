#!/usr/bin/python3
#
# Program:      ProP-PD Data Analysis Pipeline
# Class:        phageDataManager
# Description:  Analysis pipeline to process peptides returned from replicated ProP-PD selections
# Version:      1.0.0
# Last Edit:    1/1/2021
# Citation:     Under Preparation
# Author contact: Norman E. Davey <norman.davey@icr.ac.uk> Institute Of Cancer Research, Chelsea, SW3 6JB, London, United Kingdom.
# 
# License:
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http:#www.gnu.org/licenses/>.
#
# Copyright (C) 2020  Norman E. Davey


__doc__ = """
# ProP-PD Data Analysis Pipeline
### Code for the analysis tools for the ProP-PD data

"""

## TODO

####################################################################################################################################################################################
####################################################################################################################################################################################	

import os
import sys
import json
import inspect
import logging

from collections import Counter

#-----

file_path = os.path.abspath(os.path.dirname(inspect.stack()[0][1]))

#-----

sys.path.append(os.path.join(file_path,"../../"))
import config_reader

sys.path.append(os.path.join(os.path.dirname(__file__),"../../utilities"))
import utilities_error

#-----

import option_reader

import phageDataExternalDatasetsLoader 
import phageDataProteinAnnotator
import phageDataUtilities
import phagePeptoolsRunner

#-----
logger = logging.getLogger(__name__)
#-----

####################################################################################################################################################################################
####################################################################################################################################################################################	

class phageDataReader():
	
	####################################################################################################################################################################################
	####################################################################################################################################################################################
	### INITIALISE ANALYSIS INITIALISE ANALYSIS INITIALISE ANALYSIS INITIALISE ANALYSIS INITIALISE ANALYSIS INITIALISE ANALYSIS INITIALISE ANALYSIS INITIALISE ANALYSIS INITIALISE #####
	####################################################################################################################################################################################
	####################################################################################################################################################################################

	def __init__(self):
		"""
		Initialise all data structures and options for future analyses
		"""

		self.options = {}
		self.options.update(config_reader.load_configeration_options(sections=["general", "phage_libraries_manager"]))
		self.options.update(option_reader.load_commandline_options(self.options,self.options))
		
		if self.options['debug']:
			logging_level = logging.DEBUG
		elif self.options['verbose']:
			logging_level = logging.INFO
		else:
			logging_level = logging.CRITICAL 

		logging.basicConfig(
			format='%(asctime)s %(levelname)-8s [%(funcName)s] %(message)s',
			datefmt='%H:%M:%S',
			level=logging_level
			)

		self.setup_options()
		self.check_directory()
		self.setup_helper_objects()
		self.setup_data_structures()

	####################################

	def setup_options(self):
		"""
		Initialise options for future analyses
		"""

		self.options["peptools_data_path"] = os.path.join(self.options["data_path"],"phage_display_data")

		if self.options['protein_accession'] != None:
			self.options['protein_accession'] = self.options['protein_accession'].split(",")

		if self.options['library'] == None and self.options['bait'] == None:
			self.options['run_create_protein_output'] = True

		####
		
		self.options['slimfinder_use_overlap'] = True
		self.options['use_peptide_aa_freqs'] = True

		####

		self.library_info = {}
		with open(os.path.join(os.path.dirname(__file__),"../static/library_info.json" )) as outfile:
			self.library_info = json.load(outfile)
			
		if self.options['library'] == "intPM": self.options['mutant'] = os.path.join(self.options["peptools_data_path"],"mutations","intPM.tsv")

		self.read_analysis_bait_list()

	####################################

	def setup_helper_objects(self):
		"""
		Initialise helper objects for future analyses
		"""

		self.phagePeptoolsRunnerObj = phagePeptoolsRunner.phagePeptoolsRunner()
		self.phagePeptoolsRunnerObj.options = self.options
		
		###
		
		self.phageDataExternalDatasetsLoaderObj = phageDataExternalDatasetsLoader.phageDataExternalDatasetsLoader()
		self.phageDataExternalDatasetsLoaderObj.options = self.options
		self.elm_curation = self.phageDataExternalDatasetsLoaderObj.grabELMData()

		###

		self.phageDataUtilitiesObj = phageDataUtilities.phageDataUtilities()
		self.phageDataUtilitiesObj.options = self.options

	####################################

	def setup_data_structures(self):
		"""
		Initialise data structures for future analyses

		TODO Define all objects 
		"""
		
		self.data = {}
		self.data_files = {}
		self.bait_data = {}
		self.libraries = []
		self.screen_files = []
		self.use_screen = []
		self.selection_day_replicates = {}
		self.selection_day_replicate_mapping = {}
		self.stats = {}
		self.mutant_mapping = {}
		self.uniprot_mapping = {}
		self.bait_proteins = {}
		self.library = {}

		self.headers = [
		'overlap',
		'Hit',
		'mutant',
		'SeqStart',
		'SeqStop',
		'ProteinAcc',
		'GeneName',
		'ProteinName',
		'Virus',
		"motifs",
		'Region',
		'SNP',
		'Mutagenesis'
		]

	####################################
	
	def initialise_data_structures(self,library,bait,selection_id):
		"""
		Initialise data structures for a bait

		Args:
			library (str): library identifier of library for processing
			bait (str): bait identifier of bait for processing
			selection_id (str): selection identifier of selection for processing

		"""

		if bait not in self.data_files: self.data_files[bait] = {}
		if bait not in self.data:self.data[bait] = {}
		if bait not in self.stats: self.stats[bait] = {}

		if library not in self.data_files[bait]: self.data_files[bait][library] = {}
		if library not in self.libraries: self.libraries.append(library)
		if library not in self.data[bait]:self.data[bait][library] = {'screens':{},"status":False}
		if library not in self.stats[bait]: self.stats[bait][library] = {}

		self.data[bait][library]['screens'][selection_id] = {}
		self.data[bait][library]['screens'][selection_id]["details"] = {}

	####################################

	def check_directory(self):
		"""
		Check exist of requiredc directories and create if necessary
		"""

		if not os.path.exists(self.options["peptools_data_path"]):
			os.mkdir(self.options["peptools_data_path"])

		for dir_name in ["metadata","peptools","barcodes","peptide_counts","curation","annotated","json","proteins","libraries","specificity_determinants"]:
			if not os.path.exists(os.path.join(self.options["peptools_data_path"], dir_name)):
				os.mkdir(os.path.join(self.options["peptools_data_path"],dir_name ))

	####################################

	def read_analysis_bait_list(self):
		"""
		Read user file to define screen and selections to analyse a subset of the available data

		Format: tab-delimited

		Layout:
		[1] bait - bait identifier
		[2] library - library short code
		[3] control - True/False whether the screen is a control screen
		[4] use_screens - selections to use (only use these). All screens are used if empty
		[5] skip_screens  - selections to skip (only used when use_screens is empty)

		"""

		self.options['analysis_bait_list'] = None

		if self.options['analysis_bait_list_file'] != None:
			self.options['analysis_bait_list'] = {}
			for line in open(os.path.abspath(self.options['analysis_bait_list_file'])).read().split("\n"):

				line_bits = line.split("\t")
				
				bait = line_bits[0]
				library = line_bits[1]
				control = line_bits[2]
				use_screens = ""
				skip_screens = ""

				if len(line_bits) >= 4:
					use_screens = line_bits[3]
				
				if len(line_bits) >= 5:
					skip_screens = line_bits[4]
				
				if bait not in self.options['analysis_bait_list']:
					self.options['analysis_bait_list'][bait] = {
						"library":[],
						"control":control == "Control",
						"use_screens":[],
						"skip_screens":[]
						}
					
				self.options['analysis_bait_list'][bait]["library"] += library.split(",")
				if len(use_screens) > 0:
					self.options['analysis_bait_list'][bait]["use_screens"] += use_screens.split(",")
				
				if len(skip_screens) > 0:
					self.options['analysis_bait_list'][bait]["skip_screens"] += skip_screens.split(",")

	####################################################################################################################################################################################
	####################################################################################################################################################################################
	### PEPTOOLS PEPTOOLS PEPTOOLS PEPTOOLS PEPTOOLS PEPTOOLS PEPTOOLS PEPTOOLS PEPTOOLS PEPTOOLS PEPTOOLS PEPTOOLS PEPTOOLS PEPTOOLS PEPTOOLS PEPTOOLS PEPTOOLS PEPTOOLS PEPTOOLS #####
	### PEPTOOLS PEPTOOLS PEPTOOLS PEPTOOLS PEPTOOLS PEPTOOLS PEPTOOLS PEPTOOLS PEPTOOLS PEPTOOLS PEPTOOLS PEPTOOLS PEPTOOLS PEPTOOLS PEPTOOLS PEPTOOLS PEPTOOLS PEPTOOLS PEPTOOLS #####
	####################################################################################################################################################################################
	####################################################################################################################################################################################

	def getScreenPeptides(self,library,bait):
		"""
		Takes all peptides returned from the screen replicates and creates a peptide
		list for downstream analysis.

		NOTE Remove peptides that have >10 Alanines as they are set to wildcards in the
		     Peptools screen to allow Cysteine->Alanine mutations in the library design to 
			 be mapped. Peptides with >10 Alanines set as wildcards hit 100s of peptides.
		
		TODO ENHANCEMENT Map through library design to pre-discard non-library peptides and 
		     decrease PepTools runtime

		Args:
			library (str): library identifier of library for processing
			bait (str): bait identifier of bait for processing
		"""

		peptides = []

		if self.options['use_selection_day_replicates']:
			for replicate in self.selection_day_replicate_mapping[bait][library]:
				for selection_id in self.selection_day_replicate_mapping[bait][library][replicate]:
					for peptide in list(self.data[bait][library]['screens'][selection_id]['phage_peptide_data'].keys()):
						if len(peptide) - peptide.count("A") >= 5:
							peptides.append(peptide)
		else:
			for replicate in self.selection_day_replicate_mapping[bait][library]:
				primary_peptides = []
				for selection_id in self.selection_day_replicate_mapping[bait][library][replicate]:
					if list(self.selection_day_replicate_mapping[bait][library][replicate][selection_id].values())[0] == 'primary':
						for peptide in list(self.data[bait][library]['screens'][selection_id]['phage_peptide_data'].keys()):
							if len(peptide) - peptide.count("A") >= 5: 
								peptides.append(peptide)
								primary_peptides.append(peptide)

				if len(primary_peptides) < 100:
					for selection_id in self.selection_day_replicate_mapping[bait][library][replicate]:
						if list(self.selection_day_replicate_mapping[bait][library][replicate][selection_id].values())[0] != 'primary':
							for peptide in list(self.data[bait][library]['screens'][selection_id]['phage_peptide_data'].keys()):
								if len(peptide) - peptide.count("A") >= 5:
									peptides.append(peptide)

				del primary_peptides

		if len(peptides) == 0: return

		peptides = list(set(peptides))
		peptides.sort()
		return peptides	
		
	####################################
	
	def update_phage_peptools_data_dictionary(self,library,bait,hit):
		"""
		Takes PepTools peptide mapping hit, processes them and adds the important information to the scripts dictionary data structures.
		All data for the hit is added to the self.data dictionary - self.data[bait][library]['phage_peptools_data'][hit_id]
		
		Args:
			library (str): library identifier of library for processing
			bait (str): bait identifier of bait for processing
			hit (dictionary): PepTools peptide mapping hit
		"""
	
		hit_id = hit['ProteinAcc'] + "_" + str(hit['SeqStart']) + "_" + hit['Hit'].replace("C","A")
		
		logger.debug(library + " - " + bait + " - " + hit_id)

		########################
		
		self.data[bait][library]['phage_peptools_data'][hit_id] = {}
		self.data[bait][library]['phage_peptools_data'][hit_id]["Library_Peptide"] = hit["Library_Peptide"]
		self.data[bait][library]['phage_peptools_data'][hit_id]['overlap_masked_peptide'] = ""
		self.data[bait][library]['phage_peptools_data'][hit_id]['Regex'] = hit["Regex"]
		self.data[bait][library]['phage_peptools_data'][hit_id]['Designed_Mutant'] = False

		if 'Conservation QFO' in hit:self.data[bait][library]['phage_peptools_data'][hit_id]['Conservation'] = hit['Conservation QFO']
		if 'Conservation Metazoa' in hit:self.data[bait][library]['phage_peptools_data'][hit_id]['Conservation'] = hit['Conservation Metazoa'] 
		
		if self.options['mutant'] != None:
			self.data[bait][library]['phage_peptools_data'][hit_id]['Library_Peptide'] = hit["Library_Peptide"]
			self.data[bait][library]['phage_peptools_data'][hit_id]['Mutant_Peptide'] = hit["Mutant_Peptide"]
			self.data[bait][library]['phage_peptools_data'][hit_id]['Designed_Mutant'] = hit["Designed_Mutant"]
			self.data[bait][library]['phage_peptools_data'][hit_id]['Designed_Mutations'] = hit["Designed_Mutations"]

		self.data[bait][library]['phage_peptools_data'][hit_id]['interactor'] = {
			'confidence':0,
			'pmids':[],
			'method_details':[]
			}

		for header in self.headers:
			if header in hit:
				self.data[bait][library]['phage_peptools_data'][hit_id][header] = hit[header]
			else:
				self.data[bait][library]['phage_peptools_data'][hit_id][header] = ""

		########################

		if hit['mutant']  != None:
			self.data[bait][library]['phage_peptools_data'][hit_id]['mutant'] = hit['mismatch_str'] 

		if hit['ProteinAcc'] not in self.peptools_hits_overlaps: self.peptools_hits_overlaps[hit['ProteinAcc']] = {}
		self.peptools_hits_overlaps[hit['ProteinAcc']][hit['SeqStart']] = hit_id

		if 'Mutagenesis' in self.data[bait][library]['phage_peptools_data'][hit_id]:
			mutagenesis_list = []
			for mutagenesis in self.data[bait][library]['phage_peptools_data'][hit_id]['Mutagenesis']:
				try:
					mutation = ""
					if 'mutation' in mutagenesis['description']:
						mutation = str(mutagenesis['description']['mutation'])
					mutagenesis_list.append(mutagenesis['name'].strip('.') + ":" +  mutation + ":" + str(mutagenesis['description']['start']))
				except:
					logger.error("update_phage_peptools_data_dictionary - library")

			self.data[bait][library]['phage_peptools_data'][hit_id]['Mutagenesis']= "|".join(mutagenesis_list)
			
		if 'Region' in self.data[bait][library]['phage_peptools_data'][hit_id]:
			region_list = []
			for region in self.data[bait][library]['phage_peptools_data'][hit_id]['Region']:
				region_list.append(region['name'].strip('.')  + ":" + str(region['description']['start']))

			self.data[bait][library]['phage_peptools_data'][hit_id]['Region']= "|".join(region_list)

		if 'shared_ontology' in  hit:
			self.data[bait][library]['phage_peptools_data'][hit_id]['shared_ontology'] = hit['shared_ontology']

		motifs = []
		if "Motif" in hit:
			for motif in hit["Motif"]:
				motifs.append(motif['name'])

		self.data[bait][library]['phage_peptools_data'][hit_id]['motifs'] = motifs

		structures = []
		if "Structure" in hit:
			for structure in hit["Structure"]:
				structures.append(structure['name'])

		self.data[bait][library]['phage_peptools_data'][hit_id]['structures'] = structures

		modifications = []
		if "Modification" in hit:
			modifications = hit['Modification']

		self.data[bait][library]['phage_peptools_data'][hit_id]['modification'] = modifications
	
	####################################			
	
	def initialiseHit(self,hit):
		"""
		Takes PepTools peptide mapping hit and adds data required in later parts of the analysis.
		Most of the code is for remapping peptides in that are from the mutated designed peptides.

		Args:
			hit (dictionary): PepTools peptide mapping hit

		Returns:
			hit (dictionary): Updated PepTools peptide mapping hit
		"""

		incorrect_mapping = False
		mutant = False
		mismatch_str = ""

		if self.options['mutant'] != None:

			mismatch_str = ""
			hit["Mutant_Peptide"] = ""
			hit["Designed_Mutations"] = {}

			if hit['Regex'] in self.mutations_re_reverse_mapping_info:
				mutant = True

				for i in range(0,len(hit['Regex'])):
					if hit['Regex'][i] == ".":
						if hit['Hit'][i] in ['C','A']:
							mismatch_str += "."
						else:
							hit["Designed_Mutant"] = True
							mismatch_str += self.mutations_re_reverse_mapping_info[hit['Regex']]['library_peptide'][i]
							hit["Designed_Mutations"][i + hit['SeqStart']] = {"offset":i,'wt':hit['Hit'][i],'mut':self.mutations_re_reverse_mapping_info[hit['Regex']]['library_peptide'][i]}
					else:
						mismatch_str += "."

				library_peptide = self.mutations_re_reverse_mapping_info[hit['Regex']]['library_peptide']
				hit["Library_Peptide"] = self.mutations_re_reverse_mapping_info[hit['Regex']]['library_peptide']
				hit["Mutant_Peptide"] = mismatch_str
				hit["Designed_Mutant"] = True

			else:
				mismatch_str = ""
				for i in range(0,len(hit['Regex'])):
					mismatch_str += "."
					if hit['Regex'][i] == ".":
						if hit['Hit'][i] not in  ['C','A']:
							incorrect_mapping = True

				library_peptide = hit['Hit'].replace("C","A")

				hit["Library_Peptide"] = library_peptide
				hit["Mutant_Peptide"] = mismatch_str
				hit["Designed_Mutant"] = False

		else:
			for i in range(0,len(hit['Regex'])):
				if hit['Regex'][i] == ".":
					if hit['Hit'][i] not in  ['C','A']:
						incorrect_mapping = True

			library_peptide = hit['Hit'].replace("C","A")

			hit["Library_Peptide"] = library_peptide
			hit["Mutant_Peptide"] = ""
			hit["Designed_Mutations"] = {}
			hit["Designed_Mutant"] = False

		hit['incorrect_mapping'] = incorrect_mapping
		hit['mutant'] = mutant
		hit['mismatch_str'] = mismatch_str

		return hit

	####################################

	def createMotifOptionForPeptools(self,bait):
		"""
		Creates a string of the regular expression of the specificity determinants for the baits
		
		Args:
			bait (str): bait identifier of bait for processing

		Returns:
			motif (RE): Regular expression of the specificity determinants for the baits
		"""

		motif = ""
		if bait in self.elm_manual_curation:
			elm_class_consensuses = []
			for elm_class in self.elm_manual_curation[bait]:
				elm_class_consensuses.append(self.elm_manual_curation[bait][elm_class]['re'])
			
			if len(elm_class_consensuses) > 0:
				if len(elm_class_consensuses) == 1:
					motif = elm_class_consensuses[0]
				else:
					motif =  "(" + ")|(".join(elm_class_consensuses) + ")"
		return motif

	####################################

	def runPeptools(self,peptides,taxon_id,bait_uniprot,bait):
		"""
		Runs PepTools

		Args:
			bait_uniprot (str): bait UniProt accession identifier of bait for processing
			bait (str): bait identifier of bait for processing
			peptides (list): ProP-PD seleted peptides to be annotated
			taxon_id (str): taxon identifier of library

		Returns:
			peptools data (dictionary):  PepTools peptide mapping data
		"""

		motif = self.createMotifOptionForPeptools(bait)

		domains = list(self.bait_data[bait]['domain'].keys())
		if bait_uniprot in self.uniprot_mapping: bait_uniprot = self.uniprot_mapping[bait_uniprot]

		return self.phagePeptoolsRunnerObj.runPeptools(peptides,taxon_id,bait_uniprot,domains,motif)

	####################################

	def getPepToolsResultsChunks(self,library,bait,peptides,taxon_id):
		"""
		Setup and runs PepTools. Splits peptides into chunks for processing

		Args:
			library (str): library identifier of library for processing
			bait (str): bait identifier of bait for processing
			peptides (list): ProP-PD seleted peptides to be annotated
			taxon_id (str): taxon identifier of library
		"""

		peptools_hits_data = []
		missing_peptools_hits_data = []

		## process PepTools in chunks - due to server limits and speed considerations
		for i in range(int(len(peptides)/self.options['peptools_download_chunk_size']) + 1):
			
			##################################################################
			###### run PepTools
			##################################################################

			bait_uniprot = bait.split("_")[-3]

			## define peptides in chunk for submission
			peptools_peptides = peptides[i*self.options['peptools_download_chunk_size']:i*self.options['peptools_download_chunk_size']+self.options['peptools_download_chunk_size']]
			
			## run PepTools
			peptools_job_details = self.runPeptools(peptools_peptides,taxon_id,bait_uniprot,bait)

			self.data[bait][library]['peptools_job_id'] = peptools_job_details['job_id']
			self.data[bait][library]['peptools_hits_file'] = peptools_job_details['peptools_hits_file']

			##################################################################
			###### check PepTools response
			##################################################################
			
			if 'phage_peptools_data' not in self.data[bait][library]:
				self.data[bait][library]['phage_peptools_data'] = {}

			if not os.path.exists(self.data[bait][library]['peptools_hits_file']):
				self.data[bait][library]['status'] = False
				continue
			
			with open(self.data[bait][library]['peptools_hits_file']) as data_file:
				response = json.load(data_file)

				## check PepTools response
				if 'result' in response:
					## PepTools analysis successful
					peptools_hits_data += response['result']['result']
					
					## Add unmapped peptides to missing peptides list
					if 'missing' in response['result']['info']['params']['query']:
						missing_peptools_hits_data += response['result']['info']['params']['query']['missing']['data']

					## Sets status that screen returned peptools results
					self.data[bait][library]['status'] = True  
				else:
					## PepTools analysis failed
					if "status" in response:
						if response["status"] == "Error":
							logger.info("Error running PepTools:" + library + ":" + bait)
							if self.options['retry_after_peptools_error']: ## If True - reruns one time if failed
								logger.info("Deleting",self.data[bait][library]['peptools_hits_file'])

								os.remove(self.data[bait][library]['peptools_hits_file'])

								logger.info("Rerunning PepTools")

								peptools_job_details = self.runPeptools(peptools_peptides,taxon_id,bait_uniprot,bait)

					self.data[bait][library]['status'] = False

			##################################################################

		return [peptools_hits_data,missing_peptools_hits_data]
	
	####################################

	def annotateScreenInteractingPeptides(self,library,bait):
		"""
		Checks PepTools results vs the input peptide to define the mapped/unmapped peptides
		"""

		if library in ["Rand"]:
			pass
		else:
			mapped_proppd_peptides = list(set([x['Regex'].replace('.',"A") for x in self.data[bait][library]['phage_peptools_data'].values()]))
			
			for selection_id in self.data[bait][library]['screens']:
				if selection_id in self.data[bait][library]['screens']:
					self.data[bait][library]['screens'][selection_id]['interacting_peptides'] = list(self.data[bait][library]['screens'][selection_id]['phage_peptide_data'].keys())
					self.data[bait][library]['screens'][selection_id]['mapped_interacting_peptides'] = set(self.data[bait][library]['screens'][selection_id]['interacting_peptides']).intersection(list(self.data[bait][library]['phage_peptools_data'].keys()))
					self.data[bait][library]['screens'][selection_id]['unmapped_interacting_peptides'] = set(self.data[bait][library]['screens'][selection_id]['interacting_peptides']).difference(list(self.data[bait][library]['phage_peptools_data'].keys()))

			self.data[bait][library]['mapped_interacting_peptides'] = set(self.data[bait][library]['interacting_peptides']).intersection(mapped_proppd_peptides)
			self.data[bait][library]['unmapped_interacting_peptides'] = set(self.data[bait][library]['interacting_peptides']).difference(mapped_proppd_peptides)

			logger.info("Peptide Mapping")
			logger.info("- total:" + str(len(self.data[bait][library]['interacting_peptides'])))
			logger.info("- mapped:" + str(len(self.data[bait][library]['mapped_interacting_peptides'])))
			logger.info("- unmapped:" + str(len(self.data[bait][library]['unmapped_interacting_peptides'])))
				
	####################################################################################################################################################################################
	####################################################################################################################################################################################
	####################################################################################################################################################################################

	def make_selection_id(self,screen_file_tag,screen_file_info):
		"""
		Make unique screen identifier from screen info
		"""

		return "_".join([
			screen_file_tag,
			screen_file_info['Protein ID'].replace(",","").replace("_","-").replace(" ","-").replace("/","-").replace(".","").replace(";","").replace(":",""),
			screen_file_info['Start'],
			screen_file_info['End'] ])

	####################################

	def make_bait_id(self,screen_file_info):
		"""
		Make unique bait identifier from bait info
		"""

		return "_".join([
				screen_file_info['Gene Name'],
				screen_file_info['Domain Name'].replace(",","").replace("_","-").replace(" ","-").replace("/","-").replace(".","").replace(";","").replace(":",""),
				screen_file_info['Domain Number(s)'].replace(",","").replace("_","-").replace(" ","-").replace("/","-").replace(".","").replace(";","").replace(":",""),
				screen_file_info['UP Accession'],
				screen_file_info['Start'],
				screen_file_info['End']])

	###################################################################################################################################################################################
	###  PROCESS SCREEN INFO FILES
	###################################################################################################################################################################################

	def get_screen_file_list(self):
		"""
		Find all the screen_files in the screen_file directory
		"""

		for file in os.listdir(os.path.join(self.options["peptools_data_path"],"peptide_counts")):
			if file.count("info.txt") == 1:
				self.screen_files.append(file)

	####################################

	def parse_screen_file(self,screen_file):
		"""
		Parse screen_file into a dictionary
		"""

		screen_file_path = os.path.join(self.options["peptools_data_path"],"peptide_counts",screen_file)

		lines = open(screen_file_path).read().strip().split("\n")
		
		screen_file_info = {}

		for line in lines[1:]:
			line_bits = line.split("\t")
			if len(line_bits) == 2:
				screen_file_info[line_bits[0]] = line_bits[1]

		return screen_file_info

	####################################

	def parse_screen_files(self):
		"""
		Finds peptide count files available for processing
		"""
		self.get_screen_file_list()

		for screen_file in self.screen_files:
			self.process_screen_file(screen_file)

	####################################

	def process_screen_file(self,screen_file):
		"""
		Finds files peptide count files available for processing, read peptide info into a dictionary, initialise data structures 

		TODO Annotate each step
		"""
		try:	
			screen_file_info = self.parse_screen_file(screen_file)
			accession = screen_file_info['UP Accession']
			library =  screen_file_info['Library ID']

			#-------------------------------------#

			screen_file_tag = screen_file.split(".")[0]
			selection_id  = self.make_selection_id(screen_file_tag,screen_file_info)
			bait  = self.make_bait_id(screen_file_info)

			#-------------------------------------#

			if self.options['library'] in self.library_info['library_merged']:
				if library in self.library_info['library_merged'][self.options['library']]:
					library = self.options['library']
				else:
					return
				
			#-------------------------------------#
			
			in_check_analysis_bait_list = self.check_analysis_bait_list(bait,library,selection_id)
		
			if not in_check_analysis_bait_list:
				return
			
			#-------------------------------------#

			if self.options['library'] != None:
				if self.options['slimfinder_only'] == False:
					if library !=  self.options['library']: return
				else:
					if library !=  self.options['library'] and library != "Rand" : return

			#-------------------------------------#

			if 'Experimental Conditions Description' in screen_file_info:
				if screen_file_info['Experimental Conditions Description'] not in ['1mM DTT',""]:
					condition_library = library + "-" + screen_file_info['Experimental Conditions Description'].replace(" ","_")
					self.library_info['taxon_library_mapping'][condition_library] = self.library_info['taxon_library_mapping'][library]
					library = condition_library

			#-------------------------------------#

			if self.options['bait'] != None:
				if bait != self.options['bait'] and accession != self.options['bait'] and screen_file_info['Gene Name'].count(self.options['bait']) == 0 and screen_file_info['Domain Name'].count(self.options['bait']) == 0: 
					return

			if library not in self.library_info['taxon_library_mapping']:
				logger.debug("Skipping " + bait + ":" + library + " not in " + ",".join(list(self.library_info['taxon_library_mapping'].keys())))
				return

			if accession == "":
				if bait.split("_")[0] != "Library":
					logger.debug("ERROR: No accession " + screen_file + " " + bait + " " + library)
				return
			
			#-------------------------------------#

			self.initialise_data_structures(library,bait,selection_id)

			data_files = os.path.join(self.options["peptools_data_path"],"peptide_counts", screen_file_tag + "_PEPTIDE_COUNTS.tsv")
			self.data_files[bait][library][selection_id] = data_files

			self.data[bait][library]['screens'][selection_id]['phage_peptide_data'] = self.parse_data_file(data_files,library)
			self.data[bait][library]['status'] = True
			self.bait_proteins[bait] = {"accession":accession,"start":screen_file_info['Start'],"end":screen_file_info['End']}

			#-------------------------------------#
			
			if library == "Library":
				if not self.options["run_library_screens"]:
					logger.info("ERROR: Bait is GST " + bait + " " + library)
					return

			if bait not in self.data:
				logger.info("ERROR: Bait missing " + bait + " " + library)
				return

			if library not in self.data[bait]:
				logger.info("ERROR: Library missing " + bait + " " + library)
				return

			if selection_id not in self.data[bait][library]['screens']:
				logger.info("ERROR: Screen missing " + bait + " " + library + " " +  selection_id)
				return

			#-----
			
			self.data[bait][library]['screens'][selection_id]["details"] = screen_file_info
			
		except Exception as e:
			logger.error("ERROR@process_screen_file: " + str(screen_file))

			utilities_error.printError()
			raise

	####################################

	def check_analysis_bait_list(self, bait,library,selection_id):
		"""
		Check if the screen is in the user defined analysis_bait_list. 
		If use_screens is set then use only those screens
		Otherwise if skip_screens is set skip these screens
		If analysis_bait_list == None then use all screens

		Args:
			library (str): library identifier of library for processing
			bait (str): bait identifier of bait for processing
			selection_id (str): selection identifier of selection for processing
		"""
		
		in_check_analysis_bait_list = True
		if self.options['analysis_bait_list'] != None:
			in_check_analysis_bait_list = False
			if bait in self.options['analysis_bait_list']:
				if library in self.options['analysis_bait_list'][bait]["library"]:
					in_check_analysis_bait_list = True

					if len(self.options['analysis_bait_list'][bait]['use_screens']) > 0 and selection_id not in self.options['analysis_bait_list'][bait]['use_screens']:
						in_check_analysis_bait_list = False
						logger.debug("Using " + str(selection_id))
					elif len(self.options['analysis_bait_list'][bait]['skip_screens']) > 0 and selection_id in self.options['analysis_bait_list'][bait]['skip_screens']:
						in_check_analysis_bait_list = False
						logger.debug("Skipping " + str(selection_id))
		
		return in_check_analysis_bait_list

	###################################################################################################################################################################################
	###  PROCESS SCREEN INFO FILES
	###################################################################################################################################################################################

	def process_selection_day_replicates(self):
		"""
		Process data for the selection day replicates
		"""

		for bait in self.data:
			for library in self.data[bait]:
				for selection_id in self.data[bait][library]['screens']:
					screen_file_info = self.data[bait][library]['screens'][selection_id]["details"] 

					if bait not in self.data and screen_file_info['Protein ID'] != "#N/A":
						logger.info("ERROR: #Failed to load:" + bait + " " + library + selection_id)

					elif screen_file_info['Protein ID'] != "#N/A":
						try:
							screen_file_info['Selection Day'] = int(screen_file_info['Selection Day'].replace("Day","").replace(" ",""))
						except:
							logger.error("Selection Day error " + bait + " " + library + " " + screen_file_info['Selection Day'])

							screen_file_info['Selection Day'] = 0

						Experiment_Replica_Nbr = screen_file_info['Library ID'] + "_" + screen_file_info["Experiment Replica Nbr."]
						Experiment_Replica_Day = screen_file_info['Selection Day']
						Researcher_Name = screen_file_info['Researcher Name']
						
						self.data[bait][library]['screens'][selection_id]["details"]["Experiment Replica Nbr."] = Experiment_Replica_Nbr

						logger.debug(
							"\t".join([
								str(screen_file_info['Selection Day']),
								Experiment_Replica_Nbr,
								bait,
								library,
								selection_id,
								Researcher_Name, 
								str(self.data[bait][library]['screens'][selection_id]["details"]["ELISA Quality"])
								])
							)
						
						if bait not in self.selection_day_replicates: self.selection_day_replicates[bait] = {}
						if library not in self.selection_day_replicates[bait]: self.selection_day_replicates[bait][library] = {}
						if selection_id not in self.selection_day_replicates[bait][library]: self.selection_day_replicates[bait][library][selection_id] = {}

						#---

						if bait not in self.selection_day_replicate_mapping: self.selection_day_replicate_mapping[bait] = {}
						if library not in self.selection_day_replicate_mapping[bait]: self.selection_day_replicate_mapping[bait][library] = {}
						if Experiment_Replica_Nbr not in self.selection_day_replicate_mapping[bait][library]: self.selection_day_replicate_mapping[bait][library][Experiment_Replica_Nbr] = {}

						self.selection_day_replicate_mapping[bait][library][Experiment_Replica_Nbr][selection_id] = {Experiment_Replica_Day:"secondary"}

	####################################
		
	def	load_bait_protein_details(self):
		"""
		Annotate the baits
		"""

		phageDataProteinAnnotatorObj = phageDataProteinAnnotator.phageDataProteinAnnotator()
		phageDataProteinAnnotatorObj.elm_curation = self.elm_curation
		
		for bait in self.bait_proteins:

			accession = self.bait_proteins[bait]['accession']
			start = self.bait_proteins[bait]['start']
			end = self.bait_proteins[bait]['end']

			if bait not in self.bait_data:
				try:
					if accession.count(",") > 0:
						self.bait_data[bait] = phageDataProteinAnnotatorObj.get_protein_details(accession.split(",")[0],start,end)
					else:
						self.bait_data[bait] = phageDataProteinAnnotatorObj.get_protein_details(accession,start,end)
				except:
					raise

	####################################

	def process_screen_files(self):
		"""
		Run each step of the screen file processing to load the bait data for analysis
		"""

		self.parse_screen_files()
		self.load_bait_protein_details()
		self.process_selection_day_replicates()
		self.calculate_primary_replicates()
		self.calculate_replicate_counts()

		logger.info("Processing " + str(len(list(self.data.keys()))) + " baits")

		#self.interactions = self.phageDataExternalDatasetsLoaderObj.getInteractions(self.bait_data)
		self.motif_manual_curation = self.phageDataExternalDatasetsLoaderObj.grabCuratedMotifData(self.bait_data)
		self.elm_manual_curation = self.phageDataExternalDatasetsLoaderObj.grabCuratedELMData()

	####################################

	def calculate_primary_replicates(self):
		"""
		Calculate which replicate day is the primary day for the replicate
		"""

		for bait in self.selection_day_replicates:
			for library in self.selection_day_replicates[bait]:
				for replicate in self.selection_day_replicate_mapping[bait][library]:
					try:
						last_day = [0,None]
						for subreplicate in self.selection_day_replicate_mapping[bait][library][replicate]:
							try:
								days = self.selection_day_replicate_mapping[bait][library][replicate][subreplicate]
								day = int(list(days.keys())[0])

								if day > last_day[0]:
									last_day = [day,subreplicate]
								if day == last_day[0]:
									"ADD SOMETHING TO CHOOSE IN AN INTELLIGENT WAY AFTER TIE BREAK"
							except:
								logger.error("calculate_primary_replicates - subreplicate " + ":".join([bait,library, subreplicate,str(days),str(day)]))
								raise

						self.selection_day_replicate_mapping[bait][library][replicate][last_day[1]][last_day[0]] = "primary"
					except:
						logger.error("calculate_primary_replicates  " + ":".join([bait,library, subreplicate]))

	####################################s

	def calculate_replicate_counts(self):
		"""
		Count the number of replicates for each screen
		"""

		for bait in self.selection_day_replicates:
			for library in self.selection_day_replicates[bait]:
				self.data[bait][library]['replicates_count'] = 0

				for replicate in self.selection_day_replicate_mapping[bait][library]:
					for selection_id in self.selection_day_replicate_mapping[bait][library][replicate]:
						if self.options['use_selection_day_replicates']:
							if list(self.selection_day_replicate_mapping[bait][library][replicate][selection_id].values())[0] == "primary":
								self.data[bait][library]['replicates_count'] += 1

						elif self.options['use_selection_day_replicates'] == False:
							if list(self.selection_day_replicate_mapping[bait][library][replicate][selection_id].values())[0] == "primary":
								self.data[bait][library]['replicates_count'] += 1
			
	####################################

	def parse_data_library_files(self):
		"""
		Parse the peptide NGS count files for the whole library sequencing
		"""
		
		counter = 0
		for bait in self.data_files:
			for library in self.data_files[bait]:
				for selection_id in self.data_files[bait][library]:

					data_file = self.data_files[bait][library][selection_id]

					for line in open(data_file).read().split("\n")[1:]:
						try:
							line_bits = line.split("\t")

							peptide_length = "16"
							asterisk_count = 0

							if library in  self.library_info['terminal_libraries']:
								if line_bits[1] == peptide_length and int(line_bits[2]) > int(self.options['prefilter_count_cutoff']) and line_bits[0].count("*") == asterisk_count:# SLOW and line_bits[0] in designed_library:

									if line_bits[0].replace("X","A") not in self.library:
										self.library[line_bits[0].replace("X","A")] = 0

									counter += 1
									self.library[line_bits[0].replace("X","A")] += int(line_bits[2])

						except:
							logger.error("Line error " + line + " in " + data_file)
				
		logger.error("Loaded:" + os.path.join(self.options["peptools_data_path"],"libraries",self.options["library"] + ".tdt"))

	####################################
	####################################

	def parse_data_file(self,data_file,library):
		"""
		Parse the peptide NGS count file for a bait selection
	
		Args:
			data_file (path): path to NGS peptide count file f
			library (str): library identifier of library for processing
		"""

		data_file_content = {}
		for line in open(data_file).read().split("\n")[1:]:
			line_bits = line.split()
			try:
				if library in  self.library_info['terminal_libraries']:
					peptide = line_bits[0].split("*")[0]
					if len(peptide) >= 7 and int(line_bits[2]) > int(self.options['prefilter_count_cutoff']):
						data_file_content[line_bits[0].split("*")[0].replace("X","A") + "$"] = line_bits[2]
				else:
					peptide_length = "16"
					asterisk_count = 0

					if line_bits[1] == peptide_length and int(line_bits[2]) > int(self.options['prefilter_count_cutoff']) and line_bits[0].count("*") == asterisk_count:
						data_file_content[line_bits[0].replace("X","A")] = line_bits[2]
			except:
				logger.error("Line error " + line + " in " + data_file)

		return data_file_content
		

	###################################################################################################################################################################################
	###################################################################################################################################################################################
	### CRAPOME CRAPOME CRAPOME CRAPOME CRAPOME CRAPOME CRAPOME CRAPOME CRAPOME CRAPOME CRAPOME CRAPOME CRAPOME CRAPOME CRAPOME CRAPOME CRAPOME CRAPOME CRAPOME CRAPOME CRAPOME    ####
	### CRAPOME CRAPOME CRAPOME CRAPOME CRAPOME CRAPOME CRAPOME CRAPOME CRAPOME CRAPOME CRAPOME CRAPOME CRAPOME CRAPOME CRAPOME CRAPOME CRAPOME CRAPOME CRAPOME CRAPOME CRAPOME    ####
	###################################################################################################################################################################################
	###################################################################################################################################################################################
	
	def createCrapome(self):
		"""
		Creates the data on peptide specificity. 
		The "crapome" is a dictionary of peptides with their frequency across baits.
		Data is collapse by domains type 
		"""

		self.crapome = {}

		for library in self.libraries:
			if self.options['library'] != None:
				if self.options['library'] != library: continue

			if library not in self.crapome:
					self.crapome[library] = {}

			out_file_json = os.path.join(self.options["peptools_data_path"],"metadata", library + ".crapome.json")

			if not os.path.exists(out_file_json) or self.options["run_create_crapome"]:
				for bait in self.data:

					try:
						bait_domain =  self.screens_details[bait]['pfam_id']
					
						if bait_domain.split("_")[-1].isdigit():
							bait_domain = "_".join(bait_domain.split("_")[:-1])

						if bait_domain == "":
							bait_domain = bait
					except:
						bait_domain = bait

					if library in self.data[bait]:
						for selection_id in self.data[bait][library]['screens']:

							for peptide in self.data[bait][library]['screens'][selection_id]['phage_peptide_data']:
								if peptide not in self.crapome[library]:
									self.crapome[library][peptide] = {}


								if bait_domain not in self.crapome[library][peptide]:
									self.crapome[library][peptide][bait_domain] = []

								self.crapome[library][peptide][bait_domain].append(self.data[bait][library]['screens'][selection_id]['phage_peptide_data'][peptide])

				if self.options['bait'] == None:
					for library in self.crapome:
						outstr = ""
						for peptide in self.crapome[library]:
							if len(self.crapome[library][peptide]) > 4:

								domain_str_list = []
								for domain in self.crapome[library][peptide]:
									try:
										domain_str =  domain + ":"
										domain_str += ",".join(self.crapome[library][peptide][domain])
										domain_str_list.append(domain_str)
									except:
										pass

								outstr += str(len(self.crapome[library][peptide])) +  "\t" + peptide + "\t" + library + "\t" + ",".join(domain_str_list) +  "\n"

						logger.debug(outstr)

						out_file = os.path.join(self.options["peptools_data_path"],"metadata", library + ".crapome.tdt")
						out_file_json = os.path.join(self.options["peptools_data_path"],"metadata", library + ".crapome.json")

						open(out_file,"w").write(outstr)

						with open(out_file_json, 'w') as outfile:
							json.dump(self.crapome[library], outfile)
			else:
				self.crapome[library] = json.loads(open(out_file_json, 'r').read())

	########################################################################

	def filter_peptides_by_crapome(self,peptides,library):
		"""
		Filter peptides using the crapome

		Args:
			peptides (list): list of peptides for filtering
			library (str): library identifier of library for processing
		"""
		
		filtered_peptides = []
		for peptide in peptides:
			if peptide in self.crapome[library]:
				if len(self.crapome[library][peptide]) < self.options["promiscuity_cutoff"]:
					filtered_peptides.append(peptide)
			else:
				filtered_peptides.append(peptide)

		return filtered_peptides

	###################################################################################################################################################################################
	###################################################################################################################################################################################
	### METADATA  METADATA  METADATA  METADATA  METADATA  METADATA  METADATA  METADATA  METADATA  METADATA  METADATA  METADATA  METADATA  METADATA  METADATA  METADATA  METADATA    ###
	### METADATA  METADATA  METADATA  METADATA  METADATA  METADATA  METADATA  METADATA  METADATA  METADATA  METADATA  METADATA  METADATA  METADATA  METADATA  METADATA  METADATA    ###
	###################################################################################################################################################################################
	###################################################################################################################################################################################
	
	###################################################################################################################################################################################
	###  SET LIBRARY METADATA
	###################################################################################################################################################################################

	def setLibraryMetadata(self):
		"""
		Sets the metadata for each library
		"""

		use_tags = ['protein_accession', 'cluster', 'protein_gene', 'protein_name', 'protein_organism', 'protein_organism_host', 'protein_length', 'disordered_residues', 'disordered_residues_percentage', 'protein_taxonomy']
		for library in self.libraries:
			library_data = {"proteins":{},"species":{},"taxonomy":{}}
			if os.path.exists(os.path.join(self.options["peptools_data_path"],"libraries",library + ".tdt")):
				designed_library = open(os.path.join(self.options["peptools_data_path"],"libraries",library + ".tdt")).read().strip().split("\n")
				tags = designed_library[0].strip().split("\t")

				for peptide in designed_library[1:]:
					try:
						line_bits = peptide.strip().split("\t")

						tmp_data = {}

						peptide = ""

						for tag_iter in range(0,len(line_bits)):
							tag_name = tags[tag_iter]
							tag_data = line_bits[tag_iter]
							if tag_name in use_tags:
								tmp_data[tag_name] = tag_data

							if tag_name == 'library_peptide':
								peptide = tag_data

						if "protein_organism" in tmp_data:
							if tmp_data["protein_organism"] not in library_data["species"]:
								library_data["species"][tmp_data["protein_organism"]] = []

							if tmp_data["protein_accession"] not in library_data["species"][tmp_data["protein_organism"]]:
								library_data["species"][tmp_data["protein_organism"]].append(tmp_data["protein_accession"])

						if  "protein_accession" in tmp_data:
							if tmp_data["protein_accession"] not in library_data["species"]:
								library_data["proteins"][tmp_data["protein_accession"]] = tmp_data
								library_data["proteins"][tmp_data["protein_accession"]]["peptides"] = []

							if peptide not in library_data["proteins"][tmp_data["protein_accession"]]["peptides"]:
								library_data["proteins"][tmp_data["protein_accession"]]["peptides"].append(peptide)
								library_data["proteins"][tmp_data["protein_accession"]]["peptides_count"] = len(library_data["proteins"][tmp_data["protein_accession"]]["peptides"])

						if  "protein_taxonomy" in tmp_data:
							if tmp_data["protein_taxonomy"] not in library_data["taxonomy"]:
								library_data["taxonomy"][tmp_data["protein_taxonomy"]] = []

							if tmp_data["protein_organism"] not in library_data["taxonomy"][tmp_data["protein_taxonomy"]]:
								library_data["taxonomy"][tmp_data["protein_taxonomy"]].append(tmp_data["protein_organism"])

					except:
						logger.error("")
						raise

				designed_library_size = len(designed_library)
				with open(os.path.join(self.options["peptools_data_path"],"libraries",library + ".details.json"), 'w') as outfile:
					logger.error("Library metadata written to:"  + os.path.join(self.options["peptools_data_path"],"libraries",library + ".details.json"))
	
	###################################################################################################################################################################################
	###  SET SCREEN STATS
	###################################################################################################################################################################################

	def setScreenMetadata(self,library,bait):
		"""
		Sets the metadata for each screen

		Args:
			library (str): library identifier of library for processing
			bait (str): bait identifier of bait for processing
		"""
		try:
			for tag in ['specific','specific_replicate','overlap','specific_overlap','replicate','highcount','motifs','interaction','structure','consensus']:

				tag_proportions = None

				if len(self.bait_stats[tag]) > 0:
					tag_proportions = "%1.3f"%(float(self.bait_stats[tag].count(True))/len(self.bait_stats[tag]))
				else:
					tag_proportions = "0"

				if len(self.bait_stats[tag]) > 0:
					self.stats[bait][library][tag] = tag_proportions
					self.stats[bait][library][tag + "_count"] = self.bait_stats[tag].count(True)
				else:
					self.stats[bait][library][tag] = ''
					self.stats[bait][library][tag + "_count"] = ''

			#-----------

			self.data[bait][library]['consensus_details'] = {}
			for motif in self.elm_manual_curation[bait]:
				self.data[bait][library]['consensus_details'][motif] = {'re':self.elm_manual_curation[bait][motif]['re']}

			self.data[bait][library]['gene_name'] = self.bait_data[bait]['protein']['gene_name']
			self.data[bait][library]['protein_name'] = self.bait_data[bait]['protein']['protein_name']

			try:
				self.data[bait][library]['domain_name'] = list(self.bait_data[bait]['domain'].values())[0]['domain_name']
				self.data[bait][library]['pfam_id'] = list(self.bait_data[bait]['domain'].values())[0]['pfam_id']
				self.data[bait][library]['pfam_accession'] = list(self.bait_data[bait]['domain'].values())[0]['pfam_accession']
				self.data[bait][library]['clan_name'] = list(self.bait_data[bait]['domain'].values())[0]['clan_name']
				self.data[bait][library]['domain_description'] = list(self.bait_data[bait]['domain'].values())[0]['domain_description']
				self.data[bait][library]['domain_start'] = bait.split("_")[-2]
				self.data[bait][library]['domain_end'] = bait.split("_")[-1]
			except:
				self.data[bait][library]['domain_name'] = self.data[bait][library]['gene_name'] + "*"
				self.data[bait][library]['pfam_id'] = self.data[bait][library]['gene_name'] + "*"
				self.data[bait][library]['pfam_accession'] = self.data[bait][library]['gene_name'] + "*"
				self.data[bait][library]['clan_name'] = ""
				self.data[bait][library]['domain_description'] = ""
				self.data[bait][library]['domain_start'] = bait.split("_")[-2]
				self.data[bait][library]['domain_end'] = bait.split("_")[-1]

			#----------

			self.stats[bait][library]['peptides_count'] = len(self.bait_stats['overlap'])
			self.stats[bait][library]['bait'] = bait
			self.stats[bait][library]['library'] = library

			for tag in ['sequence_logo_peptides','SLiMFinder_motif','consensus_details','gene_name','protein_name','domain_name','pfam_id','pfam_accession','clan_name','domain_start','domain_end']:
				if tag in self.data[bait][library]:
					self.stats[bait][library][tag] = self.data[bait][library][tag]
				else:
					self.stats[bait][library][tag] = "-"

			if bait in self.elm_manual_curation:
				self.stats[bait][library]['consensus_details_str'] = "|".join([elm_class + ":" + self.elm_manual_curation[bait][elm_class]["re"] for elm_class in self.elm_manual_curation[bait]])


			for tag in ['complex_structures_found','complex_structures_findable','screens_count','replicates_count']:
				if tag in self.data[bait][library]:
					self.stats[bait][library][tag] = str(self.data[bait][library][tag])

			self.stats[bait][library]['peptides_count'] = len(self.bait_stats['overlap'])

		except Exception as e:
			logger.error(library + " - " + bait)
			utilities_error.printError()
