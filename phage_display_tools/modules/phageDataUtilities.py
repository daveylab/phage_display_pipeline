import hashlib

class phageDataUtilities():

	####-----------####
	
	def __init__(self):
		self.options = {}

	####-----------####

	def hamming(self,s1, s2):
		return sum(c1 != c2 for c1, c2 in zip(s1, s2))

	####-----------####

	def similarity(self,real_peptides,other_peptides):
		similiarity = {}
		for peptide_a in real_peptides:
			similiarity[peptide_a] = {}
			for peptide_b in other_peptides:
				if peptide_a == peptide_b: continue

				mismatches = self.hamming(peptide_a,peptide_b)

				if mismatches > 3: continue
				if mismatches not in similiarity[peptide_a]:
					similiarity[peptide_a][mismatches] = []

				similiarity[peptide_a][mismatches].append(peptide_b.replace(".","A"))

		return similiarity

	####-----------####

	def complexity(self,peptide):
		if len(set(list(peptide))) > int(self.options['complexity_cutoff']):
			return True
		else:
			False

	####-----------####

	def filter_low_complexity(self,peptides):
		filtered_peptides = []
		for peptide in peptides:
			if self.complexity(peptide):
				filtered_peptides.append(peptide)

		return filtered_peptides

	####-----------####

	def params_to_hash(self, params):
		param_str = ""
		param_keys = list(params.keys())
		param_keys.sort()

		for param in param_keys:
			param_str += param+"="+str(params[param])+"\n"

		hash = str(hashlib.md5(param_str.encode()).hexdigest())
		return hash

	