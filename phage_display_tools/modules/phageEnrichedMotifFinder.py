#!/usr/bin/python3
#
# Program:      ProP-PD Data Analysis Pipeline
# Class:        phageEnrichedMotifFinder
# Description:  Analysis pipeline to process peptides returned from replicated ProP-PD selections
# Version:      1.0.0
# Last Edit:    1/1/2021
# Citation:     Under Preparation
# Author contact: Norman E. Davey <norman.davey@icr.ac.uk> Institute Of Cancer Research, Chelsea, SW3 6JB, London, United Kingdom.
# 
# License:
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http:#www.gnu.org/licenses/>.
#
# Copyright (C) 2020  Norman E. Davey


__doc__ = """
# ProP-PD Data Analysis Pipeline
### Code for the analysis tools for the ProP-PD data

"""

import os
import sys
import logging

#-----
logger = logging.getLogger(__name__)
#-----

sys.path.append(os.path.join(os.path.dirname(__file__),"../../data_management"))
import queryManager

sys.path.append(os.path.join(os.path.dirname(__file__),"../../motif"))
import alignPeptidesBySLiMFinder

#-----

import phageDataUtilities

#-----

class phageEnrichedMotifFinder():

	def __init__(self):
		self.options = {}

		self.phageDataUtilitiesObj = phageDataUtilities.phageDataUtilities()

	########################################################################

	def alignPhagePeptidesBySLiMFinder(self,peptides,termini=False):
		"""
		Align peptides using SLiMFinder. Finds motif using SLiMFinder and use motif to align peptides.

		Args:
			peptides (list): list of peptides for alignment
		"""

		logger.info("Align peptides using SLiMFinder.")

		try:
			cleaned_peptides = []
			for peptide in peptides:
				if len(peptide.split(" ")) == 1:
					cleaned_peptides.append(peptide)
			
			querySLiMFinderManagerObj = queryManager.queryManager()
			querySLiMFinderManagerObj.options['dataset_type'] = "motif_alignment"
			querySLiMFinderManagerObj.options['task'] = "align_peptides_by_slimfinder"
			querySLiMFinderManagerObj.options['topranks'] = 1			
			querySLiMFinderManagerObj.options["variant"] = "fast_heuristic" #self.options['slimfinder_variant'] #'exhaustive_heuristic' #'fast_heuristic','exhaustive_heuristic'
			querySLiMFinderManagerObj.options['use_peptide_aa_freqs'] = self.options['use_peptide_aa_freqs']
			querySLiMFinderManagerObj.options['peptides'] = list(set(cleaned_peptides))

			slimfinder_results_response = querySLiMFinderManagerObj.main()
			
			if 'data' in slimfinder_results_response:
				if 'status' in slimfinder_results_response['data']:
					slimfinder_results = slimfinder_results_response['data']
					slimfinder_results['motif'] = {}
					slimfinder_results["out_str"] = slimfinder_results_response['data']['status'] + ":" + slimfinder_results_response['data']['status_details']

					return slimfinder_results
				else:
					slimfinder_results = slimfinder_results_response['data']
					slimfinder_results_response['data']["out_str"] = slimfinder_results_response['data']['motif'][1]['motif'] + "(" + slimfinder_results_response['data']['motif'][1]['sig']  + ")"
					return slimfinder_results_response['data']
			else:
				slimfinder_results = {}
				slimfinder_results['motif'] = {}
				slimfinder_results["out_str"] = "Not Set"
				return slimfinder_results
			
			##########
			
		except Exception as e:
			logger.error("alignPhagePeptidesBySLiMFinder")
			raise

	####################################

	def ranked_peptides(self,bait,library,ranked_peptides_cutoff,use_replicate="all"):
		"""
		Ranks peptides by NGS sequencing count and returns a set number (ranked_peptides_cutoff) of top ranked peptides .

		Args:
			library (str): library identifier of library for processing
			bait (str): bait identifier of bait for processing
			ranked_peptides_cutoff (int): number of peptides to return
			use_replicate (str): this never changes - we always use the day relicates
		"""

		ranker = {}
		all_peptides = {}

		for replicate in self.selection_day_replicate_mapping[bait][library]:
			for screen_id in self.selection_day_replicate_mapping[bait][library][replicate]:
				for peptide in list(self.data[bait][library]['screens'][screen_id]['phage_peptide_data'].keys()):
					if use_replicate == "all" or use_replicate == screen_id:
						peptide_count = int(self.data[bait][library]['screens'][screen_id]['phage_peptide_data'][peptide])

						if peptide_count not in ranker:
							ranker[peptide_count] = []

						ranker[peptide_count].append(peptide)

						if peptide not in all_peptides:
							all_peptides[peptide] = peptide_count

		ranker_counts = list(ranker.keys())
		ranker_counts.sort()
		ranker_counts.reverse()

		ranked_peptides = []
		for ranker_count in ranker_counts:
			ranked_peptides += list(set(ranker[ranker_count]))

			if len(list(set(ranked_peptides))) > ranked_peptides_cutoff:
				break


		ranked_peptides_clean = []
		for test_peptide in ranked_peptides:
			use_peptide = True
			for ranked_peptide in ranked_peptides_clean:
				diff = sum(1 for a, b in zip(ranked_peptide, test_peptide) if a != b)
				if diff <= 3:
					#if self.options['verbose']:print("Skipping SLiMFinder Peptide - variant - ",ranked_peptide,all_peptides[ranked_peptide], test_peptide,all_peptides[test_peptide],diff)
					use_peptide = False

			if use_peptide:
				ranked_peptides_clean.append(test_peptide)

		ranked_peptides_clean = list(set(ranked_peptides_clean))
		ranked_peptides = ranked_peptides_clean[:ranked_peptides_cutoff]

		del all_peptides
		return ranked_peptides

	########################################################################

	def findMotif(self,bait,library,peptides):
		"""
		Find enriched motifs and align peptides using SLiMFinder. Update data structure with motif informations.

		Args:
			library (str): library identifier of library for processing
			bait (str): bait identifier of bait for processing
			peptides (list): list of peptides for alignment
		"""

		if not os.path.exists(self.options["clustalw_path"]):
			logger.critical("clustalw_path:" + self.options["clustalw_path"] + " does not exist. SLiMFinder motif enrichment analysis will fail")
			logger.critical("Install clustalw")
			logger.critical("Update clustalw_path in config_local.ini")

		if not os.path.exists(self.options["blastplus_path"]):
			logger.critical("blastplus_path:" + self.options["blastplus_path"] + " does not exist. SLiMFinder motif enrichment analysis will fail")
			logger.critical("Install blast+")
			logger.critical("Update blastplus_path in config_local.ini")

		self.enriched_motif_data = {}

		self.phageDataUtilitiesObj.options = self.options
		unique_use_peptides = list(set(peptides))
		unique_filtered_use_peptides = self.phageDataUtilitiesObj.filter_low_complexity(unique_use_peptides)
		self.enriched_motif_data['sequence_logo_peptides'] = []
		self.enriched_motif_data['slimfinder_input_peptides'] = peptides

		if library in  self.library_info['terminal_libraries']:
			unique_filtered_use_peptides = ["XXXXXXXXXXXX" + unique_filtered_use_peptide for unique_filtered_use_peptide in unique_filtered_use_peptides]
			self.enriched_motif_data['peptide_scores_SLiMFinder'] = self.alignPhagePeptidesBySLiMFinder(unique_filtered_use_peptides,termini=True)
		else:
			self.enriched_motif_data['peptide_scores_SLiMFinder'] = self.alignPhagePeptidesBySLiMFinder(unique_filtered_use_peptides)

		logger.info("Library - " + bait)
		logger.info("Unique replicated peptides - " + str(len(unique_filtered_use_peptides)))
		logger.debug("\n".join(unique_filtered_use_peptides))

		try:
			if bait in self.elm_manual_curation:
				logger.info("Expected Motifs - " + " | ".join([motif_class + ":" + self.elm_manual_curation[bait][motif_class]['re'] for motif_class in self.elm_manual_curation[bait]]))
		except:
			logger.info("Expected Motifs list not found in findMotif")
			
		logger.info("Enriched Motif - " + self.enriched_motif_data['peptide_scores_SLiMFinder']['out_str'])
	
		if len(self.enriched_motif_data['peptide_scores_SLiMFinder']['motif']) > 0:
			self.enriched_motif_data['SLiMFinder_motif'] = self.enriched_motif_data['peptide_scores_SLiMFinder']['motif'][1]

		if 'sig_aligned_peptides' in self.enriched_motif_data['peptide_scores_SLiMFinder']:
			sig_aligned_peptides = self.enriched_motif_data['peptide_scores_SLiMFinder']['sig_aligned_peptides']
			self.enriched_motif_data['peptide_scores_SLiMFinder']['sig_aligned_peptides'] = [sig_aligned_peptide.replace("X","-") for sig_aligned_peptide in sig_aligned_peptides]

			logger.debug("# Sig aligned peptides")
			logger.debug("\n".join(self.enriched_motif_data['peptide_scores_SLiMFinder']['sig_aligned_peptides']))

			self.enriched_motif_data['sequence_logo_peptides'] = self.enriched_motif_data['peptide_scores_SLiMFinder']['sig_aligned_peptides']

		return self.enriched_motif_data

	########################################################################
	########################################################################
	########################################################################

	def runSLiMFinderOnlyAnalysis(self):
		"""
		Run only SLiMFinder on the screen data

		TODO: 
		- Set new option parameter for ranked_peptides_cutoffs
		- Set new option parameter for aa_freq_sources
		"""

		bait_counter = 0

		for bait in self.data:
			out_str = ""
			bait_counter += 1

			ranked_peptides_cutoffs = [int(x) for x in str(self.options["peptide_length"]).split(",")]#,50]#,100]#,250]# ,

			for library in self.libraries:
				
				#use_replicates = []

				#for replicate in self.selection_day_replicate_mapping[bait][library]:
				#	for screen_id in self.selection_day_replicate_mapping[bait][library][replicate]:
				#		use_replicates.append(screen_id)

				#if len(use_replicates) > 0:
				#	use_replicates.append('all')

				use_replicates = ['all']

				if library in self.data[bait]:
					aa_freq_sources = ["Equal_aa_Freq","Human_aa_Freq","Human_Disorderome_aa_Freq","Human_Disorderome_aa_Freq_with_W_masking","Returned_Peptides_aa_freq"]
					aa_freq_sources = ["Human_Disorderome_aa_Freq","Returned_Peptides_aa_freq"]
					aa_freq_sources = ["Returned_Peptides_aa_freq"]

					for ranked_peptides_cutoff in ranked_peptides_cutoffs:
						for use_replicate in use_replicates:
							str_chunk = []

							ranked_peptides = self.ranked_peptides(bait,library,ranked_peptides_cutoff,use_replicate)
							
							#print use_replicate, len(use_replicates),len(ranked_peptides)

							for aa_freq_source in aa_freq_sources:

								unique_filtered_use_peptides = list(set(ranked_peptides))
								unique_filtered_use_peptides.sort()
								#print(unique_filtered_use_peptides)

								alignPeptideObj = alignPeptidesBySLiMFinder.alignPeptidesBySLiMFinder()
								alignPeptideObj.options["peptides"] = ",".join(unique_filtered_use_peptides)
								alignPeptideObj.options['blast_path'] = self.options["blastplus_path"]
								alignPeptideObj.options["make_output"] = False

								try:
									if "Equal_aa_Freq" == aa_freq_source:
										alignPeptideObj.options["remake"] = False
										alignPeptideObj.options["slimfinder_background_frequencies"] = "equal"
										alignPeptideObj.setup_paths()
										alignPeptideObj.parse_peptides()

									if "Human_aa_Freq"  == aa_freq_source:
											alignPeptideObj.options["remake"] = False
											alignPeptideObj.options["slimfinder_background_frequencies"] = 0.0
											alignPeptideObj.setup_paths()
											alignPeptideObj.parse_peptides()


									if "Human_Disorderome_aa_Freq"  == aa_freq_source:
											alignPeptideObj.options["remake"] = False
											alignPeptideObj.setup_paths()
											alignPeptideObj.parse_peptides()

									if "Human_Disorderome_aa_Freq_with_W_masking" == aa_freq_source:
											unique_filtered_use_peptides_WX = [peptide.replace("W","X") for peptide in list(set(ranked_peptides))]
											alignPeptideObj.options["peptides"] = ",".join(unique_filtered_use_peptides_WX)
											alignPeptideObj.options["remake"] = False

											alignPeptideObj.setup_paths()
											alignPeptideObj.parse_peptides()

									if "Returned_Peptides_aa_freq"  == aa_freq_source:
										alignPeptideObj.options["remake"] = False
										alignPeptideObj.setup_paths()
										alignPeptideObj.parse_peptides()
										alignPeptideObj.use_peptide_aa_freqs()

									alignPeptideObj.run_slimfinder()
									alignPeptideObj.parse_slimfinder_motifs()

									if len(alignPeptideObj.data["motifs"]) > 0:
										str_chunk += [
										library + " " + str(ranked_peptides_cutoff),
										"%-18s"%alignPeptideObj.data["motifs"][1]["motif"],
										"%-10s"%alignPeptideObj.data["motifs"][1]["sig"],
										str(alignPeptideObj.data["motifs"][1]["upcs"]),
										str(alignPeptideObj.data["motifs"][1]["peptide upc count"])]
									else:
										str_chunk += [
										library + " " + str(ranked_peptides_cutoff),
										"%-18s"%"-",
										"%-10s"%"-",
										"-",
										"-"]

									del alignPeptideObj
								except:
									str_chunk += [
									library + " " + str(ranked_peptides_cutoff),
									"%-18s"%"ERROR",
									 "%-10s"%"ERROR",
									 "ERROR",
									 "ERROR"]

									raise


							out_str += "\t".join(str_chunk) + "\t"
				else:
					for ranked_peptides_cutoff in ranked_peptides_cutoffs:

						str_chunk = [
						library + " " + str(ranked_peptides_cutoff),
						"%-18s"%"x",
						"%-10s"%"x",
						"x",
						"x"]
			
						out_str += "\t".join(str_chunk) + "\t"

			logger.info(out_str + "\t" + bait)