#!/usr/bin/python3
#
# Program:      ProP-PD Data Analysis Pipeline
# Description:  Analysis pipeline to process peptides returned from replicated ProP-PD selections
# Version:      1.0.0
# Last Edit:    1/1/2021
# Citation:     Under Preparation
# Author contact: Norman E. Davey <norman.davey@icr.ac.uk> Institute Of Cancer Research, Chelsea, SW3 6JB, London, United Kingdom.
# 
# License:
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http:#www.gnu.org/licenses/>.
#
# Copyright (C) 2020  Norman E. Davey


__doc__ = """
# ProP-PD Data Analysis Pipeline
### Code for the analysis tools for the ProP-PD data

#### The major functionalities of the pipeline are:
- Read NGS peptide count files
- Read NGS selection information files
- Define screen data from library, bait, replicate, selection and condition information
- Create screen metadata
- Create crapome of promiscuous peptides
- Map peptides to human proteome using the PepTools server (see http://slim.icr.ac.uk/tools/peptools/)
- Annotate peptides using the PepTools server (mutations, PTMs, etc - see http://slim.icr.ac.uk/tools/peptools/help#instancesAnnotations)
- Annotate peptides with ELM consensuses, curated motifs, bait interaction data and bait shared significant ontology
- Finds enriched motifs using SLiMFinder algorithm
- Set peptides quality checks
- Define the replicated, overlapping, high NGS count and significant specificity score peptides
- Define the peptide consensus confidence level
- Create outputs 

See README.md for details

#### Options:
###### Dataset	
--library  [STRING]: library identifier of library for processing
--bait  [STRING]:  bait identifier of bait for processing
--analysis_bait_list_file [PATH]: File containing the library and baits for processing

###### View Data
--show_screens BOOL [False]: Show the screens that are available as one line per screen
--show_screens_collapse BOOL [False] Collapse the available screens to one line per bait [True]

###### Tasks	
--create_library_metadata BOOL [False]: Run functions to create library metadata data
--run_create_crapome BOOL [False]: Run functions to create library crapome of peptide promiscuity data
--run_library_screens BOOL [False]: Run the whole library NGS sequencing datasets
--run_peptools BOOL [True]: Run PepTools to annotate screen peptides
--run_slimfinder BOOL [True]: Run SLiMFinder to find enriched specificity determinants in the screen peptides
--slimfinder_only BOOL [False]: Run SLiMFinder Only
--run_create_protein_output BOOL [False]: Run functions to create protein-centric data

###### Paths					
--blast_path [PATH] Path to the blast library bin folder (e.g. /home/tools/blast/bin/)
--blastplus_path [PATH] Path to the blast+ library bin folder (e.g. /home/tools/blast+/bin/)
--clustalw_path [PATH] Path to the clustalw executable
--slimfinder_path [PATH] Path to the SLiMFinder python script
--slimsuite_path [PATH] Path to the SLiMSuite directory

###### Data Management	
--data_path [PATH] Path to the data directory
--remake BOOL [False] Remake the data overwriting preprocessed data
--remake_age INT [180]: Age to remake old data overwriting preprocessed data
--retry_after_peptools_error BOOL [False] Whether to retry after PepTools after an error

###### Cutoffs
--mean_count_proportion_cutoff FLOAT [0.0005]: Confidence level cutoff for the mean count proportion
--promiscuity_cutoff INT [3] Maximum number of baits returning a peptide for a specific peptide. Peptides above the cut-off are defined as promiscuous
--prefilter_count_cutoff INT [1] Minimum number of peptide
--complexity_cutoff INT [3] Minimum number of unique amino acids for filtering low complexity peptides

###### Motifs
--consensus RE:  Annotate the peptides with a consensus
--merge_mismatched_peptides BOOL [False] Merge non-designed peptides with single point mutations
--remove_duplicates BOOL [False] Remove all but one peptides for peptides that are mapped to multiple proteins

###### Filters
--require_highcount BOOL [False] Filter peptides based on high NGS counts
--require_interaction BOOL [False] Filter peptides based on known interactions between bait and peptide containing protein
--require_motifs BOOL [False] Filter peptides based on on overlap with curated motifs
--require_mutagenesis BOOL [False] Filter peptides based on overlap with mutagenesis
--require_region BOOL [False] Filter peptides based on overlap with functional region
--require_snp BOOL [False] Filter peptides based on overlap with SNPs
--require_overlap BOOL [False] Filter peptides based on overlapping peptid
--require_specific BOOL [False] Filter peptides based on specific (non-promiscuous) peptides
--require_replicate BOOL [False] Filter peptides based on replicated peptides
--require_specific_replicate BOOL [False] Filter peptides based on specific replicates
--require_structure BOOL [False] Filter peptides based on overlap with motif structure

###### Peptools				
--use_selection_day_replicates BOOL [True] Whether to use all selection day replicates [True] or the last selection day replicate [False]

#### Requires: 
##### Software:

- SLiMTools library for data retrieval and management, motif analysis, PSSM definition, etc.
- SLiMFinder software as part of the SLIMSuite library (included in the SLiMTools library)
- BLAST+ as a requirement for SLiMFinder
- ClustalW as a requirement for SLiMFinder

NB: paths needs to be set in the ../config.ini file

"""
############################################################################################################
############################################################################################################

import sys,os

sys.path.append(os.path.join(os.path.dirname(__file__),"./modules"))
from phageDataManager import phageDataManager

############################################################################################################
############################################################################################################

if __name__ == "__main__":
	try:
		phageDataManagerObj = phageDataManager()
		phageDataManagerObj.main()
	except Exception as e:
		print(e)
		raise

############################################################################################################
############################################################################################################
