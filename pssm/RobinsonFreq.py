aa_frequency = {}
aa_frequency["Robinson"] = {
'A': 78.05,
'C': 19.25,
'D': 53.64,
'E': 62.95,
'F': 38.56,
'G': 73.77,
'H': 21.99,
'I': 51.42,
'K': 57.44,
'L': 90.19,
'M': 22.43,
'N': 44.87,
'P': 52.03,
'Q': 42.64,
'R': 51.29,
'S': 71.20,
'T': 58.41,
'V': 64.41,
'W': 13.30,
'Y': 32.16,
"-": 1
}

def getRobinsonFreq():
    aa_freq = {}
    for k,v in aa_frequency['Robinson'].items():
        aa_freq[k] = float(v)/1000
    return aa_freq

if __name__ == "__main__":
    freqSum = 0
    for k,v in aa_frequency['Robinson'].items():
        freqSum += v
    print(freqSum)

    freqNorm = getRobinsonFreq()
    freqSum = 0
    for k,v in freqNorm.items():
        freqSum += v
    print(freqSum)
