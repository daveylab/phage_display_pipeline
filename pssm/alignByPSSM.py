import os,requests,json,hashlib,pprint

class alignByPSSM():

	#=============================================================================================
	#=============================================================================================
	#=============================================================================================

	def __init__(self):
		self.option = {}
		self.option["url_server"] = "http://slim.icr.ac.uk/peptools_webservices/"
		self.option["remake"] = False
		self.option["gap_penalty"] = -3

		self.data = {}
		self.data["pssm_peptides"] = None

	#=============================================================================================
	#=============================================================================================
	#=============================================================================================

	def get_PSSM(self, pssm_score_scheme=None):
		if self.option['pssm_score_scheme'] != None and pssm_score_scheme == None:
			pssm_score_scheme = self.option['pssm_score_scheme']

		if self.data["pssm_peptides"] == None:
			self.data["pssm_peptides"] = self.data["peptides"]

		peptides = self.data["pssm_peptides"]
		peptides.sort()


		hash = str(hashlib.md5(",".join(peptides).encode()).hexdigest())
		print(hash)
		self.option["pssm_file"] = os.path.join(self.option["data_path"],"pssm",hash + "." + pssm_score_scheme + ".json")

		if not os.path.exists(os.path.join(self.option["data_path"],"pssm")):
			os.mkdir(os.path.join(self.option["data_path"],"pssm"))

		if not os.path.exists(self.option["pssm_file"]) or self.option["remake"]:

			self.session = requests.Session()

			params = {
			'pssm_type' : pssm_score_scheme,#self.option['pssm_score_scheme'] = MOTIPS,ratio_sum,P_sum,frequency,binomal,binomal log,log odds.
			'peptides' : peptides
			}

			url = self.option["url_server"]  + "createPSSM"
			resp = self.session.post(url, data = json.dumps(params))

			status = resp.status_code
			if status != 200:
				print("Problem with getting instances", params)
				print("Details:", resp.json())
				raise

			isRunning = True
			while isRunning:
				resp = self.session.post(url, data=json.dumps(params))
				content = resp.json()
				if "status" in content:
					isRunning = self.waiter(resp.json()['status'])
				else:
					isRunning = False

			self.data["motif"] = content["motif"]
			self.data["pssm"] = content["pssm"]
			self.data["warnings"] = content["warnings"]

			open(self.option["pssm_file"],"w").write(json.dumps(content))
		else:
			with open(self.option["pssm_file"]) as data_file:
				content = json.load(data_file)
				self.data["motif"] = content["motif"]
				self.data["pssm"] = content["pssm"]
				self.data["warnings"] = content["warnings"]

		#---

		self.data["pssm_length"] = len(self.data["pssm"]['A'])

		scores = []
		for aa in self.data["pssm"]:
			scores.append(min(self.data["pssm"][aa]))

		self.data["pssm_min_scores"] = min(scores)
		self.data["pssm_length"] = len(self.data["pssm"]['A'])

	#=============================================================================================
	#=============================================================================================
	#=============================================================================================

	def get_pssm_motif(self,skip=[]):
		self.get_PSSM(pssm_score_scheme="binomial log")

	#=============================================================================================
	#=============================================================================================
	#=============================================================================================

	def stripGaps(self,peptides,gap_proportion=0.75):
		columns = {}
		for i in range(0,len(peptides[0])):
			columns[i] = []
			for j in range(0,len(peptides)):
				columns[i].append(peptides[j][i])

		use_columns = []

		for i in range(0,len(columns)):
			if float(columns[i].count("-"))/len(columns[i]) < gap_proportion:
				use_columns.append(i)

		peptides_degapped = []
		for i in range(0,len(peptides)):
			peptides_degapped.append(peptides[i][min(use_columns):max(use_columns)+1])

		return peptides_degapped

	#=============================================================================================
	#=============================================================================================
	#=============================================================================================

	def score_peptide(self, test_peptide,start = None, end = None):
		peptide = test_peptide.upper().strip('-')
		peptide_length = len(peptide)

		max_peptide_scores = {"peptide":peptide,"sequence":"","flank_score":-100000000000000, "score":-100000000000000};

		if start == None and end == None:
			start = -self.data["pssm_length"] + self.data["pssm_length"]/2
			end = self.data["pssm_length"] - self.data["pssm_length"]/2
		if start == 0 and end == 0:
			start = 0
			end = 1

		for seq_iter in range(start,end):
			if seq_iter < 0:
				padding_length = abs(seq_iter)
				tmp_peptide  = "-"*padding_length + peptide[max(0,seq_iter): max(0,seq_iter) + self.data["pssm_length"]+ seq_iter]

			elif seq_iter > (peptide_length - self.data["pssm_length"]):
				padding_length = seq_iter - (peptide_length - self.data["pssm_length"])
				tmp_peptide = peptide[seq_iter: seq_iter + self.data["pssm_length"] - (seq_iter - (peptide_length- self.data["pssm_length"]))] + "-"*padding_length
			else:
				tmp_peptide  = peptide[seq_iter: seq_iter + self.data["pssm_length"]]

			if len(tmp_peptide) < self.data["pssm_length"]:
				tmp_peptide = tmp_peptide + "-"*(self.data["pssm_length"] - len(tmp_peptide))

			summer = []
			flank_summer = 0
			core_summer = 0

			for i in range(0,self.data["pssm_length"]):
				if tmp_peptide[i] in self.data["pssm"]:
					summer.append( self.data["pssm"][tmp_peptide[i]][i])
				else:
					summer.append(self.option["gap_penalty"])

			"""
			for i in range(0,self.data["pssm_length"]):
				if i not in self.data["motif_core"]:
					if tmp_peptide[i] in self.data["pssm"]:
						flank_summer += self.data["pssm"][tmp_peptide[i]][i]
				else:
					if tmp_peptide[i] in self.data["pssm"]:
						core_summer += self.data["pssm"][tmp_peptide[i]][i]
			"""

			peptide_score = sum(summer)
			if peptide_score > max_peptide_scores["score"]:
				max_peptide_scores = {"peptide":peptide,'pssm_aligned_peptide':tmp_peptide,"score":peptide_score,"scores":summer,"flank_score":flank_summer,"core_score":core_summer,"sequence":test_peptide};

		return max_peptide_scores

	#def score_peptides(self):
	#	for peptide in alignByPSSMObj.data['peptides']:
	#		peptide_data = self.score_peptide(peptide)

	def interatively_align(self,max_iteractions=0):
		self.data["pssm_peptides"] = self.data["peptides"]
		self.data['tmp_peptides'] = self.data["peptides"]

		iterations = 1
		for j in range(0,iterations):
			for i in range(0,len(self.data['tmp_peptides'])):
				self.data["pssm_peptides"] = []
				for peptide in self.data['tmp_peptides']:
					if peptide.replace("-","") != self.data['tmp_peptides'][i].replace("-",""):
						self.data["pssm_peptides"].append(peptide)
					else:
						pass

				self.data["pssm_peptides"] = self.stripGaps(self.data["pssm_peptides"])
				self.get_PSSM()

				self.data['new_alignment_peptides'] = []
				for peptide in self.data['tmp_peptides']:
					peptide_data = self.score_peptide(peptide)
					self.data['new_alignment_peptides'].append(peptide_data['pssm_aligned_peptide'])

				self.data['tmp_peptides'] = self.data['new_alignment_peptides']

		self.data["pssm_peptides"] = self.stripGaps(self.data['tmp_peptides'])
		self.get_PSSM()

		for peptide in self.data['peptides']:
			peptide_data = self.score_peptide(peptide)

		return self.data['peptides']

	def split_align(self):

		self.data["pssm_peptides"] = self.data["peptides"]
		self.data['tmp_peptides'] = self.data["peptides"]
		self.data['aligned_peptides'] = {}

		split_size = 2

		if len(self.data["peptides"]) < 3:
			for peptide in self.data['tmp_peptides']:
				self.data['aligned_peptides'][peptide] = {  'core_score': 0,
				'flank_score': 0,
				'score': 0,
				'peptide': peptide,
				'pssm_aligned_peptide': peptide,
				'sequence': peptide}

			self.data["motif"] = "?"
		else:
			self.get_PSSM()

			for split_size in range(2,len(self.data["peptides"])):

				best_matches = {}

				for peptide in self.data['tmp_peptides']:
					peptide_data = self.score_peptide(peptide)
					if peptide_data['score'] not in best_matches:
						best_matches[peptide_data['score']] = []

					#print peptide,peptide_data
					best_matches[peptide_data['score']].append(peptide_data['pssm_aligned_peptide'])

				best_matches_scores = list(best_matches.keys())
				best_matches_scores.sort()
				best_matches_scores.reverse()

				best_matches_peptides = []
				for best_matches_score in best_matches_scores:
					#print best_matches_score,best_matches[best_matches_score]
					best_matches_peptides += best_matches[best_matches_score]
					#print best_matches_score, best_matches[best_matches_score]

				self.data["pssm_peptides"] = self.stripGaps(best_matches_peptides[:split_size])

				self.get_PSSM()
				#print self.data['motif']

				for peptide in best_matches_peptides[:split_size]:
					peptide_data = self.score_peptide(peptide)

				for peptide in self.data['tmp_peptides']:
					peptide_data = self.score_peptide(peptide.strip("-"))
					self.data['aligned_peptides'][peptide] = peptide_data


		left_overhangs = []
		right_overhangs = []

		for peptide in self.data['tmp_peptides']:
			left_overhang = peptide.replace("-","").split(self.data['aligned_peptides'][peptide]['pssm_aligned_peptide'].replace("-",""))[0]
			right_overhang = peptide.replace("-","").split(self.data['aligned_peptides'][peptide]['pssm_aligned_peptide'].replace("-",""))[1]

			self.data['aligned_peptides'][peptide]['aligned_peptide_left_overhang'] = left_overhang
			self.data['aligned_peptides'][peptide]['aligned_peptide_right_overhang'] = right_overhang

			left_overhangs.append(len(left_overhang))
			right_overhangs.append(len(right_overhang))

			max_left_overhang = max(left_overhangs)
			max_right_overhang = max(right_overhangs)

		for peptide in self.data['tmp_peptides']:
			left_overhang_length = len(self.data['aligned_peptides'][peptide]['aligned_peptide_left_overhang'])
			right_overhang_length = len(self.data['aligned_peptides'][peptide]['aligned_peptide_right_overhang'])

			left_overhang_alignment = "-"*(max_left_overhang - left_overhang_length) + self.data['aligned_peptides'][peptide]['aligned_peptide_left_overhang']
			right_overhang_alignment = self.data['aligned_peptides'][peptide]['aligned_peptide_right_overhang'] + "-"*(max_right_overhang - right_overhang_length)
			self.data['aligned_peptides'][peptide]['aligned_peptide'] = left_overhang_alignment + self.data['aligned_peptides'][peptide]['pssm_aligned_peptide']  + right_overhang_alignment


		return self.data


	def align(self):
		self.data["pssm_peptides"] = self.data["peptides"]
		self.data['tmp_peptides'] = self.data["peptides"]
		self.data['pssm_aligned_peptides'] = {}

		print("\n".join(self.data["peptides"]))
		self.get_PSSM()
		print(self.data['motif'])
		for peptide in self.data['tmp_peptides']:
			peptide_data = self.score_peptide(peptide.strip("-"))
			self.data['pssm_aligned_peptides'][peptide] = peptide_data

		return self.data

if __name__ == "__main__":

	alignByPSSMObj = alignByPSSM()

	alignByPSSMObj.option['data_path'] = "/Users/normandavey/Documents/Work/Websites/slimdb/data/"
	alignByPSSMObj.option['pssm_score_scheme'] = "psi_blast IC"
	alignByPSSMObj.option['pssm_score_scheme'] = "binomial log"
	#alignByPSSMObj.option['pssm_score_scheme'] = "MOTIPS"

	alignByPSSMObj.data['peptides'] = 'KLITYDNPAYEGIDVD,FLTHPSSLVTFDNPAY,PSSFVTFDNPAFEPVD,LVTFDNPAFEPGDETI,LDNPTTLISADNPVFE,PSSLVTFDNPAYEPLD,TTPTKLITYDNPAYEG,NPSTFVTFDNPAYEPI,FVTHPSSFVTFDNPAF,RPTSLVTFDNPAFEPG,FVTFDNPAYEPIDETL,LITYDNPAFESFDPED,DRPADLVTFDNPVYDP,LSSPQRLITFDNPAYE,LVTYDNPAFEGFNPED,ERPADLVTFDNPVYDP,QRLITFDNPAYEGEDV,PSSFVTFDNPAFEPGD'.split(',')
	alignByPSSMObj.data['peptides'] = 'LLSQIPVERQALTELE,MISDMSQQLSRSGQVE,SVEQKIRDIQSKVEEQ,SEDGVSVWRQHLVFLL,TIFNCFYALNNIFLGL,MFLRLIDDNGIVLNSI,NCKKLVEMMEQHDRGS,KAIKLLDESYKKAEIA,LGGVILGAATFFCWMA,SVQKLVEDRWNKPQKT,MAARLCCQLDPARDVL,CVGSLLLKWEDQFNLL,PLYGNEGCGWMGWLLS'.split(',')
	alignByPSSMObj.data['peptides'] = 'LLSQIPVERQALTELE,MISDMSQQLSRSGQVE,SVEQKIRDIQSKVEEQ,SEDGVSVWRQHLVFLL,TIFNCFYALNNIFLGL,MFLRLIDDNGIVLNSI,NCKKLVEMMEQHDRGS,KAIKLLDESYKKAEIA,LGGVILGAATFFCWMA,SVQKLVEDRWNKPQKT,MAARLCCQLDPARDVL,CVGSLLLKWEDQFNLL,PLYGNEGCGWMGWLLS'.split(',')

	print(alignByPSSMObj.data['peptides'])
	#alignByPSSMObj.get_PSSM()
	#alignByPSSMObj.interatively_align()

	alignByPSSMObj.data["pssm_peptides"] = None
	alignByPSSMObj.get_PSSM()
	aligned_peptides = alignByPSSMObj.split_align()
	pprint.pprint(aligned_peptides['aligned_peptides'])
	print(alignByPSSMObj.data["motif"])
	for peptide in aligned_peptides['aligned_peptides']:
		#print peptide,"\t",
		print(aligned_peptides['aligned_peptides'][peptide]['aligned_peptide'])#,"\t",
		#print "%2.2f"%aligned_peptides['aligned_peptides'][peptide]['score']
