import math, json, os, sys,pprint, random
from blosum62_freq import getFreqRatio as _getFreqRatio
from RobinsonFreq import getRobinsonFreq as _getRobinsonFreq


aa_frequency_set = "Robinson"
mat = "blosum62"
mat_files = {
	"cserzo_altered": os.path.join(os.path.dirname(os.path.realpath(__file__)), "matrices", "cserzo_altered.matrix.txt"),
	"cserzo":os.path.join(os.path.dirname(os.path.realpath(__file__)),"matrices","cserzo.matrix.txt"),
	"grantham": os.path.join(os.path.dirname(os.path.realpath(__file__)),"matrices", "grantham.matrix.txt"),
	"blosum62": os.path.join(os.path.dirname(os.path.realpath(__file__)),"matrices", "blosum62.matrix.txt"),
	"blosum45": os.path.join(os.path.dirname(os.path.realpath(__file__)), "matrices", "blosum45.matrix.txt"),
	"mclachlan": os.path.join(os.path.dirname(os.path.realpath(__file__)),"matrices", "mclachlan.matrix.txt")
}

mat_file = mat_files[mat]

MAX_INDEPENDENT_OBSERVATION = 400  # for calculate N, n
EFFECTIVE_ALPHABET = 20
kEpsilon = 0.0001
kPSIScaleFactor = 200
BLAST_SCORE_MIN = -32768
PSEUDO_MAX = 1000000 # effective infinity
kPositScalingNumIterations = 10
BLAST_KARLIN_LAMBDA0_DEFAULT = 0.5
BLAST_SCORE_MAX = 32767
BLAST_KARLIN_LAMBDA_ACCURACY_DEFAULT = 1.e-5
BLAST_KARLIN_LAMBDA_ITER_DEFAULT = 17

aa_frequency = {}
aa_frequency["all"] = {
 '-': 1,
 'A': 0.086167,
 'C': 0.012759,
 'D': 0.052951,
 'E': 0.061395,
 'F': 0.040392,
 'G': 0.071295,
 'H': 0.021925,
 'I': 0.060238,
 'K': 0.052746,
 'L': 0.098334,
 'M': 0.024703,
 'N': 0.041559,
 'P': 0.047353,
 'Q': 0.038562,
 'R': 0.054707,
 'S': 0.066961,
 'T': 0.056144,
 'V': 0.067440,
 'W': 0.013183,
 'Y': 0.030613
}

aa_frequency["test"] = {
 '-': 1,
 'A': 0.05,
 'C': 0.05,
 'D': 0.05,
 'E': 0.05,
 'F': 0.05,
 'G': 0.05,
 'H': 0.05,
 'I': 0.05,
 'K': 0.05,
 'L': 0.05,
 'M': 0.05,
 'N': 0.05,
 'P': 0.05,
 'Q': 0.05,
 'R': 0.05,
 'S': 0.05,
 'T': 0.05,
 'V': 0.05,
 'W': 0.05,
 'Y': 0.05
}


aa_frequency["discut0.5"] ={
"-": 0.05,
"A":0.079609,
"C":0.005029,
"D":0.057989,
"E":0.08815,
"F":0.015527,
"G":0.075008,
"H":0.024831,
"I":0.028494,
"K":0.06696,
"L":0.055605,
"M":0.021528,
"N":0.047938,
"P":0.085282,
"Q":0.055837,
"R":0.067414,
"S":0.0964,
"T":0.065369,
"V":0.044676,
"W":0.004848,
"Y":0.013504}

aa_frequency["backgroundPSI"] = {
"A": 0.074216,
"R": 0.051614,
"N": 0.044646,
"D": 0.053626,
"C": 0.024687,
"Q": 0.034260,
"E": 0.054312,
"G": 0.074147,
"H": 0.026213,
"I": 0.067917,
"L": 0.098908,
"K": 0.058156,
"M": 0.024990,
"F": 0.047418,
"P": 0.038538,
"S": 0.057229,
"T": 0.050891,
"W": 0.013030,
"Y": 0.032282,
"V": 0.072919
}

aa_frequency['Robinson'] = _getRobinsonFreq()

aas = list("ARNDCQEGHILKMFPSTWYV-")

weight_frequencies = True
beta = 10
score_matrix_adjustment_cutoff = 0.65

lamda_values = {
"blosum62":0.2486767,#3.176,
"blosum45":0.2486767,#3.176,
"cserzo_altered":0.2486767}
# lamda_value = lamda_values[mat]
# lamda_value = 0.2670
# lamda_value = 0.3288
lamda_value = 0.317606 # ideal lambda
gap_penalty = -3


background = {
"all":{'A': 0.086167, 'C': 0.012759, 'D': 0.052951, 'E': 0.061395, 'F': 0.040392, 'G': 0.071295, 'H': 0.021925, 'I': 0.060238, 'K': 0.052746, 'L': 0.098334, 'M': 0.024703, 'N': 0.041559, 'P': 0.047353, 'Q': 0.038562, 'R': 0.054707, 'S': 0.066961, 'T': 0.056144, 'V': 0.067440, 'W': 0.013183, 'Y': 0.030613},
"0.3":{"A":0.082767,"C":0.007309,"D":0.058283,"E":0.082903,"F":0.022992,"G":0.073166,"H":0.024454,"I":0.043799,"K":0.064038,"L":0.073925,"M":0.023502,"N":0.043715,"P":0.062209,"Q":0.048512,"R":0.064976,"S":0.075305,"T":0.062569,"V":0.059192,"W":0.007122,"Y":0.019261},
"0.5":{"A":0.079609,"C":0.005029,"D":0.057989,"E":0.08815,"F":0.015527,"G":0.075008,"H":0.024831,"I":0.028494,"K":0.06696,"L":0.055605,"M":0.021528,"N":0.047938,"P":0.085282,"Q":0.055837,"R":0.067414,"S":0.0964,"T":0.065369,"V":0.044676,"W":0.004848,"Y":0.013504}
}

def read_similarity_matrix(mat_file):
	f = open(mat_file)

	header = f.next().strip().split('\t')

	idx_to_aa = dict(list(zip(list(range(0,len(header))), header)))
	similarity_matrix = {}


	max_score = 0
	for line in f:
		fields = line.strip().split('\t')
		from_aa = fields[0]
		similarity_matrix[from_aa] = {}

		for idx, score in enumerate(fields):
			if idx == 0:
				continue

			to_aa = idx_to_aa[idx]
			similarity_matrix[from_aa][to_aa] = float(score)

			if to_aa not in similarity_matrix:
				similarity_matrix[to_aa] = {}

			similarity_matrix[to_aa][from_aa] = float(score)

			if float(score) > max_score:
				max_score = float(score)

		similarity_matrix[from_aa]["-"] = -4

	similarity_matrix["-"] = {}

	for aa in similarity_matrix:
		if aa == "-":
			similarity_matrix["-"][aa] = -1
		else:
			similarity_matrix["-"][aa] = -4
	f.close()
	return similarity_matrix

class psiblastPSSM():
	def __init__(self, peptides):
		self.peptides = peptides
		self.profile = {}
		self.freqRatio_matrix = _getFreqRatio()
		self.sequenceWeight()

	def calcN(self):
		''' Table 2. Mean number of distinct amino acids N as a function of the number of independent sequences n'''
		self.Nset = {0: 0}
		nset = list(range(1, MAX_INDEPENDENT_OBSERVATION+1))
		for n in nset:
			x = 0
			for aa in aas:
				if aa == "-": continue
				pi = aa_frequency['backgroundPSI'][aa]
				x += (1 - pi)**n
				# x += math.exp(n*math.log(1 - pi))
			N = 20 - x
			self.Nset[n] = N

	def calculateAlpha(self, columnNumber):
		''' Calculate alpha - effective number of observation. If no gaps in alignment alpha is the same for all sequences. '''
		k = 20 # effective alphabet
		startPosition = 0  # if gaps, should be block start position
		endPosition = len(self.peptides[0]) # if gaps, should be block end position
		halfNumColumns = max(1, (float(endPosition - startPosition + 2)/2))
		halfNumColumns = math.trunc(halfNumColumns) # round down to integer
		columnsAccountedFor = 0
		totalDistinctCounts = 0
		#print self.seqWeights['posDistinctDistrib'][8]
		#print 
		while (columnsAccountedFor < halfNumColumns):
			#print columnNumber,k
			assert(k>=0)
			totalDistinctCounts += (self.seqWeights['posDistinctDistrib'][columnNumber][k] *k)
			columnsAccountedFor += self.seqWeights['posDistinctDistrib'][columnNumber][k]
			if (columnsAccountedFor > halfNumColumns):
				totalDistinctCounts -= ((columnsAccountedFor - halfNumColumns) * k)
				columnsAccountedFor = halfNumColumns
			k -= 1

		avgDistinctAA = float(totalDistinctCounts)/columnsAccountedFor
		smallN = self.findSmallN(avgDistinctAA, columnNumber)
	
		#print avgDistinctAA,smallN
		return smallN


	def findSmallN(self, avgDistinctAA, colNumber):

		for i in range(1, MAX_INDEPENDENT_OBSERVATION):
			if self.Nset[i] <= avgDistinctAA:
				indep = i - (self.Nset[i]-avgDistinctAA)/(self.Nset[i] - self.Nset[i-1])
			else:
				break
		i -= 1
		indep = min(indep, len(self.peptides))
		indep = max(0, indep - 1)
		return indep

	def sequenceWeight(self):
		alnLength = len(self.peptides[0])
		seqWeights = {
			"sigma": {},
			"posDistinctDistrib": {},
			"row_sigma": {},
			"norm_seq_weights": {},
			"match_weights": {}
		}

		for pos in range(0, alnLength):
			seqWeights["posDistinctDistrib"][pos] = dict.fromkeys(list(range(0,EFFECTIVE_ALPHABET+1)),0)
			seqWeights["row_sigma"] = dict.fromkeys(list(range(0, len(self.peptides))),0)
			distinct_residues_found = False
			for p in range(0, alnLength): ### alnLength - should be iterating by positions in interval if gaps occur
				residue_counts_for_column = {}
				num_distinct_residues_for_column = 0
				num_local_std_letters = 0
				sigma = 0
				# for i in range(0, alnLength):
			
				for asi in range(0, len(self.peptides)):
					kResidue = self.peptides[asi][p]
					# kResidue = self.peptides[asi][pos]
					if kResidue not in residue_counts_for_column:
						num_distinct_residues_for_column += 1
						if (kResidue != '-'):
							num_local_std_letters += 1
						residue_counts_for_column[kResidue] = 0
					residue_counts_for_column[kResidue] += 1
				sigma += num_distinct_residues_for_column
				num_local_std_letters = min(num_local_std_letters, EFFECTIVE_ALPHABET)
				seqWeights["posDistinctDistrib"][pos][num_local_std_letters] += 1
				if num_distinct_residues_for_column > 1:
					distinct_residues_found = True

				#print residue_counts_for_column
				# calculate row_sigma, an intermediate value to calculate the normalized sequence weights
				for asi in range(0, len(self.peptides)):
					# residue = self.peptides[asi][pos]
					residue = self.peptides[asi][p]
					valueTmp = residue_counts_for_column[residue] * num_distinct_residues_for_column
					seqWeights["row_sigma"][asi] += float(1)/valueTmp

				seqWeights["sigma"][p] = sigma

			if distinct_residues_found:
				weight_sum = 0
				for asi in range(0, len(self.peptides)):
					valueTmp = float(seqWeights['row_sigma'][asi])/alnLength
					# print asi, valueTmp
					seqWeights['norm_seq_weights'][asi] = valueTmp
					weight_sum += valueTmp

				for asi in range(0, len(self.peptides)):
					seqWeights['norm_seq_weights'][asi] /= weight_sum
			else:
				for asi in range(0, len(self.peptides)):
					seqWeights['norm_seq_weights'][asi] = float(1)/len(self.peptides)


		# match weights
		for pos in range(0, alnLength):
			if pos not in seqWeights['match_weights']:
				seqWeights['match_weights'][pos] = dict.fromkeys(aas,0)
			for asi in range(0, len(self.peptides)):
				aa = self.peptides[asi][pos]
				seqWeights['match_weights'][pos][aa] += seqWeights['norm_seq_weights'][asi]

		self.seqWeights = seqWeights
		self.checkSeqWeights()

	def checkSeqWeights(self):
		weightSum = {}
		for col in self.seqWeights['match_weights']:
			weightSum[col] = 0
			for aa, score in self.seqWeights['match_weights'][col].items():
				weightSum[col] += score
			if round(weightSum[col],2) != 1.0:
				print("Error in seq weights,", col, weightSum[col])

	def calculateBeta(self, colNumber, observations):
		''' Calculate beta for each column in msa. Beta is a column specific pseudocounts. Observations == alpha.'''
		# some constant values
		kPseudoMult = 500.0
		kPseudoNumerator = 0.0457  # numerator of entropy-based method
		kPseudoExponent = 0.8  # exponent of denominator
		kPseudoSmallInitial = 5.5 # small number of pseudocounts to avoid 0 probabilities in entropy-based method
		kPosEpsilon = 0.0001 # minimum return value for relative entropy
		#print self.seqWeights["match_weights"][0]
		# adjusted aa probabilities

		probAdjustedTmp = {}
		overallSum = 0
		for aa in self.seqWeights["match_weights"][colNumber]:
			if aa == "-": continue
			if aa not in probAdjustedTmp: probAdjustedTmp[aa] = 0
			valueTmp = (self.seqWeights['match_weights'][colNumber][aa] * observations) + (aa_frequency['backgroundPSI'][aa] * kPseudoSmallInitial)
			probAdjustedTmp[aa] = valueTmp
			overallSum += valueTmp

		probAdjusted = {}
		for aa in probAdjustedTmp:
			probAdjusted[aa] = float(probAdjustedTmp[aa])/overallSum
   
		# relative entropy
		relativeEntropy = 0
		for aa in probAdjusted:
			if probAdjusted[aa] > kPosEpsilon:
				relativeEntropy += probAdjusted[aa] * math.log(float(probAdjusted[aa])/aa_frequency['backgroundPSI'][aa])
		if relativeEntropy < kPosEpsilon:
			relativeEntropy = kPosEpsilon

		# calculations
		pseudoDenominator = math.pow(relativeEntropy, kPseudoExponent)
		alpha = float(kPseudoNumerator)/pseudoDenominator

		if (alpha < (1.0 - kPosEpsilon)):
			returnValue = kPseudoMult * alpha/ (1- alpha)
		else:
			returnValue = PSEUDO_MAX

		return returnValue

	def profiles(self):
		peptides = self.peptides
		profile = {}

		self.calcN() # initizial N - n table
		alpha = self.calculateAlpha(1)
		beta = self.calculateBeta(colNumber = 1, observations = alpha)
		#print self.seqWeights.keys()#['match_weights'][1]
		#print self.seqWeights['norm_seq_weights'][1]
		#print self.seqWeights['row_sigma'][1]
		#print self.seqWeights['posDistinctDistrib'][1]
		#print self.seqWeights['sigma'][1]
		#prin#t self.seqWeights['match_weights'][1]
		#print 'col, alpha, beta', 1, alpha, beta
		#print self.freqRatio_matrix;
		#print self.seqWeights['match_weights'][8]\
	
		for i in range(0,len(peptides[0])):
			profile[i] = {}
			alpha = self.calculateAlpha(i)
			beta = self.calculateBeta(colNumber = i, observations = alpha)
			#print 'col, alpha, beta', i, alpha, beta

			for i_aa in aas:
				# if i_aa == "-": continue
				g_i = 0
				g_i2 = 0
				pseudo = 0
				for j_aa in aas:
					P_j = aa_frequency[aa_frequency_set][j_aa]

					rij = self.freqRatio_matrix[i_aa][j_aa]

					f_j = self.seqWeights['match_weights'][i][j_aa]
					#print i,i_aa,j_aa,P_j,rij,f_j
				
					pseudo += f_j*rij
			
		   
				### NEW!!
				P_i = aa_frequency[aa_frequency_set][i_aa]
				f_i = self.seqWeights['match_weights'][i][i_aa]
				pseudo *= beta
				numerator = (alpha * f_i/P_i) + pseudo
				denominator = alpha + beta
				qOverPEstimate = numerator/denominator
				R_i = qOverPEstimate * P_i
			 
				#if i == 8:
				#   print i,i_aa,pseudo,P_i,f_i,R_i,numerator
				
		   
				#print R_i

				if P_i > kEpsilon:
					qOverPEstimate = R_i / P_i
				else:
					qOverPEstimate = 0

				if (qOverPEstimate == 0) or ( P_i < kEpsilon):
					s = BLAST_SCORE_MIN
				else:
					tmpV = math.log(qOverPEstimate)/lamda_value
					s = tmpV

				#stmp = s*kPSIScaleFactor

				#stmp = self.blast_Nint(stmp)
				# print i, i_aa, numerator, denominator, qOverPEstimate, R_i, s, stmp

				profile[i][i_aa] = s
		
		self.profile = profile
		
	def blast_Nint(self, score):
		score = int(round(score, 0))
		return score
	
	def score_peptides(self,test_peptides,gap_penalty=-3):
		PSSM_length = len(self.profile)
		
		self.calculatePvalDist(1000000)
		
		samples = 0
		counter = 0
		
		pssm_aligned_peptides = {}
		
		for peptide_id in test_peptides:
			peptide = test_peptides[peptide_id]
		
			peptide_length = len(peptide)
			
			best_alignment = ["",-100000]
			for seq_iter in range(-len(peptide), len(peptide)):
				samples += 1
	
				if seq_iter < 0:
					padding_length = abs(seq_iter)
					tmp_peptide  = "-"*padding_length + peptide[max(0,seq_iter): max(0,seq_iter) + PSSM_length+ seq_iter]

				elif seq_iter > (peptide_length - PSSM_length):
					padding_length = seq_iter - (peptide_length - PSSM_length)
					tmp_peptide = peptide[seq_iter: seq_iter + PSSM_length - (seq_iter - (peptide_length- PSSM_length))] + "-"*padding_length
				else:
					tmp_peptide  = peptide[seq_iter: seq_iter + PSSM_length]

				if len(tmp_peptide) < PSSM_length:
					tmp_peptide = tmp_peptide + "-"*(PSSM_length - len(tmp_peptide))

				summer_scores = []
				flank_summer = 0

				for i in range(0,PSSM_length):
					if tmp_peptide[i] == "-": 
						summer_scores.append(gap_penalty)
					elif tmp_peptide[i] in self.profile[i]:
						summer_scores.append(self.profile[i][tmp_peptide[i]])
				
			
				summer = sum(summer_scores)
				if summer > best_alignment[1]:
					best_alignment = [tmp_peptide,summer,summer_scores]
					
			
			if best_alignment[1] > self.similarity_cutoff_max:
				score_p = self.similarity_cutoff_max_p
			else:
				score_p = self.similarity_cutoff_reversed[int(best_alignment[1]*100)]
			
			left_padding = len(best_alignment[0]) - len(best_alignment[0].lstrip("-"))
			
			pssm_aligned_peptides[peptide_id] = {'peptide':best_alignment[0],'test_peptide':test_peptides[peptide_id],'score':best_alignment[1],"scores":best_alignment[2],"score_p":score_p,"left_padding":left_padding}
		
		lengths = []
		for peptide_id in list(pssm_aligned_peptides.keys()):
			lengths.append(pssm_aligned_peptides[peptide_id]['left_padding']  + len(pssm_aligned_peptides[peptide_id]['test_peptide']))
	
		max_length = max(lengths)
		for peptide_id in list(pssm_aligned_peptides.keys()):
			tmp_peptide = "-"*pssm_aligned_peptides[peptide_id]['left_padding'] + pssm_aligned_peptides[peptide_id]['test_peptide']
			tmp_peptide = tmp_peptide.ljust(max_length, '-')
			pssm_aligned_peptides[peptide_id]['aligned_peptide'] = tmp_peptide
		
		return pssm_aligned_peptides
		
	#--------------------------
	#--------------------------
	#--------------------------
	def randomPeptide(self):
		randomPeptide = "";
		for i in range(0,self.PSSM_length):
			aa_selected_pos = int(round(random.random() * (self.bins_count - 1)))
			randomPeptide += self.true_distibution_aa_selector_dict[aa_selected_pos]
	
		return randomPeptide;


	def create_true_distibution_aa_selector_dict(self):
		aa_selector_dict_bins = 100
		background_frequencies_selection = "0.3"
		summer = 0
		summer_tmp = 0
		
		self.true_distibution_aa_selector_dict = {}
		
		for aa in aas:
			if aa != "-":
				summer = int(summer_tmp*aa_selector_dict_bins)
				summer_tmp += background[background_frequencies_selection][aa]
				for i in range(summer,summer + int(math.ceil(background[background_frequencies_selection][aa]*aa_selector_dict_bins))):
					self.true_distibution_aa_selector_dict[i] = aa
		
		self.PSSM_length = len(self.profile)
		self.bins_count = len(self.true_distibution_aa_selector_dict)
		
	def calculatePvalDist(self,N):
		self.create_true_distibution_aa_selector_dict()
		
		random_peptide_score_list = []
		
		for i in range(0,N):
			random_peptide = self.randomPeptide();
			summer = 0
			for i in range(0,self.PSSM_length):
				summer += self.profile[i][random_peptide[i]]
			
			random_peptide_score_list.append(summer);
		
		random_peptide_score_list.sort()
		
		self.similarity_cutoff = {}
		self.similarity_cutoff_reversed = {}
		
		for i in range(0,N,100):
				self.similarity_cutoff[1 - (float(i)/N)] = random_peptide_score_list[i]
				self.similarity_cutoff_reversed[int(random_peptide_score_list[i]*100)] = 1 - (float(i)/N)

		for i in range(99,0,-1):
			self.similarity_cutoff[1 - (float(N - i)/N)] = random_peptide_score_list[N - i]
			self.similarity_cutoff_reversed[int(random_peptide_score_list[N - i]*100)] = 1 - (float(N - i)/N)

		last = 0
		self.similarity_cutoff_max = random_peptide_score_list[N-1]
		self.similarity_cutoff_max_p = self.similarity_cutoff_reversed[int(random_peptide_score_list[N-1]*100)]
		
		for i in range(min(self.similarity_cutoff_reversed),max(self.similarity_cutoff_reversed)):
			if i in self.similarity_cutoff_reversed:
				last = self.similarity_cutoff_reversed[i]
			else:
				self.similarity_cutoff_reversed[i] = last
				

if __name__ == "__main__":
	peptides = ["EQEITYAELNLQKA","DDANSYENVLICKQ","MQQNGYENPTYKFF","DSARVYENVGLMQQ","EGDNDYIIPLPDPK","NQGVIYSDLNLPPN","EDDGDYESPNEEEE","GNNYVYIDPTQLPY","EEEPQYEEIPIYLE","KSLTIYAQVQKPGP","KSPGEYVNIEFGSG","HVNATYVNVKCVAP","EEEEEYMPMEDLYL","PEPGPYAQPSVNTP","DESVDYVPMLDMKG","GHDGLYQGLSTATK","FDDPSYVNVQNLDK","PEEHIYDEVAADPG","ASTVQYSTVVHSGY","DSTNEYMDMKPGVS","DDAADYEPPPSNNE","YEEPVYEEVGAFPE","VYESPYADPEEIRP","GRREEYDVLDKRRG","MPPPTYTEVDPCIL","EAPPCYMDVIPEDH","DKQVEYLDLDLDSG"]
	test_peptides = ['ATEQEITYAELNLQK', 'NSYENVLICK', 'GYENPTY', 'VYENVGLM', 'DNDYIIPLPDPK', 'MDNQGVIYSDLNLPP', 'GEDDGDYESPNEEEE', 'NNYVYIDPT', 'PGFYVEAN', 'EPQYEEIPIYL', 'KSLTIYAQVQK', 'GEYVNIEF', 'YVNV', 'EEEYMPMEDLYLDIL', 'EPGPYAQPSVNTP', 'YVPML', 'PQPEYVNQPD', 'EGQYQPQP', 'GHDGLYQGLSTATK', 'DDPSYVNVQNLDK', 'EEHIYDEVAADP', 'STASTVQYSTVVHSG', 'TNEYMDMK', 'ADYEPP', 'EEPVYEEVG', 'DTEVYESPYADPE', 'NQLYNELNLGRREEYDVLD', 'EPQYQPGENL', 'PPCYMDV', 'KQVEYLDLDLD', 'KFMPPPTYTEVD']

	pssm = psiblastPSSM(peptides)
	pssm.profiles()
	pssm.calculatePvalDist(100000)
	pssm.score_peptides(test_peptides)
	
	"""
	out_path = "/tmp/test_pwm.json"
	with open(out_path, 'w') as outfile:
		json.dump(data['profile'], outfile, indent = 4, ensure_ascii=False)
	sys.exit()
	"""