import sys, os, inspect, time, requests, string

###################################################################
### download file class with sessions
###################################################################

#-----
import logging
logger = logging.getLogger(__name__)
#-----

class sessionDownloader():
	#=============================================================================================

	def __init__(self):
		self.session = requests.Session()

	def download_file(self,url,out_path,method="POST",params={},force=False,JSON=False,remake_age=365,replace_empty=True,wait_time=0.5):
		if os.path.exists(out_path):
			if force:
				logger.debug("Forced - Deleting: " + out_path)
				os.remove(out_path)

			elif (time.time() - os.path.getmtime(out_path))/60/60/24 > remake_age:
				logger.debug("Outdated - Deleting" + out_path + " - age:" + "%1.2f"%((time.time() - os.path.getmtime(out_path))/60/60/24))
				os.remove(out_path)

			elif (replace_empty and os.path.getsize(out_path) == 0):
				logger.debug("Empty - Deleting: " + out_path)
				os.remove(out_path)
			else:
				logger.debug("File exists: " + out_path)
				return {"status":"Success"}

		if not os.path.exists(out_path):
			try:
				if method == "FTP":
					import ftplib

					download_server = url.split("/")[2]
					download_path = "/".join(url.split("/")[3:-1])
					download_file = url.split("/")[-1]
					ftp_usr = ""
					ftp_pass = ""

					logger.debug("FTP: " + download_server)
					logger.debug("FTP path: " + download_path)
					logger.debug("FTP file: " + download_file)

					ftp = ftplib.FTP(download_server)

					ftp.login(ftp_usr, ftp_pass)
					ftp.cwd(download_path)
					ftp.retrbinary("RETR " + download_file ,open(out_path, 'wb').write)
					ftp.quit()

					logger.debug("FTP loaded")

					return {"status":"Success"}

				if len(params) != 0:
					resp = self.session.get(url, params=params)
				else:
					if method == "POST":
						logger.debug("POST: " + url)
						resp = self.session.post(url)
					else:
						logger.debug("GET: " + url)
						resp = self.session.get(url)

				status = resp.status_code

				if status != 200:
					logger.error("GET: " + url + ' - ' + resp.status_code)
					return {"status":"Error","status_code":resp.status_code,"details":str(resp)}

				if JSON:
					import json
					content = json.loads(resp.text)
					
					if 'data' in content:
						content = content['data']
					
					logger.debug("Writing: " + out_path)
					with open(out_path, "w") as data_file:
						json.dump(content, data_file)
				else:
					content = resp.text

					logger.debug("Writing: " + out_path)
					try:
						open(out_path,"w").write(content)
					except:
						content = "".join([x for x in content if x in string.printable])
						open(out_path,"w").write(content)
						
				time.sleep(wait_time)

				return {"status":"Success"}
			except Exception as e:
				return {"status":"Error","error_type":str(e)}

if __name__ == "__main__":
	test = "test2"

	if test == "test1":
		pdb_id = "2AST"
		pdb_file_path = os.path.dirname(inspect.stack()[0][1])

		url = "http://files.rcsb.org/download/" + pdb_id + ".pdb"
		out_path = os.path.abspath(os.path.join(pdb_file_path, pdb_id + ".pdb"))

		sessionDownloaderObj = sessionDownloader()
		response = sessionDownloaderObj.download_file(url,out_path)

		print((open(out_path).read()))
		print(response)

	if test == "test2":
		url = "ftp://ftp.ebi.ac.uk/pub/databases/intact/current/psimitab/intact.txt"
		url = "ftp://ftp.ebi.ac.uk/pub/databases/intact/current/psimitab/README"
		out_path = "./README.txt"
		sessionDownloaderObj = sessionDownloader()
		response = sessionDownloaderObj.download_file(url,out_path,method="FTP")

		print(response)
