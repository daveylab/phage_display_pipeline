import sys,traceback,pprint

###################################################################
### Errors
###################################################################

def getError():
	exc_type, exc_value, exc_tb = sys.exc_info()
	exc_file = "-"
	exc_line = "-"
	
	format_exc = traceback.format_exc().strip().split("\n")
	
	if len(format_exc) > 0:
		exc_file = format_exc[-3].strip()
	
	if len(format_exc) > 1:
		exc_line = format_exc[-2].strip()
	
	return {
		"type":str(exc_type),
		"value":str(exc_value),
		"exc_file":exc_file,
		"exc_line":exc_line,
		"format_exc":traceback.format_exc()
		}

def printError():
	pprint.pprint(getError())
	
	