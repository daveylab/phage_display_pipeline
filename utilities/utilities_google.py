import sys,re,os,json,inspect
import gspread,pprint
import dataset, sqlalchemy

from oauth2client.service_account import ServiceAccountCredentials

# Watch https://www.youtube.com/watch?v=7I2s81TsCnc
# pip install gspread
# pip install oauth2client
# pip install pyopenssl

file_path = os.path.dirname(inspect.stack()[0][1])

sys.path.append(os.path.join(file_path,"../"))
import config_reader

#----------------------------------------------------------------------------------------#

class googlesheetReader():
	def __init__(self):
		self.options = {}
		self.options['update'] = False
		self.options['download'] = True
		self.options.update(config_reader.load_configeration_options())

	def setup_access(self):
		storage_name = "-".join([self.options['table_name'],self.options['sheet_name']]).lower().replace(" ","_")
		self.options['data_out_file'] = os.path.join(self.options["data_path"], "sqlite",storage_name + ".raw.json")

		self.users  = {}

		try:
			scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']
			file_path = os.path.dirname(inspect.stack()[0][1])
			credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.join(file_path,'../utilities/','sheet.json'),scope)

			self.gc = gspread.authorize(credentials)
		except:
			print("Could not cd ..contact google")
			raise

	def download_sheet(self):
		storage_name = "-".join([self.options['table_name'],self.options['sheet_name']]).lower().replace(" ","_")
		self.options['data_out_file'] = os.path.join(self.options["data_path"], "sqlite",storage_name + ".raw.json")

		if not os.path.exists(self.options['data_out_file']) or self.options['update']:
			self.data = {}

			wks = self.gc.open(self.options['table_name']).worksheet(self.options['sheet_name'])
			self.data = wks.get_all_records()

			with open(self.options['data_out_file'], 'w') as outfile:
				json.dump(self.data, outfile)

	def get_sheet(self):

		if self.options['download']:
			self.download_sheet()

		storage_name = "-".join([self.options['table_name'],self.options['sheet_name']]).lower().replace(" ","_")
		self.options['data_out_file'] = os.path.join(self.options["data_path"], "sqlite",storage_name + ".raw.json")

		with open(self.options['data_out_file']) as json_file:
			if self.options['verbose']: print(("Writing:",self.options['data_out_file']))
			self.data = json.load(json_file)

	def add_row(self,row):
		wks = self.gc.open(self.options['table_name']).worksheet(self.options['sheet_name'])
		response = wks.append_row(row)

	def delete_row(self,row_index):
		wks = self.gc.open(self.options['table_name']).worksheet(self.options['sheet_name'])
		response = wks.delete_rows(row_index)

	def find_cell(self, query):
		wks = self.gc.open(self.options['table_name']).worksheet(self.options['sheet_name'])
		try:
			cell = wks.find(query)
		except:
			cell = None
		return cell

	def find_curation_row(self, curation_id, version_id):
		wks = self.gc.open(self.options['table_name']).worksheet(self.options['sheet_name'])
		try:
			match = None
			cell_list = wks.findall(str(curation_id))
			for idx,item in enumerate(cell_list):
				if item.col == 1:
					if str(wks.cell(item.row, 6).value) == str(version_id):
						match = item
		except Exception as e:
			match = None
		return match

#----------------------------------------------------------------------------------------#

if __name__ == "__main__":
	screenPermissionsObj = googlesheetReader()

	screenPermissionsObj.options['update'] = False
	screenPermissionsObj.options['table_name'] = "curation_backup"
	screenPermissionsObj.options['sheet_name'] = "curation_testing"
	screenPermissionsObj.setup_access()
	cell = screenPermissionsObj.find_cell("ikrystkowiak2")
	if cell is not None: screenPermissionsObj.delete_row(cell.row)
	cell = screenPermissionsObj.find_curation_row(curation_id=6323,version_id=6324)
	if cell is not None: print("cell",cell, cell.row)
	if cell is not None: screenPermissionsObj.delete_row(cell.row)

	sys.exit()


	screenPermissionsObj.options['update'] = False
	screenPermissionsObj.options['table_name'] = "Curation"
	screenPermissionsObj.options['sheet_name'] = "curation"
	screenPermissionsObj.setup_access()

	sheet_data = screenPermissionsObj.get_sheet()
	pprint.pprint(screenPermissionsObj.data)
	screenPermissionsObj.add_row(['Added row', 'Added curation row'])

	screenPermissionsObj = googlesheetReader()
	screenPermissionsObj.options['table_name'] = "Users"
	screenPermissionsObj.options['sheet_name'] = "users"
	screenPermissionsObj.setup_access()

	sheet_data = screenPermissionsObj.get_sheet()
	pprint.pprint(screenPermissionsObj.data)
	screenPermissionsObj.add_row(['Added user row', 'Added user row'])
